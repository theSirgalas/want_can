<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('room.{room_id}', function ( $user, $room_id) {
    //dd($user->chatroomsCustomer->contains($room_id)||$user->chatroomsExecutor->contains($room_id));
    return $user->chatroomsCustomer->contains($room_id)||$user->chatroomsExecutor->contains($room_id);

});
