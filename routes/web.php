<?php

use App\Http\Controllers\Cabinet\SettingController;
use App\Http\Controllers\ExecutorsController;
use App\Http\Controllers\Task\ApplicationController;
use App\Http\Controllers\Task\ResourceController;
use App\Http\Controllers\Task\ReviewController;
use App\Http\Controllers\Task\UserTaskController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegistrationController;
use App\Http\Controllers\Admin\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\Organization\OrganizationController;
use App\Http\Controllers\Cabinet\Profile\UserProfileController;
use App\Http\Controllers\Cabinet\Profile\OrganizationProfileController;
use App\Http\Controllers\Admin\Handbook\HandbookTitleController;
use App\Http\Controllers\Admin\Handbook\HandbookController;
use App\Http\Controllers\Cabinet\Task\TaskController;
use \App\Http\Controllers\Admin\Task\TaskController as AdminTaskController;
use App\Http\Controllers\Cabinet\Message\ChatController;


Route::get('/', [HomeController::class, 'index'])->name('home');

Route::post('/message/{user}',[\App\Http\Controllers\MessageController::class,'message']);

Route::group(
    [
    'prefix' => 'registration',
    'as' => 'registration.'
    ],
    function (){
        Route::get('/',[RegistrationController::class,'registerTab'])->name('tab');
        Route::get('individual',[RegistrationController::class,'individual'])->name('individual');
        Route::get('entity',[RegistrationController::class,'entity'])->name('entity');
        Route::post('user',[RegistrationController::class,'user'])->name('user');
        Route::post('organization',[RegistrationController::class,'organization'])->name('organisation');
    }
);

Auth::routes();

Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'Cabinet',
        'middleware' => ['auth'],
    ],
    function () {
        Route::group([
            'prefix' => 'profile',
            'as' => 'profile.',
            'namespace' => 'Profile',
        ],function (){
            Route::group([
                'prefix'=>'user', 'as'=> 'user.', 'namespace'=>'User'],function (){
                Route::get('/{user}',[UserProfileController::class,'form'])->name('form');
                Route::post('/add_avatar',[UserProfileController::class,'addAvatar'])->name('avatar.create');
                Route::delete('/delete_avatar/{user}',[UserProfileController::class,'deleteAvatar'])->name('avatar.delete');
                Route::post('/add_file',[UserProfileController::class,'addFile'])->name('file.create');
                Route::post('/car',[UserProfileController::class,'addCarForm'])->name('car');
                Route::post('/category',[UserProfileController::class,'addCategoryForm'])->name('category');
                Route::post('/sub_category',[UserProfileController::class,'addSubCategory'])->name('sub.category');
                Route::put('/store/{user}',[UserProfileController::class,'store'])->name('store');
                Route::delete('/delete_car',[UserProfileController::class,'deleteCar'])->name('car.delete');
            });

            Route::group(['prefix'=>'organization','as'=>'organization.','namespace'=>'Organization'],function (){
                Route::get('/create/{user}',[OrganizationProfileController::class,'create'])->name('create');
                Route::post('/create/{user}',[OrganizationProfileController::class,'store'])->name('store');
                Route::get('/edit/{user}',[OrganizationProfileController::class,'edit'])->name('edit');
                Route::put('/edit/{user}',[OrganizationProfileController::class,'update'])->name('update');
                Route::post('/category',[OrganizationProfileController::class,'addCategoryForm'])->name('category');
                Route::post('/sub_category',[OrganizationProfileController::class,'addSubCategory'])->name('sub.category');
                Route::post('/add_logo',[OrganizationProfileController::class,'addLogo'])->name('logo.create');
                Route::delete('/delete_logo/{user}',[OrganizationProfileController::class,'deleteLogo'])->name('logo.delete');
            });
        });
        Route::group(['prefix'=>'setting','as'=>'setting.','namespace'=>'Setting'],function(){
            Route::get('/{user}',[SettingController::class,'form'])->name('form');
            Route::put('/edit/{user}',[SettingController::class,'store'])->name('edit');
        });
        Route::group(['prefix'=>'task','as'=>'task.','namespace'=>'Task'],function(){
            Route::get('/{user}/customer/{status?}',[TaskController::class,'customer'])->name('customer');
            Route::get('/{user}/executor/{status?}',[TaskController::class,'executor'])->name('executor');
            Route::get('/{user}/reviews/customer',[\App\Http\Controllers\Cabinet\Task\ReviewController::class,'customer'])->name('reviews.customer');
            Route::get('/{user}/reviews/executor',[\App\Http\Controllers\Cabinet\Task\ReviewController::class,'executor'])->name('reviews.executor');
        });
        Route::group(['prefix'=>'message','as'=>'message.','namespace'=>'Message'],function(){
            Route::get('/{user}/chats/{opponents?}',[ChatController::class,'index'])->name('chats');
            Route::post('/{user}/chatroom',[ChatController::class,'chatroom'])->name('chatroom');
        });
        Route::get('/{user}/resumes/',[\App\Http\Controllers\Cabinet\ResumeController::class,'index'])->name('resume');
        Route::get('/{user}/vacancies/',[\App\Http\Controllers\Cabinet\VacanciesController::class,'index'])->name('vacancy');
        Route::get('/{user}/notification',[\App\Http\Controllers\Cabinet\Message\NotificationController::class,'index'])->name('notification');
        Route::get('/notification/view/{notification}',[\App\Http\Controllers\Cabinet\Message\NotificationController::class,'view'])->name('notification.view');
    });



Route::namespace('Executors')->prefix('executors')->as('executors.')->group(function(){
    Route::get('/',[ExecutorsController::class,'index'])->name('executors');
    Route::get('/category/{slug}',[ExecutorsController::class,'getBySlug'])->name('category');
});

Route::get('/user/{user}',[App\Http\Controllers\UserController::class,'show'])->name('user');

Route::namespace('Task')->prefix('task')->as('task.')->group(function (){
    Route::get('/',[ResourceController::class,'index'])->name('index');
    Route::get('/category/{slug}',[ResourceController::class,'getBySlug'])->name('category');
    Route::get('/show/{slug}',[ResourceController::class,'show'])->name('show');
    Route::namespace('Resource')->prefix('resource')->as('resource.')->middleware(['auth'])->group(function(){
        Route::get('/create',[ResourceController::class,'form'])->name('form');
        Route::post('/start/{task?}',[ResourceController::class,'start'])->name('start');
        Route::post('/finish/{task?}',[ResourceController::class,'finish'])->name('finish');
        Route::post('/full/{task?}',[ResourceController::class,'full'])->name('full');
        Route::post('/date/{task}',[ResourceController::class,'date'])->name('date');
        Route::post('/create',[ResourceController::class,'store'])->name('store');
        Route::post('/sub_category',[ResourceController::class,'addSubCategory'])->name('sub.category');
        Route::get('/update/{task}',[ResourceController::class,'update'])->name('update');
        Route::put('/edit/{task}',[ResourceController::class,'edit'])->name('edit');
        Route::delete('delete/{task}',[ResourceController::class,'delete'])->name('delete');
    });
    Route::namespace('User')->prefix('user')->as('user.')->group(function (){
        Route::get('/create/{user}',[UserTaskController::class,'create'])->name('create');
        Route::post('/create/{user}',[UserTaskController::class,'store'])->name('store');
        Route::post('/start',[UserTaskController::class,'start'])->name('start');
    });
    Route::namespace('Application')->prefix('application')->as('application.')->middleware(['auth'])->group(function(){
        Route::post('/create/{task}',[ApplicationController::class,'create'])->name('create');
        Route::post('/add-general/{task}',[ApplicationController::class,'addGeneralApplication'])->name('add.general');
        Route::get('/refused/{application}',[ApplicationController::class,'refused'])->name('refused');
        Route::get('/closed/{application}',[ApplicationController::class,'closed'])->name('closed');
    });
    Route::post('/add_reviews/{application}',[ReviewController::class,'addReviews'])->name('add.reviews');
});

Route::group(
    [
        'prefix' => 'vacancy',
        'as' => 'vacancy.',
        'namespace' => 'Vacancy',
    ],
    function(){
        Route::get('/',[\App\Http\Controllers\Vacancy\VacancyController::class,'index'])->name('index');
        Route::get('/create',[\App\Http\Controllers\Vacancy\VacancyController::class,'create'])->middleware('auth')->name('create');
        Route::post('/create',[\App\Http\Controllers\Vacancy\VacancyController::class,'store'])->middleware('auth')->name('store');
        Route::get('/edit/{vacancy}',[\App\Http\Controllers\Vacancy\VacancyController::class,'edit'])->middleware('auth')->name('edit');
        Route::put('/edit/{vacancy}',[\App\Http\Controllers\Vacancy\VacancyController::class,'update'])->middleware('auth')->name('update');
        Route::get('/show/{vacancy}',[\App\Http\Controllers\Vacancy\VacancyController::class,'show'])->name('show');
        Route::get('/{category}',[\App\Http\Controllers\Vacancy\VacancyController::class,'getByCategoryId'])->name('category');
        Route::delete('/{vacancy}',[\App\Http\Controllers\Vacancy\VacancyController::class,'remove'])->name('remove');
        Route::get('respond/{vacancy}',[\App\Http\Controllers\Vacancy\VacancyController::class,'respond'])->name('respond');
    });

Route::group(
    [
        'prefix' => 'resume',
        'as' => 'resume.',
        'namespace' => 'Resume',
    ],
    function(){
        Route::get('/',[\App\Http\Controllers\Resume\ResumeController::class,'index'])->name('index');

        Route::get('/create',[\App\Http\Controllers\Resume\ResumeController::class,'create'])->middleware('auth')->name('create');
        Route::post('/create',[\App\Http\Controllers\Resume\ResumeController::class,'store'])->middleware('auth')->name('store');
        Route::get('/edit/{resume}',[\App\Http\Controllers\Resume\ResumeController::class,'edit'])->middleware('auth')->name('edit');
        Route::put('/edit/{resume}',[\App\Http\Controllers\Resume\ResumeController::class,'update'])->middleware('auth')->name('update');
        Route::post('/experience',[\App\Http\Controllers\Resume\ResumeController::class,'experience'])->middleware('auth')->name('experience');
        Route::post('/education',[\App\Http\Controllers\Resume\ResumeController::class,'education'])->middleware('auth')->name('education');
        Route::get('/show/{resume}',[\App\Http\Controllers\Resume\ResumeController::class,'show'])->name('show');
        Route::delete('/{resume}',[\App\Http\Controllers\Resume\ResumeController::class,'remove'])->name('remove');
        Route::get('/{category}',[\App\Http\Controllers\Resume\ResumeController::class,'getByCategoryId'])->name('category');
        Route::get('/respond/{resume}',[\App\Http\Controllers\Resume\ResumeController::class,'respond'])->name('respond');
    });

Route::namespace('Admin')->prefix('admin')->as('admin.')->middleware(['auth', 'can:admin-panel'])->group(
    function(){
        Route::get('/',[AdminController::class,'index'])->name('index');
        Route::namespace('User')->prefix('user')->as('user.')->group(function (){
            Route::get('/',[UserController::class,'index'])->name('index')->name('index');
            Route::get('/edit/{user}',[UserController::class,'editForm'])->name('form');
            Route::put('/edit/{user}',[UserController::class,'edit'])->name('edit');
            Route::get('/view/{user}',[UserController::class,'view'])->name('view');
        });
        Route::namespace('Region')->prefix('region')->as('region.')->group(function (){
            Route::get('/',[RegionController::class,'index'])->name('index');
            Route::get('/create',[RegionController::class,'create'])->name('create');
            Route::post('/create',[RegionController::class,'store'])->name('store');
            Route::get('/view/{region}',[RegionController::class,'show'])->name('show');
            Route::get('/edit/{region}',[RegionController::class,'edit'])->name('edit');
            Route::put('/edit/{region}',[RegionController::class,'update'])->name('update');
            Route::delete('/delete/{region}',[RegionController::class,'destroy'])->name('destroy');
        });
        Route::namespace('Category')->prefix('category')->as('category.')->group(function(){
            Route::get('/',[CategoryController::class,'index'])->name('index');
            Route::get('/create',[CategoryController::class,'create'])->name('create');
            Route::post('/create',[CategoryController::class,'store'])->name('store');
            Route::get('/view/{category}',[CategoryController::class,'show'])->name('show');
            Route::get('/edit/{category}',[CategoryController::class,'edit'])->name('edit');
            Route::put('/edit/{category}',[CategoryController::class,'update'])->name('update');
            Route::delete('/delete/{category}',[CategoryController::class,'destroy'])->name('destroy');
            Route::get('/preview/{category}',[CategoryController::class,'preview'])->name('preview');
        });
        Route::namespace('Organization')->prefix('organization')->as('organization.')->group(function(){
            Route::get('/',[OrganizationController::class,'index'])->name('index');
            Route::get('/view/{organization}',[OrganizationController::class,'show'])->name('show');
            Route::get('/edit/{organization}',[OrganizationController::class,'edit'])->name('edit');
            Route::put('/edit/{organization}',[OrganizationController::class,'update'])->name('update');
        });
        Route::namespace('HandbookTitle')->prefix('handbook-title')->as('handbook.title.')->group(function(){
            Route::get('/',[HandbookTitleController::class,'index'])->name('index');
            Route::get('/create',[HandbookTitleController::class,'create'])->name('create');
            Route::post('/create',[HandbookTitleController::class,'store'])->name('store');
            Route::get('/view/{handbookTitle}',[HandbookTitleController::class,'show'])->name('show');
            Route::get('/edit/{handbookTitle}',[HandbookTitleController::class,'edit'])->name('edit');
            Route::put('/edit/{handbookTitle}',[HandbookTitleController::class,'update'])->name('update');
            Route::delete('/delete/{handbookTitle}',[HandbookTitleController::class,'destroy'])->name('destroy');
        });
        Route::namespace('Handbook')->prefix('handbook')->as('handbook.')->group(function(){
            Route::get('/',[HandbookController::class,'index'])->name('index');
            Route::get('/create',[HandbookController::class,'create'])->name('create');
            Route::post('/create',[HandbookController::class,'store'])->name('store');
            Route::get('/view/{handbook}',[HandbookController::class,'show'])->name('show');
            Route::get('/edit/{handbook}',[HandbookController::class,'edit'])->name('edit');
            Route::put('/edit/{handbook}',[HandbookController::class,'update'])->name('update');
            Route::delete('/delete/{handbook}',[HandbookController::class,'destroy'])->name('destroy');
        });
        Route::namespace('Task')->prefix('task')->as('task.')->group(function (){
            Route::get('/',[AdminTaskController::class,'index'])->name('index');
            Route::get('/view/{task}',[AdminTaskController::class,'show'])->name('view');
            Route::delete('/delete/{task}',[AdminTaskController::class,'delete'])->name('delete');
            Route::post('/sub_category',[AdminTaskController::class,'subCategory'])->name('subCategory');
        });
        Route::namespace('Resume')->prefix('resume')->as('resume.')->group(function (){
            Route::get('/',[\App\Http\Controllers\Admin\Resume\ResumeController::class,'index'])->name('index');
            Route::get('/view/{resume}',[\App\Http\Controllers\Admin\Resume\ResumeController::class,'show'])->name('view');
            Route::delete('/delete/{resume}',[\App\Http\Controllers\Admin\Resume\ResumeController::class,'delete'])->name('delete');
        });
        Route::namespace('Vacancy')->prefix('vacancy')->as('vacancy.')->group(function (){
            Route::get('/',[\App\Http\Controllers\Admin\Vacancy\VacancyController::class,'index'])->name('index');
            Route::get('/view/{vacancy}',[\App\Http\Controllers\Admin\Vacancy\VacancyController::class,'show'])->name('view');
            Route::delete('/delete/{vacancy}',[\App\Http\Controllers\Admin\Vacancy\VacancyController::class,'delete'])->name('delete');
        });
        Route::namespace('Frequency')->prefix('frequency')->as('frequency.')->group(function (){
            Route::namespace('Title')->prefix('title')->as('title.')->group(function (){
                Route::get('/',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'index'])->name('index');
                Route::get('/create',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'create'])->name('create');
                Route::post('/create',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'store'])->name('store');
                Route::get('/show/{frequencyTitle}',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'show'])->name('show');
                Route::get('/update/{frequencyTitle}',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'edit'])->name('edit');
                Route::put('/update/{frequencyTitle}',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'update'])->name('update');
                Route::delete('/destroy/{frequencyTitle}',[\App\Http\Controllers\Admin\Frequency\FrequencyTitleController::class,'delete'])->name('destroy');
            });
            Route::namespace('Section')->prefix('section')->as('section.')->group(function (){
                Route::get('/',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'index'])->name('index');
                Route::get('/create',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'create'])->name('create');
                Route::get('/show/{frequencySection}',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'show'])->name('show');
                Route::post('/create',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'store'])->name('store');
                Route::get('/update/{frequencySection}',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'edit'])->name('edit');
                Route::put('/update/{frequencySection}',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'update'])->name('update');
                Route::delete('/destroy/{frequencySection}',[\App\Http\Controllers\Admin\Frequency\FrequencySectionController::class,'delete'])->name('destroy');
            });
        });
    }
);
Route::namespace('Frequency')->prefix('frequency')->as('frequency.')->group(function (){
    Route::get('/',[\App\Http\Controllers\FrequencyController::class,'index'])->name('index');
    Route::get('/section/{frequencySection}',[\App\Http\Controllers\FrequencyController::class,'one'])->name('one');
});
Route::get('/safe_payment',function (){return view('safe_payment');})->name('safe_payment');
Route::get('/about_us',function (){return view('about_us');})->name('about_us');
Route::get('/user_agreement',function (){return view('user_agreement');})->name('user_agreement');
Route::get('/support',function (){return view('support');})->name('support');

