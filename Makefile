clear: cache config-clear dump-autoload

test: testing freshbd

freshdb: fresh seed

cache:#php artisan config:cache
	php artisan config:cache

config-clear:#php artisan config:clear
	php artisan config:clear
dump-autoload:#composer dump-autoload
	composer dump-autoload

testing:# tecting
	php vendor/bin/phpunit

seed:# seeding
	php artisan db:seed

reset:# reset migration
	php artisan migrate:reset

fresh:#fresh bd
	php artisan migrate:fresh

echo-start:#start laravel echo server
	laravel-echo-server start

serve:
	php artisan serve
