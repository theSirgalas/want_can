<?php

namespace App\Entities\Frequency;

use App\Http\Requests\Frequency\FrequencyTitleRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Symfony\Component\String\s;

/**
 * App\Entities\Frequency\FrequencyTitle
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle query()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencyTitle whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Frequency\FrequencySection[] $sections
 * @property-read int|null $titles_count
 * @property-read int|null $sections_count
 */
class FrequencyTitle extends Model
{
    use HasFactory;

    public $fillable=['title','description'];

    public static function add(string $title,string $description)
    {
        return static::create([
            'title'=>$title,
            'description'=>$description
        ]);
    }

    public function edit(string $title,string $description):void
    {
        $this->title=$title;
        $this->description=$description;
    }

    public function sections()
    {
        return $this->hasMany(FrequencySection::class,'frequency_titles_id');
    }
}
