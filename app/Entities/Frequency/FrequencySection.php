<?php

namespace App\Entities\Frequency;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Frequency\FrequencySection
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $frequency_titles_id
 * @property string $short_description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection query()
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereFrequencyTitlesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Entities\Frequency\FrequencyTitle $frequencyTitle
 * @method static \Illuminate\Database\Eloquent\Builder|FrequencySection whereShortDescription($value)
 */
class FrequencySection extends Model
{
    use HasFactory;

    public $fillable=['title','description','frequency_titles_id'];

    public static function add(string $title,string $description,int $frequency_title_id,string $short_description)
    {
        return static::create([
            'title'=>$title,
            'description'=>$description,
            'frequency_titles_id'=>$frequency_title_id,
            'short_description'=>$short_description
        ]);
    }

    public function edit(string $title,string $description, int $frequency_title_id,string $short_description):void
    {
        $this->title=$title;
        $this->description=$description;
        $this->frequency_titles_id=$frequency_title_id;
        $this->short_description=$short_description;
    }

    public function frequencyTitle()
    {
        return $this->belongsTo(FrequencyTitle::class,'frequency_titles_id');
    }
}
