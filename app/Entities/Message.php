<?php

namespace App\Entities;

use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Http\Requests\Chat\ChatRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class Message
 * @package App\Entities
 * @property int $chatroom_id
 * @property int $user_id
 * @property string $message
 * @property string view
 */
class Message extends Model
{
    use HasFactory;

    protected $connection ='mongodb';

    public $fillable=['chatroom_id','user_id','message','view'];
    public $timestamps = false;

    public function chatroom()
    {
        return $this->belongsTo(Chatroom::class);
    }

    public function user():User
    {
        return User::where('id',$this->user_id)->first();
    }

    public static function create(array $data,User $user):self
    {
        $message=new static();
            $message->chatroom_id=$data['room_id'];
            $message->user_id=$user->id;
            $message->message=$data['text'];
            $message->view=false;
        $message->save();
        return $message;
    }
}
