<?php

namespace App\Entities;

use App\Entities\Resume\Resume;
use App\Entities\Task\Application;
use App\Entities\Vacancy\Vacancy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Entities\Task\Task;
use App\Entities\User\User;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

/**
 * Class Notification
 * @package App\Entities
 * @property  integer $user_id
 * @property string $entity
 * @property integer $primary
 * @property string $notification
 * @property boolean $view
 * @property integer $type
 * @property integer $author_user_id
 * @property-read \App\Entities\Task\Task|null $taskproposal
 * @property-read \App\Entities\Task\Task|null $taskreviews
 * @property-read \App\Entities\User\User|null $user
 * @property-read \App\Entities\Resume\Resume|null $resume
 * @property-read \App\Entities\Vacancy\Vacancy|null $vacancy
 *  @property-read string $user_link
 */
class Notification extends Model
{
    use HasFactory;

    protected $connection ='mongodb';

    public $fillable=['user_id','entity','primary','notification','view','type'];

    const TYPE_REVIEWS=1;
    const TYPE_PROPOSAL=2;

    public function message(self $event){
        switch ($event->entity){
            case $event->entity==Task::class&&$event->type==self::TYPE_REVIEWS:
                return 'пользователь '.$event->userLink().' откликнулся на задание <a href="'.route('task.show',$event->task()->slug).'">'.$event->task()->title.'</a>';
            case $event->entity==Application::class&&$event->type==Application::TYPE_REFUSE:
                return 'пользователь '.$event->userLink().' отказался от исполнения задание <a href="'.route('task.show',$event->task()->slug).'">'.$event->task()->title.'</a>';
            case $event->entity==Application::class&&$event->type==Application::TYPE_CLOSED:
                return 'пользователь '.$event->userLink().' отметил исполнения задание <a href="'.route('task.show',$event->task()->slug).'">'.$event->task()->title.'</a>';
            case $event->entity==Application::class&&$event->type=Application::TYPE_OPEN:
                return 'пользователь '.$event->userLink().' принял вашу заявку на задание <a href="'.route('task.show',$event->task()->slug).'">'.$event->task()->title.'</a>';
            case $event->entity==Resume::class:
                return 'пользователь '.$event->userLink().' откликнулся на резюме <a href="'.route('resume.show',$event->resume()).'">'.$event->resume()->post_name.'</a>';
            case $event->entity==Vacancy::class:
                return 'пользователь '.$event->userLink().' откликнулся на вакансию <a href="'.route('vacancy.show',$event->vacancy()).'">'.$event->vacancy()->name.'</a>';
            default:
                return 'вам поступило предложение на участие в задании <a href="'.route('task.show',$event->task()->slug).'">'.$event->task()->title.'</a> от пользователя '.$event->userLink();
        }
    }

    public function user()
    {
        return User::where('id',$this->user_id)->first();
    }

    public function author()
    {
        return User::where('id',$this->author_user_id)->first();
    }

    public function task()
    {
        return Task::where('id',$this->primary)->first();
    }

    public function resume()
    {
        return Resume::where('id',$this->primary)->first();
    }

    public function vacancy()
    {
        return Vacancy::where('id',$this->primary)->first();
    }

    public function userLink()
    {
        return '<a href="'.route('user',$this->author()).'">'.$this->author()->short_name.'</a>';
    }

    public static function create($entity,$primary,$type,$user)
    {
        $notification=new static();
        $notification->author_user_id=Auth::user()->id;
        $notification->user_id=$user->id;
        $notification->entity=$entity;
        $notification->primary=$primary;
        $notification->type=$type;
        $notification->notification=$notification->message($notification);
        $notification->view=false;
        $notification->save();
    }

}
