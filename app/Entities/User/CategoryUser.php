<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\User\CategoryUser
 *
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryUser query()
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryUser whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryUser whereUserId($value)
 */
class CategoryUser extends Model
{
    use HasFactory;

    protected $table ="category_user";
    protected $primaryKey=['user_id','category_id'];
    public $timestamps = false;
}
