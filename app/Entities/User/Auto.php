<?php

namespace App\Entities\User;

use App\Entities\Handbook\Handbook;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\User\Auto
 *
 * @property int $id
 * @property string $mark
 * @property string $color
 * @property string $number
 * @property int $body_type_handbook_id
 * @method static \Illuminate\Database\Eloquent\Builder|Auto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Auto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Auto query()
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereBodyTypeHandbookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereMark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereNumber($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Auto whereUserId($value)
 * @property-read Handbook|null $bodyType
 */
class Auto extends Model
{
    use HasFactory;

    protected $guarded=['id'];

    public $timestamps = false;

    const NAME_START=1;

    public function bodyType()
    {
        return $this->belongsTo(Handbook::class,'body_type_handbook_id');
    }

    public static function add($mark,$color,$number,$bodyType):self
    {
        $auto=new Auto();
        return $auto->fill([
            'mark'=>$mark,
            'color'=>$color,
            'number'=>$number,
            'body_type_handbook_id'=>$bodyType
        ]);
    }

    public function edit(string $mark, string $color,string $number,int $bodyType):void
    {
        $this->mark=$mark;
        $this->color=$color;
        $this->number=$number;
        $this->body_type_handbook_id=$bodyType;
    }

    public static function isAuto(int $user_id,$number):?Auto
    {
        return self::where('user_id',$user_id)->where('number',$number)->first();
    }

}
