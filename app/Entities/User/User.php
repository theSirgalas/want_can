<?php

namespace App\Entities\User;

use App\Entities\Category;
use App\Entities\Chatroom;
use App\Entities\File;
use App\Entities\Message;
use App\Entities\Notification;
use App\Entities\Organization\Organization;
use App\Entities\Region;
use App\Entities\Resume\Resume;
use App\Entities\Social;
use App\Entities\Task\Application;
use App\Entities\Task\Reviews;
use App\Entities\Task\Task;
use App\Entities\Vacancy\Vacancy;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Str;
use App\Http\Requests\User\RegisterRequest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Date\Date;
use Kalnoy\Nestedset\Collection;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string $phone
 * @property string $about_us
 * @property string $last_login
 * @property integer $is_auto
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAboutUs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Kalnoy\Nestedset\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @property int $region_id
 * @property-read Region|null $region
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegionId($value)
 * @property string $role
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRole($value)
 * @property-read mixed $date_from_table
 * @property-read mixed $role_name
 * @property-read Organization|null $rganization
 * @property int|null $avatar_file_id
 * @property bool $status_moderate
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatarFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatusModerate($value)
 * @property-read File|null $avatar
 * @property-read File|null $avater
 * @property-read \Illuminate\Database\Eloquent\Collection|File[] $files
 * @property-read int|null $files_count
 * @property-read Organization|null $organization
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\User\Auto[] $autos
 * @property-read int|null $auto_count
 * @property-read mixed $avatar_file
 * @property-read mixed $root_category_id
 * @property-read mixed $sub_category_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Social[] $socials
 * @property-read int|null $social_count
 * @property-read int|null $autos_count
 * @property-read int|null $socials_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $work
 * @property-read int|null $work_count
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLogin($value)
 * @property bool $is_executor
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $tasksCreator
 * @property-read int|null $tasks_creator_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $tasksExecutor
 * @property-read int|null $tasks_executor_count
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsExecutor($value)
 * @property-read mixed $time_site
 * @property-read \Illuminate\Database\Eloquent\Collection|Application[] $aplications
 * @property-read int|null $aplications_count
 * @property int $reviews_count
 * @property float $reviews_score
 * @method static \Illuminate\Database\Eloquent\Builder|User whereReviewsCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereReviewsScore($value)
 * @property-read mixed $score
 * @property-read \Illuminate\Database\Eloquent\Collection|Reviews[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|Chatroom[] $chatrooms
 * @property-read int|null $chatrooms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Chatroom[] $chatroomsCustomer
 * @property-read int|null $chatrooms_customer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Chatroom[] $chatroomsExecutor
 * @property-read int|null $chatrooms_executor_count
 * @property-read mixed $avatars
 * @property-read \Illuminate\Database\Eloquent\Collection|Resume[] $resumes
 * @property-read int|null $resumes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Vacancy[] $vacancies
 * @property-read int|null $vacancies_count
 * @property-read int|null $message
 * @property-read int|null $notify
 * @property-read mixed $short_name
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const ADMIN='admin';
    const MODERATOR='moderator';
    const USER='user';

    const HAVE_AUTO=1;
    const NOT_AUTO=2;

    const AVATAR_SIZE=['width'=>128,'height'=>128];

    public static $rolesList=[
        self::ADMIN=>'Админ',
        self::MODERATOR=>'Модератор',
        self::USER=>'Пользователь',
    ];

    public static $rolesListTableSearch=[
        'Админ'=>'Админ',
        'Модератор' =>'Модератор',
        'Пользователь'=>'Пользователь',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'region_id',
        'avatar_file_id',
        'about_us',
        'phone',
        'is_auto'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function register(string $email,string $password, string $phone,int $region_id,string $name=null): self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'verify_token' => Str::uuid(),
            'phone'=>$phone,
            'role'=>self::USER,
            'region_id'=>$region_id
        ]);
    }

    public function update(array $attributes = [], array $options = [])
    {
        if (! $this->exists) {
            return false;
        }
         $this->fill($attributes);
         return $this->save($options);
    }

    public function chatroomsCustomer()
    {
        return $this->hasMany(Chatroom::class,'customer_id');
    }

    public function chatroomsExecutor()
    {
        return $this->hasMany(Chatroom::class,'executor_id');
    }

    public function tasks()
    {
        return $this->hasMany(Task::class,'user_id');
    }

    public function work()
    {
        return $this->hasMany(Task::class,'customer_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryUser()
    {
        return $this->belongsToMany(CategoryUser::class);
    }


    public function region()
    {
        return $this->belongsTo(Region::class,'region_id');
    }

    public function avatar()
    {
        return $this->belongsTo(File::class,'avatar_file_id');
    }

    public function organization()
    {
        return $this->hasOne(Organization::class);
    }

    public function files()
    {
        return $this->belongsToMany(File::class,'user_files');//->using(OrganizationFile::class);
    }

    public function autos()
    {
        return $this->hasMany(Auto::class);
    }

    public function socials()
    {
        return $this->belongsToMany(Social::class,'user_socials');
    }

    public function tasksCreator()
    {
        return $this->hasMany(Task::class,'user_id');
    }

    public function tasksExecutor()
    {
        return $this->hasMany(Task::class,'executor_id');
    }

    public function aplications()
    {
        return $this->hasMany(Application::class);
    }

    public function reviews()
    {
        return $this->hasMany(Reviews::class);
    }

    public function resumes()
    {
        return $this->hasMany(Resume::class,'author_user_id');
    }

    public function vacancies()
    {
        return $this->hasMany(Vacancy::class,'author_user_id');
    }


    public function isAdmin()
    {
        return $this->role==self::ADMIN;
    }

    public function isModerator()
    {
        return $this->role==self::MODERATOR;
    }
    public function isModerate()
    {
        return $this->status_moderate;
    }

    public function isUser()
    {
        return $this->role==self::USER;
    }

    public function getTimeSiteAttribute()
    {
        $last_time=Carbon::parse($this->last_login);
        $last_time->setLocale('ru');
        return (new Carbon())->diffForHumans($last_time,true,true,3);
    }

    public function getDateCreate(string $patern='F Y'){
        $create_at=new Date($this->created_at);
        return $create_at->format($patern);
    }


    public function getRoleNameAttribute()
    {
        return self::$rolesList[$this->role];
    }

    public function getDateFromTableAttribute()
    {
        $dateArray=explode(' ',$this->created_at);
        return $dateArray[0];
    }

    public function getRootCategoryIdAttribute()
    {
        $returnArray=[];
        foreach ($this->categories as $category){
            if($category->isRoot()){
                $returnArray[]=$category->id;
            }
        }
        return $returnArray;
    }

    public function getSubCategoryIdAttribute()
    {
        $returnArray=[];
        foreach ($this->categories as $category){
            if(!$category->isRoot()){
                $returnArray[]=$category->id;
            }
        }
        return $returnArray;
    }

    public function getTreeCategory(Collection $categories=null)
    {
        $returnCategory=[];
        if(!$categories) {
            $categories = $this->categories()->where('parent_id', null)->get();
        }
        foreach($categories as $category){
            $returnCategory[]=[
                'id'=>$category->id,
                'name'=>$category->name,
                'slug'=>$category->slug,
                'icon'=>is_object($category->icon)?$category->icon->path:'/svg/cat-holder.svg',
                'sub_category'=>$this->getTreeCategory($this->categories()->where('parent_id',$category->id)->get())
            ];
        }
        return $returnCategory;

    }

    public function socialByName(string $name):?Social
    {
        $returnSocial=null;
        foreach ($this->socials as $social){
            if($social->name==$name){
                $returnSocial=$social;
            }
        }
        return $returnSocial;
    }

    public function socialNameByName(string $name):?string
    {
        $social=$this->socialByName($name);
        if(is_object($social)){
            return $social->link;
        }
        return null;
    }

    public function getScoreAttribute()
    {
        if($this->reviews_count===0){
            return 0;
        }
        return round($this->reviews_score/$this->reviews_count,2);
    }

    public function getAvatarsAttribute()
    {
        if(is_object($this->avatar)){
            return ['path'=>$this->avatar->path,'alt'=>$this->name.'-avatar'];
        }
        return false;
    }

    public function getMessageAttribute():?int
    {
        $idExecutor=$this->chatroomsExecutor()->pluck('id')->all();
        $idCustomer=$this->chatroomsCustomer()->pluck('id')->all();
        $ids=array_merge($idCustomer,$idExecutor);
        $message=Message::where('view',false)->where('user_id','!=',$this->id)->whereIn('chatroom_id',$ids)->count();
        if($message>0){
            return $message;
        }else{
            return null;
        }
    }

    public function getNotifyAttribute():?int
    {
        if($notify=Notification::where('user_id',$this->id)->where('view',false)->count()){
            return $notify;
        }else{
            return null;
        }
    }

    public function getShortNameAttribute()
    {
        return substr($this->name, 0, strpos($this->name, " ")+3).".";
    }

    public function isHaveAuto():bool
    {
        return $this->is_auto==self::HAVE_AUTO;
    }

    public function isNotHaveAuto():bool
    {
        return $this->is_auto==self::NOT_AUTO;
    }

}
