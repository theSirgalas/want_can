<?php

namespace App\Entities\Task;

use App\Entities\Category;
use App\Entities\Chatroom;
use App\Entities\File;
use App\Entities\Handbook\Handbook;
use App\Entities\User\User;
use App\Helpers\ArrayHelpers;
use App\Helpers\StringHelpers;
use App\Http\Requests\Task\CreateRequest;
use App\Http\Requests\Task\CreateUserRequest;
use App\Http\Requests\Task\EditRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * App\Entities\Task\Task
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $from
 * @property string $to
 * @property string|null $start_date
 * @property string|null $finish_date
 * @property bool $now
 * @property float $price
 * @property bool $is_need_documents
 * @property bool $is_safe_payment
 * @property bool $is_finished
 * @property int $user_id
 * @property int $customer_id
 * @property int $payment_handbook_id
 * @property int $category_id
 * @property int $sub_category_id
 * @property int $place_service
 * @property int $file_id
 * @property int $type_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereFinishDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIsFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIsNeedDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIsSafePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereNow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePaymentHandbookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereUserId($value)
 * @mixin \Eloquent
 * @property-read Category $category
 * @property-read User $customer
 * @property-read Handbook $payment
 * @property-read Category $subCategory
 * @property-read User $user
 * @property int $palace_service
 * @property-read File $file
 * @property-read mixed $place
 * @property-read mixed $price_type
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePalaceService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereTypePrice($value)
 * @property bool $is_close
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereIsClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Task wherePlaceService($value)
 * @property int|null $executor_id
 * @property-read mixed $date
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereExecutorId($value)
 * @property-read mixed $create_array
 * @property-read mixed $finish_array
 * @property-read mixed $start_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Task\Application[] $applications
 * @property-read int|null $applications_count
 * @property int|null $application_id
 * @property-read \App\Entities\Task\Application|null $application
 * @property-read mixed $new_applications
 * @property-read mixed $view_applications
 * @method static \Illuminate\Database\Eloquent\Builder|Task whereApplicationId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Chatroom[] $chatrooms
 * @property-read int|null $chatrooms_count
 * @property-read mixed $start_date_array
 * @property-read mixed $status
 */
class Task extends Model
{
    use HasFactory;

    const REMOTE=0;
    const IN_PLACE=1;

    const FIXED=0;
    const FROM=1;
    const TO=2;

    const OPEN_STATUS=0;
    const IN_WORK_STATUS=1;
    const FINISH_STATUS=2;
    const CLOSED_STATUS=3;


    public static $placeServiceName=[
        self::REMOTE=>'Можно выполнить удаленно',
        self::IN_PLACE=>'Нужно присутствие по адресу'
    ];

    public static $priceTypeName=[
        self::FIXED=>'Фиксирована',
        self::FROM=>'От',
        self::TO=>'До',
    ];

    public static $statusArray=[
        self::OPEN_STATUS=>'Открыто',
        self::IN_WORK_STATUS=>'Выполняется',
        self::FINISH_STATUS=>'Выполнено',
        self::CLOSED_STATUS=>'Закрыто'

    ];

    public $guarded=[];

    public static function add(CreateRequest $request):self
    {
        $task= new Task();
        return Task::create([
            'title'=>$request->title,
            'slug'=>StringHelpers::getSlug(Str::slug($request->title),Task::class),
            'description'=>$request->description,
            'from'=>$request->from,
            'to'=>$request->to,
            'start_date'=>$task->getStartDate($request),
            'finish_date'=>!ArrayHelpers::arrayEmptyFields([$request->finish_date,$request->finish_time])?StringHelpers::stringToTimestamp($request->finish_date.' '.$request->finish_time):null,
            'now'=>$request->now??false,
            'price'=>(float)$request->price,
            'is_need_documents'=>$request->is_need_documents??false,
            'is_safe_payment'=>$request->is_safe_payment,
            'user_id'=>Auth::user()->id,
            'payment_handbook_id'=>$request->payment_handbook_id,
            'category_id'=>$request->category_id,
            'sub_category_id'=>$request->sub_category_id,
            'place_service'=>$request->place_service,
            'type_price'=>$request->type_price
        ]);
    }

    public static function addUser(CreateUserRequest $request):self
    {
        $task=new static();
        return Task::create([
            'title'=>$request->title,
            'slug'=>StringHelpers::getSlug(Str::slug($request->title),Task::class),
            'description'=>$request->description,
            'from'=>$request->from,
            'to'=>$request->to,
            'start_date'=>$task->getStartDate($request),
            'finish_date'=>!ArrayHelpers::arrayEmptyFields([$request->finish_date,$request->finish_time])?StringHelpers::stringToTimestamp($request->finish_date.' '.$request->finish_time):null,
            'now'=>$request->now??false,
            'price'=>(float)$request->price,
            'is_need_documents'=>$request->is_need_documents??false,
            'is_safe_payment'=>$request->is_safe_payment,
            'user_id'=>Auth::user()->id,
            'payment_handbook_id'=>$request->payment_handbook_id,
            'category_id'=>$request->category_id,
            'sub_category_id'=>$request->sub_category_id,
            'place_service'=>$request->place_service,
            'type_price'=>$request->type_price,
            'executor_id'=>$request->executor_id
        ]);
    }

    public function edit(EditRequest $request)
    {
        $this->title=$request->title;
        $this->slug=StringHelpers::getSlug(Str::slug($request->title),Task::class);
        $this->description=$request->description;
        $this->from=$request->from;
        $this->to=$request->to;
        $this->start_date=$this->getStartDate($request);
        $this->finish_date=!ArrayHelpers::arrayEmptyFields([$request->finish_date,$request->finish_time])?StringHelpers::stringToTimestamp($request->finish_date.' '.$request->finish_time):null;
        $this->now=$request->now??false;
        $this->price=(float)$request->price;
        $this->is_need_documents=$request->is_need_documents;
        $this->is_safe_payment=$request->is_safe_payment;
        $this->user_id=Auth::user()->id;
        $this->payment_handbook_id=$request->payment_handbook_id;
        $this->category_id=$request->category_id;
        $this->sub_category_id=$request->sub_category_id;
        $this->place_service=$request->place_service;
        $this->type_price=$request->type_price;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(Category::class);
    }

    public function payment()
    {
        return $this->belongsTo(Handbook::class,'payment_handbook_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    public function application()
    {
        return $this->belongsTo(Application::class,'application_id');
    }

    public function chatrooms()
    {
        return $this->hasMany(Chatroom::class);
    }


    public function getPlaceAttribute()
    {
        return self::$placeServiceName[$this->place_service];
    }

    public function getPriceTypeAttribute()
    {
        return self::$priceTypeName[$this->type_price];
    }

    public function isCustomer()
    {
        if(empty(Auth::user())){
            return false;
        }
        return Auth::user()->id==$this->user_id;
    }

    public function isApplicant()
    {
        if(empty(Auth::user())){
            return false;
        }
        $application=$this->applications()->pluck('user_id')->all();
        return in_array(Auth::user()->id,$application);
    }

    public function isExecutor()
    {
        if(!is_object(Auth::user())){
            return false;
        }
        return Auth::user()->id==$this->executor_id;
    }

    public function isPermissionApplication()
    {
        if(empty(Auth::user())){
            return false;
        }
        if(Auth::user()->id==$this->user_id){
            return false;
        }
        if(!empty($this->customer_id)){
            return false;
        }
        $application=$this->applications()->pluck('user_id')->all();
        return !in_array(Auth::user()->id,$application);
    }

    public function getDateAttribute()
    {
        $returnArray=[];
        if(!empty($this->start_date)){
            $returnArray['начать работу']=StringHelpers::dateToString($this->start_date);
        }
        if(!empty($this->finish_date)){
            $returnArray['окончить работу']=StringHelpers::dateToString($this->finish_date);
        }
        return $returnArray;
    }

    public function getStartArrayAttribute()
    {
        $date= new Carbon($this->start_date);
        return['date'=>$date->format('j.n.Y'),'time'=>$date->format('H:i')];
    }

    public function getCreatedAtArrayAttribute()
    {
        $date= new Carbon($this->created_at);
        return['date'=>$date->format('d.m.Y'),'time'=>$date->format('H:i')];
    }

    public function getFinishArrayAttribute()
    {
        $date= new Carbon($this->finish_date);
        return['date'=>$date->format('j.n.Y'),'time'=>$date->format('H:i')];
    }

    public function getStartDateArrayAttribute()
    {
        return StringHelpers::dateToString($this->date_start);
    }

    public function getStartDate($request):?string
    {
        if (!empty($request->now)) {
            return 'now()';
        }
        if (ArrayHelpers::arrayEmptyFields([$request->start_date, $request->start_time])) {
            return StringHelpers::stringToTimestamp($request->start_date . ' ' . $request->start_time);
        }
        return null;
    }

    public function getStatusAttribute()
    {
        if($this->is_close){
            return self::$statusArray[self::CLOSED_STATUS];
        }
        if($this->is_finished){
            return self::$statusArray[self::FINISH_STATUS];
        }
        if(is_object($this->application)){
            return self::$statusArray[self::IN_WORK_STATUS];
        }
        return self::$statusArray[self::OPEN_STATUS];
    }



    public function getNewApplicationsAttribute()
    {
        return $this->applications()->where('viewers',false)->get();
    }
    public function getViewApplicationsAttribute()
    {
        return $this->applications()->where('viewers',true)->get();
    }
}
