<?php

namespace App\Entities\Task;

use App\Entities\User\User;
use App\Http\Requests\Application\CreateRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

/**
 * App\Entities\Task\Application
 *
 * @property int $id
 * @property int $price
 * @property string $comment
 * @property int $task_id
 * @property int $customer_id
 * @property boolean $is_refuse
 * @property boolean is_close
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Task $task
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Application newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Application newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Application query()
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereUserId($value)
 * @property bool $viewers
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereViewers($value)
 * @property-read \App\Entities\Task\Reviews|null $reviews
 */
class Application extends Model
{
    use HasFactory;
    public $guarded=['id'];

    const TYPE_OPEN=1;
    const TYPE_REFUSE=2;
    const TYPE_CLOSED=3;

    public static function add(CreateRequest $request,Task $task):self
    {
        return static::create([
            'price'=>$request->price_app,
            'comment'=>$request->comment,
            'task_id'=>$task->id,
            'user_id'=>Auth::user()->id,
        ]);
    }


    public function task()
    {
        return $this->belongsTo(Task::class,);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reviews()
    {
        return $this->hasOne(Reviews::class);
    }

    public function getDateCreate(string $patern='d F Y'){
        $create_at=new Date($this->created_at);
        return $create_at->format($patern);
    }

}
