<?php

namespace App\Entities\Task;

use App\Entities\User\User;
use App\Http\Requests\Reviews\AddRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

/**
 * App\Reviews
 *
 * @property int $id
 * @property float $score
 * @property string $comment
 * @property int $application_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews query()
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Entities\Task\Application|null $application
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Reviews whereUserId($value)
 */
class Reviews extends Model
{
    use HasFactory;

    public $guarded=['id'];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function add(AddRequest $request,Application $application)
    {
        return Reviews::create([
            'score'=>$request->score,
            'comment'=>$request->comment,
            'application_id'=>$application->id,
            'user_id'=>$application->user_id
        ]);
    }

    public function getDateCreate(string $patern='d F Y'){
        $create_at=new Date($this->created_at);
        return $create_at->format($patern);
    }
}
