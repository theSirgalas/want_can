<?php

namespace App\Entities\Organization;

use App\Entities\Category;
use App\Entities\File;
use App\Entities\Region;
use App\Entities\Social;
use App\Entities\User\User;
use App\Http\Requests\Organization\AdminEditRequest;
use App\Http\Requests\Organization\CabinetEditRequest;
use App\Http\Requests\Organization\RegisterRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

/**
 * App\Entities\Organization\Organization
 *
 * @property int $id
 * @property string $name
 * @property string $inn
 * @property int $region_id
 * @property string $legal_address
 * @property string $fact_address
 * @property string $email
 * @property string $phone
 * @property string $about_us
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization query()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereAboutUs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereFactAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereLegalAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereUserId($value)
 * @mixin \Eloquent
 * @property int $moderation_status
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereModerationStatus($value)
 * @property-read User|null $user
 * @property-read mixed $status
 * @property int|null $file_id
 * @property-read Region $region
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereFileId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Organization\OrganizationFile[] $files
 * @property-read int|null $files_count
 * @property-read \Kalnoy\Nestedset\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @property int|null $logo_file_id
 * @property-read File|null $logo
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereLogoFileId($value)
 * @property-read mixed $sub_category_id
 * @property-read \Illuminate\Database\Eloquent\Collection|Social[] $socials
 * @property-read int|null $socials_count
 */
class Organization extends Model
{
    use HasFactory;

    protected $guarded=['id'];

    protected $table ='organizations';

    const STATUS_CREATE=0;
    const STATUS_MODERATE=1;
    const STATUS_REFUSED=2;

    public static $statusName=[
        self::STATUS_CREATE=>'Создана',
        self::STATUS_MODERATE=>'Прошла модерацию',
        self::STATUS_REFUSED=>'Не прошла модерацию',
    ];

    public static function register(array $request, int $user_id):self
    {
        return static::create([
            'name'=>$request['name'],
            'inn'=>$request['inn'],
            'region_id'=>$request['region_id'],
            'legal_address'=>$request['legal_address'],
            'fact_address'=>$request['fact_address'],
            'email'=>$request['email'],
            'phone'=>$request['phone'],
            'about_us'=>array_key_exists('about_us',$request)?$request['about_us']:null,
            'moderation_status'=>self::STATUS_CREATE,
            'user_id'=>$user_id
            ]);
    }


    public function editAdmin(AdminEditRequest $request):void
    {
        $this->moderation_status=$request->status;
    }


    public function edit(CabinetEditRequest $request):void
    {
        $this->name=$request->name;
        $this->inn=$request->inn;
        $this->region_id=$request->region_id;
        $this->legal_address=$request->legal_address;
        $this->fact_address=$request->fact_address;
        $this->email=$request->email;
        $this->phone=$request->phone;
        $this->about_us=$request->about_us;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function getStatusAttribute(){

        return self::$statusName[$this->moderation_status];
    }

    public function region()
    {
        return $this->belongsTo(Region::class,'region_id');
    }

    public function files()
    {
        return $this->belongsToMany(File::class,'organization_files');//->using(OrganizationFile::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_organizations');
    }

    public function logo()
    {
        return $this->belongsTo(File::class,'logo_file_id');
    }

    public function socials()
    {
        return $this->belongsToMany(Social::class,'organization_socials');
    }

    public function isNotModerate(): bool
    {
        return $this->status === self::STATUS_CREATE;
    }

    public function isModerate(): bool
    {
        return $this->status === self::STATUS_MODERATE;
    }

    public function isRefused(): bool
    {
        return $this->status === self::STATUS_REFUSED;
    }

    public function getSubCategoryIdAttribute()
    {
        $returnArray=[];
        foreach ($this->categories as $category){
            if(!$category->isRoot()){
                $returnArray[]=$category->id;
            }
        }
        return $returnArray;
    }

    public function socialByName(string $name):?Social
    {
        $returnSocial=null;
        foreach ($this->socials as $social){
            if($social->name==$name){
                $returnSocial=$social;
            }
        }
        return $returnSocial;
    }

    public function socialNameByName(string $name):?string
    {
        $social=$this->socialByName($name);
        if(is_object($social)){
            return $social->link;
        }
        return null;
    }

}
