<?php

namespace App\Entities\Resume;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Resume\WorkExperience
 *
 * @property int $id
 * @property string $company_name
 * @property string $post
 * @property string $month_start
 * @property string $years_start
 * @property string $month_end
 * @property string $years_end
 * @property string $obligation
 * @property int $resume_id
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience query()
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereMouthEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereMouthStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereObligation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience wherePost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereResumeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereYearsEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereYearsStart($value)
 * @mixin \Eloquent
 * @property-read \App\Entities\Resume\Resume $resume
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereMonthEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereMonthStart($value)
 * @property bool $now
 * @method static \Illuminate\Database\Eloquent\Builder|WorkExperience whereNow($value)
 * @property-read mixed $end_work
 */
class WorkExperience extends Model
{
    use HasFactory;

    public $guarded=['id'];

    public $timestamps = false;

    public static function add(array $request):self
    {
        $workExperience=new WorkExperience();
        return $workExperience->fill([
           'company_name'=>$request['company_name'],
           'post'=>$request['post'],
           'month_start'=>$request['month_start'],
           'years_start' =>$request['years_start'],
           'month_end' => $request['month_end'],
           'years_end' => $request['years_end'],
           'obligation' =>$request['obligation'],
           'now'=>!empty($request['now'])?$request['now']:false
        ]);
    }

    public function edit(array $request):void
    {
        $this->company_name=$request['company_name'];
        $this->post=$request['post'];
        $this->month_start=$request['month_start'];
        $this->years_start=$request['years_start'];
        $this->month_end=$request['month_end'];
        $this->years_end=$request['years_end'];
        $this->obligation=$request['obligation'];
        $this->now=!empty($request['now'])?$request['now']:false;
    }

    public function resume()
    {
        return $this->belongsTo(Resume::class);
    }

    public function getEndWorkAttribute()
    {
        if($this->now){
            $carbon=new Carbon();
            return $carbon->format('m').':'.$carbon->format('Y');
        }
        return $this->getMouthNumber($this->month_end).':'.$this->years_end;
    }

    public function isExperience(int $id, int $resumeId):bool
    {
        return $this->resume->id == $resumeId && $this->id == $id;
    }

    public function getMouthNumber($mouth)
    {
        return mb_strlen($mouth)==1?'0'.$mouth:$mouth;
    }


}
