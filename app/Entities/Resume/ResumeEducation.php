<?php

namespace App\Entities\Resume;

use App\Http\Requests\Resume\CreateRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Resume\ResumeEducation
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $specialty
 * @property int $resume_id
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereResumeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereSpecialty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereYears($value)
 * @property string $years_start
 * @property string $years_end
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereYearsEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeEducation whereYearsStart($value)
 */
class ResumeEducation extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $guarded=['id'];

    public static function add(array $request):self
    {
        $resumeEducation=new ResumeEducation();
        return $resumeEducation->fill([
            'name'=>$request['name'],
            'specialty'=>$request['specialty'],
            'years_start'=>$request['years_start'],
            'years_end'=>$request['years_end']
        ]);
    }

    public function edit(array $request)
    {
        $this->name=$request['name'];
        $this->specialty=$request['specialty'];
        $this->years_start=$request['years_start'];
        $this->years_end=$request['years_end'];
    }

    public function isEducation(int $id,int $resumeId):bool
    {
        return $this->resume_id==$resumeId&&$this->id==$id;
    }

}
