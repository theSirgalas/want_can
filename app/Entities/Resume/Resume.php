<?php

namespace App\Entities\Resume;

use App\Entities\Category;
use App\Entities\Handbook\Handbook;
use App\Entities\User\User;
use App\Http\Requests\Resume\CreateRequest;
use App\Http\Requests\Resume\EditRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Entities\Resume\Resume
 *
 * @property int $id
 * @property string $post_name
 * @property int $category_id
 * @property int $work_handbook_id
 * @property string $experience
 * @property int $education_handbook_id
 * @property int $sex_handbook_id
 * @property string $age
 * @property int $trips
 * @property bool $moving
 * @property string $citizenship
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Resume newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Resume newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Resume query()
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereCitizenship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereEducationHandbookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereExperience($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereMoving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume wherePostName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereSexHandbookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereTrips($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereWorkHandbookId($value)
 * @mixin \Eloquent
 * @property-read Category $category
 * @property-read Handbook $education
 * @property-read int|null $experience_count
 * @property-read Handbook $sex
 * @property-read Handbook $work
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereSex($value)
 * @property string $about_us
 * @property string $salary
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Resume\ResumeEducation[] $educations
 * @property-read int|null $educations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Resume\WorkExperience[] $experiences
 * @property-read int|null $experiences_count
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereAboutUs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereSalary($value)
 * @property int $author_user_id
 * @property-read mixed $sex_status
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereAuthorUserId($value)
 * @property-read mixed $moving_status
 * @property-read mixed $trips_status
 * @property bool $is_closed
 * @property-read mixed $time_site
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Resume whereIsClosed($value)
 */
class Resume extends Model
{
    use HasFactory;
    public $guarded=['id'];

    const MAN=1;
    const WOMAN=2;

    const YES=1;
    const NO=2;

    const CAN=1;
    const CAN_T=2;
    const MAYBE=3;

    public static $sexName=[
        self::MAN=>'мужской',
        self::WOMAN=>'женский'
    ];

    public static $tripsName=[
        self::CAN_T=>'Не готов',
        self::CAN=>'Готов',
        self::MAYBE=>'Инногда',
    ];

    public static $movingName=[
        self::YES=>'Готов',
        self::NO=>'Не готов',
    ];

    public static function add(CreateRequest $request):self
    {
         return self::create([
            'post_name'=>$request->post_name,
            'category_id'=>$request->category_id,
            'work_handbook_id'=>$request->work_handbook_id,
            'experience'=>$request->experience,
            'education_handbook_id'=>$request->education_handbook_id,
            'sex'=>$request->sex,
            'age'=>$request->age,
            'trips'=>$request->trips,
            'moving'=>$request->moving,
            'citizenship'=>$request->citizenship,
            'about_us'=>$request->about_us,
            'salary'=>$request->salary,
            'author_user_id'=>Auth::user()->id
        ]);
    }

    public function edit(EditRequest $request)
    {
        $this->post_name=$request->post_name;
        $this->category_id=$request->category_id;
        $this->work_handbook_id=$request->work_handbook_id;
        $this->experience=$request->experience;
        $this->education_handbook_id=$request->education_handbook_id;
        $this->sex=$request->sex;
        $this->age=$request->age;
        $this->trips=$request->trips;
        $this->moving=$request->moving;
        $this->citizenship=$request->citizenship;
        $this->about_us=$request->about_us;
        $this->salary=$request->salary;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function work()
    {
        return $this->belongsTo(Handbook::class,'work_handbook_id');
    }

    public function education()
    {
        return $this->belongsTo(Handbook::class,'education_handbook_id');
    }

    public function sex()
    {
        return $this->belongsTo(Handbook::class,'sex_handbook_id');
    }

    public function experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }

    public function educations()
    {
        return $this->hasMany(ResumeEducation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'author_user_id');
    }

    public function getSexStatusAttribute()
    {
        return self::$sexName[$this->sex];
    }

    public function getTripsStatusAttribute()
    {
        return self::$tripsName[$this->trips];
    }

    public function getTimeSiteAttribute()
    {
        $last_time=Carbon::parse($this->last_login);
        $last_time->setLocale('ru');
        return (new Carbon())->diffForHumans($last_time,true,true,3);
    }

    public function getMovingStatusAttribute():string
    {
        if($this->moving){
            return 'Готов к перезду';
        }
        return 'Не готов к перезду';
    }

    public function isCanRedact(User $user)
    {
        if($user->isAdmin()){
            return true;
        }
        if($user->id==$this->author_user_id){
            return true;
        }
        return false;
    }

    public function canReviews()
    {
        if(Auth::user()->id==$this->author_user_id){
            return false;
        }
        if($this->is_closed){
            return true;
        }
        return true;
    }
}
