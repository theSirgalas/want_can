<?php

namespace App\Entities;

use App\Helpers\ArrayHelpers;
use App\Http\Requests\Region\CreateRequest;
use App\Http\Requests\Region\UpdateRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * App\Entities\Region
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Region query()
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $moderation_status
 * @method static \Illuminate\Database\Eloquent\Builder|Region whereModerationStatus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Region[] $children
 * @property-read int|null $children_count
 * @property-read string $address
 * @property-read Region|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|Region roots()
 */
class Region extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public static function add(CreateRequest $request)
    {
        return static::create([
            'name' => $request->name,
            'email' => Str::slug($request->name),
            'parent_id' => $request->parent,

        ]);
    }

    /**
     * @param UpdateRequest $request
     */
    public function edit(UpdateRequest $request)
    {
        $this->name=$request->name;
        $this->slug=$this->name==$request->name?$this->slug:Str::slug($request->name);
        $this->parent_id=$request->parent;
    }


    public function getPath(): string
    {
        return ($this->parent ? $this->parent->getPath() . '/' : '') . $this->slug;
    }

    public function getAddressAttribute(): string
    {
        return ($this->parent ? $this->parent->address . ', ' : '') . $this->name;
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    public function scopeRoots(Builder $query)
    {
        return $query->where('parent_id', null);
    }

    public static function allIdName():array
    {
        return ArrayHelpers::map(self::all(),'id','name');
    }

}
