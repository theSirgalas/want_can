<?php

namespace App\Entities;

use App\Entities\Task\Task;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Entities\Chatroom
 *
 * @property int $id
 * @property int $customer_id
 * @property int $executor_id
 * @property int $task_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read User $customer
 * @property-read User $executor
 * @property-read Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom query()
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereExecutorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chatroom whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Chatroom extends Model
{
    use HasFactory;

    public $guarded=['id'];

    public static function add(User $customer,User $executor):self
    {
        return Chatroom::create([
            'customer_id'=>$customer->id,
            'executor_id'=>$executor->id
        ]);
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }

    public function executor()
    {
        return $this->belongsTo(User::class,'executor_id');
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function messages()
    {
        return Message::where('chatroom_id',$this->id)->get();
    }
}
