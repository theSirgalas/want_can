<?php

namespace App\Entities\Vacancy;

use App\Entities\Category;
use App\Entities\Handbook\Handbook;
use App\Entities\User\User;
use App\Http\Requests\Vacancy\CreateRequest;
use App\Http\Requests\Vacancy\EditRequest;
use App\Http\Requests\Vacancy\UpdateRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;
use Illuminate\Support\Facades\Auth;

/**
 * App\Entities\Vacancy\Vacancy
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property int $work_handbook_id
 * @property string $salary
 * @property int $payments_handbook_id
 * @property string $where_work
 * @property string $address_work
 * @property string $requirements
 * @property string $obligation
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereAddressWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereObligation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy wherePaymentsHandbookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereRequirements($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereWhereWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereWorkHandbookId($value)
 * @mixin \Eloquent
 * @property-read Category $category
 * @property-read Handbook $payments
 * @property-read Handbook $work
 * @property int $author_user_id
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereAuthorUserId($value)
 * @property bool $is_closed
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereIsClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vacancy whereIsClosed($value)
 */
class Vacancy extends Model
{
    use HasFactory;

    public $guarded=['id'];

    public static function add(CreateRequest $request):self
    {
        return self::create([
            'name'=>$request->name,
            'category_id'=>$request->category_id,
            'work_handbook_id'=>$request->work_handbook_id,
            'salary'=>$request->salary,
            'payments_handbook_id'=>$request->payments_handbook_id,
            'where_work'=>$request->where_work,
            'address_work'=>$request->address_work,
            'requirements'=>$request->requirements,
            'obligation'=>$request->obligation,
            'description'=>$request->description,
            'author_user_id'=>Auth::user()->id
        ]);
    }

    public function edit(EditRequest $request)
    {
        $this->name=$request->name;
        $this->category_id=$request->category_id;
        $this->work_handbook_id=$request->work_handbook_id;
        $this->salary=$request->salary;
        $this->payments_handbook_id=$request->payments_handbook_id;
        $this->where_work=$request->where_work;
        $this->address_work=$request->address_work;
        $this->requirements=$request->requirements;
        $this->obligation=$request->obligation;
        $this->description=$request->description;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function work()
    {
        return $this->belongsTo(Handbook::class,'work_handbook_id');
    }

    public function payments()
    {
        return $this->belongsTo(Handbook::class,'payments_handbook_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'author_user_id');
    }

    public function isCanRedact(User $user)
    {
        if($user->isAdmin()){
            return true;
        }
        if($user->id==$this->author_user_id){
            return true;
        }
        return false;
    }

    public function canReviews(User $user)
    {
        if($user->id==$this->author_user_id){
            return false;
        }
        if($this->is_closed){
            return true;
        }
        return true;
    }

    public function getCreateDateAttribute()
    {
        $created_at=Carbon::parse($this->created_at);
        $created_at->setLocale('ru');
        return (new Carbon())->diffForHumans($created_at,true,true,3);
    }
}
