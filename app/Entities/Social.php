<?php

namespace App\Entities;

use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\User\Social
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property-read \App\Entities\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Social newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Social newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Social query()
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Social whereUserId($value)
 * @mixin \Eloquent
 */
class Social extends Model
{
    protected $guarded=['id'];
    use HasFactory;
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function add(string $name,string $link):self
    {
        return Social::create([
            'name'=>$name,
            'link'=>$link,
        ]);
    }

    public function edit(string $name,string $link,int $user_id)
    {
        $this->name=$name;
        $this->link=$link;
    }


    public static function isSocial($link)
    {
        return self::where('link',$link)->first();
    }
}
