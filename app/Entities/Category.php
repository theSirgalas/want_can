<?php

namespace App\Entities;

use App\Entities\Resume\Resume;
use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Entities\Vacancy\Vacancy;
use App\Http\Requests\Category\CategoryRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Kalnoy\Nestedset\Collection;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Entities\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|Category[] $children
 * @property-read int|null $children_count
 * @property-read Category|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @property-read int|null $users_count
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category ancestorsAndSelf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category ancestorsOf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category applyNestedSetScope(?string $table = null)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category countErrors()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category d()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category defaultOrder(string $dir = 'asc')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category descendantsAndSelf($id, array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category descendantsOf($id, array $columns = [], $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category fixSubtree($root)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category fixTree($root = null)
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category getNodeData($id, $required = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category getPlainNodeData($id, $required = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category getTotalErrors()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category hasChildren()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category hasParent()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category isBroken()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category leaves(array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category makeGap(int $cut, int $height)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category moveNode($key, $position)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category newModelQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category newQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category orWhereAncestorOf(bool $id, bool $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category orWhereDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category orWhereNodeBetween($values)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category orWhereNotDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category query()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category rebuildSubtree($root, array $data, $delete = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category rebuildTree(array $data, $delete = false, $root = null)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category reversed()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category root(array $columns = [])
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereAncestorOf($id, $andSelf = false, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereAncestorOrSelf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereCreatedAt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereDescendantOf($id, $boolean = 'and', $not = false, $andSelf = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereDescendantOrSelf(string $id, string $boolean = 'and', string $not = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereId($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereIsAfter($id, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereIsBefore($id, $boolean = 'and')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereIsLeaf()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereIsRoot()
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereLft($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereName($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereNodeBetween($values, $boolean = 'and', $not = false)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereNotDescendantOf($id)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereParentId($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereRgt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereSlug($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereUpdatedAt($value)
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category withDepth(string $as = 'depth')
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category withoutRoot()
 * @mixin \Eloquent
 * @property int|null $icon_file_id
 * @property string|null $subs_id
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereIconFileId($value)
 * @property-read \App\Entities\File|null $icon
 * @property int|null $file_id
 * @method static \Kalnoy\Nestedset\QueryBuilder|Category whereFileId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $rootTask
 * @property-read int|null $root_task_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Task[] $subTask
 * @property-read int|null $sub_task_count
 * @property-read mixed $image_path
 * @property-read \Illuminate\Database\Eloquent\Collection|Resume[] $resumes
 * @property-read int|null $resumes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Vacancy[] $vacancy
 * @property-read int|null $vacancy_count
 * @property int $turn
 * @property string $description
 */
class Category extends Model
{
    use NodeTrait;
    use HasFactory;

    const NAME_START=1;

    public static function add(CategoryRequest $request):self
    {
        $category=new static();
        $category->fill([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'parent_id' => $request->parent,
            'turn'=>$request->turn,
            'description'=>$request->description
        ]);
        return $category;
    }

    public function edit(CategoryRequest $request):void
    {
        $this->name = $request->name;
        $this->slug = Str::slug($request->name);
        $this->parent_id = $request->parent;
        $this->turn=$request->turn;
        $this->description=$request->description;
    }

    protected $guarded=['id'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function icon()
    {
        return $this->belongsTo(File::class,'file_id');
    }

    public function rootTask()
    {
        return $this->hasMany(Task::class,'category_id');
    }

    public function subTask()
    {
        return $this->hasMany(Task::class,'id','sub_catgory_id');
    }

    public function getPath(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('slug')->toArray(), [$this->slug]));
    }

    public function resumes()
    {
        return  $this->hasMany(Resume::class);
    }

    public function vacancy()
    {
        return $this->hasMany(Vacancy::class);
    }

    public static function categoryTrees(array $categories=[])
    {
        $returnArray=[];
        if(empty($categories)){
            $categories=self::get()->toTree();
        }
        foreach ($categories as $category){
            if(!$category->isRoot()){
                continue;
            }
            $returnArray[]=[
                'id'=>$category->id,
                'name'=>$category->name,
                'icon'=>is_object($category->icon)?$category->icon->path:'/svg/cat-holder.svg',
                'sub_category'=>$category->subCategoryArray($category)
            ];
        }
        return $returnArray;
    }

    public function subCategoryArray(Category $category):array
    {
        $returnArray=[];
        foreach ($category->children as $category){
            if($category->isRoot()){
                continue;
            }
            $returnArray[]=[
                'id'=>$category->id,
                'name'=>$category->name,
                'icon'=>is_object($category->icon)?$category->icon->path:'/svg/cat-holder.svg',
                'sub_category'=>$this->subCategoryArray($category)
            ];

        }
        usort($returnArray, function($a,$b){
            return strcmp($a["name"], $b["name"]);
        });
        return $returnArray;
    }

    public function getImagePathAttribute()
    {
        return is_object($this->icon)?$this->icon->path:'/svg/cat-holder.svg';
    }

    private function dateStart($request)
    {
        if($request->now){
            return 'now()';
        }
        return $request->data_start;
    }

    public static function categoryForHome()
    {
        $parentCategories=Category::whereNull('parent_id')->orderBy('turn')->get();
        $returnArray=[];
        foreach ($parentCategories as $parentCategory ){
            /** @var $parentCategory Category */
            $returnArray[]=[
                'category'=>$parentCategory,
                'icon'=>is_object($parentCategory->icon)?$parentCategory->icon->path:'/svg/cat-holder.svg',
                'count'=> $parentCategory->rootTask()->count()
            ];
        }
        return $returnArray;
    }

    public function getSubsIdAttribute()
    {
        if(empty($this->children)){
            return [];
        }
        $returnArray=[];
        foreach ($this->children as $sub){
            $returnArray[]=$sub->id;
        }
        return $returnArray;
    }
}
