<?php

namespace App\Entities;

use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Entities\Organization\Organization;
/**
 * App\Entities\File
 *
 * @property int $id
 * @property string $url
 * @property string $extension
 * @property string $name
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File query()
 * @method static \Illuminate\Database\Eloquent\Builder|File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUrl($value)
 * @mixin \Eloquent
 * @property-read string $path
 * @property-read \App\Entities\Category|null $category
 * @property-read Organization|null $organization
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @property-read int|null $users_count
 */
class File extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    const UPLOAD_DIR = 'public';


    public static function add(UploadedFile $file,string $url): self
    {
        $fileFind=File::where(['name'=>$file->getClientOriginalName()])->first();
        if(is_object($fileFind)){
            return $fileFind;
        }
        return static::create([
            'url' => $url,
            'extension' => $file->getClientOriginalExtension(),
            'name' => $file->hashName(),
            'slug' => $file->getClientOriginalName()
        ]);

    }

    public function organization()
    {
        return $this->hasOne(Organization::class);
    }

    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class,'user_files');//->using(OrganizationFile::class);
    }



    public function path()
    {
        return storage_path(self::UPLOAD_DIR).$this->url;
    }

    public function getPathAttribute():string
    {
        if(empty($this->extension)){
            return $this->url;
        }
        return Storage::url($this->url.DIRECTORY_SEPARATOR.$this->name);
    }


}
