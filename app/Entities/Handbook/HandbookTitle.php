<?php

namespace App\Entities\Handbook;

use App\Http\Requests\Handbook\HandbookTitleRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Entities\Handbook\HandbookTitle
 *
 * @property int $id
 * @property string $title
 * @property string $key
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle query()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookTitle whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Handbook\Handbook[] $handbook
 * @property-read int|null $handbook_count
 */
class HandbookTitle extends Model
{
    use HasFactory;
    protected $fillable=['title','key'];

    public function handbook()
    {
        return $this->hasMany(Handbook::class,'handbook_title_id');
    }

    public static function add(HandbookTitleRequest $request):self
    {
       return static::create([
           'title'=>$request->title,
           'key'=>$request->key??Str::slug($request->title),
       ]);
    }

    public function edit(HandbookTitleRequest $request)
    {
        $this->title=$request->title;
        $this->key=$request->title;
    }
}
