<?php

namespace App\Entities\Handbook;

use App\Http\Requests\Handbook\HandbookRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Handbook
 *
 * @property int $id
 * @property string $title
 * @property string $code
 * @property string $short_description
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook query()
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereTitle($value)
 * @mixin \Eloquent
 * @property int $handbook_title_id
 * @method static \Illuminate\Database\Eloquent\Builder|Handbook whereHandbookTitleId($value)
 * @property-read \App\Entities\Handbook\HandbookTitle $handbookTitle
 */
class Handbook extends Model
{
    use HasFactory;

    protected $guarded=['id'];

    public $timestamps = false;

    public function handbookTitle()
    {
        return $this->belongsTo(HandbookTitle::class,'handbook_title_id');
    }

    public static function add(HandbookRequest $request)
    {
        return static::create([
            'title'=>$request->title,
            'code'=>$request->code,
            'short_description'=>$request->short_description,
            'description'=>$request->description,
            'handbook_title_id'=>$request->handbook_title_id
        ]);
    }

    public function edit(HandbookRequest $request)
    {
        $this->title=$request->title;
        $this->code=$request->code;
        $this->short_description=$request->short_description;
        $this->description=$request->description;
        $this->handbook_title_id=$request->handbook_title_id;
    }
}
