<?php


namespace App\Services\Frequency;


use App\Entities\Frequency\FrequencyTitle;
use App\Http\Requests\Frequency\FrequencyTitleRequest;
use App\Repositories\Interfaces\FrequencyTitleRepositoryInterface;

class FrequencyTitleService
{
    public $repository;

    public function __construct(FrequencyTitleRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function create(FrequencyTitleRequest $request):FrequencyTitle
    {
        return FrequencyTitle::add($request->title,$request->description);
    }

    public function edit(FrequencyTitleRequest $request,FrequencyTitle $frequencyTitle):FrequencyTitle
    {
        $frequencyTitle->edit($request->title,$request->description);
        $this->repository->save($frequencyTitle);
        return $frequencyTitle;
    }
}
