<?php


namespace App\Services\Frequency;


use App\Entities\Frequency\FrequencySection;
use App\Http\Requests\Frequency\FrequencySectionRequest;
use App\Repositories\Interfaces\FrequencySectionRepositoryInterface;

class FrequencySectionService
{
    public $repository;

    public function __construct(FrequencySectionRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function create(FrequencySectionRequest $request):FrequencySection
    {
        return FrequencySection::add(
            $request->title,
            $request->description,
            $request->frequency_titles_id,
            $request->short_description);
    }

    public function edit(FrequencySectionRequest $request,FrequencySection $frequencySection):FrequencySection
    {
        $frequencySection->edit(
            $request->title,
            $request->description,
            $request->frequency_titles_id,
            $request->short_description
        );
        $this->repository->save($frequencySection);
        return $frequencySection;
    }
}
