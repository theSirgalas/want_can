<?php


namespace App\Services\Resume;


use App\Entities\Resume\Resume;
use App\Entities\Resume\ResumeEducation;
use App\Entities\Resume\WorkExperience;
use App\Helpers\ArrayHelpers;
use App\Http\Requests\Resume\CreateRequest;
use App\Http\Requests\Resume\EditRequest;
use App\Repositories\Interfaces\Resume\ResumeRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ResumeService
{
    public $repository;

    public function __construct(ResumeRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function create(CreateRequest $request)
    {
        $resume=Resume::add($request);
        if(!ArrayHelpers::multiplyArrayEmptyFields($request->experiences)){
            foreach ($request->experiences as $experience) {
                $experience=WorkExperience::add($experience);
                $resume->experiences()->save($experience);
            }
        }
        if (!ArrayHelpers::multiplyArrayEmptyFields($request->educations)){
            foreach ($request->educations as $education){
                $education=ResumeEducation::add($education);
                $resume->educations()->save($education);
            }
        }

        return $resume;
    }

    public function edit(EditRequest $request,Resume $resume)
    {
        return DB::transaction(function () use ($request,$resume) {
            $resume->edit($request);
            $this->repository->save($resume);

            foreach ($request->experiences as $key => $experienceRequest) {
                if($resume->experiences->isEmpty()){
                    $experience = WorkExperience::add($experienceRequest);
                    $resume->experiences()->save($experience);
                    continue;
                }
                foreach ($resume->experiences as $experience) {
                    if ($experience->isExperience($key, $resume->id)) {
                        $experience->edit($experienceRequest);
                        $experience->save();
                        continue;
                    }
                        $experience = WorkExperience::add($experienceRequest);
                        $resume->experiences()->save($experience);

                }
            }
            foreach ($request->educations as $key => $educationRequest) {
                if($resume->educations->isEmpty()){
                    $education = ResumeEducation::add($educationRequest);
                    $resume->educations()->save($education);
                    continue;
                }
                foreach ($resume->educations as $education) {
                    if ($education->isEducation($key, $resume->id)) {
                        $education->edit($educationRequest);
                        $education->save();
                        continue;
                    }
                        $education = ResumeEducation::add($educationRequest);
                        $resume->educations()->save($education);
                }
            }
        });
    }

    public function delete(Resume $resume)
    {
        if(!$resume->isCanRedact(\Auth::user())){
            throw new \RuntimeException('Вы неимеете права на эти действия',403);
        }
        $resume->is_closed=true;
        $this->repository->save($resume);
    }

}
