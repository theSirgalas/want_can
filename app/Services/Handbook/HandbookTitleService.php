<?php


namespace App\Services\Handbook;


use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use App\Http\Requests\Handbook\HandbookTitleRequest;
use App\Repositories\Handbook\HandbookTitleRepository;

class HandbookTitleService
{
    public $repository;

    public function __construct(HandbookTitleRepository $repository)
    {
        $this->repository=$repository;
    }

    public function create(HandbookTitleRequest $request):HandbookTitle
    {
        return $handbookTitle=HandbookTitle::add($request);
    }

    public function edit(HandbookTitleRequest $request, HandbookTitle $handbookTitle)
    {
        $handbookTitle->edit($request);
        return $this->repository->save($handbookTitle);
    }

    public function dropDownHandbookTitle():array
    {
        $returnArray=[];
        foreach ($this->repository->all() as $handbook){
            /**
             * @var $handbook Handbook
             */
            $returnArray[$handbook->id]=$handbook->title;
        }
        return $returnArray;
    }
}
