<?php


namespace App\Services\Handbook;


use App\Entities\Handbook\Handbook;
use App\Http\Requests\Handbook\HandbookRequest;
use App\Repositories\Handbook\HandbookRepository;

class HandbookService
{
    public $repository;

    public function __construct(HandbookRepository $repository)
    {
        $this->repository=$repository;
    }

    public function dropDownHandbook($key):array
    {
        $returnArray=[];
        foreach ($this->repository->getHandbookByTitleKey($key) as $handbook){
            /**
             * @var $handbook Handbook
             */
            $returnArray[$handbook->id]=$handbook->title;
        }
        return $returnArray;
    }

    public function create(HandbookRequest $request):Handbook
    {
        return Handbook::add($request);
    }

    public function edit(HandbookRequest $request, Handbook $handbook):Handbook
    {
        $handbook->edit($request);
        return $this->repository->save($handbook);
    }


}
