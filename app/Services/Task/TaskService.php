<?php


namespace App\Services\Task;


use App\Entities\File;
use App\Entities\Task\Application;
use App\Entities\Task\Task;
use App\Http\Requests\Task\CreateRequest;
use App\Http\Requests\Task\CreateUserRequest;
use App\Http\Requests\Task\EditRequest;
use App\Repositories\Interfaces\Task\TaskRepositoryInterface;
use http\Exception\RuntimeException;
use Illuminate\Support\Facades\Auth;
use  Illuminate\Support\Facades\DB;

class TaskService
{
    public $repository;

    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function create(CreateRequest $request):Task
    {
        $task= Task::add($request);
        if($request->has('file')){
            $this->addFile($task,$request->file('file'));
        }
        return $task;
    }

    public function createUser(CreateUserRequest $request):Task
    {
        return DB::transaction(function () use($request) {
            $task = Task::addUser($request);
            if ($request->has('file')) {
                $this->addFile($task, $request->file('file'));
            }
            $appliction=Application::create([
                'price'=>$request->price,
                'comment'=>'создано заказчиком',
                'task_id'=>$task->id,
                'user_id'=>$request->executor_id,
            ]);
            $task->application_id=$appliction->id;
             return $this->repository->save($task);
        });
    }

    public function edit(EditRequest $request,Task $task):Task
    {
        return DB::transaction(function () use($request, $task) {
            $task->edit($request);
            if ($request->has('file')) {
                $this->addFile($task, $request->file('file'));
            }
            return $this->repository->save($task);
        });
    }

    private function addFile(Task $task, $icon)
    {
        $file=File::add($icon,'/task/'.$task->id);
        $task->file()->associate($file);
        $task->save();
        $icon->store('/task/' . $task->id,'public');
    }

    public function delete(Task $task)
    {
        if($task->user_id!=Auth::user()->id){
            throw new \RuntimeException('Вы не имеете права удалять эту задачу',500);
        }
        $task->is_finished=true;
        $this->repository->save($task);
    }

    public function addGeneral( Application $application,Task $task)
    {
        if(Auth::user()->id!=$task->user_id){
            throw new \RuntimeException('Вы не имеете право на это действие');
        }
        $task->application_id=$application->id;
        $task->executor_id=$application->user_id;
        $this->repository->save($task);
    }

    public function applicationViews(Task $task){
        $task->applications()->update(['viewers'=>true]);
    }



}
