<?php


namespace App\Services\Task;


use App\Entities\Task\Application;
use App\Entities\Task\Reviews;
use App\Http\Requests\Reviews\AddRequest;
use App\Repositories\Interfaces\Task\ReviewsRepositoryInterface;
use App\Repositories\Interfaces\Task\TaskRepositoryInterface;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ReviewsService
{
    public $repository;
    public $userRepository;
    public $taskRepository;

    public function __construct(ReviewsRepositoryInterface $repository,UserRepositoryInterface $userRepository,TaskRepositoryInterface $taskRepository)
    {
        $this->repository=$repository;
        $this->userRepository=$userRepository;
        $this->taskRepository=$taskRepository;
    }

    public function create(AddRequest $request,Application $application)
    {
        return DB::transaction(function () use($request, $application) {
            $reviews=Reviews::add($request,$application);
            $application->user->reviews_count++;
            $application->user->reviews_score=$application->user->reviews_score+$reviews->score;
            $this->userRepository->save($application->user);
            $application->task->is_finished=true;
            $this->taskRepository->save($application->task);
            return $reviews;
        });
    }
}
