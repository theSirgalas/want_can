<?php


namespace App\Services\Task;


use App\Entities\Task\Application;
use App\Entities\Task\Task;
use App\Events\NotificationEvent;
use App\Http\Requests\Application\CreateRequest;
use App\Repositories\Interfaces\Task\ApplicationRepositoryInterface;
use App\Repositories\Interfaces\Task\TaskRepositoryInterface;
use App\Repositories\Task\ApplicationRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApplicationService
{
    public $repository;
    public $taskRepository;

    public function __construct(ApplicationRepositoryInterface $repository,TaskRepositoryInterface $taskRepository)
    {
        $this->repository=$repository;
        $this->taskRepository=$taskRepository;
    }

    public function create(CreateRequest $request,Task $task):Application
    {
        $applications=Application::where('user_id',Auth::user()->id)->where('task_id',$task->id)->exists();
        if(!empty($applications)){
            throw new \Exception('вы уже подали заявку на это задание');
        }
        $user=Auth::user();
        return Application::add($request,$task);
    }

    public function refused(Application $application,Task $task)
    {
        if(!$task->executor_id==Auth::user()->id){
            throw new \DomainException('Вы не имеете права это делать',403);
        }
        event(new NotificationEvent(Application::class,$task->id,$task->application->user,Application::TYPE_REFUSE));
        return DB::transaction(function () use ($application,$task) {
            $task->executor_id = null;
            $task->application_id = null;
            $this->taskRepository->save($task);
            $application->is_refuse = true;
            $this->repository->save($application);
        });
    }

    public function closed(Application $application,Task $task)
    {
        if(!$task->executor_id==Auth::user()->id){
            throw new \DomainException('Вы не имеете права это делать',403);
        }
        event(new NotificationEvent(Application::class,$task->id,$task->application->user,Application::TYPE_CLOSED));
        $application->is_close = true;
        $this->repository->save($application);
    }
}
