<?php


namespace App\Services;


use App\Entities\Chatroom;
use App\Entities\User\User;
use App\Repositories\Interfaces\ChatroomRepositoryInterface;

class ChatroomService
{
    public $repository;

    public function __construct(ChatroomRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function getChats(User $user,int $chatroomId)
    {
        $chat=$this->repository->one($chatroomId);
        if($chat->customer_id==$user->id||$chat->executor_id==$user->id){
            $returnArray=[
                'opponent_link'=>route('user',$chat->customer_id==$user->id?$chat->executor->id:$chat->customer->id),
                'opponent'=>$chat->customer_id==$user->id?$chat->executor->short_name:$chat->customer->short_name,
                'chatroom_id'=>$chat->id,
                'status'=>'online',
                'messages'=>[]
            ];
            foreach ($chat->messages() as $message){
                array_push($returnArray['messages'],[
                    'user_id'=>$message->user_id,
                    'text'=>$message->message
                ]);
                if(!$message->view){
                    $message->view=true;
                    $message->save();
                }
            }

            return $returnArray;
        }else{
            throw new \RuntimeException('вы не имеете права просматривать этот чат',403);
        }


    }

    public function getRooms(User $user)
    {
        $returnArray=[];
        foreach($user->chatroomsCustomer as $chatroomCustomer){
            $returnArray[$chatroomCustomer->id]=[
                'userId'=>$chatroomCustomer->executor->id,
                'userName'=>$chatroomCustomer->executor->short_name,
                'userAvatar'=>$chatroomCustomer->executor->avatars,
                'chatroom_id'=>$chatroomCustomer->id,
                'customer'=>true
            ];
        }
        foreach($user->chatroomsExecutor as $chatroomExecutor){
            $returnArray[$chatroomExecutor->id]=['userId'=>$chatroomExecutor->customer->id,'userName'=>$chatroomExecutor->customer->short_name,'userAvatar'=>$chatroomExecutor->customer->avatars,'chatroom_id'=>$chatroomExecutor->id,'customer'=>false];
        }
        return $returnArray;
    }

    public function first(User $user,User $opponent=null)
    {

        if(!$opponent){
            return array_key_first($this->getRooms($user));
        }

        if($chatRooms=$this->repository->findByUsers($user,$opponent)){
            return $chatRooms->id;
        }
        $chatRooms=Chatroom::add($user,$opponent);
        return $chatRooms->id;
    }

}
