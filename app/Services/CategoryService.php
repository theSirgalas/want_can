<?php


namespace App\Services;


use App\Entities\Category;
use App\Entities\File;
use App\Http\Requests\Category\CategoryRequest;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\FileRepositoryInterface;
use App\Repositories\Interfaces\User\UserRepositoryInterface;

class CategoryService
{
    public $repository;
    public $fileRepository;

    public function __construct(CategoryRepositoryInterface $repository,FileRepositoryInterface $fileRepository)
    {
        $this->repository=$repository;
        $this->fileRepository=$fileRepository;
    }

    public function parentSelect():array
    {
        $returnArray=[];
        $parents = Category::defaultOrder()->withDepth()->get();
        foreach ($parents as $parent){
            $returnArray[$parent->id]=$parent->name;
        }
        return $returnArray;
    }

    public function create(CategoryRequest $request):Category
    {
        $category=Category::add($request);
        if ($icon = $request->file('icon')) {
            $this->addFile($category,$icon);
        }
        return $this->repository->save($category);
    }

    public function edit(CategoryRequest $request, Category $category)
    {
        $category->edit($request);
        if ($icon = $request->file('icon')) {
            $this->addFile($category,$icon);
        }
        return $this->repository->save($category);
    }

    public function iconPreview(Category $category)
    {
        $arrayPrev=['theme'=>"fas"];
        if(is_object($category->icon)){
            $arrayPrev['initialPreview']=["<img src='".$category->icon->path."'>"];
            $arrayPrev["initialPreviewAsData"]=false;
            $arrayPrev["initialPreviewFileType"]='image';
            $arrayPrev['initialPreviewConfig']=[['type'=>'image','caption' => $category->icon->name,'url'=>$category->icon->url,"width"=> '120px', ]];
        }
        return  json_encode($arrayPrev);
    }

    private function addFile(Category $category,$icon)
    {
        $file=File::add($icon,'/category/'.$category->slug);
        $category->icon()->associate($file);
        $icon->store('/category/' . $category->slug,'public');
    }

    public function dropDownParent():array
    {
        $returnArray=[];
        foreach (Category::whereIsRoot()->get() as $category){
            /**
             * @var $category Category
             */
            $returnArray[]=[
                'id'=>$category->id,
                'name'=>$category->name,
                'icon'=>is_object($category->icon)?$category->icon->path:'/svg/cat-holder.svg',
                ];
        }
        return $returnArray;
    }

    public function getSubCategory(int $id)
    {
        $category=$this->repository->one($id);
        return $category->subCategoryArray($category);
    }



}
