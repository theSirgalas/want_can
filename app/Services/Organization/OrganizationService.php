<?php


namespace App\Services\Organization;


use App\Entities\Category;
use App\Entities\File;
use App\Entities\Organization\OrganizationFile;
use App\Entities\Organization\Organization;
use App\Entities\Social;
use App\Events\OrganizationEdit;
use App\Events\UserEdit;
use App\Helpers\ArrayHelpers;
use App\Http\Requests\Organization\AdminEditRequest;
use App\Http\Requests\Organization\CabinetEditRequest;
use App\Http\Requests\Organization\CreateRequest;
use App\Http\Requests\Organization\LogoRequest;
use App\Http\Requests\Organization\RegisterRequest;
use App\Http\Requests\User\AvatarRequest;
use App\Repositories\Interfaces\FileRepositoryInterface;
use App\Repositories\Organization\OrganizationRepository;
use App\Repositories\User\UserRepository;
use App\Services\User\UserService;
use App\Entities\User\User;
use Illuminate\Support\Facades\DB;

class OrganizationService
{
    public $userRepository;
    public $organisationRepository;
    public $fileRepository;

    public function __construct(UserRepository $userRepository, OrganizationRepository $organisationRepository,FileRepositoryInterface $fileRepository)
    {
        $this->userRepository=$userRepository;
        $this->organisationRepository=$organisationRepository;
        $this->fileRepository=$fileRepository;
    }


    public function register(RegisterRequest $request):Organization
    {
        return DB::transaction(function () use($request){
            $user = User::register($request->email, $request->password, $request->phone, $request->region_id);
            $userEntity = $this->userRepository->save($user);
            $organization = Organization::register($request->all(), $userEntity->id);
            $organization = $this->organisationRepository->save($organization);
            $files = $request->file('files');
            if ($request->hasFile('files')) {
                $this->addFile($files, $organization);
            }
            return $organization;
        });

    }
    public function create(CreateRequest $request,User $user):Organization
    {
        return DB::transaction(function () use($request,$user){
            $organization = Organization::register($request->all(), $user->id);
            $organization = $this->organisationRepository->save($organization);
            $this->attachCategory($organization,$request->category);
            $this->createAttacheSocial($organization,$request->social);
            return $organization;
        });

    }

    public function editAdmin(AdminEditRequest $request, Organization $organization)
    {
        $organization->editAdmin($request);
        return $this->organisationRepository->save($organization);
    }

    public function editProfile(CabinetEditRequest $request,Organization $organization)
    {
        $organization->edit($request);
        $organization = $this->organisationRepository->save($organization);
        $this->attachCategory($organization,$request->category);
        $this->createAttacheSocial($organization,$request->social);
    }

    public function addFile($files,Organization $organization){
        foreach ($files as $file){
           $fileModel=$this->fileRepository->save(File::add($file,'/organization/'.$organization->user->id));
           $organization->files()->attach($fileModel);
           $file->store('/organization/' . $organization->user->id,'public');
        }
    }

    public function dropDownParentCategory(Organization $organization):array
    {
        $returnArray=[];
        foreach ($organization->categories as $category){
            if($category->isRoot()){
                $returnArray[]=[
                    'id'=>$category->id,
                    'name'=>$category->name,
                    'icon'=>is_object($category->icon)?$category->icon->path:null,
                    'sub_category'=>$category->subCategoryArray($category)
                ];
            }
        }
        return $returnArray;
    }

    public function dropDownParent():array
    {
        $returnArray=[];
        foreach (Category::whereIsRoot()->get() as $category){
            /**
             * @var $category Category
             */
            $returnArray[]=[
                'id'=>$category->id,
                'name'=>$category->name,
                'icon'=>is_object($category->icon)?$category->icon->path:null,
            ];
        }
        return $returnArray;
    }

    public function addLogo(LogoRequest $request,Organization $organization)
    {
        DB::transaction(function () use ($request,$organization) {
            $file = $request->file('logo');
            $fileUpload = $this->fileRepository->save(File::add($file, '/organization/' . $organization->user_id . '/logo'));
            event(new OrganizationEdit($organization,$file,$fileUpload));
        });
    }

    public function deleteLogo(Organization $organization)
    {
        $this->fileRepository->remove($organization->logo->id);
    }

    public function attachCategory(Organization $organization,array $categories)
    {
        $attachCategoryId=[];
        foreach ($categories as $category)
        {
            if(empty($category['category'])){
                continue;
            }
            if(array_key_exists('subCategory',$category)){
                $subCategories=explode(',',$category['subCategory']);
                foreach ($subCategories as $subCategory){
                    if(!empty($subCategory)){
                        array_push($attachCategoryId,$subCategory);
                    }
                }
            }
            array_push($attachCategoryId,$category['category']);
        }
        $organization->categories()->sync($attachCategoryId);
    }

    public function createAttacheSocial(Organization $organization, array $socials)
    {
        $socialsId=[];
        foreach ($socials as $key=>$social){
            if(ArrayHelpers::arrayEmptyFields([$key,$social])){
                continue;
            }
            if(empty($social)){
                continue;
            }
            if($socialModel=Social::isSocial($social)){
                $socialsId[]=$socialModel->id;
            }else{
                $socialModel=Social::add($key,$social);
                $socialsId[]=$socialModel->id;
            }
        }
        $notId=array_diff($organization->socials()->pluck('id')->toArray(),$socialsId);
        if(!empty($notId)){
            $organization->socials()->whereIn('id',$notId)->delete();
        }
        $organization->socials()->sync($socialsId);

    }
}
