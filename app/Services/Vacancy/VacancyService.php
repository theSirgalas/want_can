<?php


namespace App\Services\Vacancy;


use App\Entities\Resume\Resume;
use App\Entities\Vacancy\Vacancy;
use App\Http\Requests\Vacancy\CreateRequest;
use App\Http\Requests\Vacancy\EditRequest;
use App\Repositories\Interfaces\Vacancy\VacancyRepositoryInterface;

class VacancyService
{
    public $repository;

    public function __construct(VacancyRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function create(CreateRequest $request)
    {
        return Vacancy::add($request);
    }

    public function edit(EditRequest $request,Vacancy $vacancy)
    {
        $vacancy->edit($request);
        $this->repository->save($vacancy);
    }

    public function delete(Vacancy $vacancy)
    {
        if(!$vacancy->isCanRedact(\Auth::user())){
            throw new \RuntimeException('Вы неимеете права на эти действия',403);
        }
        $vacancy->is_closed=true;
        $this->repository->save($vacancy);
    }
}
