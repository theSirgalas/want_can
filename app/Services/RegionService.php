<?php


namespace App\Services;


use App\Entities\Region;
use App\Http\Requests\Region\CreateRequest;
use App\Http\Requests\Region\UpdateRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Repositories\Interfaces\RegionRepositoryInterface;

class RegionService
{
    public $repository;

    public function __construct(RegionRepositoryInterface  $regionRepository)
    {
        $this->repository=$regionRepository;
    }

    public function dropDownRegion():array
    {
        $returnArray=[];
        foreach ($this->repository->all() as $region){
            /**
             * @var $region Region
             */
            $returnArray[$region->id]=$region->name;
        }
        return $returnArray;
    }

    public function create(CreateRequest $request):Region
    {
        return Region::add($request);
    }

    public function edit(UpdateRequest $request,Region $region)
    {
        $region->edit($request);
        $this->repository->save($region);
    }


}
