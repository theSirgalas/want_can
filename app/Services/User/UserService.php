<?php


namespace App\Services\User;


use App\Entities\Category;
use App\Entities\File;
use App\Entities\Social;
use App\Entities\User\Auto;
use App\Entities\User\CategoryUser;
use App\Entities\User\UserFile;
use App\Events\UserEdit;
use App\Helpers\ArrayHelpers;
use App\Http\Requests\User\AvatarRequest;
use App\Http\Requests\User\CabinetEditRequest;
use App\Http\Requests\User\AdminEditRequest;
use App\Http\Requests\User\FileRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\FileRepositoryInterface;
use App\Repositories\Interfaces\SocialRepositoryInterface;
use App\Repositories\SocialRepository;
use App\Repositories\User\UserRepository;
use App\Entities\User\User;
use Illuminate\Support\Arr;
use  Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;


class UserService
{
    public $repository;
    public $fileRepository;
    public $categoryRepository;
    public $socialRepository;

    public function __construct(
        UserRepository $repository,
        FileRepositoryInterface $fileRepository,
        CategoryRepositoryInterface $categoryRepository,
        SocialRepositoryInterface $socialRepository
    )
    {
        $this->repository=$repository;
        $this->fileRepository=$fileRepository;
        $this->categoryRepository=$categoryRepository;
        $this->socialRepository=$socialRepository;
    }

    public function create(RegisterRequest $request)
    {
        $user=User::register($request->email,$request->password,$request->phone,$request->region_id,$request->name);
        return $this->repository->save($user);
    }

    public function edit(int $id, AdminEditRequest $request){
        $user=$this->repository->one($id);

        $user->update(
            $request->only([
                'name',
                'email',
                'password',
                'phone',
                'region_id',
                'role',
                'about_us',
                'is_auto'
            ])
        );
    }

    public function attributeArray(User $user)
    {
        $returnArray=$user->getAttributes();
        unset($returnArray['password']);
        unset($returnArray['email_verified_at']);
        unset($returnArray['remember_token']);
        unset($returnArray['region_id']);
        $returnArray['region']=$user->region->name;
        return $returnArray;
    }

    public function dropDownUserParentCategory(User $user):array
    {
        $returnArray=[];
        foreach ($user->categories as $category){
            if($category->isRoot()){
                $returnArray[]=[
                    'id'=>$category->id,
                    'name'=>$category->name,
                    'icon'=>is_object($category->icon)?$category->icon->path:null,
                    'sub_category'=>$category->subCategoryArray($category)
                ];
            }
        }
        return $returnArray;
    }

    public function profileEdit(CabinetEditRequest $request,User $user)
    {
        return DB::transaction(function () use ($request,$user){
             $user->update($request->all());
             if(!empty($request->auto)){
                 foreach($request->auto as $oneAuto){
                     if(ArrayHelpers::arrayEmptyFields($oneAuto)){
                         continue;
                     }
                     if($auto=Auto::isAuto($user->id,$oneAuto['number'])){
                         $auto->edit($oneAuto['mark'],$oneAuto['color'],$oneAuto['number'],$oneAuto['body_type_handbook_id']);
                         $auto->save();
                     }else{
                         $autoArray[]=Auto::add($oneAuto['mark'],$oneAuto['color'],$oneAuto['number'],$oneAuto['body_type_handbook_id']);
                     }
                 }
                 if(!empty($autoArray)){
                     $user->autos()->saveMany($autoArray);
                 }
             }

            $this->attachCategory($user,$request->category);
            $this->createAttacheSocial($user,$request->social);
        });

    }

    public function addAvatar(AvatarRequest $request,$user)
    {
        DB::transaction(function () use ($request,$user) {
            $file = $request->file('avatar');
            $fileUpload = $this->fileRepository->save(File::add($file, '/user/' . $user->id . '/avatar'));
            event(new UserEdit($user,$file,$fileUpload));
        });
    }

    public function deleteAvatar($user)
    {
        $this->fileRepository->remove($user->avatar->id);
    }

    public function addFile(FileRequest $request,User $user)
    {
        DB::transaction(function () use ($request,$user){
            foreach ($request->file('files') as $file){
                $fileUpload=$this->fileRepository->save(File::add($file,'/user/'.$user->id.'/files'));
                $file->store('/user/' . $user->id.'/files','public');
                $fileUpload->users()->attach($user);
            }
        });
    }

    public function attachCategory(User $user,array $categories)
    {
        $attachCategoryId=[];
        foreach ($categories as $category)
        {
            if(empty($category['category'])){
                continue;
            }
            if(array_key_exists('subCategory',$category)){
                $subCategories=explode(',',$category['subCategory']);
                foreach ($subCategories as $subCategory){
                    if(!empty($subCategory)){
                        array_push($attachCategoryId,$subCategory);
                    }
                }
            }
            array_push($attachCategoryId,$category['category']);
        }
        $user->categories()->sync($attachCategoryId);
    }

    public function createAttacheSocial(User $user, array $socials)
    {
        $socialsId=[];
        foreach ($socials as $key=>$social){
            if(ArrayHelpers::arrayEmptyFields([$key,$social])){
                continue;
            }
            if(empty($social)){
                continue;
            }
            if($socialModel=Social::isSocial($social)){
                $socialsId[]=$socialModel->id;
            }else{
                $socialModel=Social::add($key,$social);
                $socialsId[]=$socialModel->id;
            }
        }
        $notId=array_diff($user->socials()->pluck('id')->toArray(),$socialsId);
        if(!empty($notId)){
            $user->socials()->whereIn('id',$notId)->delete();
        }
        $user->socials()->sync($socialsId);

    }



}
