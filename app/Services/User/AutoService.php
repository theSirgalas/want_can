<?php


namespace App\Services\User;


use App\Http\Requests\User\AjaxAutoDeleteRequest;
use App\Repositories\Interfaces\User\AutoRepositoryInterface;

class AutoService
{
    public $repository;

    public function __construct(AutoRepositoryInterface $repository)
    {
        $this->repository=$repository;
    }

    public function remove(AjaxAutoDeleteRequest $request)
    {
        $auto=$this->repository->one($request->car);
        if($auto->user_id!=$request->user){
            throw new \RuntimeException('Вы не имеете права удалять этот автомобиль.',500);
        }
        $auto->delete();
    }
}
