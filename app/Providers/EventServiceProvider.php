<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\UserEdit;
use App\Listener\User\ResizeListener;
use App\Events\OrganizationEdit;
use App\Events\NotificationEvent;
use App\Listener\NotificationListener;
use App\Listener\Organization\OrganizationResizelistener;
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserEdit::class=>[
            ResizeListener::class
        ],
        OrganizationEdit::class=>[
            OrganizationResizelistener::class
        ],
        NotificationEvent::class=>[
            Notificationlistener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
