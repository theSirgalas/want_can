<?php

namespace App\Providers;


use App\Entities\Handbook\Handbook;
use App\Repositories\CategoryRepository;
use App\Repositories\ChatroomRepository;
use App\Repositories\FileRepository;
use App\Repositories\FrequencySectionRepository;
use App\Repositories\FrequencyTitleRepository;
use App\Repositories\Handbook\HandbookRepository;
use App\Repositories\Handbook\HandbookTitleRepository;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\ChatroomRepositoryInterface;
use App\Repositories\Interfaces\FileRepositoryInterface;
use App\Repositories\Interfaces\FrequencySectionRepositoryInterface;
use App\Repositories\Interfaces\FrequencyTitleRepositoryInterface;
use App\Repositories\Interfaces\Handbook\HandbookRepositoryInterface;
use App\Repositories\Interfaces\Handbook\HandbookTitleRepositoryInterface;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use App\Repositories\Interfaces\Resume\ResumeRepositoryInterface;
use App\Repositories\Interfaces\SocialRepositoryInterface;
use App\Repositories\Interfaces\Task\ApplicationRepositoryInterface;
use App\Repositories\Interfaces\Task\ReviewsRepositoryInterface;
use App\Repositories\Interfaces\Task\TaskRepositoryInterface;
use App\Repositories\Interfaces\User\AutoRepositoryInterface;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use App\Repositories\Interfaces\Vacancy\VacancyRepositoryInterface;
use App\Repositories\RegionRepository;
use App\Repositories\Resume\ResumeRepository;
use App\Repositories\SocialRepository;
use App\Repositories\Task\ApplicationRepository;
use App\Repositories\Task\ReviewsRepository;
use App\Repositories\Task\TaskRepository;
use App\Repositories\User\AutoRepository;
use App\Repositories\User\UserRepository;
use App\Repositories\Vacancy\VacancyRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\Organization\OrganizationRepositoryInterface;
use App\Repositories\Organization\OrganizationRepository;

class RepositoryProviders extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            RegionRepositoryInterface::class,
            RegionRepository::class
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            OrganizationRepositoryInterface::class,
            OrganizationRepository::class
        );
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
        $this->app->bind(
            FileRepositoryInterface::class,
            FileRepository::class
        );
        $this->app->bind(
            HandbookRepositoryInterface::class,
            HandbookRepository::class
        );
        $this->app->bind(
            HandbookTitleRepositoryInterface::class,
            HandbookTitleRepository::class
        );
        $this->app->bind(
            AutoRepositoryInterface::class,
            AutoRepository::class
        );
        $this->app->bind(
            SocialRepositoryInterface::class,
            SocialRepository::class
        );
        $this->app->bind(
            TaskRepositoryInterface::class,
            TaskRepository::class
        );
        $this->app->bind(
            ApplicationRepositoryInterface::class,
            ApplicationRepository::class
        );
        $this->app->bind(
            ReviewsRepositoryInterface::class,
            ReviewsRepository::class
        );
        $this->app->bind(
            ChatroomRepositoryInterface::class,
            ChatroomRepository::class
        );
        $this->app->bind(
            ResumeRepositoryInterface::class,
            ResumeRepository::class
        );
        $this->app->bind(
            VacancyRepositoryInterface::class,
            VacancyRepository::class
        );
        $this->app->bind(
            FrequencyTitleRepositoryInterface::class,
            FrequencyTitleRepository::class
        );
        $this->app->bind(
            FrequencySectionRepositoryInterface::class,
            FrequencySectionRepository::class
        );
    }
}
