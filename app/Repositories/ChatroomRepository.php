<?php


namespace App\Repositories;


use App\Entities\Chatroom;
use App\Entities\User\User;
use App\Repositories\Interfaces\ChatroomRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ChatroomRepository implements ChatroomRepositoryInterface
{
    public function one(int $id):Chatroom
    {
        if(!$chatroom=Chatroom::find($id)){
            throw new \DomainException('не найдено',404);
        }
        return $chatroom;
    }

    public function all():Collection
    {
        return Chatroom::all();
    }

    public function save(Chatroom $chatroom):Chatroom
    {
        if(!$chatroom->save()){
            throw new \RuntimeException('не сохранилось',404);
        }
        return $chatroom;
    }

    public function remove(Chatroom $chatroom):void
    {
        $chatroom->delete();
    }

    public function findForTask(int $id, int $taskId):Chatroom
    {
        return Chatroom::where('id',$id)->where('task_id',$taskId)->first();
    }

    public function findByUsers(User $customer,User $executor):?Chatroom
    {
        if(!$chatroom=Chatroom::where([
                ['customer_id','=',$customer->id],
                ['executor_id','=',$executor->id]
            ])
            ->orWhere([
                ['customer_id','=',$executor->id],
                ['executor_id','=',$customer->id]
            ])
            ->first()
        ){
           return null;
        }
        return $chatroom;
    }
}
