<?php


namespace App\Repositories;


use App\Entities\Social;
use App\Repositories\Interfaces\SocialRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SocialRepository implements SocialRepositoryInterface
{
    public function one(string $link):Social
    {
        return Social::where('link',$link)->first();
    }

    public function all():Collection
    {
        return Social::all();
    }

}
