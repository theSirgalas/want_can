<?php


namespace App\Repositories\Interfaces\Vacancy;


use App\Entities\Vacancy\Vacancy;
use Illuminate\Database\Eloquent\Collection;

interface VacancyRepositoryInterface
{

    public function one(int $id):Vacancy;

    public function all():Collection;

    public function save(Vacancy $vacancy):Vacancy;

    public function remove(Vacancy $vacancy):void;

}
