<?php


namespace App\Repositories\Interfaces;


use App\Entities\Region;
use Illuminate\Database\Eloquent\Collection;

interface RegionRepositoryInterface
{
    public function one(int $id):Region;

    public function all():Collection;

    public function save(Region $region):Region;

    public function remove(Region $region);
}
