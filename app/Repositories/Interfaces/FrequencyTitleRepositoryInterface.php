<?php


namespace App\Repositories\Interfaces;


use App\Entities\Category;
use App\Entities\Frequency\FrequencyTitle;
use App\Entities\Region;
use Illuminate\Database\Eloquent\Collection;

interface FrequencyTitleRepositoryInterface
{
    public function one(int $id):FrequencyTitle;

    public function all():Collection;

    public function save(FrequencyTitle $frequencyTitle):FrequencyTitle;

    public function remove(FrequencyTitle $frequencyTitle):void;

}
