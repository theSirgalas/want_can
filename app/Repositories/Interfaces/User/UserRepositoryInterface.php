<?php


namespace App\Repositories\Interfaces\User;


use App\Entities\User\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    public function one(int $id):User;
    public function oneFromEmail(string $email):?User;
    public function all():Collection;
    public function save(User $user):User;
}
