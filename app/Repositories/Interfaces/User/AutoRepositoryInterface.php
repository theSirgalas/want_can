<?php


namespace App\Repositories\Interfaces\User;


use App\Entities\User\Auto;
use Illuminate\Database\Eloquent\Collection;

interface AutoRepositoryInterface
{
    public function one(int $id):Auto;

    public function all():Collection;

    public function save(Auto$user):Auto;

}
