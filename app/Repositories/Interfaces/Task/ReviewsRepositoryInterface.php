<?php


namespace App\Repositories\Interfaces\Task;


use App\Entities\Task\Reviews;

interface ReviewsRepositoryInterface
{
    public function getOneByApp($applicationId):?Reviews;
}
