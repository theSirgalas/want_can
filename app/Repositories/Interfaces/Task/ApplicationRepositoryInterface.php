<?php


namespace App\Repositories\Interfaces\Task;


use App\Entities\Task\Application;
use App\Entities\Task\Task;
use Illuminate\Database\Eloquent\Collection;

interface ApplicationRepositoryInterface
{
    public function one(int $id):Application;

    public function all():Collection;

    public function save(Application $task):Application;

    public function remove(Application $task):void;

}
