<?php


namespace App\Repositories\Interfaces\Task;

use App\Entities\Category;
use App\Entities\Task\Task;
use Illuminate\Database\Eloquent\Collection;

interface TaskRepositoryInterface
{
    public function one(int $id):Task;

    public function all():Collection;

    public function save(Task $task):Task;

    public function remove(Task $task):void;

    public function allByCategory(Category $category):Collection;

    public function oneBySlug(string $slug):Task;

}
