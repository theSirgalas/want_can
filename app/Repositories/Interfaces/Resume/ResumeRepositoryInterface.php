<?php


namespace App\Repositories\Interfaces\Resume;


use App\Entities\Resume\Resume;
use Illuminate\Database\Eloquent\Collection;

interface ResumeRepositoryInterface
{
    public function one(int $id):Resume;

    public function all():Collection;

    public function save(Resume $resume):Resume;

    public function delete(Resume $resume):void;

    public function maxId():int;

    public function experienceExist(int $id):bool;

    public function educationExist(int $id):bool;
}
