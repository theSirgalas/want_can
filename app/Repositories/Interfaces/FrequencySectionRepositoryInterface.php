<?php


namespace App\Repositories\Interfaces;


use App\Entities\Category;
use App\Entities\Frequency\FrequencySection;
use App\Entities\Region;
use Illuminate\Database\Eloquent\Collection;

interface FrequencySectionRepositoryInterface
{
    public function one(int $id):FrequencySection;

    public function all():Collection;

    public function save(FrequencySection $frequencySection):FrequencySection;

    public function remove(FrequencySection $frequencySection):void;
}
