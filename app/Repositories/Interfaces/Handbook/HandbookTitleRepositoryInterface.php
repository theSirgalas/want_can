<?php


namespace App\Repositories\Interfaces\Handbook;

use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use Illuminate\Database\Eloquent\Collection;


interface HandbookTitleRepositoryInterface
{
    public function one(int $id):HandbookTitle;

    public function all():Collection;

    public function save(HandbookTitle $handbook):HandbookTitle;

    public function remove(HandbookTitle $handbook);
}
