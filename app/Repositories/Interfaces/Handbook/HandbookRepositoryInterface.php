<?php


namespace App\Repositories\Interfaces\Handbook;

use App\Entities\Handbook\Handbook;
use Illuminate\Database\Eloquent\Collection;


interface HandbookRepositoryInterface
{
    public function one(int $id):Handbook;

    public function all():Collection;

    public function save(Handbook $handbook):Handbook;

    public function remove(Handbook $handbook);
}
