<?php

namespace App\Repositories\Interfaces\Organization;

use App\Entities\Organization\Organization;
use Illuminate\Database\Eloquent\Collection;

interface OrganizationRepositoryInterface
{
    public function one(int $id):Organization;

    public function all():Collection;

    public function save(Organization $organization):Organization;

    public function delete(Organization $organization):void;

}
