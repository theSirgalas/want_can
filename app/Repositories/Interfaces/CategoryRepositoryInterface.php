<?php


namespace App\Repositories\Interfaces;


use App\Entities\Category;
use App\Entities\Region;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepositoryInterface
{
    public function one(int $id):Category;

    public function all():Collection;

    public function save(Category $category):Category;

    public function remove(Category $category):void;

    public function oneBySlug(string $slug):?Category;

    public function allParent();

    public function getAllSubCategory($categoryId);
}
