<?php


namespace App\Repositories\Interfaces;


use App\Entities\Chatroom;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Collection;

interface ChatroomRepositoryInterface
{
    public function one(int $id):Chatroom;

    public function all():Collection;

    public function save(Chatroom $chatroom):Chatroom;

    public function remove(Chatroom $chatroom):void;

    public function findForTask(int $id, int $taskId):Chatroom;

    public function findByUsers(User $customer,User $executor):?Chatroom;
}
