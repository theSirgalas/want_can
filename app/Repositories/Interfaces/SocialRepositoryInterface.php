<?php


namespace App\Repositories\Interfaces;

use App\Entities\File;
use App\Entities\Social;
use Illuminate\Database\Eloquent\Collection;

interface SocialRepositoryInterface
{
    public function one(string $link):Social;

    public function all():Collection;

}
