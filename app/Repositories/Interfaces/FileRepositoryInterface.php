<?php


namespace App\Repositories\Interfaces;

use App\Entities\File;

interface FileRepositoryInterface
{
    public function one(int $id);

    public function all();

    public function save(File $file):File;

    public function remove(int $id);
}
