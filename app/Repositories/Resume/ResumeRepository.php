<?php


namespace App\Repositories\Resume;


use App\Entities\Resume\Resume;
use App\Entities\Resume\ResumeEducation;
use App\Entities\Resume\WorkExperience;
use App\Repositories\Interfaces\Resume\ResumeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ResumeRepository implements ResumeRepositoryInterface
{
    public function one(int $id):Resume
    {
        if(!$resume=Resume::find($id)){
            throw new \DomainException('not find',404);
        }
        return  $resume;
    }

    public function all():Collection
    {
        return Resume::all();
    }

    public function save(Resume $resume):Resume
    {
        if(!$resume->save()){
            throw new \RuntimeException('not save',500);
        }
        return $resume;
    }

    public function delete(Resume $resume):void
    {
        $resume->delete();
    }

    public function maxId():int
    {
        return Resume::max('id');
    }

    public function experienceExist(int $id):bool
    {
        return WorkExperience::where(['id'=>$id])->exists();
    }

    public function educationExist(int $id):bool
    {
        return ResumeEducation::where(['id'=>$id])->exists();
    }
}
