<?php


namespace App\Repositories;


use App\Entities\Region;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use http\Exception\RuntimeException;
use Illuminate\Database\Eloquent\Collection;

class RegionRepository implements RegionRepositoryInterface
{
    public function one(int $id):Region
    {
        if(!$region=Region::find($id)){
            throw new \DomainException('not find',404);
        }
        return $region;
    }

   public function all():Collection
   {
       return Region::all();
   }

   public function save(Region $region):Region
   {
       if(!$region->save()){
           throw new \RuntimeException('not save',500);
       }
       return $region;
   }

   public function remove(Region $region)
   {
       $region->delete();
   }
}
