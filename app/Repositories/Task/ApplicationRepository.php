<?php


namespace App\Repositories\Task;


use App\Entities\Task\Application;
use App\Repositories\Interfaces\Task\ApplicationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;

class ApplicationRepository implements ApplicationRepositoryInterface
{
    public function one(int $id):Application
    {
        if(!$application=Application::find($id)){
            throw new \DomainException('не найдено',404);
        }
        return $application;
    }

    public function all():Collection
    {
        return Application::all();
    }

    public function save(Application $application):Application
    {
        if(!$application->save()){
            throw new \RuntimeException('не сохранилось',500);
        }
        return $application;
    }

    public function remove(Application $application):void
    {
        $application->delete();
    }
}
