<?php


namespace App\Repositories\Task;


use App\Entities\Category;
use App\Entities\Task\Task;
use App\Repositories\Interfaces\Task\TaskRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class TaskRepository implements TaskRepositoryInterface
{
    public function one(int $id):Task
    {
        if(!$task=Task::find($id)){
            throw new \DomainException('задание не найдено',404);
        }
        return $task;
    }

    public function all():Collection
    {
        return Task::all();
    }

    public function save(Task $task):Task
    {
        if(!$task->save()){
            throw new \RuntimeException('изменения не сохранились',500);
        }
        return $task;
    }

    public function remove(Task $task):void
    {
        $task->delete();
    }

    public function allByCategory(Category $category):Collection
    {
        return Task::where('category_id',$category->id)->orWhere('sub_category_id',$category->id)->get();
    }

    public function oneBySlug(string $slug):Task
    {
        if(!$task=Task::where(['slug'=>$slug])->with('application')->first()){
            throw new \DomainException('задание не найдено',404);
        }
        return $task;
    }

}
