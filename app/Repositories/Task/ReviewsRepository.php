<?php


namespace App\Repositories\Task;


use App\Entities\Task\Application;
use App\Entities\Task\Reviews;
use App\Entities\Task\Task;
use App\Repositories\Interfaces\Task\ReviewsRepositoryInterface;

class ReviewsRepository implements ReviewsRepositoryInterface
{

    public function getOneByApp($applicationId):?Reviews
    {
        if(!$reviews=Reviews::where('application_id',$applicationId)){
            return null;
        }
        return $reviews;
    }

    public function getReviewsByCustomer($userId)
    {
        $applicationId=Task::where('user_id',$userId)->select('application_id');
        return Reviews::whereIn('application_id',$applicationId);
    }
}
