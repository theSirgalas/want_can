<?php


namespace App\Repositories;


use App\Entities\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{

    public function one(int $id):Category
    {
        if(!$category=Category::find($id)){
            throw new \DomainException('not find',404);
        }
        return $category;
    }

    public function oneByName(string $name):Category
    {
        if(!$category=Category::where('name',$name)->first()){
            throw new \DomainException('not find',404);
        }
        return $category;
    }

    public function all():Collection
    {
        return Category::all();
    }

    public function allParent()
    {
        return Category::whereNull('parent_id')->get();
    }

    public function getAllSubCategory($categoryId)
    {
        if($categoryId){
            return Category::where('parent_id',$categoryId)->get();
        }
        return Category::whereNotNull('parent_id')->get();
    }


    public function save(Category $category):Category
    {
        if(!$category->save()){
            throw new \RuntimeException('not save',500);
        }
        return $category;
    }

    public function remove(Category $category):void
    {
        $category->delete();
    }

    public function oneBySlug(string $slug=null):?Category
    {
        if($slug){
           return $category=Category::where('slug',$slug)->first();
        }
        return null;
    }
}
