<?php


namespace App\Repositories\Handbook;


use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use App\Repositories\Interfaces\Handbook\HandbookRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class HandbookRepository implements HandbookRepositoryInterface
{
    public function one(int $id):Handbook
    {
        if(!$handbook=Handbook::find($id)){
            throw new \DomainException('not find',404);
        }
        return $handbook;
    }

    public function all():Collection
    {
        return Handbook::all();
    }

    public function save(Handbook $handbook):Handbook
    {
        if(!$handbook->save()) {
            throw new \RuntimeException('not save',500);
        }
        return $handbook;
    }

    public function remove(Handbook $handbook)
    {
        $handbook->delete();
    }

    public function getHandbookByTitleKey(string $key)
    {
        return Handbook::whereIn('handbook_title_id',function ($query)use($key){
            $query->select('id')->from('handbook_titles')->where('key',$key);
        })->get();
    }
}
