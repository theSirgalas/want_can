<?php


namespace App\Repositories\Handbook;


use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use App\Repositories\Interfaces\Handbook\HandbookTitleRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class HandbookTitleRepository implements HandbookTitleRepositoryInterface
{
    public function one(int $id):HandbookTitle
    {
        if(!$handbookTitle=HandbookTitle::find($id)){
            throw new \DomainException('not find',404);
        }
        return $handbookTitle;
    }

    public function all():Collection
    {
        return HandbookTitle::all();
    }

    public function save(HandbookTitle $handbookTitle):HandbookTitle
    {
        if(!$handbookTitle->save()) {
            throw new \RuntimeException('not save',500);
        }
        return $handbookTitle;
    }

    public function remove(HandbookTitle $handbookTitle)
    {
        $handbookTitle->delete();
    }


}
