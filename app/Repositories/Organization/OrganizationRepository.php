<?php


namespace App\Repositories\Organization;


use App\Entities\Organization\Organization;
use App\Repositories\Interfaces\Organization\OrganizationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OrganizationRepository implements OrganizationRepositoryInterface
{
    public function one(int $id):Organization
    {
        if(!$organization=Organization::find($id))
        {
            throw new \DomainException('not find',404);
        }
        return $organization;
    }

    public function all():Collection
    {
        return Organization::all();
    }

    public function save(Organization $organization):Organization
    {
        if(!$organization->save()){
            throw new \RuntimeException('not save',500);
        }
        return $organization;
    }

    public function delete(Organization $organization): void
    {
        $organization->delete();
    }

    public function oneByUserId(int $userId):?Organization
    {
        return Organization::where(['user_id'=>$userId])->first();
    }

}
