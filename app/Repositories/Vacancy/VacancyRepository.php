<?php


namespace App\Repositories\Vacancy;


use App\Entities\Vacancy\Vacancy;
use App\Repositories\Interfaces\Vacancy\VacancyRepositoryInterface;

use Illuminate\Database\Eloquent\Collection;

class VacancyRepository implements VacancyRepositoryInterface
{

    public function one(int $id):Vacancy
    {
        if(!$vacancy=Vacancy::find($id)){
            throw new \DomainException('not find',404);
        }
        return $vacancy;
    }

    public function all():Collection
    {
        return Vacancy::all();
    }

    public function save(Vacancy $vacancy):Vacancy
    {
        if(!$vacancy->save()){
            throw new \RuntimeException('not find',500);
        }
        return $vacancy;
    }

    public function remove(Vacancy $vacancy):void
    {
        $vacancy->delete();
    }

}
