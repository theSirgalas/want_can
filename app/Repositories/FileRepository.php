<?php


namespace App\Repositories;

use App\Entities\File;
use App\Repositories\Interfaces\FileRepositoryInterface;
use Illuminate\Support\Facades\Storage;

class FileRepository implements FileRepositoryInterface
{
    public function one(int $id):File
    {
        return File::find($id);
    }

    public function all()
    {
        return File::all();
    }

    public function save(File $file):File
    {
        if(!$file->save()){
            throw new \RuntimeException('not save',500);
        }
        return $file;
    }

    public function remove(int $id)
    {
        $file=$this->one($id);
        Storage::delete('public/'.$file->url);
        $file->delete();
    }
}
