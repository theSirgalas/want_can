<?php


namespace App\Repositories;


use App\Entities\Frequency\FrequencySection;
use App\Repositories\Interfaces\FrequencySectionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FrequencySectionRepository implements FrequencySectionRepositoryInterface
{
    public function one(int $id):FrequencySection
    {
        if($frequencySection=FrequencySection::find($id)) {
            throw new \DomainException('not find',404);
        }
        return $frequencySection;
    }

    public function all():Collection
    {
        return FrequencySection::all();
    }

    public function save(FrequencySection $frequencySection):FrequencySection
    {
        if(!$frequencySection->save()){
            throw new \RuntimeException('не сохранено',500);
        }
        return $frequencySection;
    }

    public function remove(FrequencySection $frequencySection):void
    {
        $frequencySection->delete();
    }
}
