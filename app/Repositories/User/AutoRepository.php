<?php


namespace App\Repositories\User;


use App\Entities\User\Auto;
use App\Repositories\Interfaces\User\AutoRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AutoRepository implements AutoRepositoryInterface
{
    public function one(int $id):Auto
    {
        if(!$user=Auto::find($id)){
            throw new \DomainException('nor find',404);
        }
        return $user;
    }

    public function all():Collection
    {
        return Auto::all();
    }

    public function save(Auto$user):Auto
    {
        if(!$user->save()){
            throw new \RuntimeException('not save',500);
        }
        return $user;
    }
}
