<?php


namespace App\Repositories\User;


use App\Entities\User\User;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements UserRepositoryInterface
{
    public function one(int $id):User
    {
        if(!$user=User::find($id)){
            throw new \DomainException('nor find',404);
        }
        return $user;
    }
    public function oneFromEmail(string $email):?User
    {
        $user=User::where('email',$email)->first();
        if(!empty($user)){
            return $user;
        }
        return null;
    }

    public function all():Collection
    {
        return User::all();
    }

    public function save(User $user):User
    {
        if(!$user->save()){
            throw new \RuntimeException('not save',500);
        }
        return $user;
    }
}
