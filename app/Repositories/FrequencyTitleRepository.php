<?php


namespace App\Repositories;


use App\Entities\Frequency\FrequencyTitle;
use App\Repositories\Interfaces\FrequencyTitleRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FrequencyTitleRepository implements FrequencyTitleRepositoryInterface
{
    public function one(int $id):FrequencyTitle
    {
        if($frequencyTitle=FrequencyTitle::find($id)) {
            throw new \DomainException('not find',404);
        }
        return $frequencyTitle;
    }

    public function all():Collection
    {
        return FrequencyTitle::all();
    }

    public function save(FrequencyTitle $frequencyTitle):FrequencyTitle
    {
        if(!$frequencyTitle->save()){
            throw new \RuntimeException('не сохранено',500);
        }
        return $frequencyTitle;
    }

    public function remove(FrequencyTitle $frequencyTitle):void
    {
        $frequencyTitle->delete();
    }
}
