<?php

namespace App\Events;

use App\Entities\File;
use App\Entities\Organization\Organization;
use App\Entities\User\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class OrganizationEdit
 * @package App\Events
 * @property Organization $organization
 * @property $file
 * @property File $upload
 */
class OrganizationEdit
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $organization;
    public $file;
    public $upload;
    public function __construct(Organization $organization,$file,File $upload)
    {
        $this->organization=$organization;
        $this->file=$file;
        $this->upload=$upload;
    }

}
