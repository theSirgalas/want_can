<?php

namespace App\Events;

use App\Entities\File;
use App\Entities\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserEdit
 * @package App\Events
 * @property User $user
 * @property $file
 * @property File $upload
 */
class UserEdit
{
    use Dispatchable, SerializesModels;

    public $user;
    public $file;
    public $upload;
    public function __construct(User $user,$file,File $upload)
    {
        $this->user=$user;
        $this->file=$file;
        $this->upload=$upload;
    }

}
