<?php

namespace App\Events;

use App\Entities\Message;
use App\Entities\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PrivateChat implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data,User $user)
    {

        $this->data=$data;
        $this->user=$user;
        $this->dontBroadcastToCurrentUser();
    }

    public function broadcastOn()
    {
        Message::create($this->data,$this->user);
        return new PrivateChannel('room.'.$this->data['room_id']);
    }


}
