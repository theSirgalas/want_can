<?php

namespace App\Events;

use App\Entities\Notification;
use App\Entities\User\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class NotificationEvent
 * @package App\Events
 * @property string $entity
 * @property int $primary
 * @property int $type
 * @property User $user
 */
class NotificationEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $entity;
    public $primary;
    public $type;
    public $user;
    public function __construct($entity,$primary,$user,$type=Notification::TYPE_REVIEWS)
    {
        $this->entity=$entity;
        $this->primary=$primary;
        $this->type=$type;
        $this->user=$user;
    }


    public function broadcastOn()
    {
        return [];
    }
}
