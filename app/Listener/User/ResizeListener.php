<?php

namespace App\Listener\User;

use App\Entities\User\User;
use App\Events\UserEdit;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic;

class ResizeListener
{
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param  UserEdit  $event
     * @return void
     */
    public function handle(UserEdit $event)
    {
        $image=ImageManagerStatic::make($event->file->getRealPath());
        $image->resize(User::AVATAR_SIZE['height'],User::AVATAR_SIZE['height'], function($img) {
            $img->aspectRatio();
            $img->upsize();
        });
        $path=public_path('/storage/user/' . Auth::user()->id . '/avatar/');
        if(!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $image->save($path.$event->upload->name);
        $event->user->avatar_file_id = $event->upload->id;
        $event->user->save();
    }
}
