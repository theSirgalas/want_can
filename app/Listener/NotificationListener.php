<?php

namespace App\Listener;

use App\Entities\Notification;
use App\Events\NotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class NotificationListener
{

    public function __construct()
    {
        //
    }


    public function handle(NotificationEvent $event)
    {
        Notification::create($event->entity,$event->primary,$event->type,$event->user);
    }
}
