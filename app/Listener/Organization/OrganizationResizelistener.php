<?php

namespace App\Listener\Organization;

use App\Entities\User\User;
use App\Events\OrganizationEdit;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic;

class OrganizationResizelistener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrganizationEdit  $event
     * @return void
     */
    public function handle(OrganizationEdit $event)
    {
        $image=ImageManagerStatic::make($event->file->getRealPath());

        $image->resize(User::AVATAR_SIZE['height'],User::AVATAR_SIZE['height'], function($img) {
            $img->aspectRatio();
            $img->upsize();
        });
        $path=public_path('/storage/organization/' . Auth::user()->id . '/logo/');
        if(!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $image->save($path.$event->upload->name);
        $event->organization->logo_file_id = $event->upload->id;
        $event->organization->save();
    }
}
