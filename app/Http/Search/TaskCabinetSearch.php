<?php


namespace App\Http\Search;


use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Http\Requests\Task\CabinetSearchRequest;

class TaskCabinetSearch
{
    CONST IS_OPEN='is_open';
    CONST IS_WORK='is_work';
    CONST IS_FINISHED='is_finished';

        public function search(CabinetSearchRequest $request,User $user, $userType='customer')
        {
            if($userType=='customer'){
                $query=$user->tasksCreator();
            }else{
                $query=$user->tasksExecutor();
            }
            if(empty($request->status)){
                return $query->orderBy('start_date')->paginate(12);
            }
            if($request->status==self::IS_OPEN){
                $query->where(['is_finished'=>false])->whereNull('application_id');
            }

            if($request->status==self::IS_WORK){
                $query->where(['is_finished'=>false])->whereNotNull('application_id');
            }

            if($request->status==self::IS_FINISHED){
                $query->where(['is_finished'=>true]);
            }

            return $query->orderBy('start_date')->paginate(12);

        }
}
