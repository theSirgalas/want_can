<?php


namespace App\Http\Search;


use App\Entities\Resume\Resume;
use App\Http\Requests\Resume\SearchRequest;

class ResumeSearch
{
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(SearchRequest $request)
    {
        $query=Resume::where('is_closed',false);
        if(empty($request->all)&&!empty($request->categories)){
            $query->whereIn('category_id',$request->categories);
        }
        return $query->orderBy('id','desc')->paginate(12);
    }
}
