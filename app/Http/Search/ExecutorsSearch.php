<?php


namespace App\Http\Search;


use App\Entities\User\User;
use App\Http\Requests\User\ExecutorsSearchRequest;
use Illuminate\Support\Facades\DB;

class ExecutorsSearch
{
    public function search(ExecutorsSearchRequest $request)
    {
        $query= User::where('is_executor',true);
        $query->where(function ($query)use($request){
            if(!empty($request->categories)){
                $userIdInCategory=DB::table('category_user')
                    ->select('user_id')
                    ->whereIn('category_id',$request->categories);
                $query->whereIn('id',$userIdInCategory);
            }
            if(!empty($request->sub_categories))
            {
                $userIdInCategory=DB::table('category_user')
                    ->select('user_id')
                    ->whereIn('category_id',$request->sub_categories);
                $query->orWhereIn('id',$userIdInCategory,'or');
            }
        });

        return $query->orderBy('id','desc')->paginate(20);
    }
}
