<?php


namespace App\Http\Search;


use App\Entities\Task\Task;
use App\Http\Requests\Task\SearchRequest;

class TaskSearch
{
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(SearchRequest $request)
    {
        $query=Task::where('is_finished',false)->where('is_close',false);
        if(empty($request->all)){
            if(!empty($request->categories)){
                $query->whereIn('category_id',$request->categories);
            }
            if(!empty($request->sub_categories)){

                $query->whereIn('sub_category_id',$request->sub_categories);
            }
        }
        return $query->orderBy('start_date','desc')->paginate(12);
    }
}
