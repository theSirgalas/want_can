<?php


namespace App\Http\Search;


use App\Entities\Vacancy\Vacancy;
use App\Http\Requests\Vacancy\SearchRequest;

class VacancySearch
{
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(SearchRequest $request)
    {
        $query=Vacancy::where('is_closed',false);
            if(empty($request->all)&&!empty($request->categories)){
                $query->whereIn('category_id',$request->categories);
            }
        return $query->orderBy('id','desc')->paginate(12);
    }
}
