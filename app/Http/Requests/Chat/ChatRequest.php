<?php

namespace App\Http\Requests\Chat;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChatRequest
 * @package App\Http\Requests\Chat
 *
 * @property string $text
 * @property int $room_id
 * @property int $user_id
 */
class ChatRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'text'=>'required|string',
            'room_id'=>'required|integer|exists:chatrooms,id',
            'user_id'=>'required|integer|exists:users,id'
        ];
    }
}
