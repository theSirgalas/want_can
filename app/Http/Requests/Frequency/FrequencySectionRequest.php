<?php

namespace App\Http\Requests\Frequency;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FrequencySectionRequest
 * @package App\Http\Requests\Frequency
 * @property string $title
 * @property string $description
 * @property int $frequency_titles_id
 * @property int $short_description
 *
 */
class FrequencySectionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>'required|string',
            'description'=>'nullable|string',
            'short_description'=>'required|string',
            'frequency_titles_id'=>'required|integer|exists:frequency_titles,id'
        ];
    }
}
