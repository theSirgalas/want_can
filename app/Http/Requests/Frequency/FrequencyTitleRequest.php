<?php

namespace App\Http\Requests\Frequency;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FrequencyTitleRequest
 * @package App\Http\Requests\Frequency
 * @property string $title
 * @property string $description
 */
class FrequencyTitleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|string',
            'description'=>'nullable|string'
        ];
    }
}
