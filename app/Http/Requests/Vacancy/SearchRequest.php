<?php

namespace App\Http\Requests\Vacancy;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SearchRequest
 * @package App\Http\Requests\Vacancy
 * @property array $categories
 * @property integer $all
 */
class SearchRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'all'=>'nullable|integer',
            'categories.*'=>'nullable|integer',
        ];
    }
}
