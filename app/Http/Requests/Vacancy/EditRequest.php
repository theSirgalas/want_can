<?php

namespace App\Http\Requests\Vacancy;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Vacancy
 * @property string $name
 * @property int $category_id
 * @property int $work_handbook_id
 * @property string $salary
 * @property integer $payments_handbook_id
 * @property string $where_work
 * @property string $address_work
 * @property string $requirements
 * @property string $obligation
 * @property string $description
 */
class EditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string|max:512',
            'category_id'=>'required|integer|exists:categories,id',
            'work_handbook_id'=>'required|integer|exists:handbooks,id',
            'salary'=>'string|max:512',
            'payments_handbook_id'=>'required|integer|exists:handbooks,id',
            'where_work'=>'string|max:512',
            'address_work'=>'string|max:512',
            'requirements'=>'string',
            'obligation'=>'string',
            'description'=>'string'
        ];
    }
}
