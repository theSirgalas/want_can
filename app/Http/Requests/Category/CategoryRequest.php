<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CategoryRequest
 * @package App\Http\Requests\Category
 *
 * @property string name
 * @property string $parent
 * @property integer $turn
 * @property string $description
 */
class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'parent' => 'nullable|integer|exists:categories,id',
            'icon'=>'image|mimes:jpg,jpeg,png,svg,gif',
            'turn'=>'nullable|integer',
            'description'=>'nullable|string'
        ];
    }
}
