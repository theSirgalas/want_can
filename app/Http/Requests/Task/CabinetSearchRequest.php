<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CabinetSearchRequest
 * @package App\Http\Requests\Task
 * @property string $status
 */
class CabinetSearchRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status'=>'nullable|string',
        ];
    }
}
