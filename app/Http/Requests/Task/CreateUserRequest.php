<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Task
 * @property string $title
 * @property string $description
 * @property string $from
 * @property string $to
 * @property string|null $start_date
 * @property string|null $start_time
 * @property string|null $finish_date
 * @property string|null $finish_time
 * @property bool $now
 * @property float $price
 * @property bool $is_need_documents
 * @property bool $is_safe_payment
 * @property bool $is_finished
 * @property int $payment_handbook_id
 * @property int $category_id
 * @property int $sub_category_id
 * @property int $place_service
 * @property int $type_price
 * @property int $executor_id
 */
class CreateUserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'=>'required|string|max:512',
            'description'=>'nullable|string',
            'from'=>'nullable|string|max:1024',
            'to'=>'nullable|string|max:1024',
            'start_date'=>'nullable|string',
            'start_time'=>'nullable|string',
            'finish_date'=>'nullable|string',
            'finish_time'=>'nullable|string',
            'now'=>'nullable|bool',
            'price'=>'required|integer',
            'is_need_documents'=>'nullable|bool',
            'is_safe_payment'=>'nullable|bool',
            'payment_handbook_id'=>'required|exists:handbooks,id',
            'category_id'=>'required|exists:categories,id',
            'sub_category_id'=>'nullable|exists:categories,id',
            'place_service'=>'required|integer|max:1|min:0',
            'file'=>'nullable|file|mimes:jpg,jpeg,png,pdf,doc,docx',
            'type_price'=>'required|integer',
            'executor_id'=>'required|exists:users,id'
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'Поле Название является обязательным',
            'price.required'=>'Поле Цена является обязательным',
            'payment_handbook_id.required'=>'Поле Оплата является обязательным',
            'category_id.required'=>'Поле Категория является обязательным',

            'place_service.required'=>'Поле Место оказаная услуг является обязательным',
            'type_price.required'=>'Поле тип оплаты оказаная услуг является обязательным',
        ];
    }
}
