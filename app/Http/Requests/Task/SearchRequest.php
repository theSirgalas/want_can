<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SearchRequest
 * @package App\Http\Requests\Task
 * @property int $categories
 * @property int $sub_categories
 * @property int $all
 */
class SearchRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'all'=>'nullable|integer',
            'categories.*'=>'nullable|integer',
            'sub_categories.*'=>'nullable|integer'
        ];
    }
}
