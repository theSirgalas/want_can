<?php

namespace App\Http\Requests\Resume;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Resume
 * @property string $post_name
 * @property string $category_id
 * @property int $work_handbook_id
 * @property string $experience
 * @property string $education_handbook_id
 * @property string $sex
 * @property string $age
 * @property int $trips
 * @property boolean $moving
 * @property boolean $boolean
 * @property string $citizenship
 * @property string $post
 * @property string $month_start
 * @property string $years_start
 * @property string $month_end
 * @property string $years_end
 * @property string $obligation
 * @property array $experiences
 * @property array $educations
 * @property string $about_us
 * @property string $salary
 *
 */
class EditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'post_name'=>'required|string|max:512',
            'category_id'=>'required|integer|exists:categories,id',
            'work_handbook_id'=>'required|integer|exists:handbooks,id',
            'experience'=>'string|max:512',
            'education_handbook_id'=>'required|integer|exists:handbooks,id',
            'sex'=>'integer',
            'age'=>'string|max:100',
            'trips'=>'required|integer',
            'moving'=>'required|boolean',
            'citizenship'=>'string',
            'about_us'=>'nullable|string',
            'salary'=>'string|max:150',
            'experience.*.company_name'=>'nullable|string|max:512',
            'experience.*.post'=>'nullable|string|max:512',
            'experience.*.month_start'=>'nullable|string|max:100',
            'experience.*.years_start'=>'nullable|string|max:10',
            'experience.*.month_end'=>'nullable|string|max:100',
            'experience.*.years_end'=>'nullable|string|max:10',
            'experience.*.obligation'=>'nullable|string',
            'education.*.name'=>'nullable|string',
            'education.*.specialty'=>'nullable|string',
            'education.*.years_start'=>'nullable|string|max:10',
            'education.*.years_end'=>'nullable|string|max:10'
        ];
    }
}
