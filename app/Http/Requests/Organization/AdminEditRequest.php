<?php

namespace App\Http\Requests\Organization;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EditRequest
 * @package App\Http\Requests\Organization
 *
 * @property string $name
 * @property string $inn
 * @property string $email
 * @property string $password
 * @property string $region_id
 * @property string $legal_address
 * @property string $fact_address
 * @property string $phone
 * @property string $about_us
 * @property int $status
 */
class AdminEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moderation_status'=>'integer'
        ];
    }
}
