<?php

namespace App\Http\Requests\Organization;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class OrganizationRequest
 * @package App\Http\Requests\Organization
 * @property string $name
 * @property string $inn
 * @property string $email
 * @property string $password
 * @property string $region_id
 * @property string $legal_address
 * @property string $fact_address
 * @property string $phone
 * @property string $about_us
 * @property array $category
 * @property array $social
 */
class RegisterRequest extends FormRequest
{

    public function authorize():bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:255',
            'inn'=>'required|string|max:20|unique:organizations',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'region_id'=>'required|integer|exists:regions,id',
            'legal_address'=>'required|string',
            'fact_address'=>'required|string',
            'phone' => 'required|string|max:20',
            'files.*'=>'nullable|file|mimes:jpg,jpeg,png,pdf,doc,docx',
        ];
    }
}
