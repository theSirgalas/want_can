<?php

namespace App\Http\Requests\Organization;


use App\Entities\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

/**
 * Class CabinetEditRequest
 * @package App\Http\Requests\Organization
 * @property User $user
 * @property string $name
 * @property string $inn
 * @property string $email
 * @property string $password
 * @property string $region_id
 * @property string $legal_address
 * @property string $fact_address
 * @property string $phone
 * @property string $about_us
 * @property array $category
 * @property array $social
 */
class CabinetEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:255',
            'inn'=>[
                'required',
                'string',
                'max:20',
                Rule::unique('organizations','inn')->ignore($this->user->organization->id),
            ],
            'email' => ['required','string','email','max:255',Rule::unique('users','email')->where(function ($query){
                return $query->whereNull('email')->where('id','!=',$this->user->id);
            })],
            'region_id'=>'required|integer|exists:regions,id',
            'legal_address'=>'required|string',
            'fact_address'=>'required|string',
            'phone' => 'required|string|max:20',
            'about_us'=>'nullable|string',
            'category.*.category'=>'nullable|integer|exists:categories,id',
            'category.*.subCategory'=>'nullable|string',
            'social.*'=>['nullable','string',Rule::unique('socials','link')->where(function ($query){
                $socialId=DB::table('organization_socials')->select('social_id')->where('organization_id',$this->user->organization->id);
                return $query->whereNotIn('id',$socialId);
            })],
        ];
    }
}
