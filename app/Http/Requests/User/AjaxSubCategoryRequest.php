<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AjaxSubCategoryRequest
 * @package App\Http\Requests\User
 * @property int $category_id
 * @property int user_id
 * @property int $name
 */
class AjaxSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required|int',
            'user_id'=>'required|int',
            'name'=>'required|int',
        ];
    }
}
