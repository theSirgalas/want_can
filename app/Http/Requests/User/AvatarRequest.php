<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AvatarRequest
 * @package App\Http\Requests\User
 * @property $avatar
 * @property int $user
 */
class AvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'=>'required|image|mimes:jpg,jpeg,png',
            'user'=>'required|int|exists:users,id'
        ];
    }
}
