<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegiserRequest
 * @package App\Http\Requests\User
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property int $region_id
 */
class RegisterRequest extends FormRequest
{

    public function authorize():bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|max:16',
            'region_id'=>'required|integer|exists:regions,id'
        ];
    }
}
