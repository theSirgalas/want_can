<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AjaxAutoDeleteRequest
 * @package App\Http\Requests\User
 * @property int $car
 * @property int $user
 */
class AjaxAutoDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user'=>'required|int|exists:users,id',
            'car'=>'required|int|exists:autos,id'
        ];
    }
}
