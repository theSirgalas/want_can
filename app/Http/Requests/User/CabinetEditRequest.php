<?php

namespace App\Http\Requests\User;

use App\Entities\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

/**
 * Class CabinetEditRequest
 * @package App\Http\Requests\User
 * @property User $user
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $region_id
 * @property string $about_us
 * @property array $auto
 * @property array $category
 * @property array $social
 */

class CabinetEditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($this->user->id),
            ],
            'phone' => 'required|string|max:16',
            'region_id'=>'required|integer|exists:regions,id',
            'is_auto'=>'required|integer',
            'about_us'=>"nullable|string",
            'auto.*.mark'=>'nullable|string|max:255',
            'auto.*.color'=>'nullable|string|max:255',
            'auto.*.number'=>[
                'nullable',
                'string',
                'max:255',
                Rule::unique('autos')->where(function ($query) {
                    return $query->whereNotIn('id',$this->user->autos()->pluck('id'));
                })],
            'auto.*.body_type_handbook_id'=>'nullable|integer|exists:handbooks,id',
            'category.*.category'=>'nullable|integer|exists:categories,id',
            'category.*.subCategory'=>'nullable|string',
            'social.*'=>['nullable','string',Rule::unique('socials','link')->where(function ($query){
                $socialId=DB::table('user_socials')->select('social_id')->where('user_id',$this->user->id);
                return $query->whereNotIn('id',$socialId);
            })],
        ];
    }
}
