<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AjaxCategoryRequest
 * @package App\Http\Requests\User
 * @property int $name
 * @property int $user
 * @property bool $is_new
 */
class AjaxCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|int',
            'user'=>'required|int|exists:users,id',
            'is_new'=>'required|bool'
        ];
    }
}
