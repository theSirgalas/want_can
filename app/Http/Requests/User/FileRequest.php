<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FileRequest
 * @package App\Http\Requests\User
 * @property $files
 * @property int $user
 */
class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files.*'=>'required|file|mimes:jpg,jpeg,png,pdf,txt,doc,docx',
            'user'=>'required|int|exists:users,id'
        ];
    }
}
