<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ExecutorsRequest
 * @package App\Http\Requests\User
 * @property int $categories
 * @property int $sub_categories
 */
class ExecutorsSearchRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'categories.*'=>'nullable|integer',
            'sub_categories.*'=>'nullable|integer'
        ];
    }
}
