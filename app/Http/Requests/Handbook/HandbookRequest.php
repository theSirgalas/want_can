<?php

namespace App\Http\Requests\Handbook;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class HandbookRequest
 * @package App\Http\Requests\Handbook
 * @property string $title
 * @property string $code
 * @property string $short_description
 * @property string $description
 * @property int $handbook_title_id
 */
class HandbookRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|string|max:256',
            'code'=>'nullable|string|max:256',
            'short_description'=>'nullable|string|max:256',
            'description'=>'nullable|string',
            'handbook_title_id'=>'required|integer|exists:handbook_titles,id'
        ];
    }
}
