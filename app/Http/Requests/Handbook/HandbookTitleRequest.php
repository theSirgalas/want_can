<?php

namespace App\Http\Requests\Handbook;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class HandbookTitleRequest
 * @package App\Http\Requests\Handbook
 *
 * @property string $title
 * @property string $key
 */
class HandbookTitleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'=>'required|string|max:256',
            'key'=>'required|string|max:256'
        ];
    }
}
