<?php

namespace App\Http\Requests\Reviews;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AddRequest
 * @package App\Http\Requests\Reviews
 * @property int $score
 * @property string $comment
 */
class AddRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'score'=>'nullable|numeric',
            'comment'=>'nullable|string'
        ];
    }
}
