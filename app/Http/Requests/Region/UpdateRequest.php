<?php

namespace App\Http\Requests\Region;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Region
 * @property string $name
 * @property integer $parent
 */

class UpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique('regions','name')->where(function ($query) {
                    return $query->whereNull('name')->orWhere('id','!=',$this->parent);
                })
            ],
            'parent' => 'nullable|exists:regions,id',
        ];
    }
}
