<?php

namespace App\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Application
 * @property int $price_app
 * @property string $comment
 */
class CreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'price_app'=>'nullable|integer',
            'comment'=>'nullable|string',
        ];
    }
}
