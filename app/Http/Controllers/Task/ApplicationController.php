<?php

namespace App\Http\Controllers\Task;

use App\Entities\Notification;
use App\Entities\Task\Application;
use App\Entities\Task\Task;
use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Application\CreateRequest;
use App\Services\Task\ApplicationService;
use App\Services\Task\TaskService;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public $service;
    public $taskService;

    public function __construct(ApplicationService $service, TaskService $taskService)
    {
        $this->service=$service;
        $this->taskService=$taskService;
    }

    public function create(CreateRequest $request,Task $task)
    {
        try{
            $this->service->create($request,$task);
            event(new NotificationEvent(Task::class,$task->id,$task->user));
            return redirect()->route('task.show',$task->slug)
                ->with('success', 'Заявка подана');
        }catch (\Exception $e){
            return redirect()->route('task.show',$task->slug)
                ->with('error', 'Заявка не подана');
        }
    }

    public function addGeneralApplication(Request $request,Task $task)
    {
        try {
            $request->validate([
                'application' => "required|int|exists:applications,id"
            ]);
            $this->taskService->addGeneral($this->service->repository->one($request->application), $task);
            event(new NotificationEvent(Application::class,$task->id,$task->application->user,Application::TYPE_OPEN));
            return redirect()->route('task.show', $task->slug)
                ->with('success', 'Заявка подтверждена на исполнение');
        } catch (\Exception $e){
            return redirect()->route('task.show',$task->slug)
                ->with('error', 'Заявка не подтверждена на исполнение');
        }
    }

    public function refused(Application $application)
    {
        try {
            $this->service->refused($application,$application->task);
            return back()->with('success','Вы отказались от заявки');
        }catch (\DomainException $e){
            return back()->with('error',$e->getMessage());
        }catch (\Exception $e){
            return back()->with('error','Ой что-то пошло не так');
        }
    }

    public function closed(Application $application)
    {
        try {
            $this->service->closed($application,$application->task);
            return back()->with('success','Вы закрыли заявку');
        }catch (\DomainException $e){
            return back()->with('error',$e->getMessage());
        }catch (\Exception $e){
            return back()->with('error','Ой что-то пошло не так');
        }
    }
}
