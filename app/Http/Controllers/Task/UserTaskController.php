<?php

namespace App\Http\Controllers\Task;

use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Task\CreateRequest;
use App\Http\Requests\Task\CreateUserRequest;
use App\Services\CategoryService;
use App\Services\Handbook\HandbookService;
use App\Services\Task\TaskService;
use Illuminate\Http\Request;

class UserTaskController extends Controller
{
    public $taskService;
    public $categoryService;
    public $handbookService;

    public function __construct(
        TaskService $taskService,
        CategoryService $categoryService,
        HandbookService $handbookService
    ){
        $this->taskService=$taskService;
        $this->categoryService=$categoryService;
        $this->handbookService=$handbookService;
    }

    public function create(User $user)
    {
        return view('task.users.create',[
            'parentCategories'=>$this->categoryService->dropDownParent(),
            'handbooks'=>$this->handbookService->dropDownHandbook('price'),
            'priceType'=>Task::$priceTypeName,
            'placeServices'=>Task::$placeServiceName,
            'remote'=>Task::REMOTE,
            'user'=>$user
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        try{
            $task=$this->taskService->createUser($request);
            return redirect()->route('task.show',$task->slug)
                ->with('success', 'Задание создано');
        }catch (\Exception $e){
            return redirect()->route('task.resource.form')
                ->with('error', 'Задание не создано');
        }
    }

    public function start()
    {
        $startDate=['date'=>null,'time'=>null];
        return view('task.users._start',compact('startDate'));
    }
}
