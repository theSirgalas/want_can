<?php

namespace App\Http\Controllers\Task;

use App\Entities\Category;
use App\Entities\Task\Task;
use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\AjaxSubCategoryRequest;
use App\Http\Requests\Task\CreateRequest;
use App\Http\Requests\Task\EditRequest;
use App\Http\Requests\Task\SearchRequest;
use App\Http\Search\TaskSearch;
use App\Services\CategoryService;
use App\Services\Handbook\HandbookService;
use App\Services\Task\TaskService;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public $taskService;
    public $categoryService;
    public $handbookService;

    public function __construct(
        TaskService $taskService,
        CategoryService $categoryService,
        HandbookService $handbookService
    ){
        $this->taskService=$taskService;
        $this->categoryService=$categoryService;
        $this->handbookService=$handbookService;
    }

    public function form()
    {
        return view('task.resource.create',[
            'parentCategories'=>$this->categoryService->dropDownParent(),
            'handbooks'=>$this->handbookService->dropDownHandbook('price'),
            'priceType'=>Task::$priceTypeName,
            'placeServices'=>Task::$placeServiceName,
            'remote'=>Task::REMOTE
        ]);
    }

    public function store(CreateRequest $request)
    {
        try{
            $task=$this->taskService->create($request);
            return redirect()->route('task.show',$task->slug)
                ->with('success', 'Задание создано');
        }catch (\Exception $e){
            return redirect()->route('task.resource.form')
                ->with('error', 'Задание не создано');
        }

    }

    public function show(string $slug)
    {
        $task=$this->taskService->repository->oneBySlug($slug);
        $this->taskService->applicationViews($task);
        return view('task.show',compact('task'));
    }

    public function update(Task $task){
        return view('task.resource.edit',[
            'parentCategories'=>$this->categoryService->dropDownParent(),
            'handbooks'=>$this->handbookService->dropDownHandbook('price'),
            'priceType'=>Task::$priceTypeName,
            'placeServices'=>Task::$placeServiceName,
            'remote'=>Task::REMOTE,
            'task'=>$task
        ]);
    }

    public function edit(EditRequest $request,Task $task)
    {
        try{
            $this->taskService->edit($request,$task);
                return redirect()->route('task.show',$task->slug)
                ->with('success', 'Задание отредактировано');
            }catch (\RuntimeException $e){
                return redirect()->route('task.resource.update',$task)
                ->with('error', 'Задание не отредактировано');
            }catch (\Exception $e){
                return redirect()->route('task.resource.update',$task)
                ->with('error', 'Задание не отредактировано');
            }

    }

    public function delete(Task $task)
    {
        try{
            $this->taskService->delete($task);
            return redirect()->route('task.resource.index')
                ->with('success', 'Задание закрыто');
        } catch (\Exception | \RuntimeException $e){
            return redirect()->route('task.show',$task->slug)
            ->with('success', 'Задание не закрыто');}
    }

    public function index(SearchRequest $request)
    {
        $categories=Category::categoryTrees();
        $tasks=(new TaskSearch())->search($request);
        $requestArr=$request->all();
        return view('task.index',compact('tasks','categories','requestArr'));
    }


    public function addSubCategory(Request $request)
    {
        $request->validate([
            'category_id'=>'required|int|exists:categories,id',
            'task_id'=>'nullable|int|exists:task,id'
        ]);
        return view('task.resource._subCategory',[
            'subCategories'=>$this->categoryService->getSubCategory($request->category_id),
        ]);
    }

    public function start(Task $task=null)
    {
       $startDate=['date'=>null,'time'=>null];
       if(is_object($task)){
           $startDate=$task->start_array;
       }
        return view('task.resource._start',compact('startDate'));
    }

    public function finish(Task $task=null)
    {
        $finishDate=['date'=>null,'time'=>null];
        if(is_object($task)){
            $finishDate=$task->finish_array;
        }
        return view('task.resource._finish',compact('finishDate'));
    }

    public function full(Task $task=null)
    {
        $startDate=['date'=>null,'time'=>null];
        $finishDate=['date'=>null,'time'=>null];
        if(is_object($task)){
            $startDate=$task->start_array;
            $finishDate=$task->finish_array;
        }
        return view('task.resource._full',compact('startDate','finishDate'));
    }

    public function date(Task $task)
    {
        $startDate=$task->start_date?$task->start_array:['date'=>null,'time'=>null];
        $finishDate=$task->finish_date?$task->finish_array:['date'=>null,'time'=>null];
        if($task->start_date&&$task->finish_date){
            return view('task.resource._full',compact('startDate','finishDate'));
        }
        if($task->finish_date){
            return view('task.resource._finish',compact('finishDate'));
        }
        return view('task.resource._start',compact('startDate'));
    }

    public function getBySlug($slug)
    {
        $category=$this->categoryService->repository->oneBySlug($slug);
        $categories=Category::categoryTrees();
        if($category->isRoot()){
            $tasks=$category->rootTask()->paginate(12);
        }else{
            $tasks=$category->subTask()->paginate(12);
        }
        $requestArr=[
            'categories'=>[$category->id],
            'sub_categories'=>$category->subs_id
        ];
        return view('task.category',compact('tasks','category','categories','requestArr'));
    }


}
