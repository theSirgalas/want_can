<?php

namespace App\Http\Controllers\Task;

use App\Entities\Task\Application;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reviews\AddRequest;
use App\Services\Task\ReviewsService;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public $service;

    public function __construct(ReviewsService $service)
    {
        $this->service=$service;
    }

    public function addReviews(AddRequest $request, Application $application)
    {
        try{
            $this->service->create($request,$application);
            return redirect()->route('task.show',$application->task->slug)
                ->with('success', 'Отзыв оставлен');
        }catch (\Exception $e){
            return redirect()->route('task.show',$application->task->slug)
                ->with('error', 'Отзыв не оставлен');
        }
    }
}
