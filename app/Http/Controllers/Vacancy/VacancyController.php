<?php

namespace App\Http\Controllers\Vacancy;

use App\Entities\Category;
use App\Entities\Resume\Resume;
use App\Entities\Vacancy\Vacancy;
use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Vacancy\EditRequest;
use App\Http\Requests\Vacancy\SearchRequest;
use App\Http\Search\VacancySearch;
use App\Services\CategoryService;
use App\Services\Handbook\HandbookService;
use App\Services\Vacancy\VacancyService;
use Illuminate\Http\Request;
use App\Http\Requests\Vacancy\CreateRequest;

class VacancyController extends Controller
{

    public $categoryService;
    public $handbookService;
    public $service;

    public function __construct(CategoryService $categoryService,HandbookService $handbookService,VacancyService $service)
    {
        $this->categoryService=$categoryService;
        $this->handbookService=$handbookService;
        $this->service=$service;
    }


    public function index(SearchRequest $request)
    {
        $categories=$this->categoryService->dropDownParent();
        $vacancies=(new VacancySearch())->search($request);
        $requestArr=$request->all();
        return view('vacancy.index',compact('vacancies','categories','requestArr'));
    }

    public function create()
    {
        return view('vacancy.create',[
            'categories'=>$this->categoryService->dropDownParent(),
            'works'=>$this->handbookService->dropDownHandbook('work'),
            'salarys'=>$this->handbookService->dropDownHandbook('salary')
        ]);
    }

    public function store(CreateRequest $request)
    {
        try{
            $vacancy=$this->service->create($request);
            return redirect()->route('vacancy.show',$vacancy)
                ->with('success', 'Резюме сохранено');
        }catch (\RuntimeException $e){
            return redirect()->route('vacancy.create')
                ->with('error', 'Вакансия  не сохранена');
        }

    }

    public function edit(Vacancy $vacancy)
    {
        return view('vacancy.edit',[
            'categories'=>$this->categoryService->dropDownParent(),
            'works'=>$this->handbookService->dropDownHandbook('work'),
            'salarys'=>$this->handbookService->dropDownHandbook('salary'),
            'vacancy'=>$vacancy
        ]);
    }

    public function update(EditRequest $request,Vacancy $vacancy)
    {
        try{
            $this->service->edit($request,$vacancy);
            return redirect()->route('vacancy.show',$vacancy);
        }catch (\RuntimeException $e)
        {
            return redirect()->route('vacancy.edit',$vacancy)
                ->with('error','Вакансия не сохранена');
        }
    }

    public function show(Vacancy $vacancy)
    {
        return view('vacancy.show',compact('vacancy'));
    }

    public function remove(Vacancy $vacancy)
    {
        $this->service->delete($vacancy);
    }

    public function getByCategoryId(Category $category)
    {
        $vacancies=$category->vacancy()->paginate(12);
        $categories=$this->categoryService->dropDownParent();
        $requestArr=[];
        return view('vacancy.index',compact('vacancies','categories','requestArr'));
    }

    public function respond(Vacancy $vacancy)
    {
        event(new NotificationEvent(Vacancy::class,$vacancy->id,$vacancy->user));
        return redirect()->route('vacancy.show',$vacancy)
            ->with('success','Вы откликнулись');
    }



}
