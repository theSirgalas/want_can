<?php

namespace App\Http\Controllers\Admin\Frequency;

use App\Entities\Frequency\FrequencyTitle;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frequency\FrequencyTitleRequest;
use App\Services\Frequency\FrequencyTitleService;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;

class FrequencyTitleController extends Controller
{
    public $service;

    public function __construct(FrequencyTitleService $service)
    {
        $this->service=$service;
    }

    public function index()
    {
        $frequencies=$this->service->repository->all();
        return view('admin.frequency.title.index',compact('frequencies'));
    }

    public function create()
    {
        return view('admin.frequency.title.create');
    }


    public function store(FrequencyTitleRequest $request)
    {
        try{
            $frequencyTitle=$this->service->create($request);
            return redirect()->route('admin.frequency.title.show',$frequencyTitle)
                ->with('success', 'Заголовок вопросов создан');
        }catch (\RuntimeException $e) {
            return redirect()->route('admin.frequency.title.index')
                ->with('error', 'Заголовок вопросов  не создан');
        }
    }

    public function show(FrequencyTitle $frequencyTitle)
    {
        return view('admin.frequency.title.show',compact('frequencyTitle'));
    }

    public function edit(FrequencyTitle $frequencyTitle)
    {
        return view('admin.frequency.title.edit',compact('frequencyTitle'));
    }

    public function update(FrequencyTitleRequest $request, FrequencyTitle $frequencyTitle)
    {
        try{
            $this->service->edit($request,$frequencyTitle);
            return redirect()->route('admin.frequency.title.show',$frequencyTitle)
                ->with('success', 'Заголовок вопросов отредактирован');
        }catch (\RuntimeException $e){
            return redirect()->route('admin.frequency.title.edit',$frequencyTitle)
                ->with('error', 'Заголовок вопросов  не отредактирован');
        }
    }

    public function destroy(FrequencyTitle $frequencyTitle)
    {
        $this->service->repository->remove($frequencyTitle);
        return redirect()->route('admin.frequency.title.index')
            ->with('error', 'Заголовок вопросов  удален');
    }
}
