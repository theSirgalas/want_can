<?php

namespace App\Http\Controllers\Admin\Frequency;

use App\Entities\Frequency\FrequencySection;
use App\Entities\Frequency\FrequencyTitle;
use App\Helpers\ArrayHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frequency\FrequencySectionRequest;
use App\Services\Frequency\FrequencySectionService;
use App\Services\Frequency\FrequencyTitleService;
use Illuminate\Http\Request;

class FrequencySectionController extends Controller
{
    public $service;
    public $serviceTitle;

    public function __construct(FrequencySectionService $service,FrequencyTitleService $serviceTitle)
    {
        $this->service=$service;
        $this->serviceTitle=$serviceTitle;
    }


    public function index()
    {
        $sections=$this->service->repository->all();
        $titles=array_merge([''=>''],ArrayHelpers::map($this->serviceTitle->repository->all(),'title','title'));
        return view('admin.frequency.section.index',compact('sections','titles'));
    }


    public function create()
    {
        $titles=ArrayHelpers::map($this->serviceTitle->repository->all(),'id','title');
        return view('admin.frequency.section.create',compact('titles'));
    }


    public function store(FrequencySectionRequest $request)
    {
        try{
            $frequencySection=$this->service->create($request);
            return redirect()->route('admin.frequency.section.show',$frequencySection)
                ->with('success', 'Под заголовок вопросов создан');
        }catch (\RuntimeException $e) {
            return redirect()->route('admin.frequency.section.index')
                ->with('error', 'Под заголовок вопросов  не создан');
        }
    }

    public function show(FrequencySection $frequencySection)
    {
        return view('admin.frequency.section.show',compact('frequencySection'));
    }

    public function edit(FrequencySection $frequencySection)
    {
        $titles=ArrayHelpers::map($this->serviceTitle->repository->all(),'id','title');
        return view('admin.frequency.section.edit',compact('frequencySection','titles'));
    }

    public function update(FrequencySectionRequest $request, FrequencySection $frequencySection)
    {
        try{
            $this->service->edit($request,$frequencySection);
            return redirect()->route('admin.frequency.section.show',$frequencySection)
                ->with('success', 'Под заголовок вопросов отредактирован');
        }catch (\RuntimeException $e){
            return redirect()->route('admin.frequency.section.edit',$frequencySection)
                ->with('error', 'Под заголовок вопросов  не отредактирован');
        }
    }


    public function destroy(FrequencySection $frequencySection)
    {
        $this->service->repository->remove($frequencySection);
        return redirect()->route('admin.frequency.section.index')
            ->with('error', 'Под заголовок вопросов  удален');
    }
}
