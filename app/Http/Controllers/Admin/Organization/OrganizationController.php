<?php

namespace App\Http\Controllers\Admin\Organization;

use App\Entities\Organization\Organization;
use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\AdminEditRequest;
use App\Services\Organization\OrganizationService;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public $service;

    public function __construct(OrganizationService $service)
    {
        $this->service=$service;
    }

    public function index()
    {
        return view('admin.organization.index',[
            'organizations'=>$this->service->organisationRepository->all(),
            'statusList'=>Organization::$statusName
        ]);
    }

    public function show(Organization $organization)
    {
        return view('admin.organization.show',['organization'=>$organization]);
    }


    public function edit(Organization $organization)
    {
        return view('admin.organization.edit',['organization'=>$organization,'statuses'=>Organization::$statusName]);
    }

    public function update(AdminEditRequest $request, Organization $organization)
    {
        $this->service->editAdmin($request,$organization);
        return redirect()->route('admin.organization.show',$organization)
            ->with('success', 'Юр.лицо');
    }


}
