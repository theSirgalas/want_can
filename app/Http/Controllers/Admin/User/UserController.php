<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AdminEditRequest;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Entities\User\User;

class UserController extends Controller
{
    public $service;

    public function __construct(UserService $service)
    {
        $this->service=$service;
    }

    public function index(){
       return view('admin.user.index',[
           'users'=>$this->service->repository->all(),
           'rolesList'=>User::$rolesListTableSearch
        ]);
    }


    public function editForm(User $user)
    {
        return view('admin.user.edit',[
            'user'=>$user,
            'rolesList'=>User::$rolesList
        ]);
    }

    public function edit(AdminEditRequest $request, User $user)
    {
        $this->service->edit($user->id,$request);
        return redirect()->route('admin.user.view',$user)
            ->with('success', 'Пользователь от редактирован');
    }

    public function view(User $user)
    {

        return view('admin.user.view',['user'=>$user]);
    }
}
