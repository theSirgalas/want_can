<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Category;
use App\Helpers\ArrayHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    public $service;

    public $categoryService;

    public function __construct(CategoryService $service,CategoryService $categoryService)
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
    }
    public function index()
    {
        return view('admin.category.index',[
            'categories'=>$this->service->repository->all(),
            'categoryParent'=>array_merge([''=>''],ArrayHelpers::map($this->categoryService->repository->allParent(),'name','name'))
        ]);
    }

    public function create()
    {
        return view('admin.category.create',['parents'=>$this->service->parentSelect()]);
    }

    public function store(CategoryRequest $request)
    {
        $category= $this->service->create($request);
        return redirect()->route('admin.category.show',$category)
            ->with('success', 'Категория создана');
    }

    public function show(Category $category)
    {
        return view('admin.category.show',['category'=>$category]);
    }

    public function edit(Category $category)
    {
        return view('admin.category.edit',[
            'category'=>$category,
            'parents'=>$this->service->parentSelect(),
            'parent_category_id'=>is_object($category->parent)?$category->parent->id:null
        ]);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $this->service->edit($request,$category);
        return redirect()->route('admin.category.show',$category)
            ->with('success', 'Категория отредактирована');
    }

    public function destroy(Category $category)
    {
        $this->service->repository->remove($category);
        return redirect()->route('admin.category.index');
    }

    public function preview(Category $category)
    {
        return $this->service->iconPreview($category);
    }
}
