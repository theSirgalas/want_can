<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Entities\Handbook\Handbook;
use App\Http\Controllers\Controller;
use App\Http\Requests\Handbook\HandbookRequest;
use App\Services\Handbook\HandbookService;
use App\Services\Handbook\HandbookTitleService;
use Illuminate\Http\Request;

class HandbookController extends Controller
{
    public $service;
    public $handbookTitleService;

    public function __construct(HandbookService $service,HandbookTitleService $handbookTitleService)
    {
        $this->service=$service;
        $this->handbookTitleService=$handbookTitleService;
    }

    public function index()
    {
        return view('admin.handbook.index',['handbooks'=>$this->service->repository->all()]);
    }

    public function create()
    {
        $handbookTitle=$this->handbookTitleService->dropDownHandbookTitle();
        return view('admin.handbook.create',compact('handbookTitle'));
    }

    public function store(HandbookRequest $request)
    {
        $handbook=$this->service->create($request);
        return redirect()->route('admin.handbook.show',$handbook)
            ->with('success', 'Справочник создан');
    }


    public function show(Handbook $handbook)
    {
        return view('admin.handbook.show',compact('handbook'));
    }


    public function edit(Handbook $handbook)
    {
        $handbookTitle=$this->handbookTitleService->dropDownHandbookTitle();
        return view('admin.handbook.edit',compact('handbook','handbookTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HandbookRequest $request, Handbook $handbook)
    {
        $handbook->edit($request);
        return redirect()->route('admin.handbook.show',$handbook)
            ->with('success', 'Справочник отредактирован');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Handbook $handbook)
    {
        $this->service->repository->remove($handbook);
        return redirect()->route('admin.handbook.index')
            ->with('success', 'Справочник удален');
    }
}
