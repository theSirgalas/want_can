<?php

namespace App\Http\Controllers\Admin\Handbook;

use App\Http\Controllers\Controller;
use App\Http\Requests\Handbook\HandbookTitleRequest;
use App\Services\Handbook\HandbookTitleService;
use Illuminate\Http\Request;
use App\Entities\Handbook\HandbookTitle;

class HandbookTitleController extends Controller
{
    public $service;

    public function __construct(HandbookTitleService $service){
        $this->service=$service;
    }

    public function index()
    {
        return view('admin.handbook-title.index',['handbookTitles'=>$this->service->repository->all()]);
    }


    public function create()
    {
        return view('admin.handbook-title.create');
    }


    public function store(HandbookTitleRequest $request)
    {
        $handbookTitle=$this->service->create($request);
        return redirect()->route('admin.handbook.title.show',$handbookTitle)
            ->with('success', 'Заголовок создан');
    }


    public function show(HandbookTitle $handbookTitle)
    {
        return view('admin.handbook-title.show',['handbookTitle'=>$handbookTitle]);
    }


    public function edit(HandbookTitle $handbookTitle)
    {
        return view('admin.handbook-title.edit',compact('handbookTitle'));
    }


    public function update(HandbookTitleRequest $request, HandbookTitle $handbookTitle)
    {
        $this->service->edit($request,$handbookTitle);
        return redirect()->route('admin.handbook.title.show',$handbookTitle)
            ->with('success', 'Заголовок создан');
    }


    public function destroy(HandbookTitle $handbookTitle)
    {
        $this->service->repository->remove($handbookTitle);
        return redirect()->route('admin.handbook.title.index');
    }
}
