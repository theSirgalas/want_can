<?php

namespace App\Http\Controllers\Admin\Task;

use App\Entities\Task\Task;
use App\Helpers\ArrayHelpers;
use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\Task\TaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public $service;
    public $categoryService;

    public function __construct(TaskService $service,CategoryService $categoryService)
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
    }

    public function index()
    {
        return view('admin.task.index',
            [
                'tasks'=>$this->service->repository->all(),
                'categoryParent'=>array_merge([''=>''],ArrayHelpers::map($this->categoryService->repository->allParent(),'name','name'))
            ]
        );
    }

    public function show(Task $task)
    {
        return view('admin.task.show',compact('task'));
    }


    public function destroy(Task $task)
    {
        $this->service->delete($task);
        return redirect()->route('admin.task.show',$task)
            ->with('success', 'Вакансия удалена');
    }

    public function subCategory(Request $request)
    {
        $request->validate([
            'name'=>'required|string|exists:categories,name'
        ]);
        $category=$this->categoryService->repository->oneByName($request->name);
        return $category->subCategoryArray($category);
    }
}
