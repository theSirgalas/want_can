<?php

namespace App\Http\Controllers\Admin\Vacancy;

use App\Entities\Vacancy\Vacancy;
use App\Helpers\ArrayHelpers;
use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\Vacancy\VacancyService;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public $service;
    public $categoryService;

    public function __construct(VacancyService $service,CategoryService $categoryService)
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
    }

    public function index()
    {
        $vacancies=$this->service->repository->all();
        $categoryParent=array_merge([''=>''],ArrayHelpers::map($this->categoryService->repository->allParent(),'name','name'));
        return view('admin.vacancy.index',compact('vacancies','categoryParent'));
    }

    public function show(Vacancy $vacancy)
    {
        return view('admin.vacancy.show',compact('vacancy'));
    }

    public function destroy(Vacancy $vacancy)
    {
        $this->service->delete($vacancy);
        return redirect()->route('admin.vacancy.show',$vacancy)
        ->with('success', 'Вакансия удалена');
    }

}
