<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Region;
use App\Http\Controllers\Controller;
use App\Http\Requests\Region\CreateRequest;
use App\Http\Requests\Region\UpdateRequest;
use App\Services\RegionService;
use Illuminate\Http\Request;

class RegionController extends Controller
{

    public $service;


    public function __construct(RegionService $service)
    {
        $this->service=$service;
    }
    public function index()
    {
        return view('admin.region.index',['regions'=>$this->service->repository->all()]);
    }


    public function create()
    {
        return view('admin.region.create',['regions'=>$this->service->dropDownRegion()]);
    }

    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
       $region= $this->service->create($request);
       return redirect()->route('admin.region.show',$region)
        ->with('success', 'Регион создан');
    }


    public function show(Region $region)
    {
        return view('admin.region.show',['region'=>$region]);
    }


    public function edit(Region $region)
    {
        return view('admin.region.edit',['region'=>$region,'regions'=>$this->service->dropDownRegion()]);
    }


    public function update(UpdateRequest $request, Region $region)
    {
        $this->service->edit($request,$region);
        return redirect()->route('admin.region.show',$region)
            ->with('success', 'Регион отредактирован');
    }

    public function destroy(Region $region)
    {
        $this->service->repository->remove($region);
        return redirect()->route('admin.region.index');
    }
}
