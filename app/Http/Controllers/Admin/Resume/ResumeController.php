<?php

namespace App\Http\Controllers\Admin\Resume;

use App\Entities\Resume\Resume;
use App\Helpers\ArrayHelpers;
use App\Http\Controllers\Controller;
use App\Http\Search\ResumeSearch;
use App\Services\CategoryService;
use App\Services\Resume\ResumeService;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public $service;

    public $categoryService;

    public function __construct(ResumeService $service, CategoryService $categoryService)
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
    }

    public function index()
    {
        $resumes=$this->service->repository->all();
        $categoryParent=array_merge([''=>''],ArrayHelpers::map($this->categoryService->repository->allParent(),'name','name'));
        return view('admin.resume.index',compact('resumes','categoryParent'));
    }

    public function show(Resume $resume)
    {
        return view('admin.resume.show',compact('resume'));
    }

    public function destroy(Resume $resume)
    {
        $this->service->delete($resume);
        return redirect()->route('admin.resume.show',$resume)
            ->with('success', 'Вакансия удалена');
    }
}
