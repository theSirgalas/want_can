<?php

namespace App\Http\Controllers\Resume;

use App\Entities\Category;
use App\Entities\Resume\Resume;
use App\Entities\Vacancy\Vacancy;
use App\Events\NotificationEvent;
use App\Helpers\DateHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Resume\EditRequest;
use App\Http\Requests\Resume\SearchRequest;
use App\Http\Search\ResumeSearch;
use App\Services\CategoryService;
use App\Services\Handbook\HandbookService;
use App\Services\Resume\ResumeService;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;
use App\Http\Requests\Resume\CreateRequest;

class ResumeController extends Controller
{

    public $categoryService;
    public $handbookService;
    public $service;

    public function __construct(CategoryService $categoryService,HandbookService $handbookService,ResumeService $service)
    {
        $this->categoryService=$categoryService;
        $this->handbookService=$handbookService;
        $this->service=$service;
    }

    public function index(SearchRequest $request)
    {
        $categories=$this->categoryService->dropDownParent();
        $resumes=(new ResumeSearch())->search($request);
        $requestArr=$request->all();
        return view('resume.index',compact('resumes','categories','requestArr'));
    }

    public function create()
    {

        return view('resume.create',
            [
                'categories'=>$this->categoryService->dropDownParent(),
                'works'=>$this->handbookService->dropDownHandbook('work'),
                'educations'=>$this->handbookService->dropDownHandbook('education'),
                'genders'=>Resume::$sexName,
                'trips'=>Resume::$tripsName,
                'mouths'=>DateHelpers::$months,
                'years'=>DateHelpers::years()
            ]);
    }

    public function store(CreateRequest $request)
    {
        try{
            $resume=$this->service->create($request);
            return redirect()->route('resume.show',$resume)
                ->with('success', 'Резюме сохранено');
        }catch (\RuntimeException $e){
            return redirect()->route('resume.create')
                ->with('error', 'Резюме не сохранено');
        }
    }

    public function edit(Resume $resume)
    {
        $maxId=$this->service->repository->maxId();
        $categories=$this->categoryService->dropDownParent();
        $works=$this->handbookService->dropDownHandbook('work');
        $educations=$this->handbookService->dropDownHandbook('education');
        $genders=Resume::$sexName;
        $trips=Resume::$tripsName;
        $mouths=DateHelpers::$months;
        $years=DateHelpers::years();
        return view('resume.edit',compact('categories','works','educations','genders','trips','mouths','years','resume','maxId'));
    }

    public function update(EditRequest $request,Resume $resume)
    {
        try{
            $this->service->edit($request,$resume);
            return redirect()->route('resume.show',$resume)
                ->with('success', 'Резюме сохранено');
        }catch (\RuntimeException $e){
            return redirect()->route('resume.show',$resume)
                ->with('error', 'Резюме не сохранено');
        }
    }

    public function show(Resume $resume)
    {
        return view('resume.show',compact('resume'));
    }

    public function experience(Request $request)
    {
        $request->validate([
            'id'=>'nullable|integer|exists:resumes,id',
            'count'=>'required|integer'
        ]);
        $countExperience=$request->count+1;
        $mouths=DateHelpers::$months;
        $years=DateHelpers::years();
        if($request->id){
            $resume=$this->service->repository->one($request->id);
            if(!empty($resume->experiences)){
                $experiences=$resume->experiences;
                return view('resume.include._edit-experience',compact('experiences','countExperience','mouths','years'));
            }
        }

        return view('resume.include._new-experience',compact('countExperience','mouths','years'));
    }

    public function education(Request $request)
    {
        $request->validate([
            'id'=>'integer|exists:resumes,id',
            'count'=>'integer'
        ]);
        $countEducation=$request->count+1;
        $years=DateHelpers::educationYears();
        if($request->id){
            $resume=$this->service->repository->one($request->id);
            if(!empty($resume->educations)){
                $educations=$resume->educations;
                return view('resume.include._edit-education',compact('educations','years'));
            }
        }

        return view('resume.include._new-education',compact('countEducation','years'));
    }

    public function remove(Resume $resume)
    {
        $this->service->delete($resume);
    }

    public function getByCategoryId(Category $category)
    {
        $resumes=$category->resumes()->paginate(12);
        $categories=$this->categoryService->dropDownParent();
        $requestArr=[];
        return view('resume.index',compact('resumes','categories','requestArr'));
    }

    public function respond(Resume $resume)
    {
        event(new NotificationEvent(Resume::class,$resume->id,$resume->user));
        return redirect()->route('vacancy.show',$resume)
            ->with('success','Вы откликнулись');
    }
}
