<?php

namespace App\Http\Controllers;

use App\Entities\Frequency\FrequencySection;
use App\Services\Frequency\FrequencyTitleService;
use Illuminate\Http\Request;

class FrequencyController extends Controller
{
    public $service;

    public function __construct(FrequencyTitleService $service)
    {
        $this->service=$service;
    }

    public function index()
    {
        $frequencyTitles=$this->service->repository->all();
        $inTwo=count($frequencyTitles)/2;

        $count=0;
        return view('frequency.index',compact('frequencyTitles','inTwo','count'));
    }

    public function one(FrequencySection $frequencySection)
    {
        return view('frequency.one',compact('frequencySection'));
    }
}
