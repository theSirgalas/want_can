<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\User\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }
}
