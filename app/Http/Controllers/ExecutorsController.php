<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Http\Requests\User\ExecutorsSearchRequest;
use App\Http\Search\ExecutorsSearch;
use App\Services\CategoryService;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class ExecutorsController extends Controller
{
    public $service;
    public $categoryService;

    public function __construct(UserService $service,CategoryService $categoryService)
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
    }

    public function index(ExecutorsSearchRequest $request)
    {
        return view('users.index',[
            'executors'=>(new ExecutorsSearch())->search($request),
            'categories'=>Category::categoryTrees(),
            'requestArr'=>$request->all()
        ]);
    }

    public function getBySlug($slug)
    {
        $category=$this->categoryService->repository->oneBySlug($slug);
        $categories=Category::categoryTrees();
        if($category->isRoot()){
            $executors=$category->users()->paginate(12);
        }else{
            $executors=$category->subTask()->paginate(12);
        }
        $requestArr=[
            'categories'=>[$category->id],
            'sub_categories'=>$category->subs_id
        ];
        return view('users.index',compact('executors','categories','requestArr'));
    }
}
