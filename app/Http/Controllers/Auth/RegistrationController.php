<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\RegisterRequest as OrganizationRegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Entities\User\User;
use App\Services\Organization\OrganizationService;
use App\Services\RegionService;
use App\Services\User\UserService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\User\RegisterRequest;

class RegistrationController extends Controller
{

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private $regionService;
    private $userService;
    private $organisationService;

    /**
     * RegisterController constructor.
     * @param RegionService $regionService
     * @param UserService $userService
     */
    public function __construct(RegionService $regionService, UserService $userService,OrganizationService $organisationService)
    {
        $this->middleware('guest');
        $this->regionService=$regionService;
        $this->userService=$userService;
        $this->organisationService=$organisationService;
    }


    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function user(RegisterRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('login')
            ->with('success', 'Check your email and click on the link to verify.');
    }

    public function organization(OrganizationRegisterRequest $request)
    {
        DB::beginTransaction();
        try{
            $this->organisationService->register($request);
            DB::commit();
            $with=['success','Спасибо за регистрацию'];
        }catch(\Error $e){
            DB::rollback();
            $with=['error','Что то пошло не так обратитесь к администратору'];
        }

        return redirect()->route('login')
            ->with('success', 'Check your email and click on the link to verify.');
    }


    public function individual()
    {
        return view('auth.register.individual',[
            'regions'=>$this->regionService->dropDownRegion()
        ]);
    }

    public function entity()
    {
        return view('auth.register.organization',[
            'regions'=>$this->regionService->dropDownRegion()
        ]);
    }

    public function registerTab()
    {
        return view('auth.register.register-tab');
    }
}
