<?php

namespace App\Http\Controllers\Cabinet;

use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\SettingRequest;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public $service;

    public function __construct(UserService $service)
    {
        $this->service=$service;
    }

    public function form(User $user){
        return view('cabinet.setting',['user'=>$user]);
    }

    public function store(SettingRequest $request,User $user)
    {
        if($user->id!=Auth::user()->id){
            throw new \Exception('Вы не имеете права редактировать этого пользователя',500);
        }
        try{
            $user->update(['password'=>$request->password]);
            Auth::logout();
            return redirect()->route('login')
                ->with('success', 'Пароль изменен');
        }catch (\Exception $e){
            return redirect()->route('cabinet.setting.edit',$user)
                ->with('success', 'Пароль не  изменен');
        }
    }
}
