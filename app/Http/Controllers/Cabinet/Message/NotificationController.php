<?php

namespace App\Http\Controllers\Cabinet\Message;

use App\Entities\Notification;
use App\Entities\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(User $user)
    {
        $notifications=Notification::where('user_id',$user->id)->where('view',false)->get();
        return view('cabinet.notification',compact('user','notifications'));
    }

    public function view(Notification $notification)
    {
        $notification->view=true;
        $notification->save();
    }
}
