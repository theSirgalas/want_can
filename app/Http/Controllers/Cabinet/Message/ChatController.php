<?php

namespace App\Http\Controllers\Cabinet\Message;

use App\Entities\Chatroom;
use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Services\ChatroomService;
use App\Services\User\UserService;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public $service;
    public $userService;

    public function __construct(ChatroomService $service,UserService $userService)
    {
        $this->service=$service;
        $this->userService=$userService;
    }

    public function index(User $user,int $opponent_id=null)
    {
        $opponent=$opponent_id;
        if($opponent_id){
            $opponent=$this->userService->repository->one($opponent_id);
        }
        $first=$this->service->first($user,$opponent);
        return view('cabinet.chats.index',['user'=>$user,'chatrooms'=>$this->service->getRooms($user),'first'=>$first]);
    }

    public function chatroom(Request $request, User $user)
    {
        $request->validate([
            'room_id'=>'required|integer'
        ]);
        try {
            return $this->service->getChats($user,$request->room_id);
        }catch (\DomainException | \RuntimeException $e){
            return redirect()->route('cabinet.message.chatroom',$user)
                ->with('error', 'Заявка не подана');
        }

    }
}
