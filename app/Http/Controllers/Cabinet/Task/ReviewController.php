<?php

namespace App\Http\Controllers\Cabinet\Task;

use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Services\Task\ReviewsService;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public $service;

    public function __construct(ReviewsService $service)
    {
        $this->service=$service;
    }

    public function customer(User $user){
        $reviews=$this->service->repository->getReviewsByCustomer($user->id)->paginate(12);
        return view('cabinet.reviews.customer',compact('user','reviews'));
    }

    public function executor(User $user){
        $reviews=$user->reviews()->paginate(12);
        return view('cabinet.reviews.executor',compact('user','reviews'));
    }

}
