<?php

namespace App\Http\Controllers\Cabinet\Task;

use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Task\CabinetSearchRequest;
use App\Http\Search\TaskCabinetSearch;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function customer(CabinetSearchRequest $request, User $user)
    {
        return view('cabinet.task.customer',[
            'tasks'=>(new TaskCabinetSearch())->search($request,$user),
            'user'=>$user,
            'taskCount'=>$user->tasksCreator()->count(),
            'status'=>$request->status
        ]);
    }

    public function executor(CabinetSearchRequest $request,User $user)
    {
        return view('cabinet.task.executor',[
            'tasks'=>(new TaskCabinetSearch())->search($request,$user,'executor'),
            'user'=>$user,
            'taskCount'=>$user->tasksExecutor()->count(),
            'status'=>$request->status
        ]);
    }
}
