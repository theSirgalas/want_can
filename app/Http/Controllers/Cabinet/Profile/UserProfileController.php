<?php

namespace App\Http\Controllers\Cabinet\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AjaxAutoDeleteRequest;
use App\Http\Requests\User\AjaxAutoRequest;
use App\Http\Requests\User\AjaxCategoryRequest;
use App\Http\Requests\User\AjaxSubCategoryRequest;
use App\Http\Requests\User\AvatarRequest;
use App\Http\Requests\User\CabinetEditRequest;
use App\Http\Requests\User\FileRequest;
use App\Services\CategoryService;
use App\Services\Handbook\HandbookService;
use App\Services\RegionService;
use App\Services\User\AutoService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Entities\User\User;

class UserProfileController extends Controller
{
    public $service;
    public $serviceRegion;
    public $serviceHandbook;
    public $serviceCategory;
    public $serviceAuto;

    public function __construct(
        UserService $service,
        RegionService $serviceRegion,
        HandbookService $serviceHandbook,
        CategoryService $categoryService,
        AutoService $autoService
    )
    {
        $this->service=$service;
        $this->serviceRegion=$serviceRegion;
        $this->serviceHandbook=$serviceHandbook;
        $this->serviceCategory=$categoryService;
        $this->serviceAuto=$autoService;
    }

    public function form(User $user)
    {
        return view('cabinet.profile.user.form',[
            'user'=>$user,
            'regions'=>$this->serviceRegion->dropDownRegion(),
            'handbooks'=>$this->serviceHandbook->dropDownHandbook('auto')
        ]);
    }

    public function store(CabinetEditRequest $request, User $user)
    {
        try{
            $this->service->profileEdit($request,$user);
            return redirect()->route('cabinet.profile.user.form',$user)
                ->with('success', 'Профиль отредактирован');
        }catch (\Exception $e){
            return redirect()->route('cabinet.profile.user.form',$user)
                ->with('error', 'Профиль не отредактирован');
        }
    }

    public function addCategoryForm(AjaxCategoryRequest  $request)
    {
        $user=$this->service->repository->one($request->user);

        if(!$user->categories->isEmpty()&&!$request->is_new){
           $userParentCategory=$this->service->dropDownUserParentCategory($user);
            return view('cabinet.profile.user._category',[
                'name'=>$request->name,
                'parents'=>$this->serviceCategory->dropDownParent(),
                'userParentCategories'=>$userParentCategory,
                'userSubCategory'=>$user->sub_category_id
            ]);
        }
        return view('cabinet.profile.user._category_new',[
            'name'=>$request->name,
            'parents'=>$this->serviceCategory->dropDownParent(),
        ]);
    }

    public function addSubCategory(AjaxSubCategoryRequest $request)
    {
        $user=$this->service->repository->one($request->user_id);
        return view('cabinet.profile.user._subCategory',[
            'subCategories'=>$this->serviceCategory->getSubCategory($request->category_id),
            'userSubCategory'=>$user->sub_category_id,
            'name'=>$request->name
        ]);
    }

    public function addCarForm(AjaxAutoRequest $request)
    {
        $user=$this->service->repository->one($request->user);
        if($user->isNotHaveAuto()){
            return;
        }
        if(!$user->autos->isEmpty()&&!$request->is_new){
            return view('cabinet.profile.user._car',[
                'name'=>$request->id,
                'handbooks'=>$this->serviceHandbook->dropDownHandbook('auto'),
                'userAutos'=>$user->autos
            ]);
        }
        return view('cabinet.profile.user._car_new',[
            'name'=>$request->id,
            'handbooks'=>$this->serviceHandbook->dropDownHandbook('auto')
        ]);
    }

    public function deleteCar(AjaxAutoDeleteRequest $request)
    {
        try{
            $this->serviceAuto->remove($request);
            return true;
        }catch (\RuntimeException $e){
            return $e->getMessage();
        }
    }

    public function addAvatar(AvatarRequest $request)
    {
        $user=$this->service->repository->one($request->user);
        try{
            $this->service->addAvatar($request,$user);
           return redirect()->route('cabinet.profile.user.form',$user)
                ->with('success', 'Аватар сохранен');
        }catch (\Exception $e){
           return redirect()->route('cabinet.profile.user.form',$user)
                ->with('error', 'Аватар не сохранен');
        }

    }

    public function deleteAvatar(User $user)
    {
        $this->service->deleteAvatar($user);
        return redirect()->route('cabinet.profile.user.form',$user)
            ->with('success', 'Аватар удален');
    }

    public function addFile(FileRequest $request)
    {
        $user=$this->service->repository->one($request->user);
        try {
            $this->service->addFile($request,$user);
            return redirect()->route('cabinet.profile.user.form',$user)
                ->with('success', 'Файлы сохранены');
        }catch (\Exception $e){
            return redirect()->route('cabinet.profile.user.form',$user)
                ->with('error', 'Файлы не сохранены. Обратитесь к администратору ');
        }

    }

}
