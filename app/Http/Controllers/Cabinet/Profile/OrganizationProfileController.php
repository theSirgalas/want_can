<?php

namespace App\Http\Controllers\Cabinet\Profile;

use App\Entities\Organization\Organization;
use App\Entities\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\AjaxSubCategoryRequest;
use App\Http\Requests\Organization\CabinetEditRequest;
use App\Http\Requests\Organization\CreateRequest;
use App\Http\Requests\Organization\RegisterRequest;
use App\Http\Requests\Organization\LogoRequest;
use App\Http\Requests\Organization\AjaxCategoryRequest;
use App\Services\CategoryService;
use App\Services\Organization\OrganizationService;
use App\Services\RegionService;
use Illuminate\Http\Request;

class OrganizationProfileController extends Controller
{
    public $service;
    public $categoryService;
    public $regionService;

    public function __construct(
        OrganizationService $service,
        CategoryService $categoryService,
        RegionService $regionService
    )
    {
        $this->service=$service;
        $this->categoryService=$categoryService;
        $this->regionService=$regionService;
    }


    public function addCategoryForm(AjaxCategoryRequest  $request)
    {
        $organisation=$this->service->organisationRepository->oneByUserId($request->user);
        if(!is_object($organisation)){
            return $this->returnViewCategory($request->name);
        }
        if(!$organisation->categories->isEmpty()&&!$request->is_new){
            $organizationParentCategory=$this->service->dropDownParentCategory($organisation);
            return view('cabinet.profile.user._category',[
                'name'=>$request->name,
                'parents'=>$this->categoryService->dropDownParent(),
                'userParentCategories'=>$organizationParentCategory,
                'userSubCategory'=>$organisation->sub_category_id
            ]);
        }
        return $this->returnViewCategory($request->name);
    }

    public function addSubCategory(AjaxSubCategoryRequest $request)
    {
        if(!empty($request->organization_id)){
            $organization=$this->service->organisationRepository->one($request->organization_id);
        }
        return view('cabinet.profile.user._subCategory',[
            'subCategories'=>$this->categoryService->getSubCategory($request->category_id),
            'userSubCategory'=>!empty($organization)?$organization->sub_category_id:null,
            'name'=>$request->name
        ]);
    }

    public function addLogo(LogoRequest $request)
    {
        $organization=$this->service->organisationRepository->one($request->organization);
        try {
            $this->service->addLogo($request,$organization);
            return redirect()->route('cabinet.profile.organization.edit', $organization->user)
                ->with('success', 'Логотип добавлен');
        }catch (\Exception $e){
            return redirect()->route('cabinet.profile.organization.edit',$organization->user)
                ->with('error', 'Логотип не добавлен');
        }
    }

    public function deleteLogo(Organization $organization)
    {
        try {
            $this->service->deleteLogo($organization);
            return redirect()->route('cabinet.profile.organization.form',$organization->user)
                ->with('success', 'Логотип удален');
        }catch (\Exception $e){
            return redirect()->route('cabinet.profile.organization.form',$organization->user)
                ->with('error', 'Логотип не удален');
        }

    }

    public function create(User $user){
        return view('cabinet.profile.organization.create',[
            'user'=>$user,
            'regions'=>$this->regionService->dropDownRegion()
        ]);
    }

    public function store(CreateRequest $request, User $user){
        try{
           $this->service->create($request,$user);
           return redirect()->route('cabinet.profile.organization.edit',$user)
                ->with('success', 'Организация создана');
        }catch (\Exception $e){
            return redirect()->route('cabinet.profile.organization.create',$user)
                ->with('error', 'Организация не создана');
        }
    }

    public function edit(User $user)
    {
        return view('cabinet.profile.organization.edit',[
            'user'=>$user,
            'organization'=>$user->organization,
            'regions'=>$this->regionService->dropDownRegion()
        ]);
    }

    public function update(CabinetEditRequest $request,User $user){

            $this->service->editProfile($request,$user->organization);
            return redirect()->route('cabinet.profile.organization.edit',$user)
                ->with('success', 'Организация отредактирована');

    }

    private function returnViewCategory($name)
    {
        return view('cabinet.profile.user._category_new',[
            'name'=>$name,
            'parents'=>$this->categoryService->dropDownParent(),
        ]);
    }
}
