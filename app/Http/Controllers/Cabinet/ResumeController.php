<?php

namespace App\Http\Controllers\Cabinet;

use App\Entities\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResumeController extends Controller
{
    public function index(User $user)
    {
        $resumes=$user->resumes;
        return view('cabinet.resumes',compact('resumes','user'));
    }
}
