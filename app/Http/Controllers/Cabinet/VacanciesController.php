<?php

namespace App\Http\Controllers\Cabinet;

use App\Entities\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VacanciesController extends Controller
{
    public function index(User $user)
    {
        $vacancies=$user->vacancies;
        return view('cabinet.vacancy',compact('vacancies','user'));
    }
}
