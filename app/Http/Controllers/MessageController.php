<?php


namespace App\Http\Controllers;

use App\Entities\Message;
use App\Entities\User\User;
use App\Events\PrivateChat;
use App\Http\Requests\Chat\ChatRequest;
use App\Services\ChatroomService;

class MessageController
{
    public $service;

    public function __construct(ChatroomService $service)
    {
        $this->service=$service;
    }

    public function message(ChatRequest $request,User $user)
    {
        event ( new PrivateChat($request->all(), $user));
    }
}
