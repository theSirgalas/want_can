<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('index',['categoriesArray'=>Category::categoryForHome()]);
    }
}
