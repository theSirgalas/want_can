<?php


namespace App\Helpers;


use App\Entities\Task\Task;
use Carbon\Carbon;
use Illuminate\Support\Str;

class StringHelpers
{
    public static function getSlug($name,$obj,$slug='slug')
    {
        $class=Task::where($slug,$name)->first();
        if(is_object($class)){
            $array=explode('_',$class->$slug);
            $last=$array;
            if(count($last)>1){
                $last=array_pop($array);
            }
            $plus=1;
            if(is_numeric($last)){
                $plus=(int)$last+1;
            }
            array_push($array,$plus);
            return self::getSlug(implode('_',$array),$obj,$slug);
        }
        return $name;
    }

    public static function dateToString($date):array
    {
        $carbon=new Carbon($date,'Europe/Moscow');
        return ['date'=>$carbon->format('d.m.Y'),'time'=>$carbon->format('h:i')];
    }

    public static function stringToTimestamp($string):string
    {
        $carbon=new Carbon($string,'Europe/Moscow');
        return $carbon->toDateTimeString();
    }

}
