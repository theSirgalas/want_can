<?php


namespace App\Helpers;


use Carbon\Carbon;

class DateHelpers
{
    public static $months = [
        1 => 'Январь' ,
        'Февраль' ,
        'Март' ,
        'Апрель' ,
        'Май' ,
        'Июнь' ,
        'Июль' ,
        'Август' ,
        'Сентябрь' ,
        'Октябрь' ,
        'Ноябрь' ,
        'Декабрь'
    ];

    public  static function years(){
        return range('1970',date ( 'Y' ));
    }

    public  static function educationYears(){
        $date = Carbon::now()->addYears(6);
        return range('1970',$date->format('Y'));
    }


}
