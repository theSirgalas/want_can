<?php


namespace App\Helpers;


class ArrayHelpers
{

    public static function arrayEmptyFields(array $array)
    {
        foreach ($array as $item){
            if(!is_null($item)){
                return false;
            }
        }
        return true;
    }

    public static function multiplyArrayEmptyFields(array $array)
    {
        if(count($array)>1){
            return false;
        }
        foreach ($array as $item) {
            return self::arrayEmptyFields($item);
        }
    }

    public static function searchInArray($key, array $array,string $arraykey)
    {
        if(!array_key_exists($arraykey,$array)){
            return false;
        }
        return in_array($key,$array[$arraykey]);
    }

    public static function map($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key = $element[$from];
            $value = $element[$to];
            $result[$key] = $value;
        }

        return $result;
    }



}
