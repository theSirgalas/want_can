@extends('layouts.app')
@section('meta')
    <title>Создать вакансию</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back">
                <h1>Создать вакансию</h1>
                <p class="subhead">Заполниет поля вакансии, чтобы кандидат смог получить достаточно информации</p>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        {{Form::open(['route'=>(['vacancy.store']),'method'=>'post'])}}
                        <div class="row">
                            <div class="col-xs-12 input-group">
                                <label class="input-label" for="vacancyName">Название резюме</label>
                                {{Form::text('name',old('name'),['class'=>"input", 'id'=>"vacancyName", 'placeholder'=>"Например, курьер"])}}

                            </div>
                            <div class="input-group col-xs-12 col-sm-6">
                                <label class="input-label">Категория</label>
                                <div class="ui category-dropdown fluid selection dropdown">
                                    {{Form::hidden('category_id',old('category_id'))}}
                                    @if(old('category_id'))
                                        @php
                                            $key = array_search(old('category_id'), array_column($categories, 'id'));
                                        @endphp
                                        <div class="text">{{$categories[10]['name']}}</div>
                                    @else
                                        <div class="default text">выберите категорию</div>
                                    @endif
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        @foreach($categories as $category)
                                            <div data-value="{{$category['id']}}" class="item active selected">
                                                <img class="ui avatar image" src="{{$category['icon']}}">
                                                {{$category['name']}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="input-group required col-xs-12 col-sm-6">
                                <label class="input-label">График работы</label>
                                <div class="ui fluid selection dropdown">
                                    {{Form::hidden('work_handbook_id',old('work_handbook_id'))}}
                                    <i class="dropdown icon"></i>
                                    <div class="default text">выберите график</div>
                                    <div class="menu">
                                        @foreach($works as $key=>$work)
                                            <div class="item" data-value="{{$key}}">{{$work}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 input-group">
                                <label class="input-label" for="vacancyCash">Зарплата</label>
                                {{Form::text('salary',old('salary'),['class'=>"input", 'id'=>"vacancyCash", 'placeholder'=>"заполните поле"])}}
                                <p class="input-icon rouble">₽</p>
                            </div>
                            <div class="input-group required col-xs-12 col-sm-6">
                                <label class="input-label">Частота выплат</label>
                                <div class="ui fluid selection dropdown">
                                    {{Form::hidden('payments_handbook_id',old('payments_handbook_id'))}}
                                    <i class="dropdown icon"></i>
                                    <div class="default text">выберите частоту</div>
                                    <div class="menu">
                                        @foreach($salarys as $key=>$salary)
                                            <div class="item" data-value="{{$key}}">{{$salary}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 input-group">
                                <label class="input-label" for="vacancyWhere">Где предстоит работать</label>
                                {{Form::text('where_work',old('where_work'),['class'=>"input", 'id'=>"vacancyWhere", 'placeholder'=>"заполните поле"])}}
                            </div>
                            <div class="col-xs-12 col-sm-6 input-group">
                                <label class="input-label" for="vacancyAdress">Адрес работы</label>
                                {{Form::text('address_work',old('address_work'),['class'=>"input", 'id'=>"vacancyAdress", 'placeholder'=>"заполните поле"])}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 input-group">
                                <label class="input-label" for="vacancyMusts">Требования к кандидату</label>
                                {{Form::text('requirements',old('requirements'),['class'=>"input", 'id'=>"vacancyMusts", 'placeholder'=>"заполните поле"])}}
                            </div>
                            <div class="col-xs-12 input-group">
                                <label class="input-label" for="vacancyTodo">Обязанности</label>
                                {{Form::textarea(
                                    'obligation',
                                    old('obligation'),
                                    [
                                        'class'=>"input",
                                        'id'=>"vacancyTodo",
                                        'placeholder'=>"например, перевозка людей"
                                    ]
                                )}}
                            </div>
                            <div class="col-xs-12 input-group">
                                <label class="input-label" for="vacancyCompany">Описание вакансии и компании</label>
                                {{
                                    Form::textarea(
                                        'description',
                                        old('description'),
                                        [
                                            'class'=>"input",
                                            'id'=>"vacancyCompany",
                                            'placeholder'=>"например, хороший коллектив"
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-gradient btn-lg">Опубликовать вакансию</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.phone-button').click(function(){
                let opp = $(this).attr('data-phone'),
                    gopp = $(this).text();
                $(this).html('<svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>'+opp).attr('data-phone', gopp);
            });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
