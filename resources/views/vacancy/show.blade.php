@extends('layouts.app')
@section('meta')
    <title>Вакансия {{$vacancy->name}}</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back task">
                <h1>{{$vacancy->name}}</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label><i class="icon categoria"></i>Категория</label>
                                    <div class="input output">
                                        <a href="#">
                                            <img src="{{$vacancy->category->image_path}}" alt="{{$vacancy->category->name}}">
                                            <span>{{$vacancy->category->name}}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label><i class="dat-cal icon"></i>График работы</label>
                                    <div class="input output">
                                        <span>{{$vacancy->work->title}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label><i class="rub icon"></i>Зарплата</label>
                                    <div class="input output">
                                        <span>{{$vacancy->salary}} рублей</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label>Частота выплат</label>
                                    <div class="input output">
                                        <span>{{$vacancy->payments->title}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label>Где предстоит работать</label>
                                    <div class="input output">
                                        <span>{{$vacancy->where_work}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label>Адрес работы</label>
                                    <div class="input output">
                                        <span>{{$vacancy->address_work}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="input-view">
                                    <label>Требования к кандидату</label>
                                    <div class="input output textarea">
                                        <span>{{$vacancy->requirements}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="input-view">
                                    <label>Обязанности</label>
                                    <div class="input output textarea">
                                        <span>{{$vacancy->obligation}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-view">
                                    <label><i class="dop icon"></i>Описание вакансии  и компании</label>
                                    <div class="input output textarea">
                                        <span>{{$vacancy->description}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(!empty(Auth::user())&&$vacancy->isCanRedact(Auth::user()))
                            <a href="{{route('vacancy.edit',$vacancy)}}" class="btn btn-gradient btn-lg">Отредактировать на вакансию</a>
                        @endif
                        @if(!empty(Auth::user())&&$vacancy->canReviews(Auth::user()))
                            <a href="{{route('vacancy.respond',$vacancy)}}" class="btn btn-gradient btn-lg">Откликнуться на вакансию</a>
                        @endif
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="man-block size-lg text-center">
                            <div class="left">
                                @include('layouts.includes.avatar',['user'=>$vacancy->user])
                                <div class="about">
                                    <a href="{{route('user',$vacancy->user)}}">{{$vacancy->user->name}}
                                        @include('layouts.includes.user_moderate',['user'=>$vacancy->user])
                                    </a>
                                    @include('layouts.includes.user_score',['user'=>$vacancy->user])
                                </div>
                            </div>
                        </div>
                        <div class="btn btn-svg phone-button" data-phone="{{$vacancy->user->phone}}">
                            <svg width="14" height="16">
                                <use xlink:href="#icon-phone"></use>
                            </svg>Показать телефон</div>
                    </div>
                    @if(Auth::user())
                        <a href="{{route('cabinet.message.chats',['user'=>Auth::user(),'opponents'=>$vacancy->user])}}" class="btn btn-svg chat-button"><svg width="18" height="16"><use xlink:href="#icon-message"></use></svg>Написать</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
