@extends('layouts.app')
@section('meta')
    <title>{{$frequencySection->title}}</title>
@endsection
@section('content')
    <section class="content">
    <div class="container">
        <h1>{{$frequencySection->title}}</h1>
        <p class="subhead">{{$frequencySection->short_description}}</p>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                {{$frequencySection->description}}
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
