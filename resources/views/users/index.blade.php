@extends('layouts.app')
@section('meta')
    <title>Список исполнителей</title>
@endsection
@section('content')
<section class="content">
    <div class="container">
        <div class="h1-header inline">
            <h1>Список исполнителей</h1>
            <p class="subhead">Выберите подходящего исполнителя и предложите ему задание</p>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-9">
                @foreach($executors as $executor)
                <div class="vac-res-block executor row row-no-gutters">
                    <div class="pull-right col-xs-12">
                        <div class="man-block inline size-md text-left">
                            <div class="left">
                                @include('layouts.includes.avatar',['user'=>$executor,'class'=>'avatar'])
                                <div class="about">
                                    <a href="{{route('user',$executor)}}">{{$executor->short_name}}
                                        @include('layouts.includes.user_moderate',['user'=>$executor])
                                    </a>
                                    @include('layouts.includes.user_score',['user'=>$executor])
                                </div>
                            </div>
                            <div class="right text-right">
                                <small>Был на сайте {{$executor->time_site}} назад</small><small class="hidden-xs">На сайте с {{$executor->getDateCreate()}}</small>
                                <div class="clearfix text-left">
                                    <div class="task-statistic take hidden-xs">
                                        <p>Выполнил<strong>{{$executor->tasksExecutor()->count()}} заданий</strong></p>
                                    </div>
                                    <div class="task-statistic add hidden-xs">
                                        <p>Поручил<strong>{{$executor->tasksCreator()->count()}} заданий</strong></p>
                                    </div>
                                    @foreach($executor->autos as $auto)
                                    <div class="auto">
                                        <p>{{$auto->mark}}<small>{{$auto->bodyType->title}}</small></p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pull-left text-left col-xs-12">
                        <p>{{$executor->about_us}}</p>
                        <div class="links">
                            <p>Выполняет задания в категориях:</p>
                            @if(!$executor->categories->isEmpty())

                                @foreach($executor->categories as $category)
                                    <a href="{{route('task.category',$category->slug)}}">{{$category->name}}</a>
                                @endforeach
                            @else
                                <span>категорий не выбрано</span>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <aside class="hidden-xs hidden-sm col-md-3">
                <a href="{{route('task.resource.form')}}" class="btn btn-gradient btn-lg">Создать задание</a>
                <div class="filter">
                    {{Form::open(['route'=>(['executors.executors']),'id'=>'form_search','method'=>'get'])}}
                    <div class="checkbox">
                        <label>
                            {{Form::checkbox(
                                'all',
                                1,
                                !empty($requestArr['all'])?true:null,
                                ['class'=>'check-all']
                                )
                            }}
                            <span>Все категории</span>
                        </label>
                    </div>
                    @foreach($categories as $key=>$category)
                        <div class="cat-block">
                            <div class="checkbox">
                                <label>
                                    {{Form::checkbox(
                                        'categories[]',
                                        $category['id'],
                                        App\Helpers\ArrayHelpers::searchInArray($category['id'],$requestArr,'categories')?true:null,
                                        ['class'=>"check-cat" ])
                                    }}
                                    <span>{{$category['name']}}</span>
                                </label>
                                <button class="col-lap" type="button" data-toggle="collapse" data-target="#collapseFilter_{{$key}}" aria-expanded="false" aria-controls="collapseFilter_1">
                                    <svg width="11" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439157 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98195 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0184 0.831533 10.0091 0.929385C9.99968 1.02724 9.97019 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="#252525"/>
                                    </svg>
                                </button>
                            </div>
                            @if(!empty($category['sub_category']))
                                <div class="collapse" id="collapseFilter_{{$key}}" aria-expanded="false">
                                    <div class="subcats">
                                        @foreach($category['sub_category'] as $subcategory)
                                            <div class="checkbox checkbox-sm">
                                                <label>
                                                    {{Form::checkbox(
                                                        'sub_categories[]',
                                                        $subcategory['id'],
                                                        App\Helpers\ArrayHelpers::searchInArray($subcategory['id'],$requestArr,'sub_categories')?true:false,
                                                        ["class"=>"check-subcat"]
                                                        )
                                                    }}
                                                    <span>{{$subcategory['name']}}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                    {{Form::close()}}
                </div>
            </aside>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.ui.dropdown').dropdown();

            $('.filter input').click(function(){
                if ($(this).hasClass('check-all')){
                    if($(this).is(":checked")){
                        $('.cat-block').each(function () {
                            $(this).find('input').prop('checked', true);
                        });
                    } else {
                        $('.cat-block').each(function () {
                            $(this).find('input').prop('checked', false);
                        });
                    }
                }
                $('#form_search').submit()
            });
            $('.check-cat').click(function (){
                if($(this).is(":checked")){
                    $(this).parents('.cat-block').each(function () {
                        $(this).find('input').prop('checked', true);
                    });
                } else {
                    $(this).parents('.cat-block').each(function () {
                        $(this).find('input').prop('checked', false);
                    });
                }
                $('#form_search').submit()
            });
            $('.check-subcat').change(function (){
                $('#form_search').submit()
            });
        });
        $(window).on('load',function (){
            if($('.check-all').is(":checked")) {
                $('.cat-block').each(function () {
                    $(this).find('input').prop('checked', true);
                });
            }
            $('.check-cat').each(function(){
                if($(this).is(":checked")){
                    $(this).parents('.cat-block').find('input').each(function () {
                        $(this).prop('checked', true);
                    });
                }
            });
        });
    </script>
@endsection

