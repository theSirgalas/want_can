<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="rev-block">
        <div class="row">
            <div class="col-md-7 col-lg-9">
                <div class="ans">
                    <small>Текст отзыва</small>
                    <small class="txt">{{$reviews->comment}} </small>
                </div>
                <div class="ans">
                    <small>Задание</small>
                    <small class="txt">
                        <a href="{{route('task.show',$reviews->application->task->slug)}}">{{$reviews->application->task->title}}</a></small>
                </div>
            </div>
            <div class="col-md-5 col-lg-3">
                <div class="ans">
                    <small>Оценка</small>
                    <div class="rateit svg" data-rateit-value="{{$reviews->score}}" data-rateit-ispreset="true" data-rateit-starwidth="14" data-rateit-starheight="13" data-rateit-readonly="true"></div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="ans">
                    <small>Кто оценил</small>
                    <div class="man-block inline size-xxs text-left">
                        <div class="left">
                            @include('layouts.includes.avatar',['user'=>$reviews->application->task->user])
                            <div class="about">
                                <a href="{{route('user',)}}">{{$reviews->application->task->user->name}}</a>
                                <small>Когда: {{$reviews->getDateCreate()}}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
