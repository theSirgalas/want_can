@extends('layouts.app')
@section('meta')
    <title>Отзывы</title>
@endsection
@section('content')
    <section class="content">
    <div class="container">
        <div class="w-back lk">
            <h1>Мои отзывы</h1>
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    @include('cabinet.includes.menu',['view'=>'reviews','user'=>$user])
                </div>
                <div class="col-xs-12 col-md-9">
                    <p class="lead">Всего отзывов: 1</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-7">
                            <div class="radio-group inline">
                                <label class="btn btn-lg">
                                    <a href="{{route('cabinet.task.reviews.customer',$user)}}" > Мои отзывы </a>
                                </label>
                                <label class="btn btn-lg active">
                                    <a href="{{route('cabinet.task.reviews.executor',$user)}}}" > Обо мне </a>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-5">
                            <div class="mid-mark">
                                <span class="h5">Средняя оценка: {{$user->score}}</span>
                                <div class="rateit svg" data-rateit-value="{{$user->score}}" data-rateit-ispreset="true" data-rateit-starwidth="14" data-rateit-starheight="13" data-rateit-readonly="true"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="my-views">
                            <div class="row">
                                @foreach($reviews as $review)
                                    @include('cabinet.reviews.includes.executor_review',['review'=>$review])
                                @endforeach
                            </div>
                        </div>
                    </div>
                    {{$reviews->render('layouts.includes.paginate')}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.phone-button').click(function(){
                let opp = $(this).attr('data-phone'),
                    gopp = $(this).text();
                $(this).html('<svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>'+opp).attr('data-phone', gopp);
            });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
