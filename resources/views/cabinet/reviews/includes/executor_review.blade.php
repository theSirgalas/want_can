<div class="col-xs-12 col-md-6">
    <div class="rev-block">
        <div class="row">
            <div class="col-md-7 col-lg-9">
                <div class="ans">
                    <small>Текст отзыва</small>
                    <small class="txt">{{$review->comment}}</small>
                </div>
                <div class="ans">
                    <small>Задание</small>
                    <small class="txt">
                        <a href="{{route('task.show',$review->application->task)}}}">{{$review->application->task->title}}</a></small>
                </div>
            </div>
            <div class="col-md-5 col-lg-3">
                <div class="ans">
                    <small>Оценка</small>
                    <div class="rateit svg" data-rateit-value="{{$review->score}}" data-rateit-ispreset="true" data-rateit-starwidth="14" data-rateit-starheight="13" data-rateit-readonly="true"></div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="ans">
                    <small>Кто оценил</small>
                    <div class="man-block inline size-xxs text-left">
                        <div class="left">
                            @include('layouts.includes.avatar',['user'=>$review->application->task->user])
                            <div class="about">
                                <a href="{{route('user',$review->application->task->user)}}">{{$review->application->task->user->short_name}}</a>
                                <small>Когда: {{$review->getDateCreate()}}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
