@extends('layouts.app')
@section('meta')
    <title>Мои вакансии</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back lk vacancy">
                <h1>Мои вакансии</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('cabinet.includes.menu',['view'=>'task','user'=>$user])
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <p class="lead">Всего вакансий: {{count($vacancies)}}</p>
                        <div class="row">
                            <div class="col-xs-12 col-lg-9 ">
                                @foreach($vacancies as $vacancy)
                                    <div class="task-block clearfix">
                                        <div class="pull-left">
                                            <i class="icon">
                                                <img src="{{$vacancy->category->image_path}}" alt="{{$vacancy->category->name}}">
                                            </i>
                                            <p class="lead">
                                                <a href="{{route('vacancy.show',$user)}}">{{$vacancy->name}}</a>
                                            </p>
                                            <p class="cat">
                                                <a href="{{route('vacancy.category',$vacancy->category)}}">{{$vacancy->category->name}}</a>
                                            </p>
                                        </div>
                                        <div class="pull-right">

                                            <p class="lead price text-right">
                                                {{$vacancy->salary}} ₽
                                            </p>
                                            @if(!$vacancy->is_closed)
                                                <a href="{{route('vacancy.remove',$vacancy)}}" class="btn btn-primary btn-sm resume" >Закрыть вакансию</a>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-xs-12 col-lg-3 link-check">
                                <div class="menu-lk-task">
                                    <div class="radio">
                                        <label class="task">
                                            <input name="ad-type" type="radio" data-url="{{route('cabinet.task.customer',$user)}}">
                                            <span>Задания</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="vac">
                                            <input name="ad-type" type="radio" checked>
                                            <span>Вакансии</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                            <label class="res">
                                                <input name="ad-type" type="radio" data-url="{{route('cabinet.resume',$user)}}">
                                                <span>Резюме</span>
                                            </label>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.task-block .lead').matchHeight({ byRow:true, property: 'min-height' });
            $('.ui.dropdown').dropdown();
            $('.scroll-block').scrollTop(

                $('.scroll-block').prop('scrollHeight')
            );
            $('.resume').click(function (e){
                e.preventDefault();
                const link=$(this);
                const url = link.attr('href');
                axios.delete(url).then(
                    link.remove()
                );
            });
            $('.link-check .radio input').click(function (){
                const url =  $(this).data('url')
                console.log($(this));
                if(url.length>0){
                    window.location.assign(url);
                }
            });
        });
    </script>
@endsection
