@extends('layouts.app')
@section('meta')
    <title>Профиль пользователя</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back lk">
                <div class="h1-header inline">
                    <h1>Мой кабинет</h1>
                </div>
                <p>Дата регистрации <span>{{$user->getDateCreate('d. m. Y h:i')}}</span></p>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('cabinet.includes.menu',['view'=>'user','user'=>$user])
                        <a href="#" class="btn btn-primary btn-lg save hidden-sm hidden-xs submit">Сохранить изменения</a>
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <p class="lead">Мой профиль</p>
                        <div class="row">
                            <div class="inline-nav col-xs-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Мой профиль</a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Партнеры <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Клиенты <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Найти партнеров <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Скидка партнерам <span>Скоро!</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="profile">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 col-lg-5 modify">
                                            <div class="wh-bg">
                                                <div class="img-wrap">
                                                    @include('layouts.includes.avatar',compact('user'))
                                                </div>
                                                <div class="buttons">
                                                    {{Form::open(['route'=>(['cabinet.profile.user.avatar.create']),'files' => true,'class'=>'fileForm'])}}
                                                        {{Form::token()}}
                                                        <div class="file-input-group">
                                                            {{Form::hidden('user',$user->id)}}
                                                            {{Form::file('avatar', ["id"=>"file_prof", "class"=>"inputfile",  "multiple"=>false,'data-multiple-caption'=>"{count} файла выбрано", "accept"=>".jpg, .jpeg, .png"])}}
                                                            <label class="btn" for="file_prof">
                                                                <span>Изменить фотографию</span>
                                                            </label>
                                                        </div>
                                                    {{Form::close()}}
                                                    {{Form::open(['route'=>(['cabinet.profile.user.avatar.delete',$user])])}}
                                                        @method('DELETE')
                                                    <button type="submit" class="btn btn-delete" >Удалить</button>
                                                    {{Form::close()}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4 col-lg-4 modify">
                                            @if($user->status_moderate)
                                                <div class="wh-bg">
                                                    <p><b>Личность подтверждена</b></p>
                                                </div>
                                            @else
                                                <div class="wh-bg">
                                                    <p><b>Личность не подтверждена</b></p>
                                                    <p><small>Чтобы повысить доверие заказчиков подтвердите личность.</small></p>
                                                    {{Form::open(['route'=>(['cabinet.profile.user.file.create']),'files' => true,'class'=>'fileForm'])}}
                                                        {{Form::token()}}
                                                        <div class="file-input-group">
                                                            {{Form::hidden('user',$user->id)}}
                                                            {{Form::file('files[]',["id"=>"file", "class"=>"inputfile", "data-multiple-caption"=>"{count} файла выбрано", "multiple"=>true])}}
                                                            <label class="btn btn-primary" for="file"><span>Подтвердить</span></label>
                                                        </div>
                                                    {{Form::close()}}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-lg-3 modify">
                                            <div class="wh-bg">
                                                <div class="clearfix text-left">
                                                    <div class="task-statistic take">
                                                        <p>Выполнил<strong>{{$user->tasksExecutor()->count()}} заданий</strong></p>
                                                    </div>
                                                    <div class="task-statistic add">
                                                        <p>Поручил<strong>{{$user->tasksCreator()->count()}} заданий</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {{Form::open(['route'=>(['cabinet.profile.user.store',$user]),'id'=>'form'])}}
                                @method('PUT')
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 input-group required">
                                            <label class="input-label" for="vante">ФИО</label>
                                            {{Form::text('name',old('name',$user->name),['class'=>'input','id'=>'vante','placeholder'=>'Введите ФИО','required'=>true])}}
                                        </div>
                                        <div class="input-group required col-xs-12 col-sm-6">
                                            <label class="input-label">Мой город</label>
                                            <div class="country ui fluid search selection dropdown">
                                                {{Form::hidden('region_id',old('region_id',$user->region_id),['required'=>true])}}

                                                <div class="default text">Укажите данные</div>
                                                <i class="dropdown icon"></i>
                                                <div class="menu">
                                                    @foreach($regions as $key=>$value)
                                                        <div class="item" data-value="{{$key}}">{{$value}}</div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-group required col-xs-12 col-sm-6">
                                            <label class="input-label" for="InputEmail">E-mail</label>
                                            {{Form::text('email',old('email',$user->email),['class'=>'input','id'=>'InputEmail','placeholder'=>"Укажите данные",'required'=>true]) }}
                                        </div>
                                        <div class="input-group required col-xs-12 col-sm-6">
                                            <label class="input-label" for="InputPhone">Телефон</label>
                                            {{Form::tel('phone',old('phone',$user->phone),["class"=>"input", "id"=>"InputPhone", "placeholder"=>"Укажите данные",'required'=>true])}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 input-group">
                                            <label class="input-label" for="task-description">Обо мне</label>
                                            {{Form::textarea('about_us',old('about_us',$user->about_us),["class"=>"input", "id"=>"task-description", "placeholder"=>"например, прошу связаться по телефону"])}}
                                        </div>
                                        <div class="input-group required col-xs-12">
                                            <label class="input-label">Вы на авто?</label>
                                            <div class="radio-group" data-toggle="buttons">
                                                <label class="btn btn-lg @if($user->isHaveAuto()) active @endif">
                                                    <input
                                                        type="radio"
                                                        name="is_auto"
                                                        value="1"
                                                        @if($user->isHaveAuto()) checked @endif
                                                    > Да
                                                </label>
                                                <label class="btn btn-lg @if($user->isNotHaveAuto()) active @endif">
                                                    <input
                                                        type="radio"
                                                        name="is_auto"
                                                        value="2"
                                                        @if($user->isNotHaveAuto()) checked @endif
                                                    > Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="car" data-number="{{\App\Entities\User\Auto::NAME_START}}"></div>
                                    <div id="category" data-number="{{\App\Entities\Category::NAME_START}}"></div>
                                    <p class="lead">Социальные сети</p>
                                    <div class="row socials">
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputVk"><svg width="21" height="20" viewBox="0 0 21 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M19.579 0.806C19.719 0.341 19.579 0 18.917 0H16.724C16.166 0 15.911 0.295 15.771 0.619C15.771 0.619 14.656 3.338 13.076 5.101C12.566 5.614 12.333 5.776 12.055 5.776C11.916 5.776 11.714 5.614 11.714 5.149V0.806C11.714 0.248 11.553 0 11.088 0H7.642C7.294 0 7.084 0.258 7.084 0.504C7.084 1.032 7.874 1.154 7.955 2.642V5.87C7.955 6.577 7.828 6.706 7.548 6.706C6.805 6.706 4.997 3.977 3.924 0.853C3.715 0.246 3.504 0.0010004 2.944 0.0010004H0.752C0.125 0.0010004 0 0.296 0 0.62C0 1.202 0.743 4.082 3.461 7.891C5.273 10.492 7.824 11.902 10.148 11.902C11.541 11.902 11.713 11.589 11.713 11.049V9.083C11.713 8.457 11.846 8.331 12.287 8.331C12.611 8.331 13.169 8.495 14.47 9.748C15.956 11.234 16.202 11.901 17.037 11.901H19.229C19.855 11.901 20.168 11.588 19.988 10.97C19.791 10.355 19.081 9.46 18.139 8.401C17.627 7.797 16.862 7.147 16.629 6.822C16.304 6.403 16.398 6.218 16.629 5.846C16.63 5.847 19.301 2.085 19.579 0.806Z" fill="#4D75A3"/>
                                                </svg>ВК</label>
                                            {{Form::text('social[vk]',old('social[vk]',$user->socialNameByName('vk')),['class'=>"input", "id"=>"InputVk", 'placeholder'=>"Укажите данные"])}}
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputInst"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.99775 5.99872C7.34513 5.99872 5.99647 7.34738 5.99647 9C5.99647 10.6526 7.34513 12.0013 8.99775 12.0013C10.6504 12.0013 11.999 10.6526 11.999 9C11.999 7.34738 10.6504 5.99872 8.99775 5.99872ZM17.9993 9C17.9993 7.75716 18.0106 6.52558 17.9408 5.28499C17.871 3.84402 17.5423 2.56515 16.4886 1.51144C15.4326 0.455477 14.156 0.129006 12.715 0.0592092C11.4722 -0.0105879 10.2406 0.000669702 9 0.000669702C7.75716 0.000669702 6.52558 -0.0105879 5.28499 0.0592092C3.84402 0.129006 2.56515 0.457728 1.51144 1.51144C0.455477 2.5674 0.129006 3.84402 0.0592092 5.28499C-0.0105879 6.52783 0.000669702 7.75941 0.000669702 9C0.000669702 10.2406 -0.0105879 11.4744 0.0592092 12.715C0.129006 14.156 0.457729 15.4348 1.51144 16.4886C2.5674 17.5445 3.84402 17.871 5.28499 17.9408C6.52783 18.0106 7.75941 17.9993 9 17.9993C10.2428 17.9993 11.4744 18.0106 12.715 17.9408C14.156 17.871 15.4348 17.5423 16.4886 16.4886C17.5445 15.4326 17.871 14.156 17.9408 12.715C18.0128 11.4744 17.9993 10.2428 17.9993 9ZM8.99775 13.6179C6.44227 13.6179 4.37988 11.5555 4.37988 9C4.37988 6.44452 6.44227 4.38213 8.99775 4.38213C11.5532 4.38213 13.6156 6.44452 13.6156 9C13.6156 11.5555 11.5532 13.6179 8.99775 13.6179ZM13.8047 5.27148C13.2081 5.27148 12.7263 4.78966 12.7263 4.193C12.7263 3.59635 13.2081 3.11452 13.8047 3.11452C14.4014 3.11452 14.8832 3.59635 14.8832 4.193C14.8834 4.33468 14.8556 4.475 14.8015 4.60593C14.7474 4.73686 14.6679 4.85582 14.5677 4.956C14.4676 5.05618 14.3486 5.13561 14.2177 5.18975C14.0867 5.24388 13.9464 5.27166 13.8047 5.27148Z" fill="#ED4A60"/>
                                                </svg>
                                                Instagram</label>
                                            {{Form::text('social[instagram]',old('social.instagram',$user->socialNameByName('instagram')),['class'=>"input", "id"=>"InputInst", 'placeholder'=>"Укажите данные"])}}
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputFb"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M9.999 0C4.477 0 0 4.477 0 9.999C0 14.989 3.656 19.125 8.437 19.878V12.89H5.897V9.999H8.437V7.796C8.437 5.288 9.93 3.905 12.213 3.905C13.307 3.905 14.453 4.1 14.453 4.1V6.559H13.189C11.949 6.559 11.561 7.331 11.561 8.122V9.997H14.332L13.889 12.888H11.561V19.876C16.342 19.127 19.998 14.99 19.998 9.999C19.998 4.477 15.521 0 9.999 0Z" fill="#395185"/>
                                                </svg>
                                                Facebook</label>
                                            {{Form::text('social[facebook]',old('social.facebook',$user->socialNameByName('facebook')),['class'=>"input", "id"=>"InputFb", 'placeholder'=>"Укажите данные"])}}
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputOk"><svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M5.90953 0C3.00568 0 0.64343 2.31542 0.64343 5.16188C0.64343 8.00812 3.00568 10.3225 5.90953 10.3225C8.81327 10.3225 11.1744 8.00812 11.1744 5.16188C11.1744 2.31542 8.81329 0 5.90953 0ZM5.90953 3.02438C7.11135 3.02438 8.08866 3.98372 8.08866 5.16188C8.08866 6.33954 7.11137 7.29814 5.90953 7.29814C4.70746 7.29814 3.72913 6.33956 3.72913 5.16188C3.72913 3.98372 4.70746 3.02438 5.90953 3.02438ZM1.56788 10.4981C1.04687 10.4902 0.534528 10.7419 0.237321 11.2062C-0.216874 11.9136 0.000706028 12.847 0.720588 13.2919C1.6728 13.8775 2.70607 14.293 3.77761 14.5325L0.834713 17.4187C0.232261 18.0096 0.232771 18.9666 0.835345 19.5575C1.13737 19.8524 1.53128 20 1.92619 20C2.32069 20 2.71592 19.8522 3.01702 19.5569L5.90828 16.7213L8.80209 19.5569C9.40403 20.1476 10.3801 20.1476 10.9831 19.5569C11.5858 18.9664 11.5858 18.0084 10.9831 17.4188L8.03894 14.5331C9.11078 14.2936 10.1444 13.8779 11.096 13.2919C11.8173 12.847 12.0352 11.9126 11.5811 11.2063C11.1266 10.4988 10.1748 10.2856 9.453 10.7313C7.29714 12.0606 4.51934 12.0598 2.36416 10.7313C2.11608 10.5781 1.84079 10.5023 1.56788 10.4981V10.4981Z" fill="#E27E35"/>
                                                </svg>
                                                Одноклассники</label>
                                            {{Form::text('social[ok]',old('social.ok',$user->socialNameByName('ok')),['class'=>"input", "id"=>"InputOk", 'placeholder'=>"Укажите данные"])}}
                                        </div>
                                    </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\User\CabinetEditRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script>
        $(document).ready(function(){
            /* start redact auto */
            let count=1;
            $('#car').load("{{route('cabinet.profile.user.car')}}", {"_token":"{{ csrf_token() }}", id:1,user:{{$user->id}},is_new:0},function (){
                    $('.ui.dropdown').dropdown();
                });

            $("input[name='is_auto']").change(function() {
                if ($(this).val() == 1){
                    let current = parseInt($('#car').attr('data-number'))
                    axios.post(
                        "{{route('cabinet.profile.user.car')}}",
                        {
                            "_token":"{{ csrf_token() }}",
                            id:parseInt(current),
                            user:{{$user->id}},
                            is_new:0
                        }
                    ).then(function (response){
                        $('#car').html(response.data)
                        $('.ui.dropdown').dropdown();
                    })
                }else{
                    $('#car>*').remove();
                    $('#car').attr('data-number',{{\App\Entities\User\Auto::NAME_START}})
                }
            });

            $('#car').on('click', '.add_b', function(e) {
                e.preventDefault();
                let current = parseInt($('#car').attr('data-number'))+1,
                parent = $(this).parents('#car').find('.row');
                $('#car').attr('data-number', current);
                axios.post(
                    "{{route('cabinet.profile.user.car')}}",
                    {
                        "_token":"{{ csrf_token() }}",
                        id:current,
                        user:{{$user->id}},
                        is_new:1
                    }
                ).then(function (response){
                    parent.append(response.data)
                    $('.ui.dropdown').dropdown();
                })
            });

            $('#car').on('click', '.del_b', function(e) {
                e.preventDefault();
                dataName=$(this).data('name');
                if(dataName){
                    axios.delete(
                        "{{route('cabinet.profile.user.car.delete')}}",{
                            data:{
                                "_token":"{{ csrf_token() }}",
                                car:dataName,
                                user:{{$user->id}}
                            }
                        }
                    ).then(function (response){
                        parent.append(response.data)
                        $('.ui.dropdown').dropdown();
                    }).catch(function (error){
                        console.log(error)
                    })
                }
                let current = $('#car').attr('data-number');
                $('#car').attr('data-number', parseInt(current) - 1);
                $(this).parent().remove();
            });
            /* end redact auto */
            /* start redact category */
            $('#category').load("{{route('cabinet.profile.user.category')}}",{"_token":"{{csrf_token()}}",name:1,user:{{$user->id}},is_new:0},function(){
                let current = parseInt($('#category').attr('data-number'))
                $('#category .clearfix').each(function (){
                    $('#category').attr('data-number',current);
                    current++
                })
                $('.ui.dropdown').dropdown();
                console.log('yes')
                $('.ui.dropdown.special').dropdown({useLabels: false});
            });

            $('#category').on('click', '.add_cat', function(e) {
                e.preventDefault();
                let current = parseInt($('#category').attr('data-number'))+1;
                let parent = $(this).parents('#category').find('.row');
                $('#category').attr('data-number', current);
                axios.post(
                    "{{route('cabinet.profile.user.category')}}",
                    {
                        "_token":"{{ csrf_token() }}",
                        name:current,
                        user:{{$user->id}},
                        is_new:1
                    }
                ).then(function (response){
                    parent.append(response.data)
                    $('.ui.dropdown').dropdown();
                    $('.ui.dropdown.special').dropdown({useLabels: false});
                })
            });

            $('#category').on('change','.clearfix input.category',function(){
                let category= $(this)
                let current = parseInt($('#category').attr('data-number'));
               axios.post(
                    "{{route('cabinet.profile.user.sub.category')}}",
                {
                    "_token":"{{ csrf_token() }}",
                    category_id:category.val(),
                    user_id:{{$user->id}},
                    name:current
                }).then(function (response){
                    let menu=category.parents('.categories').siblings('.sub_categories').find('.menu');
                    menu.html(response.data)
                    $('.ui.dropdown').dropdown();
                   $('.ui.dropdown.special').dropdown({useLabels: false});
                })
            });

            $('#category').on('click', '.del_cat', function(e) {
                e.preventDefault();
                let current = $('#category').attr('data-number');
                $('#category').attr('data-number', parseInt(current) - 1);
                $(this).parent().remove();
            });
            /* end redact category */

            $('.submit').click(function (e){
                e.preventDefault()
                $('#form').submit()
            })

            $('.ui.dropdown').dropdown();
            $('.ui.dropdown.special').dropdown({useLabels: false});

            $('.scroll-block').scrollTop(
                $('.scroll-block').prop('scrollHeight')
            );
            if (document.body.clientWidth > 479){
                $('.wh-bg').matchHeight({ byRow:false, property: 'min-height' });
            }

            $('.inputfile').each( function(){
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();
                $input.on( 'change', function( e ){
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName ){
                        if (fileName.length > 15){
                            var spl = fileName.split( "." );
                            fileName = spl[0].slice(0,4) + '...' + spl[0].slice(-4)+ '.' + spl[1];
                        }
                        $label.find( 'span' ).html( fileName );
                    }else{
                        $label.html( labelVal );
                    }
                });
                $input.on( 'focus', function(){ $input.addClass( 'has-focus' ); }).on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });
            $('.inputfile').change(function(){
                $(this).parents('.fileForm').submit()
            });
        });
    </script>
@endsection
