@extends('layouts.app')
@section('meta')
    <title>Профиль организации</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back lk">
                <div class="h1-header inline">
                    <h1>Мой кабинет</h1>
                </div>
                <p>Дата регистрации <span>20. 02. 2019 13:03</span></p>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('cabinet.includes.menu',['view'=>'organization','user'=>$user])
                        <a href="#" class="btn btn-primary btn-lg save hidden-sm hidden-xs submit">Сохранить изменения</a>
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <p class="lead">Профиль организации</p>
                        <div class="row">
                            <div class="inline-nav col-xs-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Мой профиль</a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Партнёры компании <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Предложить партнёрство <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled hidden-xs"><a role="tab">Скидка партнерам <span>Скоро!</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="profile">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 col-lg-5 modify">
                                            <div class="wh-bg organization">
                                                @if($organization->logo)
                                                    <img src="{{$organization->logo->path}}" alt="{{$organization->name}}-avatar" width="100"/>
                                                @else
                                                    <div class="img-wrap">
                                                        <svg viewBox="0 0 57 57" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 44.5312C0 45.9485 0.563001 47.3077 1.56515 48.3099C2.5673 49.312 3.9265 49.875 5.34375 49.875H51.6562C53.0735 49.875 54.4327 49.312 55.4349 48.3099C56.437 47.3077 57 45.9485 57 44.5312V23.1562H53.4375V44.5312C53.4375 45.0037 53.2498 45.4567 52.9158 45.7908C52.5817 46.1248 52.1287 46.3125 51.6562 46.3125H5.34375C4.87133 46.3125 4.41827 46.1248 4.08422 45.7908C3.75017 45.4567 3.5625 45.0037 3.5625 44.5312V23.1562H0V44.5312Z" />
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 16.0312C0 14.614 0.563001 13.2548 1.56515 12.2526C2.5673 11.2505 3.9265 10.6875 5.34375 10.6875H51.6562C53.0735 10.6875 54.4327 11.2505 55.4349 12.2526C56.437 13.2548 57 14.614 57 16.0312V24.5243L29.8751 31.7561C28.9741 31.9961 28.0259 31.9961 27.1249 31.7561L0 24.5243V16.0312ZM5.34375 14.25C4.87133 14.25 4.41827 14.4377 4.08422 14.7717C3.75017 15.1058 3.5625 15.5588 3.5625 16.0312V21.7882L28.0404 28.3148C28.3415 28.3952 28.6585 28.3952 28.9596 28.3148L53.4375 21.7882V16.0312C53.4375 15.5588 53.2498 15.1058 52.9158 14.7717C52.5817 14.4377 52.1287 14.25 51.6562 14.25H5.34375ZM17.8125 8.90625C17.8125 7.489 18.3755 6.1298 19.3776 5.12765C20.3798 4.1255 21.739 3.5625 23.1562 3.5625H33.8438C35.261 3.5625 36.6202 4.1255 37.6224 5.12765C38.6245 6.1298 39.1875 7.489 39.1875 8.90625V10.6875H35.625V8.90625C35.625 8.43383 35.4373 7.98077 35.1033 7.64672C34.7692 7.31267 34.3162 7.125 33.8438 7.125H23.1562C22.6838 7.125 22.2308 7.31267 21.8967 7.64672C21.5627 7.98077 21.375 8.43383 21.375 8.90625V10.6875H17.8125V8.90625Z"/>
                                                        </svg>
                                                    </div>
                                                @endif
                                                <div class="buttons">
                                                    <div class="file-input-group">
                                                        {{Form::open(['route'=>(['cabinet.profile.organization.logo.create']),'files' => true,'class'=>'fileForm'])}}
                                                            {{Form::token()}}
                                                            {{Form::hidden('organization',$organization->id)}}
                                                            {{Form::file('logo', ["id"=>"file_prof", "class"=>"inputfile",  "multiple"=>false,'data-multiple-caption'=>"{count} файла выбрано", "accept"=>".jpg, .jpeg, .png"])}}
                                                            <label class="btn" for="file_prof"><span>Изменить фотографию</span></label>
                                                        {{Form::close()}}
                                                    </div>
                                                    {{Form::open(['route'=>(['cabinet.profile.organization.logo.delete',$organization])])}}
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-delete" >Удалить</button>
                                                    {{Form::close()}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4 col-lg-4 modify">
                                            <div class="wh-bg disabled">
                                                <p><b>Сотрудники</b><span>Скоро!</span></p>
                                                <p><small>Чтобы повысить доверие заказчиков подтвердите личность.</small></p>
                                                <div class="buttons inline">
                                                    <a class="btn disabled">Сотрудники</a>
                                                    <a class="btn btn-primary disabled">Добавить</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-lg-3 modify">
                                            <div class="wh-bg">
                                                <div class="clearfix text-left">
                                                    <div class="task-statistic take">
                                                        <p>Выполнил<strong>{{$organization->user->tasksExecutor()->count()}} заданий</strong></p>
                                                    </div>
                                                    <div class="task-statistic add">
                                                        <p>Поручил<strong>{{$organization->user->tasksCreator()->count()}} заданий</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{Form::open(['route'=>(['cabinet.profile.organization.edit',$user]),'id'=>'form'])}}
                                        @method('PUT')
                                        <div class="row">
                                            <div class="col-xs-12 input-group required">
                                                <label class="input-label" for="vante">Краткое название</label>
                                                {{Form::text('name',old('name',$organization->name),['id'=>'vante','class'=>'input','placeholder'=>'Укажите данные'])}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 input-group required">
                                                <label class="input-label" for="inn">ИНН</label>
                                                {{Form::text('inn',old('inn',$organization->inn),['placeholder'=>"Укажите данные",'id'=>'inn','class'=>'input'])}}
                                                @if ($errors->has('inn'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('inn') }}</strong></span>
                                                @endif
                                            </div>
                                            <div class="input-group required col-xs-12 col-sm-6">
                                                <label class="input-label">Мой город</label>
                                                <div class="country ui fluid search selection dropdown">
                                                    <input type="hidden" name="country">
                                                    {{Form::hidden('region_id',old('region_id',$organization->region->id))}}
                                                    <i class="dropdown icon"></i>
                                                    @if(!empty($organization->region))
                                                        <div class="text">{{$organization->region->name}}</div>
                                                    @else
                                                        <div class="default text">Укажите данные</div>
                                                    @endif
                                                    <div class="menu">
                                                        @foreach($regions as $id=>$name)
                                                            <div class="item" data-value="{{$id}}">{{$name}}</div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-group required col-xs-12 col-sm-6">
                                                <label class="input-label" for="InputJurAdr">Юридический адрес</label>
                                                {{Form::text('legal_address',old('legal_address',$organization->legal_address),['class'=>'input','id'=>'InputJurAdr',"placeholder"=>"Укажите данные",'required'=>true])}}
                                                @if ($errors->has('legal_address'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('legal_address') }}</strong></span>
                                                @endif
                                            </div>
                                            <div class="input-group required col-xs-12 col-sm-6">
                                                <label class="input-label" for="InputFactAdr">Фактический адрес</label>
                                                {{Form::text('fact_address',old('legal-address',$organization->fact_address),['class'=>'input','id'=>'InputFactAdr',"placeholder"=>"Укажите данные",'required'=>true])}}
                                                @if ($errors->has('fact_address'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('fact_address') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-group required col-xs-12 col-sm-6">
                                                <label class="input-label" for="InputEmail">E-mail</label>
                                                {{Form::text('email',old('email',$organization->email),['class'=>'input','id'=>'InputEmail',"placeholder"=>"Укажите данные", 'required'=>true])}}
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                                                @endif
                                            </div>
                                            <div class="input-group required col-xs-12 col-sm-6">
                                                <label class="input-label" for="InputPhone">Телефон</label>
                                                {{Form::text('phone',old('phone',$organization->phone),['class'=>'input','id'=>'InputPhone',"placeholder"=>"Укажите данные",'required'=>true])}}
                                                @if ($errors->has('phone'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 input-group">
                                                <label class="input-label" for="task-description">Об организации</label>
                                                {{Form::textarea('about_us',old('about_ud',$organization->about_us),['id'=>'task-description','class'=>"input",'placeholder'=>'Укажите данные','rows'=>2])}}
                                            </div>
                                        </div>
                                        <div id="category" data-number="{{\App\Entities\Category::NAME_START}}"></div>
                                        <p class="lead">Социальные сети</p>
                                        <div class="row socials">
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputVk"><svg width="21" height="20" viewBox="0 0 21 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M19.579 0.806C19.719 0.341 19.579 0 18.917 0H16.724C16.166 0 15.911 0.295 15.771 0.619C15.771 0.619 14.656 3.338 13.076 5.101C12.566 5.614 12.333 5.776 12.055 5.776C11.916 5.776 11.714 5.614 11.714 5.149V0.806C11.714 0.248 11.553 0 11.088 0H7.642C7.294 0 7.084 0.258 7.084 0.504C7.084 1.032 7.874 1.154 7.955 2.642V5.87C7.955 6.577 7.828 6.706 7.548 6.706C6.805 6.706 4.997 3.977 3.924 0.853C3.715 0.246 3.504 0.0010004 2.944 0.0010004H0.752C0.125 0.0010004 0 0.296 0 0.62C0 1.202 0.743 4.082 3.461 7.891C5.273 10.492 7.824 11.902 10.148 11.902C11.541 11.902 11.713 11.589 11.713 11.049V9.083C11.713 8.457 11.846 8.331 12.287 8.331C12.611 8.331 13.169 8.495 14.47 9.748C15.956 11.234 16.202 11.901 17.037 11.901H19.229C19.855 11.901 20.168 11.588 19.988 10.97C19.791 10.355 19.081 9.46 18.139 8.401C17.627 7.797 16.862 7.147 16.629 6.822C16.304 6.403 16.398 6.218 16.629 5.846C16.63 5.847 19.301 2.085 19.579 0.806Z" fill="#4D75A3"/>
                                                </svg>ВК</label>
                                            {{Form::text('social[vk]',old('social[vk]',$organization->socialNameByName('vk')),['class'=>"input", "id"=>"InputVk", 'placeholder'=>"Укажите данные"])}}
                                            @if ($errors->has('social[vk]'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('social[vk]') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputInst"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M8.99775 5.99872C7.34513 5.99872 5.99647 7.34738 5.99647 9C5.99647 10.6526 7.34513 12.0013 8.99775 12.0013C10.6504 12.0013 11.999 10.6526 11.999 9C11.999 7.34738 10.6504 5.99872 8.99775 5.99872ZM17.9993 9C17.9993 7.75716 18.0106 6.52558 17.9408 5.28499C17.871 3.84402 17.5423 2.56515 16.4886 1.51144C15.4326 0.455477 14.156 0.129006 12.715 0.0592092C11.4722 -0.0105879 10.2406 0.000669702 9 0.000669702C7.75716 0.000669702 6.52558 -0.0105879 5.28499 0.0592092C3.84402 0.129006 2.56515 0.457728 1.51144 1.51144C0.455477 2.5674 0.129006 3.84402 0.0592092 5.28499C-0.0105879 6.52783 0.000669702 7.75941 0.000669702 9C0.000669702 10.2406 -0.0105879 11.4744 0.0592092 12.715C0.129006 14.156 0.457729 15.4348 1.51144 16.4886C2.5674 17.5445 3.84402 17.871 5.28499 17.9408C6.52783 18.0106 7.75941 17.9993 9 17.9993C10.2428 17.9993 11.4744 18.0106 12.715 17.9408C14.156 17.871 15.4348 17.5423 16.4886 16.4886C17.5445 15.4326 17.871 14.156 17.9408 12.715C18.0128 11.4744 17.9993 10.2428 17.9993 9ZM8.99775 13.6179C6.44227 13.6179 4.37988 11.5555 4.37988 9C4.37988 6.44452 6.44227 4.38213 8.99775 4.38213C11.5532 4.38213 13.6156 6.44452 13.6156 9C13.6156 11.5555 11.5532 13.6179 8.99775 13.6179ZM13.8047 5.27148C13.2081 5.27148 12.7263 4.78966 12.7263 4.193C12.7263 3.59635 13.2081 3.11452 13.8047 3.11452C14.4014 3.11452 14.8832 3.59635 14.8832 4.193C14.8834 4.33468 14.8556 4.475 14.8015 4.60593C14.7474 4.73686 14.6679 4.85582 14.5677 4.956C14.4676 5.05618 14.3486 5.13561 14.2177 5.18975C14.0867 5.24388 13.9464 5.27166 13.8047 5.27148Z" fill="#ED4A60"/>
                                                </svg>
                                                Instagram</label>
                                            {{Form::text('social[instagram]',old('social.instagram',$organization->socialNameByName('instagram')),['class'=>"input", "id"=>"InputInst", 'placeholder'=>"Укажите данные"])}}
                                            @if ($errors->has('social[instagram]'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('social[instagram]') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputFb"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M9.999 0C4.477 0 0 4.477 0 9.999C0 14.989 3.656 19.125 8.437 19.878V12.89H5.897V9.999H8.437V7.796C8.437 5.288 9.93 3.905 12.213 3.905C13.307 3.905 14.453 4.1 14.453 4.1V6.559H13.189C11.949 6.559 11.561 7.331 11.561 8.122V9.997H14.332L13.889 12.888H11.561V19.876C16.342 19.127 19.998 14.99 19.998 9.999C19.998 4.477 15.521 0 9.999 0Z" fill="#395185"/>
                                                </svg>
                                                Facebook</label>
                                            {{Form::text('social[facebook]',old('social.facebook',$organization->socialNameByName('facebook')),['class'=>"input", "id"=>"InputFb", 'placeholder'=>"Укажите данные"])}}
                                            @if ($errors->has('social[facebook]'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('social[facebook]') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="input-group col-xs-12 col-sm-6 col-lg-3">
                                            <label class="input-label" for="InputOk"><svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M5.90953 0C3.00568 0 0.64343 2.31542 0.64343 5.16188C0.64343 8.00812 3.00568 10.3225 5.90953 10.3225C8.81327 10.3225 11.1744 8.00812 11.1744 5.16188C11.1744 2.31542 8.81329 0 5.90953 0ZM5.90953 3.02438C7.11135 3.02438 8.08866 3.98372 8.08866 5.16188C8.08866 6.33954 7.11137 7.29814 5.90953 7.29814C4.70746 7.29814 3.72913 6.33956 3.72913 5.16188C3.72913 3.98372 4.70746 3.02438 5.90953 3.02438ZM1.56788 10.4981C1.04687 10.4902 0.534528 10.7419 0.237321 11.2062C-0.216874 11.9136 0.000706028 12.847 0.720588 13.2919C1.6728 13.8775 2.70607 14.293 3.77761 14.5325L0.834713 17.4187C0.232261 18.0096 0.232771 18.9666 0.835345 19.5575C1.13737 19.8524 1.53128 20 1.92619 20C2.32069 20 2.71592 19.8522 3.01702 19.5569L5.90828 16.7213L8.80209 19.5569C9.40403 20.1476 10.3801 20.1476 10.9831 19.5569C11.5858 18.9664 11.5858 18.0084 10.9831 17.4188L8.03894 14.5331C9.11078 14.2936 10.1444 13.8779 11.096 13.2919C11.8173 12.847 12.0352 11.9126 11.5811 11.2063C11.1266 10.4988 10.1748 10.2856 9.453 10.7313C7.29714 12.0606 4.51934 12.0598 2.36416 10.7313C2.11608 10.5781 1.84079 10.5023 1.56788 10.4981V10.4981Z" fill="#E27E35"/>
                                                </svg>
                                                Одноклассники</label>
                                            {{Form::text('social[ok]',old('social.ok',$organization->socialNameByName('ok')),['class'=>"input", "id"=>"InputOk", 'placeholder'=>"Укажите данные"])}}
                                            @if ($errors->has('social[ok]'))
                                                <span class="invalid-feedback"><strong>{{ $errors->first('social[ok]') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Organization\CabinetEditRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script>
        $(document).ready(function(){

            /* start redact category */
            $('#category').load("{{route('cabinet.profile.organization.category')}}",{"_token":"{{csrf_token()}}",name:1,user:{{$user->id}},is_new:0},function(){
                let current = parseInt($('#category').attr('data-number'))
                $('#category .clearfix').each(function (){
                    $('#category').attr('data-number',current);
                    current++
                })
                $('.ui.dropdown').dropdown();
            });

            $('#category').on('click', '.add_cat', function(e) {
                e.preventDefault();
                let current = parseInt($('#category').attr('data-number'))+1;
                let parent = $(this).parents('#category').find('.row');
                $('#category').attr('data-number', current);
                axios.post(
                    "{{route('cabinet.profile.organization.category')}}",
                    {
                        "_token":"{{ csrf_token() }}",
                        name:current,
                        user:{{$user->id}},
                        is_new:1
                    }
                ).then(function (response){
                    parent.append(response.data)
                    $('.ui.dropdown').dropdown();
                })
            });

            $('#category').on('change','.clearfix input.category',function(){
                let category= $(this)
                let current = parseInt($('#category').attr('data-number'));
                axios.post(
                    "{{route('cabinet.profile.organization.sub.category')}}",
                    {
                        "_token":"{{ csrf_token() }}",
                        category_id:category.val(),
                        user_id:{{$user->id}},
                        name:current
                    }).then(function (response){
                    let menu=category.parents('.categories').siblings('.sub_categories').find('.menu');
                    menu.html(response.data)
                    $('.ui.dropdown').dropdown();
                })
            });

            $('#category').on('click', '.del_cat', function(e) {
                e.preventDefault();
                let current = $('#category').attr('data-number');
                $('#category').attr('data-number', parseInt(current) - 1);
                $(this).parent().remove();
            });
            /* end redact category */
            $('.submit').click(function (e){
                e.preventDefault()
                $('#form').submit()
            })

            $('.ui.dropdown').dropdown();
            $('.ui.dropdown.special').dropdown({useLabels: false});
            $('.scroll-block').scrollTop(

                $('.scroll-block').prop('scrollHeight')
            );
            if (document.body.clientWidth > 479){
                $('.wh-bg').matchHeight({ byRow:false, property: 'min-height' });
            }
            $( '.inputfile' ).each( function(){
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();
                $input.on( 'change', function( e ){
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });
                $input.on( 'focus', function(){ $input.addClass( 'has-focus' ); }).on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });
        });
        $('.inputfile').change(function(){
            $(this).parents('.fileForm').submit()
        });
    </script>
@endsection
