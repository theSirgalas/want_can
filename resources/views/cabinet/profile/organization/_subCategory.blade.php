
@foreach($subCategories as $subCategory)
    <div class="item @if(in_array($subCategory['id'],$userSubCategory)) active selected @endif" data-value="{{$subCategory['id']}}">
        {{$subCategory['name']}}
    </div>
@endforeach
