@if(\App\Entities\Category::NAME_START==$name)
    <p class="lead">Выберите категории в которых Ваш профиль будет отображаться</p>
    <div class="row">
@endif
        <div class="cat clearfix">
                <div class="input-group categories col-xs-12 col-sm-6 ">
                    <label class=" input-label">Категория</label>
                    <div class="ui category-dropdown fluid selection dropdown">
                        {{Form::hidden("category[$name][category]",old("category[$name][category]"),['class'=>'category'])}}
                        <i class="dropdown icon"></i>
                        <div class="default text">
                            <span class="hidden-xs">выберите категорию</span>
                            <span class="visible-xs">категория</span></div>
                        <div class="menu">
                            @foreach($parents as $category)
                                <div data-value="{{$category['id']}}" class="item" >
                                    @if(!empty($category['icon']))
                                        <img class="ui avatar image" src="{{$category['icon']}}">
                                    @endif
                                    {{$category['name']}}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="input-group sub_categories required col-xs-12 col-sm-6 sub-category">
                    <label class="input-label">Подкатегория</label>
                    <div class="ui fluid special multiple selection dropdown">
                        {{Form::hidden("category[$name][subCategory]",old("category[$name][subCategory]"),['class'=>'subCategory'])}}
                        <i class="dropdown icon"></i>
                        <div class="default text"><span class="hidden-xs">выберите подкатегорию</span><span class="visible-xs">подкатегория</span></div>
                        <div class="menu"></div>
                    </div>
                </div>
                @if(\App\Entities\Category::NAME_START<$name)
                    <a href="#" class="delete del_cat">
                        <svg viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.875 4.25008H11.3333V3.06716C11.3167 2.6137 11.121 2.18533 10.789 1.87595C10.4571 1.56658 10.016 1.40144 9.56251 1.41675H7.43751C6.984 1.40144 6.54292 1.56658 6.21098 1.87595C5.87903 2.18533 5.68329 2.6137 5.66667 3.06716V4.25008H2.12501C1.93714 4.25008 1.75698 4.32471 1.62414 4.45755C1.4913 4.59039 1.41667 4.77055 1.41667 4.95841C1.41667 5.14628 1.4913 5.32644 1.62414 5.45928C1.75698 5.59212 1.93714 5.66675 2.12501 5.66675H2.83334V13.4584C2.83334 14.022 3.05722 14.5625 3.45574 14.961C3.85425 15.3595 4.39475 15.5834 4.95834 15.5834H12.0417C12.6053 15.5834 13.1458 15.3595 13.5443 14.961C13.9428 14.5625 14.1667 14.022 14.1667 13.4584V5.66675H14.875C15.0629 5.66675 15.243 5.59212 15.3759 5.45928C15.5087 5.32644 15.5833 5.14628 15.5833 4.95841C15.5833 4.77055 15.5087 4.59039 15.3759 4.45755C15.243 4.32471 15.0629 4.25008 14.875 4.25008ZM7.08334 3.06716C7.08334 2.95383 7.23209 2.83341 7.43751 2.83341H9.56251C9.76792 2.83341 9.91667 2.95383 9.91667 3.06716V4.25008H7.08334V3.06716ZM12.75 13.4584C12.75 13.6463 12.6754 13.8264 12.5425 13.9593C12.4097 14.0921 12.2295 14.1667 12.0417 14.1667H4.95834C4.77048 14.1667 4.59031 14.0921 4.45747 13.9593C4.32463 13.8264 4.25001 13.6463 4.25001 13.4584V5.66675H12.75V13.4584Z"/>
                            <path d="M6.37501 12.0417C6.56287 12.0417 6.74303 11.9671 6.87587 11.8343C7.00871 11.7014 7.08334 11.5213 7.08334 11.3334V8.50008C7.08334 8.31222 7.00871 8.13205 6.87587 7.99921C6.74303 7.86638 6.56287 7.79175 6.37501 7.79175C6.18714 7.79175 6.00698 7.86638 5.87414 7.99921C5.7413 8.13205 5.66667 8.31222 5.66667 8.50008V11.3334C5.66667 11.5213 5.7413 11.7014 5.87414 11.8343C6.00698 11.9671 6.18714 12.0417 6.37501 12.0417ZM10.625 12.0417C10.8129 12.0417 10.993 11.9671 11.1259 11.8343C11.2587 11.7014 11.3333 11.5213 11.3333 11.3334V8.50008C11.3333 8.31222 11.2587 8.13205 11.1259 7.99921C10.993 7.86638 10.8129 7.79175 10.625 7.79175C10.4371 7.79175 10.257 7.86638 10.1241 7.99921C9.9913 8.13205 9.91667 8.31222 9.91667 8.50008V11.3334C9.91667 11.5213 9.9913 11.7014 10.1241 11.8343C10.257 11.9671 10.4371 12.0417 10.625 12.0417Z"/>
                        </svg>
                    </a>
                @endif
            </div>


    </div>
@if(\App\Entities\Category::NAME_START==$name)
    <div class="col-xs-12">
        <a href="#" class="btn btn-primary more add_cat">Добавить ещё категорию</a>
    </div>
@endif
