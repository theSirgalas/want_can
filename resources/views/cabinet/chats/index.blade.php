@extends('layouts.app')
@section('meta')
    <title>Сообщения</title>
@endsection
@section('content')
<section class="content">
    <div class="container">
        <div class="w-back">
            <h1>Мои сообщения</h1>
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    @include('cabinet.includes.menu',['user'=>$user,'view'=>'chats'])
                </div>
                <div class="col-xs-12 col-md-9">
                    <p class="lead">Всего чатов: {{$user->chatroomsCustomer()->count()+$user->chatroomsExecutor()->count()}}</p>
                    @if($first&&$chatrooms)
                        <chat-component :first="{{$first}}" :chatrooms="{{json_encode($chatrooms)}}" :user="{{$user}}"></chat-component>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.ui.dropdown').dropdown();
            $('.scroll-block').scrollTop(
                $('.scroll-block').prop('scrollHeight')
            );
            $(document).find('.scroll-block').scrollTop($(document).find('.scroll-block').prop('scrollHeight'));
            $(document).find('.chats-list a').click(function() {
                $(this).addClass('active').siblings().removeClass('active');
                $(document).find('.scroll-block').scrollTop($(document).find('.scroll-block').prop('scrollHeight'));
            });
        });
        $(window).on('load',function (){
            $('.chatroom:first').addClass('active');
        });
    </script>
@endsection
