@extends('layouts.app')
@section('meta')
    <title>Настройки пользователя</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back lk">
                <h1>Мои настройки</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('cabinet.includes.menu',['view'=>'setting','user'=>$user])
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <p class="lead">Изменить пароль</p>
                        <div class="row">
                            <div class="inline-nav col-xs-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Пароль</a></li>
                                    <li role="presentation" class="disabled"><a role="tab">Черный список <span>Скоро!</span></a></li>
                                    <li role="presentation" class="disabled"><a role="tab">Уведомления <span>Скоро!</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="password">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        {{Form::open(['route'=>(['cabinet.setting.edit',$user])])}}
                                            @method('PUT')
                                            <div class="input-group required">
                                                <label class="input-label" for="InputNewPass">Новый пароль</label>
                                                {{Form::password('password',['class'=>'input','id'=>'InputNewPass',"placeholder"=>"Новый пароль",'required'=>true])}}
                                                <button class="show-pass"><svg width="20" height="13"><use xlink:href="#icon-eye"></use></svg></button>
                                            </div>
                                            <div class="input-group required @if ($errors->has('password_confirmation')) has-error @endif">
                                                <label class="input-label" for="InputNewPassConfirm">Повторите пароль</label>
                                                {{Form::password('password_confirmation',['class'=>'input','id'=>'InputNewPassConfirm',"placeholder"=>"Повторите пароль",'required'=>true])}}
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="invalid-feedback"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                                @endif
                                                <button class="show-pass"><svg width="20" height="13"><use xlink:href="#icon-eye"></use></svg></button>
                                            </div>
                                            <div class="form-footer">
                                                <button type="submit" class="btn btn-default btn-lg">Изменить пароль</button>
                                            </div>
                                        {{Form::close()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\User\SettingRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.phone-button').click(function(){
                let opp = $(this).attr('data-phone'),
                    gopp = $(this).text();
                $(this).html('<svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>'+opp).attr('data-phone', gopp);
            });
            $('.ui.dropdown').dropdown();
            $('.show-pass').click(function(e){
                e.preventDefault();
                if($(this).hasClass('active')){
                    $(this).siblings('.input').attr('type','password');
                }else{
                    $(this).siblings('.input').attr('type','text');
                }
                $(this).toggleClass('active');
            });
        });
    </script>
@endsection
