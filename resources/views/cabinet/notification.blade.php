@extends('layouts.app')
@section('meta')
    <title>Уведомления</title>
@endsection
@section('content')
<section class="content">
    <div class="container">
        <div class="w-back lk">
            <h1>Мои уведомления</h1>
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    @include('cabinet.includes.menu',['view'=>'task','user'=>$user])
                </div>
                <div class="col-xs-12 col-md-9">
                    <p class="lead">Всего уведомлений: {{count($notifications)}}</p>
                    @foreach($notifications as $notification)
                    <div class="notify">
                        <!--<p class="h5">17 новых откликов</p>-->
                        <p>{!! $notification->notification !!}</p>
                        <a href="{{route('cabinet.notification.view',$notification)}}" class="btn btn-primary btn-sm link">Прочитано</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            $('.notify .link').click(function (e) {
                e.preventDefault();
               let urlAxios = $(this).attr('href');
                blockRemove = $(this).parents('.notify');
                axios.get(
                    urlAxios
                ).then(function (response) {
                        blockRemove.remove()
                })
            });
        });
    </script>
@endsection
