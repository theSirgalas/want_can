@extends('layouts.app')
@section('meta')
    <title>Задания где заказчик</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back lk">
                <h1>Мои объявления</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        @include('cabinet.includes.menu',['view'=>'task','user'=>$user])
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <p class="lead">Всего заданий: {{$taskCount}}</p>
                        <div class="row">
                            <div class="col-xs-12 col-lg-9">
                                <div class="radio-group inline" >
                                    <label class="btn btn-lg">
                                        <a href="{{route('cabinet.task.customer',$user)}}"> Я заказчик </a>
                                    </label>
                                    <label class="btn btn-lg active">
                                        <a href="{{route('cabinet.task.executor',$user)}}">Я исполнитель </a>
                                    </label>
                                </div>
                                <div class="tapes">
                                    <div class="radio">
                                        <label>
                                            <a href="{{route(
                                                        'cabinet.task.executor',
                                                        [
                                                            'user'=>$user,
                                                            'status'=>\App\Http\Search\TaskCabinetSearch::IS_OPEN]
                                                        )}}"
                                               @if($status==\App\Http\Search\TaskCabinetSearch::IS_OPEN)class="is_active" @endif
                                            >
                                                <span>Открыто</span>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <a href="{{route(
                                                    'cabinet.task.executor',
                                                    [
                                                    'user'=>$user,
                                                    'status'=>\App\Http\Search\TaskCabinetSearch::IS_WORK
                                                    ])}}"
                                            @if($status==\App\Http\Search\TaskCabinetSearch::IS_WORK)class="is_active" @endif
                                            >
                                                <span>Выполняется</span>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <a href="{{route(
                                                    'cabinet.task.executor',[
                                                    'user'=>$user,
                                                    'status'=>\App\Http\Search\TaskCabinetSearch::IS_FINISHED
                                                    ])}}"
                                               @if($status==\App\Http\Search\TaskCabinetSearch::IS_FINISHED)class="is_active" @endif
                                            >
                                                <span>Закрыто</span>
                                            </a>
                                        </label>
                                    </div>
                                </div>
                                @if($tasks->total()>0)
                                    @foreach($tasks as $task)
                                        @include('cabinet.task.includes.task',['task'=>$task])
                                    @endforeach
                                @else
                                    <div class="task-block clearfix">
                                        Объявлений нет
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-lg-3 link-check">
                                <div class="menu-lk-task">
                                    <div class="radio">
                                        <label class="task">
                                            <input name="ad-type" type="radio" checked>
                                            <span>Задания</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="vac">
                                            <input name="ad-type" type="radio" data-url="{{route('cabinet.resume',$user)}}">
                                            <span>Вакансии</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label class="res">
                                            <input name="ad-type" type="radio" data-url="{{route('cabinet.vacancy',$user)}}">
                                            <span>Резюме</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.ui.dropdown').dropdown();
            $('.scroll-block').scrollTop(
                $('.scroll-block').prop('scrollHeight')
            );
            $('.link-check .radio input').click(function (){
                const url =  $(this).data('url')
                if(url.length>0){
                    window.location.assign(url);
                }
            });
        });
    </script>
@endsection
