<div class="task-block clearfix">
    <div class="pull-left">
        <i class="icon">
            @if(is_object($task->category->icon))
                <img src="{{$task->category->icon->path}}" alt="{{$task->category->name}}">
            @endif</i>
        <p class="lead">
            <a href="{{route('task.show',$task->slug)}}">{{$task->title}}</a>
            @if($task->now)
                <span class="label important">
                    <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M10.5481 6.33575C10.2444 5.65203 9.80274 5.03836 9.25093 4.53313L8.79559 4.11533C8.78013 4.10154 8.76152 4.09175 8.74139 4.08682C8.72126 4.08189 8.70023 4.08198 8.68015 4.08708C8.66007 4.09218 8.64154 4.10213 8.6262 4.11606C8.61086 4.12999 8.59917 4.14747 8.59216 4.16697L8.38874 4.75063C8.262 5.11679 8.02885 5.49077 7.69868 5.85849C7.67677 5.88196 7.65174 5.88822 7.63452 5.88979C7.61731 5.89135 7.59071 5.88822 7.56724 5.86631C7.54533 5.84754 7.53438 5.81937 7.53594 5.7912C7.59384 4.84921 7.31218 3.78673 6.69566 2.63036C6.18555 1.66959 5.4767 0.920068 4.59104 0.397434L3.94479 0.017194C3.86029 -0.0328788 3.75232 0.0328416 3.75702 0.131422L3.79144 0.882513C3.81491 1.39576 3.75545 1.84954 3.61462 2.22665C3.4425 2.68826 3.19527 3.11701 2.87918 3.50194C2.65921 3.76946 2.40989 4.01143 2.13591 4.2233C1.47606 4.73055 0.93951 5.3805 0.566448 6.1245C0.194304 6.87499 0.000454036 7.70126 0 8.53894C0 9.27752 0.145524 9.99262 0.433442 10.667C0.711449 11.3164 1.11265 11.9057 1.61485 12.4024C2.12183 12.9031 2.71019 13.2974 3.36583 13.5713C4.04494 13.856 4.76473 14 5.508 14C6.25127 14 6.97106 13.856 7.65017 13.5728C8.3042 13.3006 8.89893 12.9036 9.40115 12.4039C9.90814 11.9032 10.3056 11.318 10.5826 10.6686C10.87 9.99603 11.0175 9.27194 11.016 8.54051C11.016 7.7769 10.8595 7.0352 10.5481 6.33575Z" fill="#FF8450"></path>
                    </svg>
                    <span class="hidden-xs">Срочно</span>
                </span>
            @endif
        </p>
        <p class="cat">
            <a href="{{route('task.category',$task->category->slug)}}">{{$task->category->name}}</a>
            @if(is_object($task->subCategory))
                <a href="{{route('task.index',$task->subCategory->slug)}}">{{$task->subCategory->name}}</a>
            @endif
        </p>
        <p class="adress">
            <i class="adress icon"></i>{{$task->to}}
        </p>
        <div class="time-block">
            <span class="day"><i class="calendar icon"></i>{{$task->created_at_array['date']}}</span>
            <span class="time"><i class="time icon"></i>{{$task->created_at_array['time']}}</span>
        </div>
        <div class="status">{{$task->status}}</div>
    </div>
    <div class="pull-right">
        <p class="lead price text-right">
            {{$task->price}}
        </p>
        <div class="man-block inline size-xxs text-left">
            <div class="left">
                @include('layouts.includes.avatar',['user'=>$task->user])
                <div class="about">
                    <a href="{{route('user',$task->user)}}">{{$task->user->short_name}}
                        @include('layouts.includes.user_moderate',['user'=>$task->user])
                    </a>
                    @include('layouts.includes.user_small_score',['user'=>$task->user])
                </div>
            </div>
        </div>
        @if(!$task->is_finished)
            @if(!is_object($task->application))
                <a href="{{route('task.resource.update',$task)}}" class="btn btn-primary btn-sm">Редактировать</a>
            @else
                <a id="ended" href="{{route('task.resource.delete',$user)}}" class="btn btn-primary btn-sm">Закрыть задач</a>
            @endif
        @else
        <a href="{{route('task.show',$task->slug)}}" class="btn btn-primary btn-sm">Просмотреть задачу</a>
        @endif
    </div>
</div>
