@extends('layouts.app')
@section('meta')
    <title>Безопасная оплата</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <h1>Безопасная оплата</h1>
            <p class="subhead">Удобный способ оплачивать задания онлайн и получать деньги за работу на банковскую карту</p>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <img class="article-image img-responsive" src="img/lauren-manck.png" alt="Картинка статьи">
                    <h3>Что такое «Сделка без риска»</h3>
                    <p>Сервис «Сделка без риска» позволяет исполнителям и заказчикам безопасно сотрудничать на YouDo. Стоимость задания списывается с банковской карты заказчика, резервируется до момента успешного завершения задания и переводится на банковскую карту исполнителя.* YouDo выплачивает компенсацию до 10 000 рублей, если в результате действий исполнителя заказчику причинен материальный ущерб. Условия выплаты компенсации</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="info-block">
                                <div class="info-img text-center">
                                    <div class="num">01</div>
                                    <img height="63" src="svg/cards-check.svg" alt="">
                                </div>
                                <p class="h5">Создание задания</p>
                                <p>Заказчик публикует задание, договаривается с исполнителем о стоимости, сроках и условиях работы.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                            <div class="info-block">
                                <div class="info-img text-center">
                                    <div class="num">02</div>
                                    <img height="63" src="svg/seif.svg" alt="">
                                </div>
                                <p class="h5">Резервирование оплаты</p>
                                <p>Когда исполнитель выбран, стоимость задания списывается с банковской карты заказчика и хранится на специальном счете до завершения задания.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-4">
                            <div class="info-block">
                                <div class="info-img text-center">
                                    <div class="num">03</div>
                                    <img height="63" src="svg/cards.svg" alt="">
                                </div>
                                <p class="h5">Оплата задания/возврат денег</p>
                                <p>Если задание не выполнено, денежные средства возвращаются на банковскую карту заказчика. Если задание успешно выполнено, деньги поступают на банковскую карту исполнителя.</p>
                            </div>
                        </div>
                    </div>
                    <p class="h3">Преимущества «Сделки без риска»</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="info-block type-2">
                                <div class="info-img text-left">
                                    <img height="79" src="svg/ref-1.svg" alt="">
                                </div>
                                <p class="lead">100% оплата работы</p>
                                <p>После завершения задания оплата отправляется исполнителю.</p>
                            </div>
                            <div class="info-block type-2">
                                <div class="info-img text-left">
                                    <img height="79" src="svg/ref-3.svg" alt="">
                                </div>
                                <p class="lead">Гарантированный возврат</p>
                                <p>Если задание не выполнено, деньги возвращаются заказчику.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="info-block type-2">
                                <div class="info-img text-left">
                                    <img height="79" src="svg/ref-2.svg" alt="">
                                </div>
                                <p class="lead">Безопасное сотрудничество</p>
                                <p>До завершения задания деньги хранятся на специальном счете в платежной системе.</p>
                            </div>
                            <div class="info-block type-2">
                                <div class="info-img text-left">
                                    <img width="65" height="79" src="svg/ref-4.svg" alt="">
                                </div>
                                <p class="lead">Удобная оплата</p>
                                <p>Оплата задания онлайн банковской картой.</p>
                            </div>
                        </div>
                    </div>
                    <p class="h3">Частые вопросы</p>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <strong>Исполнитель не выполнил задание. Задание выполнено, но заказчик это не подтверждает. Что делать?</strong>
                                    <svg width="10" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439156 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98196 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0185 0.831533 10.0091 0.929385C9.99968 1.02724 9.9702 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="url(#paint0_linear)"></path>
                                        <defs>
                                            <linearGradient id="paint0_linear" x1="10.0123" y1="1.41884e-06" x2="-0.340848" y2="2.76792" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#53EA98"></stop>
                                                <stop offset="0.515625" stop-color="#8EC5FC"></stop>
                                                <stop offset="1" stop-color="#AE8FC5"></stop>
                                            </linearGradient>
                                        </defs>
                                    </svg>
                                </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Какая комиссия удерживается за «Сделку без риска»?</strong>
                                    <svg width="10" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439156 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98196 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0185 0.831533 10.0091 0.929385C9.99968 1.02724 9.9702 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="url(#paint0_linear)"></path>
                                        <defs>
                                            <linearGradient id="paint0_linear" x1="10.0123" y1="1.41884e-06" x2="-0.340848" y2="2.76792" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#53EA98"></stop>
                                                <stop offset="0.515625" stop-color="#8EC5FC"></stop>
                                                <stop offset="1" stop-color="#AE8FC5"></stop>
                                            </linearGradient>
                                        </defs>
                                    </svg>
                                </a>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <strong>Как подключить «Сделку без риска»?</strong>
                                    <svg width="10" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439156 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98196 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0185 0.831533 10.0091 0.929385C9.99968 1.02724 9.9702 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="url(#paint0_linear)"></path>
                                        <defs>
                                            <linearGradient id="paint0_linear" x1="10.0123" y1="1.41884e-06" x2="-0.340848" y2="2.76792" gradientUnits="userSpaceOnUse">
                                                <stop stop-color="#53EA98"></stop>
                                                <stop offset="0.515625" stop-color="#8EC5FC"></stop>
                                                <stop offset="1" stop-color="#AE8FC5"></stop>
                                            </linearGradient>
                                        </defs>
                                    </svg>
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <aside class="hidden-xs col-sm-12 col-md-3">
                    <a href="{{route('task.resource.form')}}" class="btn btn-gradient btn-lg">Создать задание</a>
                    <a href="{{route('task.index')}}" class="btn btn-nofill btn-lg">Выбрать задание</a>
                </aside>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.type-2').matchHeight({ byRow:false, property: 'min-height' });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
