<div class="proposes closed">
    <p class="h3">Исполнитель задания</p>
    <div class="propose">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3">
                <div class="man-block inline size-xxs text-left">
                    <div class="left">
                        @include('layouts.includes.avatar',['user'=>$application->user])
                        <div class="about">
                            <a href="{{'user',$application->user->id}}">{{$application->user->name}}
                                @include('layouts.includes.user_moderate',['user'=>$application->user])
                            </a>
                            @include('layouts.includes.user_score',['user'=>$application->user])
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-9">
                <div class="quest">
                    <div class="price"><b>Исполнитель предложил <span>{{$application->price}} ₽</span></b></div>
                    <p>{{$application->comment}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
