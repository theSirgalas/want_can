<div class="proposes">
    <p class="h3">У задания {{$task->applications->count()}} предложение</p>
    <div class="divider horizontal left">Новые предложения</div>
    @foreach($task->new_applications as $application)
        <div class="propose">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-3">
                    <div class="man-block inline size-xxs text-left">
                        <div class="left">
                            @include('layouts.includes.avatar',['user'=>$application->user])
                            <div class="about">
                                <a href="{{route('user',$application->user)}}">{{$application->user->name}}
                                    @include('layouts.includes.user_moderate',['user'=>$application->user])
                                </a>
                                @include('layouts.includes.user_score',['user'=>$application->user])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-6">
                    <div class="quest">
                        <div class="price"><b>Исполнитель предложил <span>{{$application->price}} ₽</span></b></div>
                        <p>{{$application->comment}}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    {{Form::open(['route'=>(['task.application.add.general',$task->id])])}}
                        @method('POST')
                        {{Form::hidden('application',$application->id)}}
                        {{Form::submit('Выбрать исполнителя',['class'=>"btn btn-lg btn-gradient"])}}
                    {{Form::close()}}
                    <div class="btn btn-svg phone-button" data-phone="{{$application->user->phone}}"><svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>Показать телефон</div>
                    <a href="#" class="btn btn-svg chat-button">
                        <svg width="18" height="16">
                            <use xlink:href="#icon-message"></use>
                        </svg>Написать
                    </a>
                </div>
            </div>
        </div>
    @endforeach
    <div class="divider horizontal left">Просмотренные предложения</div>
    @foreach($task->view_applications as $application)
    <div class="propose">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3">
                <div class="man-block inline size-xxs text-left">
                    <div class="left">
                        @include('layouts.includes.avatar',['user'=>$application->user])
                        <div class="about">
                            <a href="{{route('user',$application->user)}}">{{$application->user->name}}
                                @include('layouts.includes.user_moderate',['user'=>$application->user])
                            </a>
                            @include('layouts.includes.user_score',['user'=>$application->user])
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-6">
                <div class="quest">
                    <div class="price"><b>Исполнитель предложил <span>{{$application->price}} ₽</span></b></div>
                    <p>{{$application->comment}}!</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                {{Form::open(['route'=>(['task.application.add.general',$task->id])])}}
                    @method('POST')
                    {{Form::hidden('application',$application->id)}}
                    {{Form::submit('Выбрать исполнителя',['class'=>"btn btn-lg btn-gradient"])}}
                {{Form::close()}}
                <div class="btn btn-svg phone-button" data-phone="{{$application->user->phone}}"><svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>Показать телефон</div>
                <a href="{{route('cabinet.message.chats',['user'=>Auth::user(),'opponents'=>$application->user])}}" class="btn btn-svg chat-button"><svg width="18" height="16"><use xlink:href="#icon-message"></use></svg>Написать</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
