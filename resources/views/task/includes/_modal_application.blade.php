<div class="modal fade" id="otclick" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p class="modal-title">Откликнуться на задание</p>
            </div>
            <div class="modal-body">
                {{Form::open(['route'=>(['task.application.create',$task])])}}
                <div class="form-group">
                    <label for="money-how">Стоимость выполнения</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="rub icon"></i></div>
                        {{Form::text('price_app',old('price_app'),['class'=>"input", 'id'=>"money-how"])}}
                        @if ($errors->has('price_app'))
                            <span class="invalid-feedback"><strong>{{ $errors->first('price_app') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment-from-me">Комментарий</label>
                    <div class="input-group">
                        {{Form::textarea('comment',old('comment'),["class"=>"input", 'rows'=>"6", 'placeholder'=>"укажите дополнительную информацию", 'id'=>"comment-from-me"])}}
                        @if ($errors->has('comment'))
                            <span class="invalid-feedback"><strong>{{ $errors->first('comment') }}</strong></span>
                        @endif
                    </div>
                </div>
                {{Form::button('Предложить свою услугу',['type'=>"submit", 'class'=>"btn btn-gradient btn-lg"])}}
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
