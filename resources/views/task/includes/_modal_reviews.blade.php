<div id="reviev-user" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <p class="modal-title">Оставить отзыв</p>
            </div>
            <div class="modal-body">
                <div class="rev-block">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="ans">
                                <small>Кого оцениваете</small>
                                <div class="man-block inline size-xxs text-left">
                                    <div class="left">
                                        @include('layouts.includes.avatar',['user'=>$task->application])
                                        <div class="about">
                                            <a href="{{route('user',$task->application->user->id)}}">{{$task->application->user->name}} </a>
                                            <small>Когда: {{$task->application->getDateCreate()}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="ans">
                                <small>Задание</small>
                                <p class="txt"><a href="{{route('task.show',$task->slug)}}">{{$task->title}}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                {{Form::open(['route'=>(['task.add.reviews',$task->application]),'class'=>'review-form'])}}
                @method('POST')
                <div class="form-group">
                    <label>Ваша оценка</label>
                    <div class="input-group">
                        {{Form::hidden('score',old('score'),['id'=>"backing-mark"])}}
                        <div id="rateit-mark" class="rateit svg" data-rateit-resetable="false" data-rateit-backingfld="#backing-mark" data-rateit-starwidth="17" data-rateit-starheight="16"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="review-from-me">Текст отзыва</label>
                    <div class="input-group">
                        {{Form::textarea('comment',old('comment'),
                            [
                                'class'=>"input",
                                'rows'=>"6",
                                'placeholder'=>"Ваш отзыв",
                                "id"=>"review-from-me"
                            ]
                        )}}
                    </div>
                </div>
                {{Form::submit('Оставить отзыв',["class"=>"btn btn-gradient btn-lg"])}}
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
