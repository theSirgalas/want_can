@if($task->isCustomer()&&!$task->is_finished)
    @if(!is_object($task->application))
        <a href="{{route('task.resource.update',$task)}}" class="btn btn-default btn-lg">Редактировать</a>
        {{Form::open(['route'=>(['task.resource.delete',$task])])}}
        @method('DELETE')
            {{Form::submit('Удалить',['class'=>"btn btn-lg delete"])}}
        {{Form::close()}}
    @else
        <a id="ended" href="{{route('task.resource.delete',$task)}}" class="btn btn-default btn-lg">Завершить задание</a>
    @endif
@else
    <a href="{{route('task.resource.form')}}" class="btn btn-default btn-lg">Создать новое задание</a>
@endif
@if($task->isPermissionApplication())
    <a data-toggle="modal" data-target="#otclick" href="#" class="btn btn-gradient btn-lg">Предложить свою услугу</a>
@endif
@if($task->isExecutor()&&!$task->application->is_close)
    <a href="{{route('task.application.refused',$task->application)}}" class="btn btn-gradient btn-lg">Отказаться от исполнеия услуги</a>
    <a href="{{route('task.application.closed',$task->application)}}" class="btn btn-gradient btn-lg">Закрыть задачу</a>
@endif
