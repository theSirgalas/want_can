<div class="calendar-block">
    <div class="ui calendar" id="date_calendar">
        <div class="ui input left icon">
            <i class="calendar icon"></i>
            {{Form::text('start_date',old('start_date',$startDate['date']),['placeholder'=>"00.00.0000"])}}
        </div>
    </div>
    <div class="ui calendar" id="no_ampm">
        <div class="ui input left icon">
            <i class="time icon"></i>
            {{Form::text('start_time',old('start_date',$startDate['time']),['placeholder'=>"00:00"])}}
        </div>
    </div>
</div>
