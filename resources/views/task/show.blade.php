@extends('layouts.app')
@section('meta')
    <title>Задание: {{$task->title}}</title>
@endsection
@section('content')
<section class="content">
    <div class="container">
        <div class="w-back task">
            <h1>{{$task->title}}
            @if($task->now)
                <span class="label important">
                    <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="SVG namespace">
                        <path d="M10.5481 6.33575C10.2444 5.65203 9.80274 5.03836 9.25093 4.53313L8.79559 4.11533C8.78013 4.10154 8.76152 4.09175 8.74139 4.08682C8.72126 4.08189 8.70023 4.08198 8.68015 4.08708C8.66007 4.09218 8.64154 4.10213 8.6262 4.11606C8.61086 4.12999 8.59917 4.14747 8.59216 4.16697L8.38874 4.75063C8.262 5.11679 8.02885 5.49077 7.69868 5.85849C7.67677 5.88196 7.65174 5.88822 7.63452 5.88979C7.61731 5.89135 7.59071 5.88822 7.56724 5.86631C7.54533 5.84754 7.53438 5.81937 7.53594 5.7912C7.59384 4.84921 7.31218 3.78673 6.69566 2.63036C6.18555 1.66959 5.4767 0.920068 4.59104 0.397434L3.94479 0.017194C3.86029 -0.0328788 3.75232 0.0328416 3.75702 0.131422L3.79144 0.882513C3.81491 1.39576 3.75545 1.84954 3.61462 2.22665C3.4425 2.68826 3.19527 3.11701 2.87918 3.50194C2.65921 3.76946 2.40989 4.01143 2.13591 4.2233C1.47606 4.73055 0.93951 5.3805 0.566448 6.1245C0.194304 6.87499 0.000454036 7.70126 0 8.53894C0 9.27752 0.145524 9.99262 0.433442 10.667C0.711449 11.3164 1.11265 11.9057 1.61485 12.4024C2.12183 12.9031 2.71019 13.2974 3.36583 13.5713C4.04494 13.856 4.76473 14 5.508 14C6.25127 14 6.97106 13.856 7.65017 13.5728C8.3042 13.3006 8.89893 12.9036 9.40115 12.4039C9.90814 11.9032 10.3056 11.318 10.5826 10.6686C10.87 9.99603 11.0175 9.27194 11.016 8.54051C11.016 7.7769 10.8595 7.0352 10.5481 6.33575Z" fill="#FF8450"></path>
                    </svg>
                </span>
            @endif
            </h1>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>
                                    <i class="icon categoria"></i>Категория
                                </label>
                                <div class="input output">
                                    <a href="#">
                                        @if(is_object($task->category->icon))
                                            <img src="{{$task->category->icon->path}}" alt="{{$task->category->name}}">
                                        @endif
                                        <span>{{$task->category->name}}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @if(is_object($task->subCategory))
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label>Подкатегория</label>
                                    <div class="input output">
                                        <a href="#">
                                            @if(is_object($task->subCategory->icon))
                                                <img src="{{$task->subCategory->icon->path}}" alt="{{$task->subCategory->name}}">
                                            @endif
                                            <span>{{$task->subCategory->name}}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label><i class="adress icon"></i>Откуда</label>
                                <div class="input output">
                                    <span>{{$task->from}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label><i class="adress icon"></i>Куда</label>
                                <div class="input output">
                                    <span>{{$task->to}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($task->date as $key=>$date)
                            <div class="col-xs-12 col-md-7 col-lg-6">
                                <div class="input-view">
                                    <label><i class="dat-cal icon"></i>Дата и время</label>
                                    <div class="input output">
                                        <span>{{$key}}</span><div class="time-block">
                                            <span class="day"><i class="calendar icon"></i>{{$date['date']}}</span>
                                            <span class="time"><i class="time icon"></i>{{$date['time']}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label><i class="rub icon"></i>Цена</label>
                                <div class="input output">
                                    <span>{{$task->price}} рублей</span> - <span>{{$task->price_type}}</span>
                                </div>
                            </div>
                        </div>
                        @if(is_object($task->payment))
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-view">
                                    <label><i class="payment icon"></i>Оплата</label>
                                    <div class="input output">
                                        <span>{{$task->payment->title}}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-view">
                                <label><i class="dop icon"></i>Дополнительно</label>
                                <div class="input output">
                                    <span>{{$task->description}}</span>
                                </div>
                            </div>
                        </div>
                        @if(is_object($task->file))
                            <div class="col-xs-12">
                                <div class="input-view">
                                    <label>Прикрепленные файлы</label><br/>
                                    <a class="upl-file" href="{{$task->file->path}}" target="_blank">
                                        <svg width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip02)">
                                                <path d="M6.91399 17.8525C4.53097 16.4654 3.72094 13.3989 5.10804 11.0159L8.62948 4.96616C8.81493 4.64755 9.22299 4.54029 9.54096 4.72538C9.85904 4.91053 9.9673 5.31832 9.78185 5.63693L6.26041 11.6867C5.24354 13.4336 5.83726 15.6829 7.58476 16.7001C9.33225 17.7173 11.5813 17.1227 12.5982 15.3758L16.2873 9.03789C16.9346 7.92592 16.5565 6.49485 15.4446 5.8476C14.3325 5.20029 12.9014 5.57832 12.2542 6.69029L8.9004 12.452C8.62308 12.9285 8.78505 13.542 9.26159 13.8193C9.73813 14.0967 10.3516 13.9346 10.6289 13.4581L13.815 7.98453C14.0004 7.66592 14.4085 7.55866 14.7265 7.74375C15.0445 7.9289 15.1528 8.33669 14.9673 8.6553L11.7813 14.1289C11.134 15.2409 9.70284 15.6189 8.59088 14.9716C7.47892 14.3244 7.10078 12.8932 7.74803 11.7813L11.1018 6.01952C12.1186 4.27266 14.3677 3.678 16.1153 4.69523C17.8628 5.71241 18.4565 7.9618 17.4397 9.70866L13.7505 16.0465C12.3634 18.4295 9.297 19.2396 6.91399 17.8525Z"></path>
                                            </g>
                                            <defs>
                                                <clipPath id="clip02">
                                                    <rect width="16" height="16" fill="white" transform="translate(8.04895) rotate(30.2028)"></rect>
                                                </clipPath>
                                            </defs>
                                        </svg>
                                        <span>{{$task->file->slug}}</span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                    @include('task.includes._buttons',['task'=>$task])
                </div>
                <div class="col-xs-12 col-md-3">
                        <div class="man-block size-lg text-center">
                            <div class="left">
                                @include('layouts.includes.avatar',['user'=>$task->user])
                                <div class="about">
                                    <a href="{{route('user',$task->user)}}">{{$task->user->name}}
                                        @include('layouts.includes.user_moderate',['user'=>$task->user])
                                    </a>
                                    @include('layouts.includes.user_score',['user'=>$task->user])
                                </div>
                            </div>
                        </div>
                        <div class="btn btn-svg phone-button" data-phone="{{$task->user->phone}}">
                            <svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>Показать телефон
                        </div>
                        @if(Auth::user())
                            <a href="{{route('cabinet.message.chats',['user'=>Auth::user(),'opponents'=>$task->user])}}" class="btn btn-svg chat-button">
                                <svg width="18" height="16">
                                    <use xlink:href="#icon-message"></use>
                                </svg>Написать
                            </a>
                        @endif
                    </div>
            </div>
        </div>
    </div>
        @if($task->isCustomer())
            @if(is_object($task->application))
                @include('task.includes._application',['application'=>$task->application])
            @else
                @include('task.includes._applications',['task'=>$task])
            @endif
        @endif

    </div>
    @if($task->isPermissionApplication())
        @include('task.includes._modal_application')
    @endif
    @if(is_object($task->application)&&!$task->is_finished)
        @include('task.includes._modal_reviews')
    @endif
</section>
@endsection
@section('scripts')

    <script>
        $(document).ready(function(){
            $('.phone-button').click(function(){
                let opp = $(this).attr('data-phone'),
                    gopp = $(this).text();
                $(this).html('<svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>'+opp).attr('data-phone', gopp);
            });
            $('.ui.dropdown').dropdown();
            @if(is_object($task->application)&&!$task->is_finished)
                $('#ended').click(function(e){
                    e.preventDefault();
                    $('#reviev-user').modal();
                });
            @endif
        });
    </script>
@endsection
