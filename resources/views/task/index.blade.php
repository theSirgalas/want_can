@extends('layouts.app')
@section('meta')
    <title>Задания</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="h1-header inline">
                <h1>Список заданий</h1>
                <p class="subhead">Выберите подходящее задание и “Хочу выполнить”, чтобы предложить заказчику свои услуги</p>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    @foreach($tasks as $task)
                        <div class="task-block clearfix">
                        <div class="pull-left">
                            <i class="icon">
                                @if(is_object($task->category->icon))
                                    <img src="{{$task->category->icon->path}}" alt="{{$task->category->name}}" width="28" height="28">
                                @endif
                            </i>
                            <p class="lead">
                                <a href="{{route('task.show',$task->slug)}}">{{$task->title}}</a>
                                @if($task->now)
                                    <span class="label important">
                                        <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.5481 6.33575C10.2444 5.65203 9.80274 5.03836 9.25093 4.53313L8.79559 4.11533C8.78013 4.10154 8.76152 4.09175 8.74139 4.08682C8.72126 4.08189 8.70023 4.08198 8.68015 4.08708C8.66007 4.09218 8.64154 4.10213 8.6262 4.11606C8.61086 4.12999 8.59917 4.14747 8.59216 4.16697L8.38874 4.75063C8.262 5.11679 8.02885 5.49077 7.69868 5.85849C7.67677 5.88196 7.65174 5.88822 7.63452 5.88979C7.61731 5.89135 7.59071 5.88822 7.56724 5.86631C7.54533 5.84754 7.53438 5.81937 7.53594 5.7912C7.59384 4.84921 7.31218 3.78673 6.69566 2.63036C6.18555 1.66959 5.4767 0.920068 4.59104 0.397434L3.94479 0.017194C3.86029 -0.0328788 3.75232 0.0328416 3.75702 0.131422L3.79144 0.882513C3.81491 1.39576 3.75545 1.84954 3.61462 2.22665C3.4425 2.68826 3.19527 3.11701 2.87918 3.50194C2.65921 3.76946 2.40989 4.01143 2.13591 4.2233C1.47606 4.73055 0.93951 5.3805 0.566448 6.1245C0.194304 6.87499 0.000454036 7.70126 0 8.53894C0 9.27752 0.145524 9.99262 0.433442 10.667C0.711449 11.3164 1.11265 11.9057 1.61485 12.4024C2.12183 12.9031 2.71019 13.2974 3.36583 13.5713C4.04494 13.856 4.76473 14 5.508 14C6.25127 14 6.97106 13.856 7.65017 13.5728C8.3042 13.3006 8.89893 12.9036 9.40115 12.4039C9.90814 11.9032 10.3056 11.318 10.5826 10.6686C10.87 9.99603 11.0175 9.27194 11.016 8.54051C11.016 7.7769 10.8595 7.0352 10.5481 6.33575Z" fill="#FF8450"></path>
                                    </svg>
                                    <span class="hidden-xs">Срочно</span></span></p>
                                @endif
                            <p class="cat">
                                <a href="{{route('task.category',$task->category->slug)}}">{{$task->category->name}}</a>
                                @if(is_object($task->subCategory))
                                    <a href="{{route('task.index',$task->subCategory->slug)}}">{{$task->subCategory->name}}</a>
                                @endif
                            </p>
                            <p class="adress">
                                <i class="adress icon"></i>{{$task->to}}</p>
                            <div class="time-block">

                                <span class="day"><i class="calendar icon"></i>{{$task->created_at_array['date']}}</span>
                                <span class="time"><i class="time icon"></i>{{$task->created_at_array['time']}}</span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <p class="lead price text-right">
                                {{$task->price}} ₽
                            </p>
                            <div class="man-block inline size-xxs text-left">
                                <div class="left">
                                        @include('layouts.includes.avatar',['user'=>$task->user])
                                    <div class="about">
                                        <a href="{{route('user',$task->user)}}">{{$task->user->short_name}}
                                            @include('layouts.includes.user_moderate',['user'=>$task->user])
                                        </a>
                                        @include('layouts.includes.user_score',['user'=>$task->user])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <aside class="hidden-xs hidden-sm col-md-3">
                    <a href="{{route('task.resource.form')}}" class="btn btn-gradient btn-lg">Создать задание</a>
                    <div class="filter">
                        {{Form::open(['route'=>(['task.index']),'id'=>'form_search','method'=>'get'])}}
                            <div class="checkbox">
                                <label>
                                    {{Form::checkbox(
                                        'all',
                                        1,
                                        !empty($requestArr['all'])?true:null,
                                        ['class'=>'check-all']
                                        )
                                    }}
                                    <span>Все категории</span>
                                </label>
                            </div>
                            @foreach($categories as $key=>$category)
                            <div class="cat-block">
                                <div class="checkbox">
                                    <label>
                                        {{Form::checkbox(
                                            'categories[]',
                                            $category['id'],
                                            App\Helpers\ArrayHelpers::searchInArray($category['id'],$requestArr,'categories')?true:null,
                                            ['class'=>"check-cat" ])
                                        }}
                                        <span>{{$category['name']}}</span>
                                    </label>
                                    <button class="col-lap" type="button" data-toggle="collapse" data-target="#collapseFilter_{{$key}}" aria-expanded="false" aria-controls="collapseFilter_1">
                                        <svg width="11" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439157 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98195 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0184 0.831533 10.0091 0.929385C9.99968 1.02724 9.97019 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="#252525"/>
                                        </svg>
                                    </button>
                                </div>
                                @if(!empty($category['sub_category']))
                                    <div class="collapse" id="collapseFilter_{{$key}}" aria-expanded="false">
                                        <div class="subcats">
                                            @foreach($category['sub_category'] as $subcategory)
                                                <div class="checkbox checkbox-sm">
                                                    <label>
                                                        {{Form::checkbox(
                                                            'sub_categories[]',
                                                            $subcategory['id'],
                                                            App\Helpers\ArrayHelpers::searchInArray($subcategory['id'],$requestArr,'sub_categories')?true:false,
                                                            ["class"=>"check-subcat"]
                                                            )
                                                        }}
                                                        <span>{{$subcategory['name']}}</span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                </div>
                                @endif
                            </div>
                        @endforeach
                        {{Form::close()}}
                    </div>
                </aside>
                {{$tasks->render('layouts.includes.paginate') }}
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            $('.filter input').change(function () {
                if ($(this).hasClass('check-all')) {
                    if ($(this).is(":checked")) {
                        $(this).parents('.filter').find('.cat-block').find('input').prop('checked', true);
                        $(this).parents('.filter').find('.cat-block').find('input.check-cat').parents('.checkbox').removeClass('here-checked');
                    } else {
                        $(this).parents('.filter').find('.cat-block').find('input').prop('checked', false);
                    }
                } else if ($(this).hasClass('check-cat')) {
                    let elements = $(this).parents('.cat-block').siblings().find('input.check-cat'), flag_arr = [],
                        flag_end, flag = $(this).is(":checked");
                    elements.each(function (index) {
                        if ($(this).is(":checked")) {
                            flag_arr[index] = 10;
                        } else {
                            flag_arr[index] = 11;
                        }
                    });
                    flag_end = $.inArray(11, flag_arr);
                    if (flag) {
                        $(this).parents('.cat-block').find('input').prop('checked', true);
                        $(this).parents('.checkbox').removeClass('here-checked');
                    } else {
                        $(this).parents('.cat-block').find('input').prop('checked', false);
                    }
                    if ((flag_end == -1) && (flag)) {
                        $(this).parents('.filter').find('.check-all').prop('checked', true);
                    } else {
                        $(this).parents('.filter').find('.check-all').prop('checked', false);
                    }
                } else {
                    let elements = $(this).parents('.checkbox').siblings().find('input'), flag_arr = [], flag_end,
                        flag = true, el_flag = $(this).is(":checked");
                    elements.each(function (index) {
                        if ($(this).is(":checked")) {
                            flag_arr[index] = 10;
                        } else {
                            flag = false;
                            flag_arr[index] = 11;
                        }
                    });
                    flag_end = $.inArray(10, flag_arr);
                    if (el_flag && flag) {
                        $(this).parents('.cat-block').find('.check-cat').prop('checked', true);
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').removeClass('here-checked');
                        let elementes = $(this).parents('.cat-block').siblings().find('input.check-cat'),
                            flag_top_arr = [], fl;
                        elementes.each(function (index) {
                            if ($(this).is(":checked")) {
                                flag_top_arr[index] = 10;
                            } else {
                                flag_top_arr[index] = 11;
                            }
                        });
                        fl = $.inArray(11, flag_top_arr);
                        if (fl == -1) {
                            $(this).parents('.filter').find('.check-all').prop('checked', true);
                        } else {
                            $(this).parents('.filter').find('.check-all').prop('checked', false);
                        }
                    } else {
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').addClass('here-checked');
                        $(this).parents('.cat-block').find('.check-cat').prop('checked', false);
                        $(this).parents('.filter').find('input.check-all').prop('checked', false);
                    }
                    if ((flag_end == -1) && (!el_flag)) {
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').removeClass('here-checked');
                    }
                }
                $('#form_search').submit()
            });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
