<div class="calendar-block">
    <div class="ui calendar" id="date_calendar">
        <div class="ui input left icon">
            <i class="calendar icon"></i>
            {{Form::text('finish_date',old('start_date',$finishDate['date']),['placeholder'=>"00:00"])}}
        </div>
    </div>
    <div class="ui calendar" id="no_ampm">
        <div class="ui input left icon">
            <i class="time icon"></i>
            {{Form::text('finish_time',old('start_date',$finishDate['time']),['placeholder'=>"00:00"])}}
        </div>
    </div>
</div>
