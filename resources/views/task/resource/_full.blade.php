<div class="calendar-block">
    <span style="margin-left: 20px;">Начало</span>
    <div class="ui calendar" id="date_calendar-period-start">
        <div class="ui input left icon">
            <i class="calendar icon"></i>
            {{Form::text('start_date',old('start_date',$startDate['date']),['placeholder'=>"00.00.0000"])}}
        </div>
    </div>
    <div class="ui calendar" id="no_ampm-period-start">
        <div class="ui input left icon">
            <i class="time icon"></i>
            {{Form::text('start_time',old('start_date',$startDate['time']),['placeholder'=>"00:00"])}}
        </div>
    </div>
</div>
<span>Конец</span>
<div class="calendar-block">
    <div class="ui calendar" id="date_calendar-period-end">
        <div class="ui input left icon">
            <i class="calendar icon"></i>
            {{Form::text('finish_date',old('start_date',$finishDate['date']),['placeholder'=>"00:00"])}}
        </div>
    </div>
    <div class="ui calendar" id="no_ampm-period-end">
        <div class="ui input left icon">
            <i class="time icon"></i>
            {{Form::text('finish_time',old('start_date',$finishDate['time']),['placeholder'=>"00:00"])}}
        </div>
    </div>
</div>
