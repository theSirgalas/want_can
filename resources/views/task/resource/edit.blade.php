@extends('layouts.app')
@section('meta')
    <title>Отредактировать задание: {{$task->title}} </title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                {{ Form::open(['route'=>(['task.resource.edit',$task]),'files' => true])}}
                @method('PUT')
                    <div class="col-xs-12 col-md-9">
                        <div class="w-back">
                            <h1>Отредактировать задание: {{$task->title}}</h1>
                            <p class="subhead">Опишите вашу задачу — исполнители сами откликнутся на ваш заказ</p>
                            <div class="row">
                                <div class="input-group col-xs-12 col-md-6">
                                    <label class="input-label">Категория</label>
                                    <div class="ui category-dropdown fluid selection dropdown">
                                        {{Form::hidden('category_id',old('category_id',$task->category_id),['id'=>"category"])}}
                                        <i class="dropdown icon"></i>
                                        @if(is_object($task->category))
                                            <div class="text">
                                                @if(is_object($task->category->icon))
                                                    <img class="ui avatar image"  src="{{$task->category->icon->path}}">
                                                @else
                                                    <img class="ui avatar image"  src="/svg/cat-holder.svg">
                                                @endif
                                                {{$task->category->name}}
                                            </div>
                                        @else
                                            <div class="default text">Выберите категорию</div>
                                        @endif
                                        <div class="menu">
                                            @foreach($parentCategories as $category)
                                                <div data-value="{{$category['id']}}" class="item @if($category['id']==$task->category->id) active selected @endif">
                                                    <img class="ui avatar image" src="{{$category['icon']}}">

                                                    {{$category['name']}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div id="sub_category" class="input-group col-xs-12 col-md-6">
                                    <label class="input-label">Подкатегория</label>
                                    <div class="ui fluid selection dropdown">
                                        <input type="hidden" name="subcategory">
                                        {{Form::hidden('sub_category_id',old('sub_category_id',$task->subCategory->id))}}
                                        <i class="dropdown icon"></i>

                                        @if(is_object($task->subCategory))
                                            <div class="text">
                                                @if(is_object($task->subCategory->icon))
                                                    <img class="ui avatar image"  src="{{$task->subCategory->icon->path}}">
                                                @endif
                                                {{$task->subCategory->name}}
                                            </div>
                                        @else
                                            <div class="default text">выберите подкатегорию</div>
                                        @endif
                                        <div class="menu"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 input-group">
                                    <label class="input-label" for="vante">Я хочу</label>
                                    {{Form::text('title',old('title',$task->title),['id'=>"vante", 'class'=>'input', 'placeholder'=>"заполните поле"])}}
                                </div>
                            </div>
                            <div class="row">
                                @if(is_object($task->file))
                                    <div class="col-xs-12">
                                        <div class="input-view">
                                            <label>Прикрепленные файлы</label><br/>
                                            <a class="upl-file" href="{{$task->file->path}}" target="_blank">
                                                <svg width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                                                    <g clip-path="url(#clip02)">
                                                        <path d="M6.91399 17.8525C4.53097 16.4654 3.72094 13.3989 5.10804 11.0159L8.62948 4.96616C8.81493 4.64755 9.22299 4.54029 9.54096 4.72538C9.85904 4.91053 9.9673 5.31832 9.78185 5.63693L6.26041 11.6867C5.24354 13.4336 5.83726 15.6829 7.58476 16.7001C9.33225 17.7173 11.5813 17.1227 12.5982 15.3758L16.2873 9.03789C16.9346 7.92592 16.5565 6.49485 15.4446 5.8476C14.3325 5.20029 12.9014 5.57832 12.2542 6.69029L8.9004 12.452C8.62308 12.9285 8.78505 13.542 9.26159 13.8193C9.73813 14.0967 10.3516 13.9346 10.6289 13.4581L13.815 7.98453C14.0004 7.66592 14.4085 7.55866 14.7265 7.74375C15.0445 7.9289 15.1528 8.33669 14.9673 8.6553L11.7813 14.1289C11.134 15.2409 9.70284 15.6189 8.59088 14.9716C7.47892 14.3244 7.10078 12.8932 7.74803 11.7813L11.1018 6.01952C12.1186 4.27266 14.3677 3.678 16.1153 4.69523C17.8628 5.71241 18.4565 7.9618 17.4397 9.70866L13.7505 16.0465C12.3634 18.4295 9.297 19.2396 6.91399 17.8525Z"></path>
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip02">
                                                            <rect width="16" height="16" fill="white" transform="translate(8.04895) rotate(30.2028)"></rect>
                                                        </clipPath>
                                                    </defs>
                                                </svg>
                                                <span>{{$task->file->slug}}</span>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-xs-12 input-group">
                                    <label class="input-label" for="task-description">Опишите задание</label>
                                    <div class="file-input-group inline">
                                        {{Form::file('file',["id"=>"file", "class"=>"inputfile", "data-multiple-caption"=>"{count} файла выбрано"])}}
                                        <label for="file">
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip02)">
                                                    <path d="M6.91399 17.8525C4.53097 16.4654 3.72094 13.3989 5.10804 11.0159L8.62948 4.96616C8.81493 4.64755 9.22299 4.54029 9.54096 4.72538C9.85904 4.91053 9.9673 5.31832 9.78185 5.63693L6.26041 11.6867C5.24354 13.4336 5.83726 15.6829 7.58476 16.7001C9.33225 17.7173 11.5813 17.1227 12.5982 15.3758L16.2873 9.03789C16.9346 7.92592 16.5565 6.49485 15.4446 5.8476C14.3325 5.20029 12.9014 5.57832 12.2542 6.69029L8.9004 12.452C8.62308 12.9285 8.78505 13.542 9.26159 13.8193C9.73813 14.0967 10.3516 13.9346 10.6289 13.4581L13.815 7.98453C14.0004 7.66592 14.4085 7.55866 14.7265 7.74375C15.0445 7.9289 15.1528 8.33669 14.9673 8.6553L11.7813 14.1289C11.134 15.2409 9.70284 15.6189 8.59088 14.9716C7.47892 14.3244 7.10078 12.8932 7.74803 11.7813L11.1018 6.01952C12.1186 4.27266 14.3677 3.678 16.1153 4.69523C17.8628 5.71241 18.4565 7.9618 17.4397 9.70866L13.7505 16.0465C12.3634 18.4295 9.297 19.2396 6.91399 17.8525Z" fill="#252525"/>
                                                </g>
                                                <defs>
                                                    <clipPath id="clip02">
                                                        <rect width="16" height="16" fill="white" transform="translate(8.04895) rotate(30.2028)"/>
                                                    </clipPath>
                                                </defs>
                                            </svg>
                                            <span>прикрепить фото</span></label>
                                    </div>
                                    {{Form::textarea('description',old('description',$task->description),["class"=>"input", "id"=>"task-description", "placeholder"=>"заполните поле"])}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group col-xs-6 w100">
                                    <label for="from" class="input-label">Откуда</label>
                                    {{Form::text('from',old('from',$task->from),['class'=>"input adress", 'id'=>"from", 'placeholder'=>"заполните поле"])}}

                                </div>
                                <div class="input-group col-xs-6 w100">
                                    <label for="where" class="input-label">Куда</label>
                                    {{Form::text('to',old('to',$task->to),['class'=>"input adress", 'id'=>"where", 'placeholder'=>"заполните поле"])}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group required @if($task->start_date&&$task->finish_date) @else col-xs-12 col-sm-8 col-md-6 @endif ">
                                    <label class="input-label">Дата и время</label>
                                    <div class="with-bg">
                                        <div id="time-select" class="ui inline selection dropdown">
                                            <input type="hidden" name="work-time-type" id="time-type" value="sev">
                                            <i class="dropdown icon"></i>
                                            @if($task->start_date&&$task->finish_date)
                                                <div class="text">Указать период</div>
                                            @else
                                                @if($task->start_date)
                                                    <div class="text">Начать работу</div>
                                                @endif
                                                @if($task->finish_date)
                                                    <div class="text">Закончить работу</div>
                                                @endif
                                            @endif
                                            <div class="menu">
                                                <div class="item active" data-value="start">Начать работу</div>
                                                <div class="item" data-value="finis">Закончить работу</div>
                                                <div class="item" data-value="full">Указать период</div>
                                            </div>
                                        </div>
                                        <div id="time-block"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-6">
                                    <div class="checkbox important">
                                        {{Form::checkbox('now',1,old('now',$task->now)?true:null,['id'=>'important'])}}
                                        <label for="important">
                                            <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.5481 6.33575C10.2444 5.65203 9.80274 5.03836 9.25093 4.53313L8.79559 4.11533C8.78013 4.10154 8.76152 4.09175 8.74139 4.08682C8.72126 4.08189 8.70023 4.08198 8.68015 4.08708C8.66007 4.09218 8.64154 4.10213 8.6262 4.11606C8.61086 4.12999 8.59917 4.14747 8.59216 4.16697L8.38874 4.75063C8.262 5.11679 8.02885 5.49077 7.69868 5.85849C7.67677 5.88196 7.65174 5.88822 7.63452 5.88979C7.61731 5.89135 7.59071 5.88822 7.56724 5.86631C7.54533 5.84754 7.53438 5.81937 7.53594 5.7912C7.59384 4.84921 7.31218 3.78673 6.69566 2.63036C6.18555 1.66959 5.4767 0.920068 4.59104 0.397434L3.94479 0.017194C3.86029 -0.0328788 3.75232 0.0328416 3.75702 0.131422L3.79144 0.882513C3.81491 1.39576 3.75545 1.84954 3.61462 2.22665C3.4425 2.68826 3.19527 3.11701 2.87918 3.50194C2.65921 3.76946 2.40989 4.01143 2.13591 4.2233C1.47606 4.73055 0.93951 5.3805 0.566448 6.1245C0.194304 6.87499 0.000454036 7.70126 0 8.53894C0 9.27752 0.145524 9.99262 0.433442 10.667C0.711449 11.3164 1.11265 11.9057 1.61485 12.4024C2.12183 12.9031 2.71019 13.2974 3.36583 13.5713C4.04494 13.856 4.76473 14 5.508 14C6.25127 14 6.97106 13.856 7.65017 13.5728C8.3042 13.3006 8.89893 12.9036 9.40115 12.4039C9.90814 11.9032 10.3056 11.318 10.5826 10.6686C10.87 9.99603 11.0175 9.27194 11.016 8.54051C11.016 7.7769 10.8595 7.0352 10.5481 6.33575Z" fill="#FF8450"/>
                                            </svg>
                                            Срочно
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group required col-xs-12 col-sm-7 col-md-6">
                                    <label class="input-label">Цена</label>
                                    <div class="with-bg">
                                        {{Form::text('price',old('price',$task->price),['placeholder'=>'например, 500 рублей'])}}
                                        <div class="ui inline selection dropdown">
                                            {{Form::hidden('type_price',old('type_price',$task->type_price))}}
                                            <i class="dropdown icon"></i>

                                            @if(!empty($task->type_price))
                                                <div class="text">{{$task->price_type}}</div>
                                            @else
                                                <div class="default text">тип оплаты</div>
                                            @endif
                                            <div class="menu">
                                                @foreach($priceType as $key=>$type)
                                                    <div class="item @if($task->type_price==$key) active selected @endif" data-value="{{$key}}">{{$type}}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group required col-xs-12 col-sm-5 col-md-6">
                                    <label class="input-label">Оплата</label>
                                    <div class="ui fluid selection dropdown">
                                        {{Form::hidden('payment_handbook_id',old('payment_handbook_id',$task->payment->id))}}
                                        <i class="dropdown icon"></i>
                                            @if(is_object($task->payment))
                                                <div class="text">{{$task->payment->id}}</div>
                                            @else
                                                <div class="default text">выберите cпособ оплаты</div>
                                            @endif
                                        <div class="menu">
                                            @foreach($handbooks as $key=>$handbook)
                                                <div class="item @if(is_object($task->payment)&&$task->payment->id==$key) active selected @endif" data-value="{{$key}}">{{$handbook}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group required col-xs-12">
                                    <label class="input-label">Место оказаная услуг</label>
                                    <div class="radio-group" data-toggle="buttons">
                                        @foreach($placeServices as $key=>$placeService)
                                            <label class="btn btn-lg @if($key===old('place_service',$task->place_service)) active @endif">
                                                {{Form::radio('place_service',old('place_service',$task->place_service),$key==old('place_service',$remote)?true:false)}}
                                                <span class="hidden-xs">{{$placeService}}</span>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    {{Form::checkbox('is_need_documents',1, old('is_need_documents',$task->is_need_documents)?true:null)}}
                                    <span>Я использую хочу-могу.рф для бизнеса, нужны закрывающие документы</span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    {{Form::checkbox('is_safe_payment',1, old('is_safe_payment',$task->is_safe_payment)?true:null,['class'=>'disable'])}}
                                    <span><a href="{{route('safe_payment')}}">Безопасная оплата</a> и гарантия возврата денег</span>
                                </label>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-gradient btn-lg">Редактировать задание</button>
                            </div>
                        </div>
                    </div>
                {{Form::close()}}
                <aside class="col-xs-12 col-md-3">
                    <p class="lead">Частые вопросы</p>
                    <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <strong>Исполнитель не выполнил задание. Задание выполнено, но заказчик это не подтверждает. Что делать?</strong>
                                </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <strong>Какая комиссия удерживается за «Сделку без риска»?</strong>
                                </a>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <strong>Как подключить «Сделку без риска»?</strong>
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Task\EditRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('body').on('change','#category',function(){
                axios.post(
                    "{{route('task.resource.sub.category')}}",
                    {
                        "_token":"{{ csrf_token() }}",
                        category_id:$(this).val(),
                        sub_category_id:{{$task->sub_category_id}}
                    }).then(function (response){
                        let menu=$('#sub_category').find('.menu');
                        menu.html(response.data)
                        $('.ui.dropdown').dropdown();
                })
            });
            $('#sub_category .menu').load("{{route('task.resource.sub.category')}}",{"_token":"{{ csrf_token() }}",category_id:{{$task->category->id}}, sub_category_id:{{$task->sub_category_id}}},function(){$('.ui.dropdown').dropdown();});
            var today = new Date();
            $( '.inputfile' ).each( function(){
                var $input	 = $( this ),
                    $label	 = $input.next( 'label' ),
                    labelVal = $label.html();
                $input.on( 'change', function( e ){
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else if( e.target.value )
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        $label.find( 'span' ).html( fileName );
                    else
                        $label.html( labelVal );
                });
                $input.on( 'focus', function(){ $input.addClass( 'has-focus' ); }).on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
            });
            $('.ui.dropdown').dropdown();

            $( "#time-block" ).load("{{route('task.resource.date',$task->id)}}",{"_token":"{{ csrf_token() }}"}, function() {
                $('#no_ampm').calendar({
                    type: 'time',
                    ampm: false
                });
                $('#date_calendar').calendar({
                    type: 'date',
                    monthFirst:false,
                    firstDayOfWeek: 1,
                    minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
                    text: {
                        days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                        monthsShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                        today: 'Сегодня',
                        now: 'Сейчас'
                    },
                    formatter: {
                        date: function (date, settings) {
                            if (!date) return '';
                            var day = date.getDate();
                            var month = date.getMonth() + 1;
                            var year = date.getFullYear();
                            return day + '.' + month + '.' + year;
                        }
                    }
                });
            });
            $('#time-select').dropdown({onChange: function(value, text, $selectedItem) {
                    console.log('YES');
                    $( "#time-block" ).load( value,{"_token":"{{ csrf_token() }}",'task_id':{{$task->id}}}, function() {
                        if(value!='full'){
                            $selectedItem.parents('.input-group').addClass('col-sm-8 col-md-6');
                            $selectedItem.parents('.input-group').siblings().find('.important').removeClass('one-line');
                            $('#no_ampm').calendar({
                                type: 'time',
                                ampm: false
                            });
                            $('#date_calendar').calendar({
                                type: 'date',
                                monthFirst:false,
                                firstDayOfWeek: 1,
                                minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
                                text: {
                                    days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                                    monthsShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                                    today: 'Сегодня',
                                    now: 'Сейчас'
                                },
                                formatter: {
                                    date: function (date, settings) {
                                        if (!date) return '';
                                        var day = date.getDate();
                                        var month = date.getMonth() + 1;
                                        var year = date.getFullYear();
                                        return day + '.' + month + '.' + year;
                                    }
                                }
                            });
                        }else{
                            $selectedItem.parents('.input-group').removeClass('col-sm-8 col-md-6');
                            $selectedItem.parents('.input-group').siblings().find('.important').addClass('one-line');
                            $('#no_ampm-period-start,#no_ampm-period-end').calendar({
                                type: 'time',
                                ampm: false
                            });
                            $('#date_calendar-period-start').calendar({
                                type: 'date',
                                monthFirst:false,
                                firstDayOfWeek: 1,
                                minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
                                endCalendar: $('#date_calendar-period-end'),
                                text: {
                                    days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                                    monthsShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                                    today: 'Сегодня',
                                    now: 'Сейчас'
                                },
                                formatter: {
                                    date: function (date, settings) {
                                        if (!date) return '';
                                        var day = date.getDate();
                                        var month = date.getMonth() + 1;
                                        var year = date.getFullYear();
                                        return day + '.' + month + '.' + year;
                                    }
                                }
                            });
                            $('#date_calendar-period-end').calendar({
                                type: 'date',
                                monthFirst:false,
                                firstDayOfWeek: 1,
                                startCalendar: $('#date_calendar-period-start'),
                                text: {
                                    days: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                                    monthsShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                                    today: 'Сегодня',
                                    now: 'Сейчас'
                                },
                                formatter: {
                                    date: function (date, settings) {
                                        if (!date) return '';
                                        var day = date.getDate();
                                        var month = date.getMonth() + 1;
                                        var year = date.getFullYear();
                                        return day + '.' + month + '.' + year;
                                    }
                                }
                            });
                        }
                    });
                }
            });
        });

    </script>
@endsection
