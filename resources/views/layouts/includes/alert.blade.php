<script>
    @if(session('success'))
        $('body').toast({
            title: '{{session('success')}}',
            showImage: '/svg/success-ico.svg',
            classImage: 'mini'
        });
    @endif
    @if(session('error'))
        $('body').toast({
            title: 'Ошибка!',
            message:'{{session('error')}}',
            showImage: '/svg/warning-ico.svg',
            classImage: 'mini'
        });
    @endif
</script>

