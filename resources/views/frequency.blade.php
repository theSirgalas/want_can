@extends('layouts.app')
@section('meta')
    <title>Частые вопросы</title>
@endsection
@section('content')
    <section class="content">
    <div class="container">
        <h1>Частые вопросы</h1>
        <p class="subhead">Выберите подходящую тему</p>
        <div class="row">
            <div class="col-xs-12 col-md-6">
            @foreach($frequencyTitles as $title)

                <div class="questions-category">
                    <p class="h3">
                        <a href="#">{{$title->title}}</a>
                    </p>
                    <p>{{$title->description}}</p>
                    <div class="questions-expand">
                        <button class="btn" type="button" data-toggle="collapse" data-target="#collapseQuestion_{{$count}}" aria-expanded="false" aria-controls="collapseQuestion_1">
                            В разделе {{count($title->sections)}} вопросов
                            <svg width="10" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439156 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98196 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0185 0.831533 10.0091 0.929385C9.99968 1.02724 9.9702 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="url(#paint0_linear)"/>
                                <defs>
                                    <linearGradient id="paint0_linear" x1="10.0123" y1="1.41884e-06" x2="-0.340848" y2="2.76792" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#53EA98"/>
                                        <stop offset="0.515625" stop-color="#8EC5FC"/>
                                        <stop offset="1" stop-color="#AE8FC5"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                        </button>
                        <div class="collapse" id="collapseQuestion_{{$count}}">
                            <div class="well">
                                @foreach($title->sections as $section)
                                    <div class="question">
                                    <a href="#">{{$section->title}}</a>
                                    <span>{{$section->description}}</span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @if($count == (int)$inTwo)
                </div>
                <div class="col-xs-12 col-md-6">
            @endif
            @php
                $count++;
            @endphp
            @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
