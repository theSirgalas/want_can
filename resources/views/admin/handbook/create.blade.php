@extends('adminlte::page')

@section('title', 'Создание справочника')

@section('content')
    {{dump($errors)}}
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Создать </h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.handbook.store'])])}}
            @csrf

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text('title', old('title'),['class'=>$errors->has('title')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"title",'required'=>true])}}
                @if ($errors->has('title'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="code" class="col-form-label">Код</label>
                {{Form::text('code',old('code'),['class'=>$errors->has('code')?"form-control is_invalid":"form-control", 'placeholder'=>"Код", "id"=>"code",'required'=>false])}}
                @if ($errors->has('code'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('code') }}</strong></span>
                @endif

            <div class="form-group">
                <label for="short-description" class="col-form-label">Сокращенное описание</label>
                {{Form::textarea('short-description',old('short-description'),['class'=>$errors->has('short-description')?"form-control is_invalid":"form-control", 'placeholder'=>"Сокращенное описание", "id"=>"short-description",'required'=>false])}}
                @if ($errors->has('short-description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('short-description') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="description" class="col-form-label">Полное название</label>
                {{Form::textarea('description',old('description'),['class'=>$errors->has('code')?"form-control is_invalid":"form-control", 'placeholder'=>"Полное название", "id"=>"description",'required'=>false])}}
                @if ($errors->has('description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="handbookTitle" class="col-form-label">Заголовок*</label>
                {{Form::select('handbook_title_id',$handbookTitle,old('handbook_title_id'),['class'=>$errors->has('handbook_title_id')?"form-control is_invalid":"form-control",  "id"=>"handbookTitle",'required'=>true])}}
                @if ($errors->has('handbook_title_id'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('handbook_title_id') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
