@extends('adminlte::page')

@section('title', $handbook->title)

@section('content_header')
    <p class="h1">{{$handbook->title}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$handbook->title}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.handbook.edit',$handbook)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.handbook.destroy', $handbook],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Ключ</th>
                        <th scope="col">Значение</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>{{$handbook->id}}</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{$handbook->title}}</td>
                    </tr>
                    <tr>
                        <td>Код</td>
                        <td>{{$handbook->code}}</td>
                    </tr>
                    <tr>
                        <td>Сокращенное название</td>
                        <td>{{$handbook->short_description}}</td>
                    </tr>
                    <tr>
                        <td>Название</td>
                        <td>{{$handbook->description}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
