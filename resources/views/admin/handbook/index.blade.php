@extends('adminlte::page')
@section('plugins.Datatables', true)

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="row">
                <div class="col-10"><p class="h3">Загаловки справочника</p> </div>
                <div class="col-2">
                    <a href="{{route('admin.handbook.create')}}" class="btn btn-primary">Создать</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Имя</th>
                        <th>email</th>
                        <th>Дата регистрации</th>
                        <th>Роль</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>{{Form::text('title',old('title'),['id'=>'title','class'=>'form-control'])}}</th>
                        <th>{{Form::text('code',old('code'),['id'=>'code','class'=>'form-control'])}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($handbooks as $handbook)
                    <tr>
                        <td>{{$handbook->title}}</td>
                        <td>{{$handbook->code}}</td>
                        <td>
                            <a href="{{route('admin.handbook.edit',$handbook)}}"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{route('admin.handbook.show',$handbook)}}"><i class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        let table = $('#example').DataTable();

        $('#title').keyup( function() {
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let title =  $('#title').val();
                    if (data[0].toLowerCase().indexOf(title)>=0){
                        return true;
                    }
                    return false;
                }
            );
            table.draw();
        } );
        $('#code').keyup( function() {
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let code =  $('#code').val();
                    if (data[1].toLowerCase().indexOf(code)>=0){
                        return true;
                    }
                    return false;
                }
            );
            table.draw();
        } );
    });
</script>
@stop
