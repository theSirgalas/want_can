@extends('adminlte::page')

@section('title',$organization->name)

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-10">{{$organization->name}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.organization.edit',$organization)}}">Редактировать</a></div>

            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Ключ</th>
                        <th scope="col">Значение</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>{{$organization->id}}</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{$organization->name}}</td>
                    </tr>
                    <tr>
                        <td>Инн</td>
                        <td>{{$organization->inn}}</td>
                    </tr>
                    <tr>
                        <td>Регион</td>
                        <td>{{$organization->region->name}}</td>
                    </tr>
                    <tr>
                        <td>Юридический адрес</td>
                        <td>{{$organization->legal_address}}</td>
                    </tr>
                    <tr>
                        <td>Фактический адрес</td>
                        <td>{{$organization->fact_address}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{$organization->email}}</td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td>{{$organization->phone}}</td>
                    </tr>
                    <tr>
                        <td>О компании</td>
                        <td>{{$organization->about_us}}</td>
                    </tr>
                    <tr>
                        <td>Пользователь создавший компанию</td>
                        <td><a href="{{route('admin.user.view',$organization->user)}}">{{$organization->user->name}}</a></td>
                    </tr>
                    <tr>
                        <td>Статус</td>
                        <td>{{$organization->status}}</td>
                    </tr>
                    <tr>
                        <td>Дата регистрации</td>
                        <td>{{$organization->create_at}}</td>
                    </tr>
                    <tr>
                        <td>Дата последнего редактирования</td>
                        <td>{{$organization->create_at}}</td>
                    </tr>
                    <tr>
                        <td>Документы</td>
                        <td>
                            @if(is_object($organization->files))
                                @foreach($organization->files as $file)
                                    <img src="{{$file->path}}" alt="">
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
