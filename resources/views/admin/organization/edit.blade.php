@extends('adminlte::page')

@section('title', 'Редактирование юр.лицо :'.$organization->name)

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Редактировать юр.лицо {{$organization->name}}</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.organization.edit',$organization])])}}
            @method('PUT')
            <div class="form-group">
                <label for="parent" class="col-form-label">Статус</label>
                {{Form::select('moderation_status',$statuses,old('moderation_status',$organization->moderation_status),['class'=>$errors->has('moderation_status')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"parent",'required'=>true])}}
                @if ($errors->has('moderation_status'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('moderation_status') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
