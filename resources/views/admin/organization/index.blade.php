@extends('adminlte::page')
@section('plugins.Datatables', true)

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="row">
                <div class="col-10"><p class="h3">Юр.лица</p> </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><label for="name">Название</label></th>
                    <th><label for="inn">Инн</label></th>
                    <th><label for="email">Email</label></th>
                    <th><label for="phone">Телефон</label></th>
                    <th><label for=status">Статус</label></th>
                    <th><label for="date">Дата регистрации</label></th>
                    <th></th>
                </tr>
                <tr>
                    <th>{{Form::text('name',old('name'),['id'=>'name','class'=>'form-control'])}}</th>
                    <th>{{Form::text('inn',old('inn'),['id'=>'inn','class'=>'form-control'])}}</th>
                    <th>{{Form::text('email',old('email'),['id'=>'email','class'=>'form-control'])}}</th>
                    <th>{{Form::text('phone',old('email'),['id'=>'email','class'=>'form-control'])}}</th>
                    <th>{{Form::select('status',$statusList, old('status'),['id'=>'status','class'=>'form-control'])}}</th>
                    <th>{{Form::text('date',old('date'),['id'=>'date','class'=>'form-control'])}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($organizations as $organization)
                    <tr>
                        <td>{{$organization->name}}</td>
                        <td>{{$organization->inn}}</td>
                        <td>{{$organization->email}}</td>
                        <td>{{$organization->phone}}</td>
                        <td>{{$organization->status}}</td>
                        <td>{{$organization->create_at}}</td>
                        <td>
                            <a href="{{route('admin.organization.edit',$organization)}}"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{route('admin.organization.show',$organization)}}"><i class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            let table = $('#example').DataTable();
            $('#name').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let userName =  $('#name').val();
                        if (data[0].toLowerCase().indexOf(userName)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#inn').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let inn =  $('#inn').val();
                        if (data[1].toLowerCase().indexOf(inn)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#email').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let email =  $('#email').val();
                        if (data[2].toLowerCase().indexOf(email)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#phone').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let phone =  $('#phone').val();
                        if (data[3].toLowerCase().indexOf(phone)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#status').change( function() { $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let status = $('#status').val();
                    if (parseInt(data[4])==parseInt(status)){
                        return true;
                    }
                    return false;
                }
            );
                table.draw();
            } );
            $('#date').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                    separator:  ' : ',
                    applyLabel: "Применить",
                    cancelLabel: "Отменить",
                    monthNames: [
                        "Январь",
                        "февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                }
            });
            $('#date').on("apply.daterangepicker",function(ev,picker){
                let date = $(this).val().split(' : ')
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        if (moment(data[5]).isSameOrAfter(date[0]) && moment(data[5]).isSameOrBefore(date[1]) ){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            });
        } );
    </script>
@stop
