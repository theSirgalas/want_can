@extends('adminlte::page')
@section('plugins.Datatables', true)
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="row">
                <div class="col-10"><p class="h3">Часто задаваемые вопросы заголовки</p> </div>
                <div class="col-2">
                    <a href="{{route('admin.frequency.title.create')}}" class="btn btn-primary">Создать</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($frequencies as $frequency)
                    <tr>
                        <td>{{$frequency->title}}</td>
                        <td>
                            <a href="{{route('admin.frequency.title.edit',$frequency)}}"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{route('admin.frequency.title.show',$frequency)}}"><i class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('js')
<script>

    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@stop
