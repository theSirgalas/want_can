@extends('adminlte::page')

@section('title', $frequencyTitle->title)

@section('content_header')
    <p class="h1">{{$frequencyTitle->title}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$frequencyTitle->title}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.frequency.title.edit',$frequencyTitle)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.frequency.title.destroy', $frequencyTitle],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$frequencyTitle->id}}</td>
                </tr>
                <tr>
                    <td>Имя</td>
                    <td>{{$frequencyTitle->title}}</td>
                </tr>
                <tr>
                    <td>Ссылка</td>
                    <td>{{$frequencyTitle->description}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
