@extends('adminlte::page')

@section('plugins.FileInput', true)

@section('title', 'Редактирование часто задоваемого вопроса :'.$frequencyTitle->title)

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Редактировать регион {{$frequencyTitle->title}}</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.frequency.title.edit',$frequencyTitle]),'files' => true])}}
            @method('PUT')

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text(
                    'title',
                    old('title',$frequencyTitle->title),
                        [
                            'class'=>$errors->has('title')?"form-control is_invalid":"form-control",
                            'placeholder'=>"Название*",
                            "id"=>"title",
                            'required'=>true
                        ]
                    )}}
                @if ($errors->has('title'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                <label for="parent" class="col-form-label">Описание</label>
                {{Form::textarea(
                    'description',
                    old('description',$frequencyTitle->description),
                    [
                        'class'=>$errors->has('description')?"form-control is_invalid":"form-control",
                        'placeholder'=>"Название*",
                        "id"=>"title",
                        'required'=>true
                    ]
                )}}
                @if ($errors->has('description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
@section('js')
@stop
