@extends('adminlte::page')

@section('title', $frequencySection->title)

@section('content_header')
    <p class="h1">{{$frequencySection->title}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$frequencySection->title}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.frequency.section.edit',$frequencySection)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.frequency.section.destroy', $frequencySection],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$frequencySection->id}}</td>
                </tr>
                <tr>
                    <td>Название</td>
                    <td>{{$frequencySection->title}}</td>
                </tr>
                <tr>
                    <td>Описание</td>
                    <td>{{$frequencySection->short_description}}</td>
                </tr>
                <tr>
                    <td>Описание</td>
                    <td>{{$frequencySection->description}}</td>
                </tr>
                @if(is_object($frequencySection->frequencyTitle))
                    <tr>
                        <td>Ссылка</td>
                        <td>{{$frequencySection->frequencyTitle->title}}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@stop
