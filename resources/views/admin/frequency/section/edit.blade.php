@extends('adminlte::page')

@section('plugins.FileInput', true)

@section('title', 'Редактирование под заголовка часто задаваемого вопроса :'.$frequencySection->title)

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Редактирование под заголовка часто задаваемого вопроса {{$frequencySection->title}}</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.frequency.section.edit',$frequencySection]),'files' => true])}}
            @method('PUT')

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text(
                    'title',
                    old('title',$frequencySection->title),
                        [
                            'class'=>$errors->has('title')?"form-control is_invalid":"form-control",
                            'placeholder'=>"Название*",
                            "id"=>"title",
                            'required'=>true
                        ]
                    )}}
                @if ($errors->has('title'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                <label for="parent" class="col-form-label">Короткое описание</label>
                {{Form::text(
                    'short_description',
                    old('short_description',$frequencySection->short_description),
                    [
                        'class'=>$errors->has('short_description')?"form-control is_invalid":"form-control",
                        'placeholder'=>"Название*",
                        "id"=>"title",
                        'required'=>true
                    ]
                )}}
                @if ($errors->has('description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                <label for="parent" class="col-form-label">Описание</label>
                {{Form::textarea(
                    'description',
                    old('description',$frequencySection->description),
                    [
                        'class'=>$errors->has('description')?"form-control is_invalid":"form-control",
                        'placeholder'=>"Название*",
                        "id"=>"title",
                        'required'=>true
                    ]
                )}}
                @if ($errors->has('description'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="parent" class="col-form-label">Заголовок</label>
                {{Form::select('frequency_titles_id',$titles,old('frequency_titles_id',$frequencySection->frequency_titles_id),['class'=>$errors->has('frequency_titles_id')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"title",'required'=>true])}}
                @if ($errors->has('frequency_titles_id'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('frequency_titles_id') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
@section('js')
@stop
