@extends('adminlte::page')
@section('plugins.Datatables', true)
@section('plugins.Select2', true)
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="row">
                <div class="col-10"><p class="h3">Часто задаваемые вопросы под заголовки</p> </div>
                <div class="col-2">
                    <a href="{{route('admin.frequency.section.create')}}" class="btn btn-primary">Создать</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Заголовок</th>
                    <th></th>
                </tr>
                <tr>
                    <th>{{Form::text('title',old('title'),['id'=>'title','class'=>'form-control'])}}</th>
                    <th>{{Form::select('frequency_title_id',$titles,old('frequency_title_id'),['id'=>'frequency_title','class'=>'form-control'])}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($sections as $section)
                    <tr>
                        <td>{{$section->title}}</td>
                        <td>{{is_object($section->frequencyTitle)?$section->frequencyTitle->title:null}}</td>
                        <td>
                            <a href="{{route('admin.frequency.section.edit',$section)}}"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{route('admin.frequency.section.show',$section)}}"><i class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('js')
    <script>

        $(document).ready(function() {
            let table = $('#example').DataTable();
            $('#title').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let name =  $('#title').val();
                        if (data[0].toLowerCase().indexOf(name.toLowerCase())>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );

            $('#frequency_title').select2({ placeholder: "Выберите из списка",allowClear: true,language: "ru",});

            $('#frequency_title').on('select2:select', function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let category =  $('#frequency_title').val();
                        if (data[1].indexOf(category)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );

        } );
    </script>
@stop
