@extends('adminlte::page')

@section('title', $category->name)

@section('content_header')
    <p class="h1">{{$category->name}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$category->name}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.category.edit',$category)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.category.destroy', $category],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$category->id}}</td>
                </tr>
                <tr>
                    <td>Имя</td>
                    <td>{{$category->name}}</td>
                </tr>
                <tr>
                    <td>Ссылка</td>
                    <td>{{$category->slug}}</td>
                </tr>
                @if(is_object($category->parent))
                    <tr>
                        <td>Регион родитель</td>
                        <td><a href="{{route('admin.region.show',$category->parent)}}">{{$category->parent->name}}</td>
                    </tr>
                @endif
                @if(is_object($category->icon))
                    <tr>
                        <td>Иконка</td>
                        <td><img width="100" src="{{$category->icon->path}}"></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@stop
