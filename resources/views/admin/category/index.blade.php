@extends('adminlte::page')
@section('plugins.Datatables', true)
@section('plugins.Select2', true)

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="row">
                <div class="col-10"><p class="h3">Категории</p> </div>
                <div class="col-2">
                    <a href="{{route('admin.category.create')}}" class="btn btn-primary">Создать</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Родитель</th>
                    <th></th>
                </tr>
                <tr>
                    <th>{{Form::text('name',old('name'),['id'=>'name','class'=>'form-control'])}}</th>
                    <th>{{Form::select('category_id',$categoryParent,old('category'),['id'=>'category','class'=>'form-control'])}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>
                            @if(is_object($category->parent))
                                {{$category->parent->name}}
                            @else
                                не установлено
                            @endif
                        </td>
                        <td>
                            <a href="{{route('admin.category.edit',$category)}}"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="{{route('admin.category.show',$category)}}"><i class="far fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('js')
<script>

    $(document).ready(function() {
        let table = $('#example').DataTable();
        $('#name').keyup( function() {
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let name =  $('#name').val();
                    if (data[0].toLowerCase().indexOf(name.toLowerCase())>=0){
                        return true;
                    }
                    return false;
                }
            );
            table.draw();
        } );
        $('#category').select2({ placeholder: "Выберите из списка",allowClear: true,language: "ru",});
        $('#category').on('select2:select', function() {
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let category =  $('#category').val();
                    if (data[1].indexOf(category)>=0){
                        return true;
                    }
                    return false;
                }
            );
            table.draw();
        } );
    });
</script>
@stop
