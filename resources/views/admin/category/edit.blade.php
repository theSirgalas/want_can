@extends('adminlte::page')
@section('plugins.Select2', true)
@section('title', 'Редактировать катергорию '.$category->name)
@section('content_header')
    <h1>Редактировать катергорию {{$category->name}}</h1>
@stop

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Редактировать катергорию {{$category->name}}</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.category.edit',$category]),'files' => true])}}
            @method('PUT')

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text('name', old('name',$category->name),['class'=>$errors->has('name')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"name",'required'=>true])}}
                @if ($errors->has('name'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="icon" class="col-form-label">Название</label>
                {{Form::file('icon',['class'=>$errors->has('icon')?"form-control is_invalid":"form-control","id"=>"icon"])}}
                @if ($errors->has('icon'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('icon') }}</strong></span>
                @endif
            </div>
            @if(is_object($category->parent))
                <div class="form-group">
                    <label for="parent" class="col-form-label">Родительская категория</label>
                    {{Form::select('parent',$parents,old('parent',$parent_category_id
),['class'=>$errors->has('parent')?"form-control is_invalid":"form-control", 'placeholder'=>"Название", "id"=>"parent"])}}
                    @if ($errors->has('parent'))
                        <span class="invalid-feedback"><strong>{{ $errors->first('parent') }}</strong></span>
                    @endif
                </div>
            @endif

            <div class="form-group">
                {{Form::text(
                    'turn',
                    old('turn',$category->turn),
                    [
                        'class'=>$errors->has('turn')?"form-control is_invalid":"form-control",
                         'placeholder'=>"Название*",
                         "id"=>"title"]
                    )
                 }}
            </div>
            <div class="form-group">
                <label for="icon" class="col-form-label">Описание</label>
                {{Form::textarea(
                    'description',
                    old('description'),
                        [
                            'class'=>$errors->has('description')?"form-control is_invalid":"form-control",
                             'placeholder'=>"Описание",
                             "id"=>"title"
                        ]
                    )
                 }}
            </div>
            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
@section('js')
    <script>
        $(window).on('load',function (){
            $.ajax({
                url: "{{route('admin.category.preview',$category)}}",
                type:"GET",
                dataType:'json',
                success: function (msg){
                    console.log(msg);
                    $("#icon").fileinput(msg);
                },
            })
            $('#parent').select2();
        });

    </script>
@stop
