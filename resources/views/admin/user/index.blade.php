@extends('adminlte::page')
@section('plugins.DateRange', true)
@section('plugins.Datatables', true)

@section('title', 'Пользователи')


@section('content_header')
    <h1>Пользователи</h1>
@stop

@section('content')
   <div class="card shadow mb-4">
       <div class="card-body">
           <table id="example" class="table table-striped table-bordered">
               <thead>
               <tr>
                   <th>Имя</th>
                   <th>email</th>
                   <th>Дата регистрации</th>
                   <th>Роль</th>
                   <th></th>
               </tr>
               <tr>
                   <th>{{Form::text('name',old('name'),['id'=>'name','class'=>'form-control'])}}</th>
                   <th>{{Form::text('email',old('email'),['id'=>'email','class'=>'form-control'])}}</th>
                   <th>{{Form::text('date',old('date'),['id'=>'date','class'=>'form-control'])}}</th>
                   <th>{{Form::select('role',$rolesList,old('role'),['id'=>'role','class'=>'form-control'])}}</th>
                   <th></th>
               </tr>
               </thead>
               <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->dateFromTable}}</td>
                            <td>{{$user->roleName}}</td>
                            <td>
                                <a href="{{route('admin.user.form',$user)}}"><i class="fas fa-edit"></i></a>&nbsp;
                                <a href="{{route('admin.user.view',$user)}}"><i class="far fa-eye"></i></a>

                            </td>
                        </tr>
                    @endforeach
               </tbody>
           </table>
       </div>
   </div>
@stop

@section('js')
    <script>

        $(document).ready(function() {
            let table = $('#example').DataTable();

            $('#name').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let userName =  $('#name').val();
                        if (data[0].toLowerCase().indexOf(userName)>=0){
                             return true;
                         }
                         return false;
                        }
                    );
                table.draw();
            } );
            $('#email').keyup( function() {
                $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let email =  $('#email').val();
                    if (data[1].toLowerCase().indexOf(email)>=0){
                         return true;
                     }
                     return false;
                    }
                );
                table.draw();
            } );
            $('#role').change( function() { $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let role =  $('#role').val();
                    if (data[3].indexOf(role)>=0){
                         return true;
                     }
                     return false;
                    }
                );
                table.draw();
            } );
            $('#date').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                    separator:  ' : ',
                    applyLabel: "Применить",
                    cancelLabel: "Отменить",
                    monthNames: [
                        "Январь",
                        "февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                }
            });
            $('#date').on("apply.daterangepicker",function(ev,picker){
                let date = $(this).val().split(' : ')
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        if (moment(data[2]).isSameOrAfter(date[0]) && moment(data[2]).isSameOrBefore(date[1]) ){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            });
        } );
    </script>
@stop
