@extends('adminlte::page')

@section('title', $user->short_name)


@section('content')

    <div class="card shadow">
        <div class="card-header">
            <div class="row">
                <div class="col-10 text-primary" >
                    @if(is_object($user->avatar))
                        <img src="{{$user->avatar->path}}" class="img-circle" height="40" >&nbsp;
                    @endif
                    {{$user->name}}
                </div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.user.edit',$user)}}">Редактировать</a></div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>{{$user->id}}</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td>{{$user->phone}}</td>
                    </tr>
                    @if(is_object($user->region))
                    <tr>
                        <td>Регион</td>
                        <td>{{$user->region->name}}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>О себе </td>
                        <td>{{$user->about_us}}</td>
                    </tr>
                    <tr>
                        <td>Дата регистрации</td>
                        <td>{{$user->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Дата последнего изменения</td>
                        <td>{{$user->updated_at}}</td>
                    </tr>
                    <tr>
                        <td>Роль </td>
                        <td>{{$user->roleName}}</td>
                    </tr>
                    @foreach($user->files as $file)
                        <tr>
                            <td>Фаил</td>
                            <td><a href="{{$file->path}}" target="_blank">Ссылка</a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop
