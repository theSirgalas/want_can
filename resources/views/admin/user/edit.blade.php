@extends('adminlte::page')

@section('title', 'Редактрирование :'.$user->name)

@section('content_header')
    <h1>Редактирование пользователя {{$user->name}}</h1>
@stop

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header text-primary">{{$user->name}}</div>
        <div class="card-box">
            <div class="col">
                {{Form::open(['route'=>(['admin.user.edit',$user])])}}
                @method('PUT')
                <div class="form-group">
                    <label for="role">Роль</label>
                    {{Form::select('role',$rolesList,$user->role,['id'=>'role','class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
                </div>
                {{Form::close()}}
            </div>

        </div>
    </div>
@stop
