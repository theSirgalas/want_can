@extends('adminlte::page')

@section('title', 'Редактирование региона :'.$region->name)

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Редактировать регион {{$region->name}}</h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.region.edit',$region])])}}
            @method('PUT')

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text('name', old('name',$region->name),['class'=>$errors->has('name')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"title",'required'=>true])}}
                @if ($errors->has('name'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="parent" class="col-form-label">Родительский регион</label>
                {{Form::select('parent',$regions,old('parent',$region->parent_id),['class'=>$errors->has('parent')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"parent",'required'=>true])}}
                @if ($errors->has('parent'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('parent') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
