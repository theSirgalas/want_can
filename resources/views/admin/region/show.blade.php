@extends('adminlte::page')

@section('title', $region->name)

@section('content_header')
    <p class="h1">{{$region->name}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$region->name}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.region.edit',$region)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.region.destroy', $region],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$region->id}}</td>
                </tr>
                <tr>
                    <td>Имя</td>
                    <td>{{$region->name}}</td>
                </tr>
                <tr>
                    <td>Ссылка</td>
                    <td>{{$region->slug}}</td>
                </tr>
                @if(is_object($region->parent))
                    <tr>
                        <td>Регион родитель</td>
                        <td><a href="{{route('admin.region.show',$region->parent)}}">{{$region->parent->name}}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@stop
