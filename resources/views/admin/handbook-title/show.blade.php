@extends('adminlte::page')

@section('title', $handbookTitle->name)

@section('content_header')
    <p class="h1">{{$handbookTitle->name}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$handbookTitle->title}}</div>
                <div class="col-2"><a class="btn btn-success" href="{{route('admin.handbook.title.edit',$handbookTitle)}}">Редактировать</a></div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.handbook.title.destroy', $handbookTitle],'class'=>"mr-1",'method'=>'post'])}}
                        @method('DELETE')
                        <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Ключ</th>
                        <th scope="col">Значение</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>id</td>
                        <td>{{$handbookTitle->id}}</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>{{$handbookTitle->title}}</td>
                    </tr>
                    <tr>
                        <td>Ключ</td>
                        <td>{{$handbookTitle->key}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
