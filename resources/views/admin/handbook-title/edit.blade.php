@extends('adminlte::page')

@section('title', 'Редактирование региона :'.$category->name)

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">Создать </h6>
        </div>
        <div class="card-body">
            {{ Form::open(['route'=>(['admin.handbook.title.store'])])}}
            @csrf

            <div class="form-group">
                <label for="name" class="col-form-label">Название</label>
                {{Form::text('title', old('title',$handbookTitle->title),['class'=>$errors->has('title')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"title",'required'=>true])}}
                @if ($errors->has('title'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <label for="parent" class="col-form-label">Ключ</label>
                {{Form::text('key',old('key',$handbookTitle->key),['class'=>$errors->has('key')?"form-control is_invalid":"form-control", 'placeholder'=>"Название*", "id"=>"key",'required'=>true])}}
                @if ($errors->has('key'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('key') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                {{Form::submit('Сохранить',["class"=>"btn btn-primary"])}}
            </div>
            {{Form::close()}}
        </div>
    </div>
@stop
