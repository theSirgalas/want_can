@extends('adminlte::page')

@section('title', $task->title)

@section('content_header')
    <p class="h1">{{$task->title}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$task->title}}</div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.task.delete', $task],'class'=>"mr-1",'method'=>'post'])}}
                    @method('DELETE')
                    <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$task->id}}</td>
                </tr>
                <tr>
                    <td>Название</td>
                    <td>{{$task->title}}</td>
                </tr>
                <tr>
                    <td>Ссылка</td>
                    <td>{{$task->slug}}</td>
                </tr>
                <tr>
                    <td>Откуда</td>
                    <td>{{$task->from}}</td>
                </tr>
                <tr>
                    <td>Куда</td>
                    <td>{{$task->to}}</td>
                </tr>
                <tr>
                    <td>Дата и время начала работы</td>
                    <td>{{$task->start_date}}</td>
                </tr>
                <tr>
                    <td>Дата и время окончания работы</td>
                    <td>{{$task->end_date}}</td>
                </tr>
                <tr>
                    <td>по настояшее время</td>
                    <td>{{$task->now?'Да':'Нет'}}</td>
                </tr>
                <tr>
                    <td>Цена</td>
                    <td>{{$task->price}}</td>
                </tr>
                <tr>
                    <td>Тип оплаты</td>
                    <td>{{$task->price_type}}</td>
                </tr>
                @if(is_object($task->payment))
                    <tr>
                        <td>Способ оплаты</td>
                        <td>{{$task->payment->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Место оказаная услуг</td>
                    <td>{{$task->place}}</td>
                </tr>
                <tr>
                    <td>Я использую хочу-могу.рф для бизнеса, нужны закрывающие документы</td>
                    <td>{{$task->is_need_documents?'Да':'Нет'}}</td>
                </tr>
                <tr>
                    <td>Статус задания</td>
                    <td>{{$task->status}}</td>
                </tr>
                <tr>
                    <td>Создатель вакансии</td>
                    <td><a href="{{route('admin.user.view',$task->user)}}">{{$task->user->name}}</a></td>
                </tr>
                <tr>
                    <td>Дата создания</td>
                    <td>{{$task->created_at}}</td>
                </tr>
                <tr>
                    <td>Дата последнего редактирования</td>
                    <td>{{$task->updated_at}}</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>

@stop
