@extends('adminlte::page')
@section('plugins.DateRange', true)
@section('plugins.Datatables', true)
@section('plugins.Select2', true)
@section('title', 'Задачи')
@section('content_header')
    <h1>Задачи</h1>
@stop

@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Пользователь</th>
                    <th>Дата начала</th>
                    <th>Дата окончания</th>
                    <th>Категория</th>
                    <th>Подкатегория</th>
                    <th></th>
                </tr>
                <tr>
                    <th>{{Form::text('name',old('name'),['id'=>'name','class'=>'form-control'])}}</th>
                    <th>{{Form::text('user_id',old('user_id'),['id'=>'user','class'=>'form-control'])}}</th>
                    <th>{{Form::text('start_date',old('start_date'),['id'=>'start_date','class'=>'form-control'])}}</th>
                    <th>{{Form::text('finish_date',old('finish_date'),['id'=>'finish_date','class'=>'form-control'])}}</th>
                    <th>{{Form::select('category_id',$categoryParent,old('category'),['id'=>'category','class'=>'form-control'])}}</th>
                    <th>{{Form::select('sub_category_id',[],old('sub_category'),['id'=>'sub_category','class'=>'form-control','disabled'=>'true'])}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->title}}</td>
                        <td><a href="{{route('admin.user.view',$task->user)}}">{{$task->user->name}}</a></td>
                        <td>{{$task->start_date}}</td>
                        <td>{{$task->finish_date}}</td>
                        <td>{{$task->category->name}}</td>
                        <td>{{is_object($task->subCategory)?$task->subCategory->name:''}}</td>
                        <td>
                            <a href="{{route('admin.task.view',$task)}}">
                                <i class="far fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script>

        $(document).ready(function() {
            let table = $('#example').DataTable();
            $('#name').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let name =  $('#name').val();
                        if (data[0].toLowerCase().indexOf(name.toLowerCase())>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#user').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let user =  $('#user').val();
                        if (data[1].toLowerCase().indexOf(user)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#start_date').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                    separator:  ' : ',
                    applyLabel: "Применить",
                    cancelLabel: "Отменить",
                    monthNames: [
                        "Январь",
                        "февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                }
            });
            $('#finish_date').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                    separator:  ' : ',
                    applyLabel: "Применить",
                    cancelLabel: "Отменить",
                    monthNames: [
                        "Январь",
                        "февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                }
            });
            $('#start_date').on("apply.daterangepicker",function(ev,picker){
                let date = $(this).val().split(' : ')
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        if (moment(data[2]).isSameOrAfter(date[0]) && moment(data[2]).isSameOrBefore(date[1]) ){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            });
            $('#finish_date').on("apply.daterangepicker",function(ev,picker){
                let date = $(this).val().split(' : ')
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        if (moment(data[3]).isSameOrAfter(date[0]) && moment(data[2]).isSameOrBefore(date[1]) ){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            });
            $('#category').select2({ placeholder: "Выберите из списка",allowClear: true,language: "ru",});

            $('#category').on('select2:select', function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let category =  $('#category').val();
                        if (data[4].indexOf(category)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
                $.ajax({
                    type:"POST",
                    url:'{{route('admin.task.subCategory')}}',
                    data:{
                        name:$(this).val(),
                        _token:"{{ csrf_token() }}"
                    },
                    success: function(data){
                      let newOptions='<option value=""></option>'
                      for( const item in data){
                            newOptions+='<option value="'+data[item].name+'" >'+data[item].name+'</option>'
                      }
                        $('#sub_category').prop("disabled", false);
                        $('#sub_category').html(newOptions);
                        $('#sub_category').select2({ placeholder: "Выберите из списка",allowClear: true, language: "ru"});
                    }
                });
            } );
            $('#sub_category').on('select2:select', function() { $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    let sub_category =  $('#sub_category').val();
                    if (data[5].indexOf(sub_category)>=0){
                        return true;
                    }
                    return false;
                }
            );
                table.draw();
            } );
        } );
    </script>
@stop
