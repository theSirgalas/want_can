@extends('adminlte::page')

@section('title', $resume->post_name)

@section('content_header')
    <p class="h1">{{$resume->post_name}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$resume->post_name}}</div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.resume.delete', $resume],'class'=>"mr-1",'method'=>'post'])}}
                    @method('DELETE')
                    <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$resume->id}}</td>
                </tr>
                <tr>
                    <td>Желаемая должность</td>
                    <td>{{$resume->post_name}}</td>
                </tr>
                @if(is_object($resume->category))
                    <tr>
                        <td>Категория</td>
                        <td>{{$resume->category->name}}</td>
                    </tr>
                @endif
                @if(is_object($resume->work))
                    <tr>
                        <td>График работы</td>
                        <td>{{$resume->work->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Стаж работы</td>
                    <td>{{$resume->experience}}</td>
                </tr>
                @if(is_object($resume->education))
                    <tr>
                        <td>Образование</td>
                        <td>{{$resume->education->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Пол</td>
                    <td>{{$resume->sex_status}}</td>
                </tr>
                <tr>
                    <td>Возраст</td>
                    <td>{{$resume->age}}</td>
                </tr>
                <tr>
                    <td>Требования к кандидату</td>
                    <td>{{$resume->requirements}}</td>
                </tr>
                <tr>
                    <td>Готовность к командировкам</td>
                    <td>{{$resume->trips_status}}</td>
                </tr>
                <tr>
                    <td>Переезд</td>
                    <td>{{$resume->moving?'Да':'Нет'}}</td>
                </tr>
                <tr>
                    <td>Гражданство</td>
                    <td>{{$resume->citizenship}}</td>
                </tr>
                <tr>
                    <td>Гражданство</td>
                    <td>{{$resume->about_us}}</td>
                </tr>
                <tr>
                    <td>Зарплата</td>
                    <td>{{$resume->salary}}</td>
                </tr>
                <tr>
                    <td>Создатель резюме</td>
                    <td><a href="{{route('admin.user.view',$resume->user)}}">{{$resume->user->name}}</a></td>
                </tr>
                <tr>
                    <td>Статус резюме</td>
                    <td>{{$resume->is_close?'Закрыта':'Открыта'}}</td>
                </tr>
                <tr>
                    <td>Дата создания</td>
                    <td>{{$resume->created_at}}</td>
                </tr>
                <tr>
                    <td>Дата последнего редактирования</td>
                    <td>{{$resume->updated_at}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
