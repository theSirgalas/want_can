@extends('adminlte::page')
@section('plugins.DateRange', true)
@section('plugins.Datatables', true)
@section('plugins.Select2', true)
@section('title', 'Резюме')
@section('content_header')
    <h1>Резюме</h1>
@stop

@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <table id="example" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Пользователь</th>
                    <th>Дата создания</th>
                    <th>Категория</th>
                    <th></th>
                </tr>
                <tr>
                    <th>{{Form::text('name',old('name'),['id'=>'name','class'=>'form-control'])}}</th>
                    <th>{{Form::text('author_user_id',old('author_user_id'),['id'=>'user','class'=>'form-control'])}}</th>
                    <th>{{Form::text('created_at',old('created_at'),['id'=>'created','class'=>'form-control'])}}</th>
                    <th>{{Form::select('category_id',$categoryParent,old('category'),['id'=>'category','class'=>'form-control'])}}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($resumes as $resume)
                    <tr>
                        <td>{{$resume->post_name}}</td>
                        <td><a href="{{route('admin.user.view',$resume->user)}}">{{$resume->user->name}}</a></td>
                        <td>{{$resume->created_at}}</td>
                        <td>{{$resume->category->name}}</td>
                        <td>
                            <a href="{{route('admin.resume.view',$resume)}}">
                                <i class="far fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script>

        $(document).ready(function() {
            let table = $('#example').DataTable();
            $('#name').keyup( function() {

                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let name =  $('#name').val();
                        if (data[0].toLowerCase().indexOf(name.toLowerCase())>=0){
                            console.log(1)
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#user').keyup( function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let user =  $('#user').val();
                        if (data[1].toLowerCase().indexOf(user)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );
            $('#created').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD',
                    separator:  ' : ',
                    applyLabel: "Применить",
                    cancelLabel: "Отменить",
                    monthNames: [
                        "Январь",
                        "февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                }
            });
            $('#created_at').on("apply.daterangepicker",function(ev,picker){
                let date = $(this).val().split(' : ')
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        if (moment(data[2]).isSameOrAfter(date[0]) && moment(data[2]).isSameOrBefore(date[1]) ){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            });
            $('#category').select2({ placeholder: "Выберите из списка",allowClear: true,language: "ru",});

            $('#category').on('select2:select', function() {
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        let category =  $('#category').val();
                        if (data[4].indexOf(category)>=0){
                            return true;
                        }
                        return false;
                    }
                );
                table.draw();
            } );

        } );
    </script>
@stop
