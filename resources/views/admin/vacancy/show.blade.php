@extends('adminlte::page')

@section('title', $vacancy->name)

@section('content_header')
    <p class="h1">{{$vacancy->name}} </p>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">{{$vacancy->name}}</div>
                <div class="col-2">
                    {{Form::open(['route'=>['admin.vacancy.delete', $vacancy],'class'=>"mr-1",'method'=>'post'])}}
                    @method('DELETE')
                    <button class="btn btn-danger">Удалить</button>
                    {{Form::close()}}
                </div>
            </div>

        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Ключ</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>{{$vacancy->id}}</td>
                </tr>
                <tr>
                    <td>Название ваукансии</td>
                    <td>{{$vacancy->name}}</td>
                </tr>
                @if(is_object($vacancy->category))
                    <tr>
                        <td>Категория</td>
                        <td>{{$vacancy->category->name}}</td>
                    </tr>
                @endif
                @if(is_object($vacancy->work))
                    <tr>
                        <td>График работы</td>
                        <td>{{$vacancy->work->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Зарплата</td>
                    <td>{{$vacancy->salary}}</td>
                </tr>
                @if(is_object($vacancy->payments))
                    <tr>
                        <td>Дата и время начала работы</td>
                        <td>{{$vacancy->payments->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Где предстоит работать</td>
                    <td>{{$vacancy->where_work}}</td>
                </tr>
                <tr>
                    <td>Адрес работы</td>
                    <td>{{$vacancy->adress_work}}</td>
                </tr>
                <tr>
                    <td>Требования к кандидату</td>
                    <td>{{$vacancy->requirements}}</td>
                </tr>
                @if(is_object($vacancy->payments))
                    <tr>
                        <td>Способ оплаты</td>
                        <td>{{$vacancy->payments->title}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Обязанности</td>
                    <td>{{$vacancy->obligation}}</td>
                </tr>
                <tr>
                    <td>Описание вакансии и компании</td>
                    <td>{{$vacancy->description}}</td>
                </tr>
                <tr>
                    <td>Создатель вакансии</td>
                    <td><a href="{{route('admin.user.view',$vacancy->user)}}">{{$vacancy->user->name}}</a></td>
                </tr>
                <tr>
                    <td>Статус вакансии</td>
                    <td>{{$vacancy->is_close?'Закрыта':'Открыта'}}</td>
                </tr>
                <tr>
                    <td>Дата создания</td>
                    <td>{{$vacancy->created_at}}</td>
                </tr>
                <tr>
                    <td>Дата последнего редактирования</td>
                    <td>{{$vacancy->updated_at}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@stop
