@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back">
                <h1>Войти</h1>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="input-group required @error('email') has-error @enderror">
                                <label class="input-label" for="InputEmail">E-mail</label>
                                <input type="email"
                                       class="input"
                                       id="InputEmail" placeholder="Укажите данные"
                                       name="email"
                                       value="{{ old('email') }}"
                                       required
                                       autocomplete="email"
                                       autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group required @error('password') has-error @enderror ">
                                <label class="input-label" for="InputPassword">Пароль</label>
                                <input type="password"
                                       class="input "
                                       id="InputPassword"
                                       placeholder="Укажите данные"
                                       name="password"
                                       required
                                       autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <button class="show-pass"><svg width="20" height="13"><use xlink:href="#icon-eye"></use></svg></button>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" required checked>
                                    <span>Я согласен с <a href="{{route('user_agreement')}}">правилами сервиса.</a></span>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span>Запомнить меня</span>
                                </label>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-gradient btn-lg">Войти</button>
                                <p>
                                    @if (Route::has('password.request'))
                                        <a  href="{{ route('password.request') }}">Забыли пароль?</a>
                                    @endif
                                </p>
                                <p>Ещё не зарегистрированы? <a href="{{ route('registration.tab') }}" class="btn">Зарегистрироваться</a></p>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-1 hidden-xs">
                        <img src="svg/login.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.type-2').matchHeight({ byRow:false, property: 'min-height' });
            $('.ui.dropdown').dropdown();
            $('.show-pass').click(function(e){
                e.preventDefault();
                if($(this).hasClass('active')){
                    $(this).siblings('.input').attr('type','password');
                }else{
                    $(this).siblings('.input').attr('type','text');
                }
                $(this).toggleClass('active');
            });
        });
    </script>
@endsection
