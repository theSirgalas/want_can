{{Form::open(['route'=>['registration.organisation'],'files'=>'true','enctype'=>"multipart/form-data"])}}
    <input type="hidden" name="face-id" value="entity">
    <div class="row">
        <div class="input-group required col-xs-12">
            <label class="input-label" for="InputName">Краткое название</label>
            {{Form::text('name',old('name'),['class'=>'input','id'=>'InputName',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputINN">ИНН</label>
            {{Form::text('inn',old('inn'),['class'=>'input','id'=>'InputINN',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('inn'))
                <span class="invalid-feedback"><strong>{{ $errors->first('inn') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label">Город</label>
            <div class="country ui fluid search selection dropdown">
                {{Form::hidden('region_id',old('region_id'))}}
                <i class="dropdown icon"></i>
                <div class="default text">Укажите данные</div>
                <div class="menu">
                    @foreach($regions as $id=>$name)
                        <div class="item" data-value="{{$id}}">{{$name}}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputEntAddr">Юридический адрес</label>
            {{Form::text('legal_address',old('legal_address'),['class'=>'input','id'=>'InputEntAddr',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('legal_address'))
                <span class="invalid-feedback"><strong>{{ $errors->first('legal_address') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputCurrAddr">Фактический адрес</label>
            {{Form::text('fact_address',old('legal-address'),['class'=>'input','id'=>'InputCurrAddr',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('fact_address'))
                <span class="invalid-feedback"><strong>{{ $errors->first('fact_address') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputEmail">E-mail</label>
            {{Form::text('email',old('email'),['class'=>'input','id'=>'InputEmail',"placeholder"=>"Укажите данные", 'required'=>true])}}
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPhone">Телефон</label>
            {{Form::text('phone',old('phone'),['class'=>'input','id'=>'InputPhone',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('phone'))
                <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPassword">Пароль</label>
            {{Form::password('password',['class'=>'input','id'=>'InputPassword',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('password'))
                <span class="invalid-feedback"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPasswordConfirmation">Повторите Пароль</label>
            {{Form::password('password_confirmation',['class'=>'input','id'=>'InputPasswordConfirmation',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('password_confirmation'))
                <span class="invalid-feedback"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="input-group required file-select">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-3 text-left">
                <label class="input-label">Подтвердите аккаунт компании</label>
            </div>
            <div class="col-xs-12 col-md-5 col-lg-5 col-lg-offset-1 text-left">
                <small>Чтобы пройти проверку прикрепите Вашу фотографию с раскрытым паспортом</small>
            </div>
            <div class="col-xs-12 col-md-3 text-right">
                <div class="file-input-group">
                    {{Form::file('files[]', ["id"=>"file", "class"=>"inputfile", "data-multiple-caption"=>"{count} файла выбрано", "multiple"=>true])}}
                    <!--<input type="file" name="file[]" id="file" class="inputfile" data-multiple-caption="{count} файла выбрано" multiple />-->
                    <label class="btn btn-primary" for="file"><span>Подтвердить</span></label>
                </div>
            </div>
        </div>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked>
            <span>Я согласен с <a href="{{route('user_agreement')}}">правилами сервиса.</a></span>
        </label>
    </div>
    <div class="form-footer register">
        <button type="submit" class="btn btn-gradient btn-lg">Зарегистрироваться</button>
        <p>Уже зарегистрированы? <a href="{{ route('login') }}" class="btn">Войти</a></p>
    </div>
{{Form::close()}}
{!! JsValidator::formRequest('App\Http\Requests\Organization\RegisterRequest') !!}
