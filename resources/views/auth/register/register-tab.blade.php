@extends('layouts.app')
@section('meta')
    <title>Регистрация</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back">
                <h1>Регистрация</h1>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <div class="input-group required">
                            <label class="input-label">Являюсь</label>
                            <div class="radio-group" data-toggle="buttons">
                                <label class="btn btn-lg active">
                                    <input type="radio" name="face" value="individual" checked> Физ<span class="hidden-xs">ическим</span> лицо<span class="hidden-xs">м</span>
                                </label><label class="btn btn-lg">
                                    <input type="radio" name="face" value="entity"> Юр<span class="hidden-xs">идическим</span> лицо<span class="hidden-xs">м</span>
                                </label>
                            </div>
                        </div>
                        <img class="loader" src="svg/loader.svg" alt="">
                        <div id="loaded-form"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('.type-2').matchHeight({ byRow:false, property: 'min-height' });
            $('.ui.dropdown').dropdown();
            $('.show-pass').click(function(e){
                e.preventDefault();
                if($(this).hasClass('active')){
                    $(this).siblings('.input').attr('type','password');
                }else{
                    $(this).siblings('.input').attr('type','text');
                }
                $(this).toggleClass('active');
            });
            $("input[name='face']").change(function() {
                $('.loader').removeClass('hide');
                $( "#loaded-form" ).load( "registration/" + $(this).val(), function() {
                    $('.loader').addClass('hide');
                    $('.country').dropdown();
                    $( '.inputfile' ).each( function(){
                        var $input	 = $( this ),
                            $label	 = $input.next( 'label' ),
                            labelVal = $label.html();
                        $input.on( 'change', function( e ){
                            var fileName = '';
                            if( this.files && this.files.length > 1 )
                                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                            else if( e.target.value )
                                fileName = e.target.value.split( '\\' ).pop();

                            if( fileName )
                                $label.find( 'span' ).html( fileName );
                            else
                                $label.html( labelVal );
                        });
                        $input.on( 'focus', function(){ $input.addClass( 'has-focus' ); }).on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
                    });
                });
            });
            $( "#loaded-form" ).load( "registration/individual", function() {
                $('.loader').addClass('hide');
                $('.country').dropdown();
                $( '.inputfile' ).each( function(){
                    var $input	 = $( this ),
                        $label	 = $input.next( 'label' ),
                        labelVal = $label.html();
                    $input.on( 'change', function( e ){
                        var fileName = '';
                        if( this.files && this.files.length > 1 )
                            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                        else if( e.target.value )
                            fileName = e.target.value.split( '\\' ).pop();

                        if( fileName )
                            $label.find( 'span' ).html( fileName );
                        else
                            $label.html( labelVal );
                    });
                    $input.on( 'focus', function(){ $input.addClass( 'has-focus' ); }).on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
                });
            });
        });
    </script>
@endsection
