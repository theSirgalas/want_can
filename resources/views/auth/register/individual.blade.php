{{ Form::open(['route'=>(['registration.user'])])}}
    <input type="hidden" name="face-id" value="individual">
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6 @error('name') has-error @enderror"  >
            <label class="input-label" for="InputName">ФИО</label>
            {{Form::text('name',old('name'),['class'=>'input','id'=>'InputName',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif

        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputEmail">E-mail</label>
            {{Form::text('email',old('email'),['class'=>'input','id'=>'InputEmail',"placeholder"=>"Укажите данные", 'required'=>true])}}
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label">Мой город</label>
            <div class="country ui fluid search selection dropdown">
                <input type="hidden" name="region_id">
                <i class="dropdown icon"></i>
                <div class="default text">Укажите данные</div>
                <div class="menu">
                    @foreach($regions as $id=>$name)
                        <div class="item" data-value="{{$id}}">{{$name}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPhone">Телефон</label>
            {{Form::text('phone',old('phone'),['class'=>'input','id'=>'InputPhone',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('phone'))
                <span class="invalid-feedback"><strong>{{ $errors->first('phone') }}</strong></span>
            @endif

        </div>
    </div>
    <div class="row">
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPassword">Пароль</label>
            {{Form::password('password',['class'=>'input','id'=>'InputPassword',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('password'))
                <span class="invalid-feedback"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="input-group required col-xs-12 col-md-6">
            <label class="input-label" for="InputPasswordConfirmation">Повторите Пароль</label>
            {{Form::password('password_confirmation',['class'=>'input','id'=>'InputPasswordConfirmation',"placeholder"=>"Укажите данные",'required'=>true])}}
            @if ($errors->has('password_confirmation'))
                <span class="invalid-feedback"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked>
            <span>Я согласен с <a href="{{route('user_agreement')}}">правилами сервиса.</a></span>
        </label>
    </div>
    <div class="form-footer register">
        <button type="submit" class="btn btn-gradient btn-lg">Зарегистрироваться</button>
        <p>Уже зарегистрированы? <a href="{{ route('login') }}" class="btn">Войти</a></p>
    </div>
{{Form::close()}}
{!! JsValidator::formRequest('App\Http\Requests\User\RegisterRequest') !!}
