<div class="row">
    <div class="col-xs-12 required col-sm-6 input-group">
        <label class="input-label" for="resumeOrgName">Название организации</label>
        {{Form::text('experiences['.$countExperience.'][company_name]',old('experiences['.$countExperience.'][company_name]'),['class'=>"input", 'id'=>"resumeOrgName", 'placeholder'=>"укажите данные"])}}
    </div>
    <div class="col-xs-12 required col-sm-6 input-group">
        <label class="input-label" for="resumeOrgVac">Должность</label>
        {{Form::text('experiences['.$countExperience.'][post]',old('experiences['.$countExperience.'][post]'),['class'=>"input", 'id'=>"resumeOrgVac", 'placeholder'=>"укажите данные"])}}
    </div>
    <div class="col-xs-12 required col-sm-6 input-group">
        <label class="input-label">Начало работы</label>
        <div class="row">
            <div class="col-xs-6 w100">
                <div class="ui fluid selection dropdown">
                    <input type="hidden" name="resumeOrgMonthStart">
                    {{Form::hidden('experiences['.$countExperience.'][month_start]'),old('experiences['.$countExperience.'][month_start]')}}
                    <i class="dropdown icon"></i>
                    <div class="default text">месяц</div>
                    <div class="menu">
                        @foreach($mouths as $key=>$mouth)
                            <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-6 w100">
                <div class="ui fluid selection dropdown">
                    <input type="hidden" name="resumeOrgYearStart">
                    {{Form::hidden('experiences['.$countExperience.'][years_start]'),old('experiences['.$countExperience.'][years_start]')}}
                    <i class="dropdown icon"></i>
                    <div class="default text">год</div>
                    <div class="menu">
                        @foreach($years as $key=>$year)
                            <div class="item" data-value="{{$year}}">{{$year}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 input-group">
        <label class="input-label">Конец</label>
        <div class="checkbox size-xs pull-right">
            <label>
                {{Form::checkbox('experiences['.$countExperience.'][now]',1,old('experiences['.$countExperience.'][now]')?true:false,['class'=>"today"])}}
                <span>настоящее время</span>
            </label>
        </div>
        <div class="row">
            <div class="col-xs-6 w100">
                <div class="ui fluid selection dropdown">
                    {{Form::hidden('experiences['.$countExperience.'][month_end]'),old('experiences['.$countExperience.'][month_end]')}}
                    <i class="dropdown icon"></i>
                    <div class="default text">месяц</div>
                    <div class="menu">
                        @foreach($mouths as $key=>$mouth)
                            <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-6 w100">
                <div class="ui fluid selection dropdown">
                    <input type="hidden" name="resumeOrgYearEnd">
                    {{Form::hidden('experiences['.$countExperience.'][years_end]',old('experiences['.$countExperience.'][years_end]'))}}
                    <i class="dropdown icon"></i>
                    <div class="default text">год</div>
                    <div class="menu">
                        @foreach($years as $key=>$year)
                            <div class="item" data-value="{{$year}}">{{$year}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 required input-group">
        <label class="input-label" for="resumeOrgToDo">Обязанности</label>
        {{Form::textarea('experiences['.$countExperience.'][obligation]',old('experiences['.$countExperience.'][obligation]'),['class'=>"input", 'id'=>"resumeOrgToDo", 'placeholder'=>"например, прошу связаться по телефону"])}}
    </div>
</div>

