@foreach($experiences as $experience)
    <div class="row">
        <div class="col-xs-12 required col-sm-6 input-group">
            <label class="input-label" for="resumeOrgName">Название организации</label>
            {{
                Form::text(
                    'experiences['.$experience->id.'][company_name]',
                    old('experiences['.$experience->id.'][company_name]',$experience->company_name),
                    [
                        'class'=>"input",
                        'id'=>"resumeOrgName",
                        'placeholder'=>"укажите данные"
                    ]
                )
            }}
        </div>
        <div class="col-xs-12 required col-sm-6 input-group">
            <label class="input-label" for="resumeOrgVac">Должность</label>
            {{
                Form::text(
                    'experiences['.$experience->id.'][post]',
                    old('experiences['.$experience->id.'][post]',$experience->post),
                    [
                        'class'=>"input",
                        'id'=>"resumeOrgVac",
                        'placeholder'=>"укажите данные"
                    ]
                )
            }}
        </div>
        <div class="col-xs-12 required col-sm-6 input-group">
            <label class="input-label">Начало работы</label>
            <div class="row">
                <div class="col-xs-6 w100">
                    <div class="ui fluid selection dropdown">
                        <input type="hidden" name="resumeOrgMonthStart">
                        {{Form::hidden(
                            'experiences['.$experience->id.'][month_start]',
                            old('experiences['.$experience->id.'][month_start]',$experience->month_start)
                            )
                        }}
                        <i class="dropdown icon"></i>
                        @if(!empty(old('experiences['.$experience->id.'][month_start]',$experience->month_start)))
                            <div class="text">{{$mouths[old('experiences['.$experience->id.'][month_start]',$experience->month_start)]}}</div>
                        @else
                            <div class="default text">месяц</div>
                        @endif
                        <div class="menu">
                            @foreach($mouths as $key=>$mouth)
                                <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 w100">
                    <div class="ui fluid selection dropdown">
                        <input type="hidden" name="resumeOrgYearStart">
                        {{
                            Form::hidden(
                                'experiences['.$experience->id.'][years_start]',
                                old('experiences['.$experience->id.'][years_start]',$experience->years_start)
                            )
                        }}
                        <i class="dropdown icon"></i>
                        @if(!empty(old('experiences['.$experience->id.'][years_start]',$experience->years_start)))
                            <div class="text">{{old('experiences['.$experience->id.'][years_start]',$experience->years_start)}}</div>
                        @else
                            <div class="default text">год</div>
                        @endif
                        <div class="menu">
                            @foreach($years as $key=>$year)
                                <div class="item" data-value="{{$year}}">{{$year}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 input-group">
            <label class="input-label">Конец</label>
            <div class="checkbox size-xs pull-right">
                <label>
                    {{
                        Form::checkbox(
                            'experiences['.$experience->id.'][now]',
                            1,
                            old('experiences['.$experience->id.'][now]',$experience->now)?true:false,
                            ['class'=>"today"]
                        )
                    }}
                    <span>настоящее время</span>
                </label>
            </div>
            <div class="row">
                <div class="col-xs-6 w100">
                    <div class="ui fluid selection dropdown">
                        {{
                            Form::hidden(
                                'experiences['.$experience->id.'][month_end]',
                                old('experiences['.$experience->id.'][month_end]',$experience->month_end)
                            )
                        }}
                        <i class="dropdown icon"></i>
                        @if(!empty(old('experiences['.$experience->id.'][month_end]',$experience->month_end)))
                            <div class="text">{{$mouths[old('experiences['.$experience->id.'][month_end]',$experience->month_start)]}}</div>
                        @else
                            <div class="default text">год</div>
                        @endif
                        <div class="menu">
                            @foreach($mouths as $key=>$mouth)
                                <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 w100">
                    <div class="ui fluid selection dropdown">
                        <input type="hidden" name="resumeOrgYearEnd">
                        {{
                            Form::hidden(
                                'experiences['.$experience->id.'][years_end]',
                                old('experiences['.$experience->id.'][years_end]',$experience->years_end)
                            )
                        }}
                        <i class="dropdown icon"></i>
                        @if(!empty(old('experiences['.$experience->id.'][years_end]',$experience->years_end)))
                            <div class="text">{{old('experiences['.$experience->id.'][years_end]',$experience->years_end)}}</div>
                        @else
                            <div class="default text">год</div>
                        @endif
                        <div class="menu">
                            @foreach($years as $year)
                                <div class="item" data-value="{{$year}}">{{$year}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 required input-group">
            <label class="input-label" for="resumeOrgToDo">Обязанности</label>
            {{
                Form::textarea(
                    'experiences['.$experience->id.'][obligation]',
                    old('experiences['.$experience->id.'][obligation]',$experience->obligation),
                    [
                        'class'=>"input",
                        'id'=>"resumeOrgToDo",
                        'placeholder'=>"например, прошу связаться по телефону"
                    ]
                )
            }}
        </div>
    </div>
@endforeach
