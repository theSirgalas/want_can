<div class="row">
    <div class="col-xs-12 required col-sm-6 input-group">
        <label class="input-label" for="resumeLearnName">Название</label>
        {{Form::text('educations['.$countEducation.'][name]',old('educations['.$countEducation.'][name]'),['class'=>"input", 'id'=>"resumeLearnName", 'placeholder'=>"укажите данные"])}}
    </div>
    <div class="col-xs-12 required col-sm-6 input-group">
        <label class="input-label" for="resumeLearnSpec">Специальность</label>
        {{Form::text('educations['.$countEducation.'][specialty]',old('educations['.$countEducation.'][specialty]'),['class'=>"input", 'id'=>"resumeLearnSpec", 'placeholder'=>"укажите данные"])}}
    </div>
    <div class="col-xs-12 required col-sm-3 input-group">
        <label class="input-label">Конец</label>
        <div class="ui fluid selection dropdown">
            <input type="hidden" name="resumeLearnFin">
            {{Form::hidden('educations['.$countEducation.'][years_end]',old('educations['.$countEducation.'][years_end]'))}}
            <i class="dropdown icon"></i>
            @if(!empty(old('educations['.$countEducation.'][years_end]')))
                <div class="default text">{{old('educations['.$countEducation.'][years_end]')}}</div>
            @else
                <div class="default text">год</div>
            @endif
            <div class="menu">
                @foreach($years as $key=>$year)
                    <div class="item" data-value="{{$year}}">{{$year}}</div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-xs-12 required col-sm-3 input-group">
        <label class="input-label">Конец</label>
        <div class="ui fluid selection dropdown">
            <input type="hidden" name="resumeLearnFin">
            {{Form::hidden('educations['.$countEducation.'][years_start]',old('educations['.$countEducation.'][years_start]'))}}
            <i class="dropdown icon"></i>
            @if(!empty(old('educations['.$countEducation.'][years_start]')))
                <div class="default text">{{old('educations['.$countEducation.'][years_start]')}}</div>
            @else
                <div class="default text">год</div>
            @endif
            <div class="menu">
                @foreach($years as $key=>$year)
                    <div class="item" data-value="{{$year}}">{{$year}}</div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9">
        <small>Если ещё учитесь — укажите год предполагаемого окончания</small>
    </div>
</div>
