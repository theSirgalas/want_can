@extends('layouts.app')
@section('meta')
    <title>Редактировать резюме {{$resume->post_name}}</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back">
                <h1>Редактировать резюме {{$resume->post_name}}</h1>
                <p class="subhead">Заполниет поля резюме, чтобы работодатель смог получить достаточно информации </p>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        {{Form::open(['route'=>(['resume.update',$resume])])}}
                            @method('PUT')
                            <div class="row">
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeName">Желаемая должность</label>
                                    {{Form::text(
                                        'post_name',
                                        old('post_name',$resume->post_name),
                                        [
                                            'class'=>"input",
                                            'id'=>"resumeName" ,
                                            'placeholder'=>'Например, курьер'
                                        ])
                                    }}
                                </div>
                                <div class="input-group required col-xs-12 col-sm-6">
                                    <label class="input-label">Категория</label>
                                    <div class="ui category-dropdown fluid selection dropdown">
                                        {{Form::hidden('category_id',old('category_id',$resume->category_id))}}
                                        @if(old('category_id'))
                                            @php
                                                $key = array_search(old('category_id'), array_column($categories, 'id'));
                                            @endphp
                                            <div class="text">{{$categories[10]['name']}}</div>
                                        @else
                                            <div class="default text">выберите категорию</div>
                                        @endif
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($categories as $category)
                                            <div data-value="{{$category['id']}}" class="item active selected">
                                                <img class="ui avatar image" src="{{$category['icon']}}">
                                                {{$category['name']}}
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group required col-xs-12 col-sm-6">
                                    <label class="input-label">График работы</label>
                                    <div class="ui fluid selection dropdown">
                                        {{Form::hidden('work_handbook_id',old('work_handbook_id',$resume->work_handbook_id))}}
                                        <i class="dropdown icon"></i>
                                        @if(!empty($resume->work_handbook_id))
                                            <div class="text">{{$works[$resume->work_handbook_id]}}</div>
                                        @else
                                            <div class="default text">выберите график</div>
                                        @endif
                                        <div class="menu">
                                            @foreach($works as $key=>$work)
                                             <div class="item" data-value="{{$key}}">{{$work}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label" for="resumeWorkExp">Стаж работы</label>
                                    {{Form::text(
                                        'experience',
                                        old('experience',$resume->experience),
                                        [
                                            'class'=>"input",
                                            'id'=>"resumeWorkExp",
                                            'placeholder'=>"заполните поле"
                                        ])
                                    }}
                                </div>
                                <div class="input-group required col-xs-12 col-sm-6">
                                    <label class="input-label">Образование</label>
                                    <div class="ui fluid selection dropdown">
                                        <input type="hidden" name="sci">
                                        {{Form::hidden('education_handbook_id',old('education',$resume->education_handbook_id))}}
                                        <i class="dropdown icon"></i>
                                        @if(!empty($education->work_handbook_id))
                                            <div class="text">{{$educations[$resume->education_handbook_id]}}</div>
                                        @else
                                            <div class="default text">выберите график</div>
                                        @endif
                                        <div class="menu">
                                            @foreach($educations as $key=>$education)
                                                <div class="item" data-value="{{$key}}">{{$education}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label">Пол</label>
                                    <div class="ui fluid selection dropdown">
                                        <input type="hidden" name="gender">
                                        {{Form::hidden('sex',old('sex',$resume->sex))}}
                                        <i class="dropdown icon"></i>
                                        @if(!empty($resume->sex))
                                            <div class="text">{{$genders[$resume->sex]}}</div>
                                        @else
                                            <div class="default text">выберите график</div>
                                        @endif
                                        <div class="menu">
                                            @foreach($genders as $key=>$gender)
                                                <div class="item" data-value="{{$key}}">{{$gender}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label" for="resumeAge">Возраст</label>
                                    {{Form::text('age',old('age',$resume->age),['class'=>"input", 'id'=>"resumeAge", 'placeholder'=>"заполните поле"])}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label">Готовность к командировкам</label>
                                    <div class="radio-group" data-toggle="buttons">
                                        <label class="btn btn-lg @if(old('trips',$resume->trips)==2) active @endif">
                                            {{Form::radio('trips',2,old('trips',$resume->trips)==2?true:null)}}
                                            <span class="hidden-xs">Не готов</span>
                                            <span class="visible-xs">Нет</span>
                                        </label>
                                        <label class="btn btn-lg @if(old('trips',$resume->trips)==1) active @endif">
                                            {{Form::radio('trips',1,old('trips',$resume->trips)==1?true:null)}}
                                            <span class="hidden-xs">Готов</span>
                                            <span class="visible-xs">Да</span>
                                        </label>
                                            <label class="btn btn-lg  @if(old('trips',$resume->trips)==3) active @endif">
                                                {{Form::radio('trips',3,old('trips',$resume->trips)==3?true:null)}} Иногда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label">Переезд</label>
                                    <div class="radio-group" data-toggle="buttons">
                                        <label class="btn btn-lg @if(!old('moving',$resume->moving)) active @endif">
                                            <input type="radio" name="relocate" value="individual" checked>
                                            {{Form::radio('moving',0,!old('moving',$resume->moving)?true:false)}}
                                            <span class="hidden-xs">Невозможен</span>
                                            <span class="visible-xs">Нет</span>
                                        </label><label class="btn btn-lg @if(old('moving',$resume->moving)) active @endif">
                                            {{Form::radio('moving',1,old('moving',$resume->moving))}}
                                            <span class="hidden-xs">Возможен</span>
                                            <span class="visible-xs">Да</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeCity">Гражданство</label>
                                    {{Form::text(
                                        'citizenship',
                                        old('citizenship',$resume->citizenship),
                                        [
                                            'class'=>"input",
                                            'id'=>"resumeCity",
                                            'placeholder'=>"например, Российская Федерация"
                                        ])
                                    }}
                                </div>
                            </div>
                            <div class="work">
                                <p class="lead">Опыт работы</p>
                                <div id="works"></div>
                                <a href="#" id="add-work-place" class="btn btn-default">Добавить ещё</a>
                            </div>
                            <div class="learning">
                                <p class="lead">Учебные заведения</p>
                                <div id="education"></div>
                                <a href="#" id="add-learn-place" class="btn btn-default">Добавить ещё</a>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 input-group">
                                    <label class="input-label" for="resumeCompany">О себе</label>
                                    {{Form::textarea(
                                        'about_us',
                                        old('about_us',$resume->about_us),
                                        [
                                            'class'=>"input",
                                            'id'=>"resumeCompany",
                                            'placeholder'=>"например, прошу связаться по телефону"
                                        ])
                                    }}
                                </div>
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeTodo">Зарплата</label>
                                    {{Form::text(
                                        'salary',
                                        old('salary',$resume->salary),
                                        [
                                            'class'=>"input",
                                            'id'=>"resumeTodo",
                                            'placeholder'=>"например, 20000 руб"
                                        ])
                                    }}
                                </div>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-gradient btn-lg">Отредактировать резюме</button>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Resume\CreateRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.ui.dropdown').dropdown();
            $('#add-work-place').click(function (e){
                e.preventDefault();
                let count= $('#works .row').length+100+{{$maxId}};
                axios.post(
                    '{{route('resume.experience')}}',
                    {count:count}
                    ).then(function (response){
                        if(count<=0){
                            $('#works').html(response.data);
                        }else{
                            $('#works').append(response.data);
                        }
                    $('.ui.dropdown').dropdown();
                });
            });
            $('#add-learn-place').click(function(e){
                e.preventDefault();
                let count= $('#education .row').length;
                axios.post(
                    '{{route('resume.education')}}',
                    {count:count}
                ).then(function (response){
                    if(count<=0){
                        $('#education').html(response.data);
                    }else{
                        $('#education').append(response.data);
                    }
                    $('.ui.dropdown').dropdown();
                });
            })
        });
        $(window).on('load',function (){
            $('#works').load('{{route('resume.experience')}}', {"_token":"{{ csrf_token() }}", id:{{$resume->id}},count:0},function (){
                $('.ui.dropdown').dropdown();
            });
            $('#education').load('{{route('resume.education')}}', {"_token":"{{ csrf_token() }}", id:{{$resume->id}},count:0},function (){
                $('.ui.dropdown').dropdown();
            });
        })
    </script>
@endsection
