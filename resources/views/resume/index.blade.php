@extends('layouts.app')
@section('meta')
    <title>Список резюме</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="h1-header inline">
                <h1>Список резюме</h1>
                <p class="subhead">Выберите подходящее резюме</p>
            </div>
            <div class="menu-check radio-group">
                <a class="btn btn-lg " href="{{route('vacancy.index')}}">Вакансии</a>
                <a class="btn btn-lg active" href="#">Резюме</a>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    @foreach($resumes as $resume)
                        <div class="vac-res-block clearfix">
                        <div class="pull-left text-left">
                            <p class="lead">
                                <a href="{{route('resume.show',$resume)}}">{{$resume->post_name}}</a>
                            </p>
                            <div class="price">{{$resume->salary}}</div>
                            <p></p>
                            <small>{{$resume->time_site}}</small>
                        </div>
                        <div class="pull-right">
                            <div class="man-block size-sm text-center">
                                <div class="left">
                                    @include('layouts.includes.avatar',['user'=>$resume->user])
                                    <div class="about">
                                        <div class="about">
                                            <a href="{{route('user',$resume->user)}}">{{$resume->user->name}}
                                                @include('layouts.includes.user_moderate',['user'=>$resume->user])
                                            </a>
                                            @include('layouts.includes.user_score',['user'=>$resume->user])
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{$resumes->render('layouts.includes.paginate') }}}}
                </div>
                <aside class="col-md-3">
                    <a href="{{route('vacancy.create')}}" class="btn btn-gradient btn-lg">Создать вакансию</a>
                    {{Form::open(['route'=>(['vacancy.index']),'id'=>'form_search','method'=>'get'])}}
                    <div class="filter">
                        <div class="checkbox">
                            <label>
                                {{Form::checkbox(
                                    'all',
                                    1,
                                    !empty($requestArr['all'])?true:null,
                                    ['class'=>'check-all']
                                    )
                                }}
                                <span>Все категории</span>
                            </label>
                        </div>
                        @foreach($categories as $category )
                            <div class="cat-block">
                                <div class="checkbox">
                                    <label>
                                        {{Form::checkbox(
                                            'categories[]',
                                            $category['id'],
                                            App\Helpers\ArrayHelpers::searchInArray($category['id'],$requestArr,'categories')?true:null,
                                            ['class'=>"check-cat" ])
                                        }}
                                        <span>{{$category['name']}}</span>
                                    </label>
                                    <!--<button class="col-lap" type="button" data-toggle="collapse" data-target="#collapseFilter_2" aria-expanded="false" aria-controls="collapseFilter_2">
                                        <svg width="11" height="5" viewBox="0 0 11 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M4.99831 4.99831C4.8315 4.99864 4.66985 4.94054 4.5414 4.83411L0.257904 1.26453C0.11211 1.14335 0.020426 0.969215 0.00302094 0.780437C-0.0143842 0.591658 0.0439157 0.403697 0.165095 0.257904C0.286274 0.11211 0.460406 0.020426 0.649185 0.00302095C0.837963 -0.0143841 1.02592 0.0439153 1.17172 0.165095L4.99831 3.36344L8.82491 0.279322C8.89793 0.22002 8.98195 0.175734 9.07215 0.149011C9.16235 0.122288 9.25693 0.113654 9.35048 0.123605C9.44402 0.133557 9.53468 0.161897 9.61723 0.206998C9.69979 0.252099 9.77261 0.313071 9.83153 0.386409C9.89691 0.459815 9.94642 0.545932 9.97697 0.639365C10.0075 0.732798 10.0184 0.831533 10.0091 0.929385C9.99968 1.02724 9.97019 1.1221 9.92245 1.20802C9.8747 1.29395 9.80972 1.36908 9.73158 1.42873L5.44808 4.87695C5.31594 4.96655 5.15759 5.00928 4.99831 4.99831Z" fill="#252525"/>
                                        </svg>
                                    </button>-->
                                </div>
                                <!-- <div class="collapse" id="collapseFilter_2" aria-expanded="false">
                                    <div class="subcats">
                                        <div class="checkbox checkbox-sm">
                                            <label>
                                                <input class="check-subcat" type="checkbox">
                                                <span>Услуги пешего курьера</span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox-sm">
                                            <label>
                                                <input class="check-subcat" type="checkbox">
                                                <span>Услуги курьера на легковом авто</span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox-sm">
                                            <label>
                                                <input class="check-subcat" type="checkbox">
                                                <span>Срочная доставка</span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox-sm">
                                            <label>
                                                <input class="check-subcat" type="checkbox">
                                                <span>Доставка продуктов</span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox-sm">
                                            <label>
                                                <input class="check-subcat" type="checkbox">
                                                <span>Доставка еды из ресторанов</span>
                                            </label>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        @endforeach
                    </div>
                    {{Form::close()}}
                </aside>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.filter input').change(function(){
                if ($(this).hasClass('check-all')){
                    if($(this).is(":checked")){
                        $(this).parents('.filter').find('.cat-block').find('input').prop('checked',true);
                        $(this).parents('.filter').find('.cat-block').find('input.check-cat').parents('.checkbox').removeClass('here-checked');
                    } else {
                        $(this).parents('.filter').find('.cat-block').find('input').prop('checked',false);
                    }
                } else if ($(this).hasClass('check-cat')) {
                    let elements = $(this).parents('.cat-block').siblings().find('input.check-cat'), flag_arr = [], flag_end, flag = $(this).is(":checked");
                    elements.each(function(index) {
                        if($(this).is(":checked")){
                            flag_arr[index] = 10;
                        } else {
                            flag_arr[index] = 11;
                        }
                    });
                    flag_end = $.inArray(11, flag_arr);
                    if (flag){
                        $(this).parents('.cat-block').find('input').prop('checked', true);
                        $(this).parents('.checkbox').removeClass('here-checked');
                    } else{
                        $(this).parents('.cat-block').find('input').prop('checked', false);
                    }
                    if ((flag_end == -1)&&(flag)){
                        $(this).parents('.filter').find('.check-all').prop('checked', true);
                    } else {
                        $(this).parents('.filter').find('.check-all').prop('checked', false);
                    }
                } else {
                    let elements = $(this).parents('.checkbox').siblings().find('input'), flag_arr = [], flag_end, flag = true, el_flag = $(this).is(":checked");
                    elements.each(function(index) {
                        if($(this).is(":checked")){
                            flag_arr[index] = 10;
                        } else {
                            flag = false;
                            flag_arr[index] = 11;
                        }
                    });
                    flag_end = $.inArray( 10, flag_arr );
                    if (el_flag && flag){
                        $(this).parents('.cat-block').find('.check-cat').prop('checked', true);
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').removeClass('here-checked');
                        let elementes = $(this).parents('.cat-block').siblings().find('input.check-cat'), flag_top_arr = [], fl;
                        elementes.each(function(index) {
                            if($(this).is(":checked")){
                                flag_top_arr[index] = 10;
                            } else {
                                flag_top_arr[index] = 11;
                            }
                        });
                        fl = $.inArray(11, flag_top_arr);
                        if (fl == -1){
                            $(this).parents('.filter').find('.check-all').prop('checked', true);
                        } else {
                            $(this).parents('.filter').find('.check-all').prop('checked', false);
                        }
                    } else{
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').addClass('here-checked');
                        $(this).parents('.cat-block').find('.check-cat').prop('checked', false);
                        $(this).parents('.filter').find('input.check-all').prop('checked', false);
                    }
                    if ((flag_end == -1)&&(!el_flag)){
                        $(this).parents('.cat-block').find('.check-cat').parents('.checkbox').removeClass('here-checked');
                    }
                }
                $('#form_search').submit()
            });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
