@extends('layouts.app')
@section('meta')
    <title>Резюме {{$resume->post_name}}</title>
@endsection
@section('content')
<section class="content">
    <div class="container">
        <div class="w-back task">
            <h1>{{$resume->post_name}}</h1>
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label><i class="icon categoria"></i>Категория</label>
                                <div class="input output">
                                    <a href="#">
                                        <img src="{{$resume->category->image_path}}" alt="{{$resume->category->name}}">
                                        <span>{{$resume->category->name}}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label><i class="dat-cal icon"></i>График работы</label>
                                <div class="input output">
                                    <span>{{$resume->work->title}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Стаж работы</label>
                                <div class="input output">
                                    <span>{{$resume->experience}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Образование</label>
                                <div class="input output">
                                    <span>{{$resume->education->title}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Пол</label>
                                <div class="input output">
                                    <span>{{$resume->sex_status}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Возраст</label>
                                <div class="input output">
                                    <span>{{$resume->age}} года</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Готовность к командировкам</label>
                                <div class="input output">
                                    <span>{{$resume->moving_status}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-view">
                                <label>Переезд</label>
                                <div class="input output">
                                    <span>{{$resume->trips_status}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-view">
                                <label>Гражданство</label>
                                <div class="input output">
                                    <span>{{$resume->citizenship}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            @if(!$resume->experiences->isEmpty())
                                <p class="lead">Опыт работы</p>
                                @foreach($resume->experiences as $experience)
                                <div class="input-view">
                                <div class="input output textarea">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <small>Назание организации</small>
                                            <p>{{$experience->company_name}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <small>Должность</small>
                                            <p>{{$experience->post}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <small>Время работы</small>
                                            <p>{{$experience->getMouthNumber($experience->month_start)}}:{{$experience->years_start}} - {{$experience->end_work}}</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <small>Обязанности</small>
                                            <p>{{$experience->obligation}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        <div class="col-xs-12">
                            @if(!$resume->educations->isEmpty())
                                <p class="lead">Учебные заведения</p>
                                @foreach($resume->educations as $education)
                                <div class="input-view">
                                <div class="input output textarea">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">
                                            <small>Назание</small>
                                            <p>{{$education->name}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <small>Специальность</small>
                                            <p>{{$education->specialty}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-2">
                                            <small>Начало</small>
                                            <p>{{$education->years_start}}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-2">
                                            <small>Конец</small>
                                            <p>{{$education->years_end}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="input-view">
                                <label><i class="dop icon"></i>О себе</label>
                                <div class="input output">
                                    <span>{{$resume->about_us}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="input-view">
                                <label><i class="rub icon"></i>Зарплата</label>
                                <div class="input output">
                                    <span>{{$resume->salary}} рублей</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!empty(Auth::user())&&$resume->isCanRedact(Auth::user()))
                        <a href="{{route('resume.edit',$resume)}}" class="btn btn-gradient btn-lg">Отредактировать вакансию</a>
                    @endif
                    @if(!empty(Auth::user())&&$resume->canReviews(Auth::user()))
                        <a href="{{route('resume.respond',$resume)}}" class="btn btn-gradient btn-lg">Откликнуться на вакансию</a>
                    @endif
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="man-block size-lg text-center">
                        <div class="left">
                            @include('layouts.includes.avatar',['user'=>$resume->user])
                            <div class="about">
                                <a href="{{route('user',$resume->user)}}">{{$resume->user->short_name}}
                                    @include('layouts.includes.user_moderate',['user'=>$resume->user])
                                </a>
                                @include('layouts.includes.user_score',['user'=>$resume->user])
                            </div>
                        </div>
                    </div>
                    <div class="btn btn-svg phone-button" data-phone="{{$resume->user->phone}}">
                        <svg width="14" height="16">
                            <use xlink:href="#icon-phone"></use>
                        </svg>Показать телефон
                    </div>
                    @if(Auth::user())
                        <a href="{{route('cabinet.message.chats',['user'=>Auth::user(),'opponents'=>$resume->user])}}" class="btn btn-svg chat-button">
                            <svg width="18" height="16">
                                <use xlink:href="#icon-message"></use>
                            </svg>Написать
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $('.phone-button').click(function(){
                let opp = $(this).attr('data-phone'),
                    gopp = $(this).text();
                $(this).html('<svg width="14" height="16"><use xlink:href="#icon-phone"></use></svg>'+opp).attr('data-phone', gopp);
            });
            $('.ui.dropdown').dropdown();
        });
    </script>
@endsection
