@extends('layouts.app')
@section('meta')
    <title>Создать резюме</title>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="w-back">
                <h1>Создать резюме</h1>
                <p class="subhead">Заполниет поля резюме, чтобы работодатель смог получить достаточно информации </p>
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        {{Form::open(['route'=>(['resume.create'])])}}
                            <div class="row">
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeName">Желаемая должность</label>
                                    {{Form::text('post_name',old('post_name'),['class'=>"input", 'id'=>"resumeName" ,'placeholder'=>'Например, курьер'])}}
                                </div>
                                <div class="input-group  required  col-xs-12 col-sm-6">
                                    <label class="input-label">Категория</label>
                                    <div class="ui category-dropdown fluid selection dropdown">
                                        {{Form::hidden('category_id',old('category_id'))}}
                                        @if(old('category_id'))
                                            @php
                                                $key = array_search(old('category_id'), array_column($categories, 'id'));
                                            @endphp
                                        @else
                                            <div class="text">выберите категорию</div>
                                        @endif
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($categories as $category)
                                            <div data-value="{{$category['id']}}" class="item active selected">
                                                <img class="ui avatar image" src="{{$category['icon']}}">
                                                {{$category['name']}}
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group required col-xs-12 col-sm-6">
                                    <label class="input-label">График работы</label>
                                    <div class="ui fluid selection dropdown">
                                        {{Form::hidden('work_handbook_id',old('work_handbook_id'))}}
                                        <i class="dropdown icon"></i>
                                        <div class="default text">выберите график</div>
                                        <div class="menu">
                                            @foreach($works as $key=>$work)
                                             <div class="item" data-value="{{$key}}">{{$work}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required  col-sm-6 input-group">
                                    <label class="input-label" for="resumeWorkExp">Стаж работы</label>
                                    {{Form::text('experience',old('experience'),['class'=>"input", 'id'=>"resumeWorkExp", 'placeholder'=>"заполните поле"])}}
                                </div>
                                <div class="input-group required col-xs-12 col-sm-6">
                                    <label class="input-label">Образование</label>
                                    <div class="ui fluid selection dropdown">
                                        <input type="hidden" name="sci">
                                        {{Form::hidden('education_handbook_id',old('education'))}}
                                        <i class="dropdown icon"></i>
                                        <div class="default text">заполните поле</div>
                                        <div class="menu">
                                            @foreach($educations as $key=>$education)
                                                <div class="item" data-value="{{$key}}">{{$education}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required  col-sm-6 input-group">
                                    <label class="input-label">Пол</label>
                                    <div class="ui fluid selection dropdown">
                                        <input type="hidden" name="gender">
                                        {{Form::hidden('sex',old('sex'))}}
                                        <i class="dropdown icon"></i>
                                        <div class="default text">заполните поле</div>
                                        <div class="menu">
                                            @foreach($genders as $key=>$gender)
                                                <div class="item" data-value="{{$key}}">{{$gender}}</div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12  required  col-sm-6 input-group">
                                    <label class="input-label" for="resumeAge">Возраст</label>
                                    {{Form::text('age',old('text'),['class'=>"input", 'id'=>"resumeAge", 'placeholder'=>"заполните поле"])}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label">Готовность к командировкам</label>
                                    <div class="radio-group" data-toggle="buttons">
                                        <label class="btn btn-lg @if(old('trips')==2) active @endif">
                                            {{Form::radio('trips',2,old('trips')==2?true:null)}}
                                            <span class="hidden-xs">Не готов</span>
                                            <span class="visible-xs">Нет</span>
                                        </label>
                                        <label class="btn btn-lg @if(old('trips')==1) active @endif">
                                            {{Form::radio('trips',1,old('trips')==1?true:null)}}
                                            <span class="hidden-xs">Готов</span>
                                            <span class="visible-xs">Да</span>
                                        </label>
                                            <label class="btn btn-lg  @if(old('trips')==3) active @endif">
                                                {{Form::radio('trips',3,old('trips')==3?true:null)}} Иногда
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12 required col-sm-6 input-group">
                                    <label class="input-label">Переезд</label>
                                    <div class="radio-group" data-toggle="buttons">
                                        <label class="btn btn-lg @if(old('moving')==1) active @endif">
                                            <input type="radio" name="relocate" value="individual" checked>
                                            {{Form::radio('moving',0,old('moving')===1?true:false)}}
                                            <span class="hidden-xs">Невозможен</span>
                                            <span class="visible-xs">Нет</span>
                                        </label><label class="btn btn-lg @if(old('moving')==2) active @endif">
                                            {{Form::radio('moving',1,old('moving')===2?true:false)}}
                                            <span class="hidden-xs">Возможен</span>
                                            <span class="visible-xs">Да</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeCity">Гражданство</label>
                                    {{Form::text('citizenship',old('citizenship'),['class'=>"input", 'id'=>"resumeCity", 'placeholder'=>"например, Российская Федерация"])}}
                                </div>
                            </div>
                            <div class="work">
                                <p class="lead">Опыт работы</p>
                                <div id="works">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label" for="resumeOrgName">Название организации</label>
                                            {{Form::text('experiences[1][company_name]',old('experiences[1][company_name]'),['class'=>"input", 'id'=>"resumeOrgName", 'placeholder'=>"укажите данные"])}}
                                        </div>
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label" for="resumeOrgVac">Должность</label>
                                            {{Form::text('experiences[1][post]',old('experiences[1][post]'),['class'=>"input", 'id'=>"resumeOrgVac", 'placeholder'=>"укажите данные"])}}
                                        </div>
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label">Начало работы</label>
                                            <div class="row">
                                                <div class="col-xs-6 w100">
                                                    <div class="ui fluid selection dropdown">
                                                        <input type="hidden" name="resumeOrgMonthStart">
                                                        {{Form::hidden('experiences[1][month_start]'),old('experiences[1][month_start]')}}
                                                        <i class="dropdown icon"></i>
                                                        <div class="default text">месяц</div>
                                                        <div class="menu">
                                                            @foreach($mouths as $key=>$mouth)
                                                                <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 w100">
                                                    <div class="ui fluid selection dropdown">
                                                        <input type="hidden" name="resumeOrgYearStart">
                                                        {{Form::hidden('experiences[1][years_start]'),old('experiences[1][years_start]')}}
                                                        <i class="dropdown icon"></i>
                                                        <div class="default text">год</div>
                                                        <div class="menu">
                                                            @foreach($years as $key=>$year)
                                                                <div class="item" data-value="{{$year}}">{{$year}}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label">Конец</label>
                                            <div class="checkbox size-xs pull-right">
                                                <label>
                                                    {{Form::checkbox('experiences[1][now]',1,old('experiences[1][now]')?true:false,['class'=>"today"])}}
                                                    <span>настоящее время</span>
                                                </label>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6 w100">
                                                    <div class="ui fluid selection dropdown">
                                                        {{Form::hidden('experiences[1][month_end]'),old('experiences[1][month_end]')}}
                                                        <i class="dropdown icon"></i>
                                                        <div class="default text">месяц</div>
                                                        <div class="menu">
                                                            @foreach($mouths as $key=>$mouth)
                                                                <div class="item" data-value="{{$key}}">{{$mouth}}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 w100">
                                                    <div class="ui fluid selection dropdown">
                                                        <input type="hidden" name="resumeOrgYearEnd">
                                                        {{Form::hidden('experiences[1][years_end]',old('experiences[1][years_end]'))}}
                                                        <i class="dropdown icon"></i>
                                                        <div class="default text">год</div>
                                                        <div class="menu">
                                                            @foreach($years as $key=>$year)
                                                                <div class="item" data-value="{{$year}}">{{$year}}</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 input-group">
                                            <label class="input-label" for="resumeOrgToDo">Обязанности</label>
                                            {{Form::textarea('experiences[1][obligation]',old('experiences[1][obligation]'),['class'=>"input", 'id'=>"resumeOrgToDo", 'placeholder'=>"например, прошу связаться по телефону"])}}
                                        </div>
                                    </div>
                                </div>
                                <a href="#" id="add-work-place" class="btn btn-default">Добавить ещё</a>
                            </div>
                            <div class="learning">
                                <p class="lead">Учебные заведения</p>
                                <div id="education">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label" for="resumeLearnName">Название</label>
                                            {{Form::text('educations[1][name]',old('educations[1][name]'),['class'=>"input", 'id'=>"resumeLearnName", 'placeholder'=>"укажите данные"])}}
                                        </div>
                                        <div class="col-xs-12 col-sm-6 input-group">
                                            <label class="input-label" for="resumeLearnSpec">Специальность</label>
                                            {{Form::text('educations[1][specialty]',old('educations[1][specialty]'),['class'=>"input", 'id'=>"resumeLearnSpec", 'placeholder'=>"укажите данные"])}}
                                        </div>
                                        <div class="col-xs-12 col-sm-3 input-group">
                                            <label class="input-label">Начало</label>
                                            <div class="ui fluid selection dropdown">
                                                {{Form::hidden('educations[1][years_start]',old('educations[1][years_start]'))}}
                                                <i class="dropdown icon"></i>
                                                <div class="default text">год</div>
                                                <div class="menu">
                                                    @foreach($years as $key=>$year)
                                                        <div class="item" data-value="{{$year}}">{{$year}}</div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 input-group">
                                            <label class="input-label">Конец</label>
                                            <div class="ui fluid selection dropdown">
                                                <input type="hidden" name="resumeLearnFin">
                                                {{Form::hidden('educations[1][years_end]',old('educations[1][years_end]'))}}
                                                <i class="dropdown icon"></i>
                                                <div class="default text">год</div>
                                                <div class="menu">
                                                    @foreach($years as $key=>$year)
                                                        <div class="item" data-value="{{$year}}">{{$year}}</div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-9">
                                            <small>Если ещё учитесь — укажите год предполагаемого окончания</small>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" id="add-learn-place" class="btn btn-default">Добавить ещё</a>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 input-group">
                                    <label class="input-label" for="resumeCompany">О себе</label>
                                    {{Form::textarea('about_us',old('about_us'),['class'=>"input", 'id'=>"resumeCompany", 'placeholder'=>"например, прошу связаться по телефону"])}}
                                </div>
                                <div class="col-xs-12 required input-group">
                                    <label class="input-label" for="resumeTodo">Зарплата</label>
                                    {{Form::text('salary',old('salary'),['class'=>"input", 'id'=>"resumeTodo", 'placeholder'=>"например, 20000 руб"])}}
                                </div>
                            </div>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-gradient btn-lg">Опубликовать резюме</button>
                            </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {!! JsValidator::formRequest('App\Http\Requests\Resume\CreateRequest') !!}
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.ui.dropdown').dropdown();
            $('#add-work-place').click(function (e){
                e.preventDefault();
                let count= $('#works .row').length;
                axios.post(
                    '{{route('resume.experience')}}',
                    {count:count}
                    ).then(function (response){
                        if(count<=0){
                            $('#works').html(response.data);
                        }else{
                            $('#works').append(response.data);
                        }
                    $('.ui.dropdown').dropdown();
                });
            });
            $('#add-learn-place').click(function(e){
                e.preventDefault();
                let count= $('#education .row').length;
                axios.post(
                    '{{route('resume.education')}}',
                    {count:count}
                ).then(function (response){
                    if(count<=0){
                        $('#education').html(response.data);
                    }else{
                        $('#education').append(response.data);
                    }
                    $('.ui.dropdown').dropdown();
                });
            })
        });
    </script>
@endsection
