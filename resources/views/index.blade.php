@extends('layouts.app')
@section('meta')
    <title>Хочу могу</title>
@endsection
@section('content')
    <section class="main-screen">
        <div class="container">
            <p class="lead">Сервис позволяет найти специалистов во всех сферах услуг</p>
            <img height="515" src="svg/main-txt.svg" alt="картинка главного экрана">
            <div class="actions">
                <a href="{{route('task.resource.form')}}" class="action fs-27">
                    <p>Создать задание</p>
                </a>
                <a href="{{route('executors.executors')}}" class="action fs-20">
                    <p>Исполнители</p>
                </a>
                <a href="{{route('vacancy.index')}}" class="action fs-18">
                    <p>Вакансии</p>
                </a>
            </div>
        </div>
    </section>
    <section class="categories">
        <div class="container">
            <p class="h1 text-center">Исполнители готовы выполнить Ваше задание</p>
            <p class="text-center">Готовы помочь вам в решении самых разнообразных задач</p>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    @php
                        $count=0;
                    @endphp
                    @foreach($categoriesArray as $key=>$category)
                            <a href="{{route(is_object(Auth::user())?'task.category':'executors.category',$category['category']->slug)}}" class="category">
                                <i class="icon">
                                    <img src="{{$category['icon']}}" alt="{{$category['category']->name}}" width=40 height=40>
                                </i>
                                <div class="info">
                                    <p><strong>{{$category['category']->name}}</strong><small>{{$category['count']}} заданий</small></p>
                                </div>
                            </a>
                        @php
                            $count++;
                        @endphp
                        @if($count!=0&&($count%6)==0)

                            </div>
                            <div class="col-xs-12 col-md-4">
                        @endif
                    @endforeach
                </div>
        </div>
    </section>
    <section class="how-it-works">
        <div class="container">
            <p class="h1 text-center">Как это работает</p>
            <p class="text-center">Расскажите о задаче — исполнители напишут сами</p>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="how text-center">
                        <img src="svg/secure-data.svg" alt="как работает 1">
                        <p class="lead">Разместите заказ</p>
                        <p>Опишите задачу. Если нужно, укажите сроки и бюджет</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="how text-center">
                        <img src="svg/select.svg" alt="как работает 1">
                        <p class="lead">Получите предложения</p>
                        <p>Исполнители сами откликнутся на ваш заказ. Обсудите детали заказа.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="how text-center">
                        <img src="svg/id-card.svg" alt="как работает 1">
                        <p class="lead">Выберите исполнителя</p>
                        <p>Выберите подходящего вам исполнителя по рейтингу, отзывам и цене</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
