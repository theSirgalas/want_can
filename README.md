# ИНструкция по развертывани.
для развертывания надо
1. composer update
2. npm install  
3. php artisan migrate
4. php artisan ui vue --auth
5. для создание симлинки
   php artisan storage:link
   
6. для установки echo -server  sudo npm install -g laravel-echo-server
7. для инициализации laravel-echo-server init
8. для запуска не обходимо команда php artisan serve
9 php artisan queue:work
