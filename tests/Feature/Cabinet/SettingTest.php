<?php

namespace Tests\Feature\Cabinet;

use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class SettingTest
 * @package Tests\Feature\Cabinet
 * @property $user
 */
class SettingTest extends TestCase
{

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user=User::factory()->create(['role' => User::USER]);
    }


    /**
     * @test
     */
    public function enterSetting()
    {
        $response = $this->actingAs($this->user)->get('/cabinet/setting/'.$this->user->id);
        $response->assertStatus(200);
    }


    /**
     * @test
     */
    public function notUser()
    {
        $response = $this->actingAs($this->user)->put(route('cabinet.setting.edit',1),['password'=>'123456','password_confirmation'=>'123456']);
        $response->assertStatus(500);
    }
    /**
     * @test
     */
    public function notLength()
    {
        dump($this->user->password);
        $response = $this->actingAs($this->user)->put(route('cabinet.setting.edit',$this->user),['password'=>'1','password_confirmation'=>'1']);
        dump($this->user->password);
        $response->assertStatus(302)->assertRedirect(route('admin.category.edit',$this->user));
    }

    /**
     * @test
     */
    public function notConfirm()
    {
        $response = $this->actingAs($this->user)->put(route('cabinet.setting.edit',$this->user),['password'=>'123456','password_confirmation'=>'111111']);
        $response->assertStatus(302)->assertRedirect(route('admin.category.edit',$this->user));
    }

    /**
     * @test
     */
    public function confirm()
    {
        $response = $this->actingAs($this->user)->put(route('cabinet.setting.edit',$this->user),['password'=>'123456','password_confirmation'=>'123456']);
        $response->assertStatus(302)->assertRedirect(route('login'));
    }
}
