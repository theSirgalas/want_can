<?php

namespace Tests\Feature;

use App\Entities\Organization\Organization;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrganizationRegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function exampleEntity()
    {
        $response = $this->get('/registration/entity');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function errorEmpty()
    {
        $response=$this->post('/registration/organization');
        $response->assertStatus(302)
            ->assertSessionHasErrors(['name', 'email', 'password','phone','region_id','inn','legal_address','fact_address']);
    }


    /**
     * @test
     */
    public function errorNotName()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'email' => $organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn' => $organization->inn,
            'legal_address' => $organization->legal_address,
            'fact_address' => $organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['name']);
    }


    /**
     * @test
     */
    public function errorNotEmail()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function errorNotPassword()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function errorPasswordNotConfirm()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret1',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function errorNotRegion()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['region_id']);
    }

    /**
     * @test
     */
    public function errorRegionId()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 0,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['region_id']);
    }

    /**
     * @test
     */
    public function errorNotInn()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address,
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['inn']);
    }


    /**
     * @test
     */
    public function errorNotLegalAddress()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'fact_address'=>$organization->fact_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['legal_address']);
    }

    /**
     * @test
     */
    public function errorNotFatAddress()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['fact_address']);
    }

    /**
     * @test
     */
    public function success()
    {
        $organization = Organization::factory()->make();
        /**
         * @var $organization Organization
         */
        $response = $this->post('/registration/organization', [
            'name' => $organization->name,
            'email'=>$organization->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $organization->phone,
            'region_id' => 1,
            'inn'=>$organization->inn,
            'legal_address'=>$organization->legal_address,
            'fact_address'=>$organization->fact_address,
            'about_us'=>$organization->about_us
        ]);
        $response->assertStatus(302);
    }

}

