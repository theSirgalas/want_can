<?php

namespace Tests\Feature;

use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/registration');

        $response->assertStatus(200);
    }
    /**
     * @test
     */
    public function testExampleIndividual()
    {
        $response = $this->get('/registration/individual');

        $response->assertStatus(200);
    }
    /**
     * @test
     */
    public function errorEmpty()
    {
        $response=$this->post('/registration/user');
        $response->assertStatus(302)
            ->assertSessionHasErrors(['name', 'email', 'password','phone','region_id']);
    }
    /**
     * @test
     */

    public function errorName()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [

            'email' => $user->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => '+7 (777) 777-77-77',
            'region_id' => 1
        ]);
        $response->assertStatus(302)
            ->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function errorEmail()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => '+7 (777) 777-77-77',
            'region_id' => 1
        ]);
        $response->assertStatus(302)
        ->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function errorNotPassword()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'phone' => '+7 (777) 777-77-77',
            'region_id' => 1
        ]);
        $response->assertStatus(302)
            ->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function errorNotConfirm()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'email'=>$user->email,
            'password' => 'secret',
            'password_confirm' => 'secret1',
            'phone' => '+7 (777) 777-77-77',
            'region_id' => 1
        ]);
        $response->assertStatus(302)
            ->assertSessionHasErrors(['password']);;
    }

    /**
     * @test
     */
    public function errorPhone()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'email'=>$user->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'region_id' => 1
        ]);
        $response->assertStatus(302)
            ->assertSessionHasErrors(['phone']);
    }

    /**
     * @test
     */
    public function errorNotRegion()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'email'=>$user->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => '+7 (777) 777-77-77',
        ]);
        $response->assertStatus(302)
            ->assertSessionHasErrors(['region_id']);
    }

    /**
     * @test
     */
    public function regionIdError()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'email'=>$user->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => '+7 (777) 777-77-77',
            'region_id' => 0
        ]);
        $response->assertStatus(302)->assertSessionHasErrors(['region_id']);
    }

    /**
     * @test
     */
    public function success()
    {
        $user = User::factory()->make();
        $response = $this->post('/registration/user', [
            'name' => $user->name,
            'email'=>$user->email,
            'password' => 'secret',
            'password_confirm' => 'secret',
            'phone' => $user->phone,
            'region_id' => 1
        ]);
        $response->assertStatus(302);
    }


}
