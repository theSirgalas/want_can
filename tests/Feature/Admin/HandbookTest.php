<?php

namespace Tests\Feature\Admin;

use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Entities\User\User;

/**
 * Class HandbookTest
 * @package Tests\Feature\Admin
 * @property User $admin
 * @property User $user
 * @property Handbook $handbook
 * @property HandbookTitle $handbookTitle
 */
class HandbookTest extends TestCase
{
    private $admin;
    private $user;
    private $handbook;
    private $handbookTitle;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
        $this->handbook=Handbook::factory()->create();
        $this->handbookTitle=HandbookTitle::factory()->create();
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/handbook');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/handbook');

        $response->assertStatus(403);
    }

    public function enterCreateAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/handbook/create');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterCreateUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/handbook/create');

        $response->assertStatus(403);
    }

    public function enterEditAdmin()
    {

        $response = $this->actingAs($this->admin)->get(route('admin.handbook.edit',$this->handbook));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->user)->get(route('admin.handbook.edit',$this->handbook));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function enterViewHandbookAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.handbook.show',$this->handbook));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewHandbookUser(){
        $response = $this->actingAs($this->user)->get(route('admin.handbook.show',$this->handbook));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function errorCreateEmpty()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.create'));
        $response->assertStatus(302)->assertSessionHasErrors(['title','handbook_title_id']);;
    }

    /**
     * @test
     */
    public function errorCreateNotTile()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.create',['handbook_title_id'=>$this->handbookTitle->id]));
        $response->assertStatus(302)->assertSessionHasErrors(['title',]);;
    }

    /**
     * @test
     */
    public function errorCreateNotHandbookTitle()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.create'),['title'=>'test-handbook']);
        $response->assertStatus(302)->assertSessionHasErrors(['handbook_title_id']);;
    }


    public function successCreate()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.create'),['title'=>'test-handbook','handbook_title_id'=>$this->handbookTitle->id]);
        $handbook=Handbook::where('title','test-handbook');
        $this->assertIsObject($handbook);
        $response->assertStatus(302)->assertRedirect(route('admin.handbook.show',$handbook));
    }


    public function successEdit()
    {
        $response = $this->actingAs($this->admin)->put(route('admin.handbook.edit',$this->handbook),['name'=>'test-handbook-edit']);
        $handbook=Handbook::where('name','test-handbook-edit');
        $this->assertIsObject($handbook);
        $response->assertStatus(302)->assertRedirect(route('admin.handbook.show',$handbook));
    }

}
