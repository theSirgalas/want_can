<?php

namespace Tests\Feature\Admin;

use App\Entities\Category;
use App\Entities\Organization\Organization;
use App\Entities\Region;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class OrganisationTest
 * @package Tests\Feature\Admin
 *
 * @property User $admin
 * @property User $user
 * @property Organization $organization
 */
class OrganisationTest extends TestCase
{

    private $admin;
    private $user;
    private $organization;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
        $this->organization=Organization::factory()->create(['name'=>'test']);
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('admin/organization');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('admin/organization');
        $response->assertStatus(403);
    }



    public function enterEditAdmin()
    {

        $response = $this->actingAs($this->admin)->get(route('admin.organization.edit',$this->organization));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->user)->get(route('admin.organization.edit',$this->organization));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function enterViewCategoryAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.organization.show',$this->organization));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewCategoryUser(){
        $response = $this->actingAs($this->user)->get(route('admin.organization.show',$this->organization));
        $response->assertStatus(403);
    }

    public function successCreate()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.organization.create'),['name'=>'test-organization']);
        $organization=Organization::where('name','test-organization');
        $this->assertIsObject($organization);
        $response->assertStatus(302)->assertRedirect(route('admin.organization.show',$organization));
    }

    public function successEdit()
    {
        $response = $this->actingAs($this->admin)->put(route('admin.organization.edit',$this->organization),['name'=>'test-organization']);
        $organization=Organization::where('name','test-organization');
        $this->assertIsObject($organization);
        $response->assertStatus(302)->assertRedirect(route('admin.organization.show',$organization));
    }
}
