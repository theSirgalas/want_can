<?php

namespace Tests\Feature\Admin;

use App\Entities\Region;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class RegionTest
 * @package Tests\Feature\Admin
 * @property User $admin
 * @property User $user
 * @property Region $region
 */
class RegionTest extends TestCase
{
    private $admin;
    private $user;
    private $region;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
        $this->region=Region::factory()->create(['name'=>'test']);
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/region');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/region');

        $response->assertStatus(403);
    }

    public function enterCreateAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/region/create');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterCreateUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/region/create');
        $response->assertStatus(403);
    }

    public function enterEditAdmin()
    {

        $response = $this->actingAs($this->admin)->get(route('admin.region.edit',$this->region));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->user)->get(route('admin.region.edit',$this->region));
        $response->assertStatus(403);
    }

    public function successCreate()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.region.create'),['name'=>'test-region']);
        $region=Region::where('name','test-region');
        $this->assertIsObject($region);
        $response->assertStatus(302)->assertRedirect(route('admin.region.show',$region));
    }

    public function successEdit()
    {
        $response = $this->actingAs($this->admin)->put(route('admin.region.edit',$this->region),['name'=>'test-region']);
        $region=Region::where('name','test-region');
        $this->assertIsObject($region);
        $response->assertStatus(302)->assertRedirect(route('admin.region.show',$region));
    }

    /**
     * @test
     */
    public function enterViewRegionAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.region.show',$this->region));
        $response->assertStatus(200);
    }
    /**
     * @test
     */
    public function enterViewRegionUser(){
        $response = $this->actingAs($this->user)->get(route('admin.region.show',$this->region));
        $response->assertStatus(403);
    }


}
