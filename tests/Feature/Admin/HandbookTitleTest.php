<?php

namespace Tests\Feature\Admin;

use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use App\Entities\User\User;
use Tests\TestCase;

/**
 * Class HandbookTitleTest
 * @package Tests\Feature\Admin
 *  @property User $admin
 * @property User $user
 * @property HandbookTitle $handbookTitle
 */
class HandbookTitleTest extends TestCase
{
    private $admin;
    private $user;
    private $handbookTitle;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
        $this->handbookTitle=HandbookTitle::factory()->create();
    }

    /**
     * @test-title
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/handbook-title');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/handbook-title');

        $response->assertStatus(403);
    }

    public function enterCreateAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/handbook-title/create');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterCreateUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/handbook-title/create');

        $response->assertStatus(403);
    }

    public function enterEditAdmin()
    {

        $response = $this->actingAs($this->admin)->get(route('admin.handbook.title.edit',$this->handbookTitle));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->user)->get(route('admin.handbook.title.edit',$this->handbookTitle));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function enterViewHandbookTitleAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.handbook.title.show',$this->handbookTitle));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewHandbookTitleUser(){
        $response = $this->actingAs($this->user)->get(route('admin.handbook.title.show',$this->handbookTitle));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function errorCreateEmpty()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.title.create'));
        $response->assertStatus(302)->assertSessionHasErrors(['title','key']);;
    }

    /**
     * @test
     */
    public function errorCreateNotTile()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.title.create',['key'=>"handbook_title_test"]));
        $response->assertStatus(302)->assertSessionHasErrors(['title']);;
    }

    /**
     * @test
     */
    public function errorCreateNotKey()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.title.create'),['title'=>'test-handbook-title']);
        $response->assertStatus(302)->assertSessionHasErrors(['key']);;
    }


    public function successCreate()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.handbook.title.create'),['title'=>'test-handbook-title','key'=>"handbook_title_test"]);
        $handbookTitle=HandbookTitle::where('title','test-handbook-title');
        $this->assertIsObject($handbookTitle);
        $response->assertStatus(302)->assertRedirect(route('admin.handbook.title.show',$handbookTitle));
    }


    public function successEdit()
    {
        $response = $this->actingAs($this->admin)->put(route('admin.handbook.title.edit',$this->handbookTitle),['name'=>'test-handbook_title-edit']);
        $handbookTitle=HandbookTitle::where('name','test-handbook_title-edit');
        $this->assertIsObject($handbookTitle);
        $response->assertStatus(302)->assertRedirect(route('admin.handbook.title.show',$handbookTitle));
    }
}
