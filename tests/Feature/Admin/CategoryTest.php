<?php

namespace Tests\Feature\Admin;

use App\Entities\Category;
use App\Entities\Region;
use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

/**
 * Class CategoryTest
 * @package Tests\Feature\Admin
 * @property User $admin
 * @property User $user
 * @property Category $category
 */
class CategoryTest extends TestCase
{
    private $admin;
    private $user;
    private $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
        $this->category=Category::factory()->create(['name'=>'test']);
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/category');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/category');

        $response->assertStatus(403);
    }

    public function enterCreateAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/category/create');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterCreateUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/category/create');

        $response->assertStatus(403);
    }

    public function enterEditAdmin()
    {

        $response = $this->actingAs($this->admin)->get(route('admin.category.edit',$this->category));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->user)->get(route('admin.category.edit',$this->category));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function enterViewCategoryAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.category.show',$this->category));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewCategoryUser(){
        $response = $this->actingAs($this->user)->get(route('admin.category.show',$this->category));
        $response->assertStatus(403);
    }

    public function successCreate()
    {
        $response = $this->actingAs($this->admin)->post(route('admin.category.create'),['name'=>'test-category']);
        $category=Category::where('name','test-category');
        $this->assertIsObject($category);
        $response->assertStatus(302)->assertRedirect(route('admin.category.show',$category));
    }

    public function successEdit()
    {
        $response = $this->actingAs($this->admin)->put(route('admin.category.edit',$this->category),['name'=>'test-category']);
        $category=Category::where('name','test-category');
        $this->assertIsObject($category);
        $response->assertStatus(302)->assertRedirect(route('admin.category.show',$category));
    }
}
