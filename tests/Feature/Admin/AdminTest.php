<?php

namespace Tests\Feature\Admin;

use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{

    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin=User::factory()->create(['role' => User::ADMIN]);
        $this->user=User::factory()->create(['role' => User::USER]);
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin');
        $response->assertStatus(200);
    }
    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin');

        $response->assertStatus(403);
    }
}
