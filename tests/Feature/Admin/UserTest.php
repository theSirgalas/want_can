<?php

namespace Tests\Feature\Admin;

use App\Entities\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class UserTest
 * @package Tests\Feature\Admin
 * @property User $admin
 * @property User $user
 */
class UserTest extends TestCase
{

    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin = User::factory()->create(['role' => User::ADMIN]);
        $this->user = User::factory()->create(['name'=>'test_feature_user','role' => User::USER]);
    }

    /**
     * @test
     */
    public function enterAdmin()
    {
        $response = $this->actingAs($this->admin)->get('/admin/user');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterUser()
    {
        $response = $this->actingAs($this->user)->get('/admin/user');

        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function enterEditUser()
    {
        $response = $this->actingAs($this->admin)->get(route('admin.user.form',$this->user));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewUserAdmin(){
        $response = $this->actingAs($this->admin)->get(route('admin.user.view',$this->user));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function enterViewUserUser(){
        $response = $this->actingAs($this->user)->get(route('admin.user.view',$this->admin));
        $response->assertStatus(403);
    }

    /**
     * @test
     */
    public function successEditUser()
    {
        $user=User::factory()->create(['name'=>'test_feature_edit_user','role' => User::USER]);
        $response = $this->actingAs($this->admin)->put(route('admin.user.edit', $user), ['role' => User::ADMIN]);
        $user=User::where('name','test_feature_edit_user')->first();
        $this->assertTrue($user->role==User::ADMIN);
        $response->assertStatus(302)->assertRedirect(route('admin.user.view', $user));
    }

}
