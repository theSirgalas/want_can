<?php

namespace Database\Factories\Entities\Vacancy;

use App\Entities\Category;
use App\Entities\Handbook\Handbook;
use App\Entities\User\User;
use App\Entities\Vacancy\Vacancy;
use Illuminate\Database\Eloquent\Factories\Factory;

class VacancyFactory extends Factory
{

    protected $model = Vacancy::class;

    public function definition()
    {
        $category=Category::whereNull('parent_id')->inRandomOrder()->first();
        $work=Handbook::where('handbook_title_id',3)->inRandomOrder()->first();
        $salary=Handbook::where('handbook_title_id',5)->inRandomOrder()->first();
        $user=User::inRandomOrder()->first();
        return [
            'name'=>$this->faker->sentence(2),
            'category_id'=>$category,
            'work_handbook_id'=>$work,
            'salary'=>(string)$this->faker->randomDigit,
            'payments_handbook_id'=>$salary,
            'where_work'=>$this->faker->sentence(2),
            'address_work'=>$this->faker->sentence(2),
            'requirements'=>$this->faker->text(300),
            'obligation'=>$this->faker->text(300),
            'description'=>$this->faker->text(300),
            'author_user_id'=>$user
        ];
    }
}
