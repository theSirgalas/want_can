<?php

namespace Database\Factories\Entities\Task;

use App\Entities\Category;
use App\Entities\Handbook\Handbook;
use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Helpers\StringHelpers;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category=Category::whereNull('parent_id')->inRandomOrder()->first();
        $subCategory=$category->children()->inRandomOrder()->first();
        $handbook=Handbook::where('handbook_title_id',2)->inRandomOrder()->first();
        $user=User::inRandomOrder()->first();
        $title=$this->faker->sentence(5);
        $startDate = $this->faker->dateTimeBetween('+0 days', '+1 month');
        $startDateClone=clone $startDate;
        return [
            'title'=>$title,
            'slug'=>Str::slug($title),
            'description'=>$this->faker->text(300),
            'from'=>$this->faker->address,
            'to'=>$this->faker->address,
            'start_date'=>$startDate,
            'finish_date'=>$this->faker->dateTimeBetween($startDate,$startDateClone->modify('+'.rand(1,15).' days')),
            'now'=>false,
            'price'=>$this->faker->randomDigit,
            'is_need_documents'=>$this->faker->boolean(),
            'user_id'=>$user->id,
            'payment_handbook_id'=>$handbook->id,
            'category_id'=>$category->id,
            'sub_category_id'=>$subCategory?$subCategory->id:null,
            'place_service'=>rand(0,1),
            'type_price'=>rand(0,2)
        ];
    }
}
