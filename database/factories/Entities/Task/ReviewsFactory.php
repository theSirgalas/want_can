<?php

namespace Database\Factories\Entities\Task;

use App\Entities\Task\Application;
use App\Entities\Task\Reviews;

use App\Entities\Task\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewsFactory extends Factory
{

    protected $model = Reviews::class;

    public function definition()
    {
        /** @var $application Application */
        $application=Application::inRandomOrder()->first();

        $score=$this->faker->randomFloat(2,0,5);
        $application->user->reviews_score+=$score;
        $application->user->reviews_count++;
        $application->user->save();
        return [
            'score'=>$this->faker->randomFloat(2,0,5),
            'comment'=>$this->faker->text(150),
            'application_id'=>$application->id,
            'user_id'=>$application->user_id
        ];
    }
}
