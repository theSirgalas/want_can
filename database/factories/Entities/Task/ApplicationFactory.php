<?php

namespace Database\Factories\Entities\Task;

use App\Entities\Task\Application;
use App\Entities\Task\Task;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApplicationFactory extends Factory
{

    protected $model = Application::class;

    public function definition()
    {
        $task=Task::inRandomOrder()->first();
        $user=User::inRandomOrder()->first();
        return [
            'price'=>$this->faker->randomDigit,
            'comment'=>$this->faker->text(150),
            'task_id'=>$task->id,
            'user_id'=>$user->id,
            'viewers'=>$this->faker->boolean(),
        ];
    }
}
