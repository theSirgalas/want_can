<?php

namespace Database\Factories\Entities\Handbook;

use App\Entities\Handbook\HandbookTitle;
use Illuminate\Database\Eloquent\Factories\Factory;

class HandbookTitleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HandbookTitle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence(1),
            'key'=>$this->faker->unique()->slug()
        ];
    }
}
