<?php

namespace Database\Factories\Entities\Handbook;

use App\Entities\Handbook\Handbook;
use Illuminate\Database\Eloquent\Factories\Factory;

class HandbookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Handbook::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->unique()->sentence(1),
            'code'=>$this->faker->biasedNumberBetween(1,10000),
            'short_description'=>$this->faker->paragraph(2),
            'description'=>$this->faker->paragraph(5),
            'handbook_title_id'=>1
        ];
    }
}
