<?php

namespace Database\Factories\Entities;

use App\Entities\Chatroom;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChatroomFactory extends Factory
{

    protected $model = Chatroom::class;

    public function definition()
    {
        $user=User::inRandomOrder()->first();
        return [
            'customer_id'=>101,
            'executor_id'=>$user->id
        ];
    }
}
