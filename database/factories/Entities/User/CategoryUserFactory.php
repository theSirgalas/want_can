<?php

namespace Database\Factories\Entities\User;

use App\Entities\Category;
use App\Entities\User\CategoryUser;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryUserFactory extends Factory
{

    protected $model = CategoryUser::class;


    public function definition()
    {

        $user=User::inRandomOrder()->first();
        $category=Category::inRandomOrder()->first();
        var_dump($user->id);
        var_dump($category->id);
        return [
            'user_id'=>$user->id,
            'category_id'=>$category->id
        ];
    }
}
