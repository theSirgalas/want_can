<?php

namespace Database\Factories;

use App\Entities\Notification;
use App\Entities\Resume\Resume;
use App\Entities\Task\Task;
use App\Entities\User\User;
use App\Entities\Vacancy\Vacancy;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;

class NotificationFactory extends Factory
{


    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $randInt=rand(0,4);
        $entityArr=[
            Task::class,
            Resume::class,
            Vacancy::class,
            Task::class,
        ];
        if($randInt<1||$randInt>3){
            $randType=rand(0,1);
            $typeArr=[
                Notification::TYPE_REVIEWS,
                Notification::TYPE_PROPOSAL,
            ];
            $type=$typeArr[$randType];
        }else{
            $type=Notification::TYPE_REVIEWS;
        }
        $user=User::inRandomOrder()->first();
        var_dump(1);
        $entity=$entityArr[$randInt]::inRandomOrder()->first();
        var_dump($entity->id);
        $userLink='<a href="'.route('user',$user).'">'.$user->name.'</a>';
        var_dump($userLink);
        $entityLink=[
            '<a href="'.route('task.show',$entity->slug).'">'.$entity->title.'</a>',
            '<a href="'.route('resume.show',$entity).'">'.$entity->post_name.'</a>',
            '<a href="'.route('vacancy.show',$entity).'">'.$entity->name.'</a>',
            '<a href="'.route('task.show',$entity->slug).'">'.$entity->title.'</a>'
        ];
        var_dump($entityLink[$randInt]);
        $notificationArray=[
             'пользователь '.$userLink.' откликнулся на задание'.$entityLink[$randInt],
             'пользователь '.$userLink.' откликнулся на резюме '.$entityLink[$randInt],
             'пользователь '.$userLink.' откликнулся на вакансию'.$entityLink[$randInt],
             'вам поступило предложение на участие в задании '.$entityLink[$randInt].' от пользователя '.$userLink
        ];
        var_dump($notificationArray[$randInt]);

        return [
            'user_id'=>$user->id,
            'entity'=>$entityArr[$randInt],
            'primary'=>$entity->id,
            'type'=>$type,
            'notification'=>$notificationArray[$randInt]
        ];
    }
}
