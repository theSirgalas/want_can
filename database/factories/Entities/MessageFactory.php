<?php

namespace Database\Factories\Entities;

use App\Entities\Chatroom;
use App\Entities\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $chatroom=Chatroom::inRandomOrder()->first();
        $user=[$chatroom->customer_id,$chatroom->executor_id];
        return [
            'chatroom_id'=>$chatroom->id,
            'user_id'=>$user[rand(0,1)],
            'message'=>$this->faker->sentence(5),
            'view'=>$this->faker->boolean(),
        ];
    }
}
