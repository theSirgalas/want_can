<?php

namespace Database\Factories\Entities\Resume;

use App\Entities\Resume\Resume;
use App\Entities\Resume\ResumeEducation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResumeEducationFactory extends Factory
{

    protected $model = ResumeEducation::class;


    public function definition()
    {
        $date=new Carbon($this->faker->dateTime);
        $resume=Resume::inRandomOrder()->first();
        return [
            'name'=>$this->faker->sentence(3),
            'specialty'=>$this->faker->text(300),
            'years_start'=>$date->format('Y'),
            'years_end'=>$date->addYears(5)->format('Y'),
            'resume_id'=>$resume
        ];
    }
}
