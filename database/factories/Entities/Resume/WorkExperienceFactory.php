<?php

namespace Database\Factories\Entities\Resume;

use App\Entities\Resume\Resume;
use App\Entities\Resume\WorkExperience;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkExperienceFactory extends Factory
{

    protected $model = WorkExperience::class;

    public function definition()
    {
        $resume=Resume::inRandomOrder()->first();
        $date =new Carbon($this->faker->dateTime);

        return [
            'company_name'=>$this->faker->sentence(2),
            'post'=>$this->faker->sentence(2),
            'month_start'=>$date->format('n'),
            'years_start'=>$date->format('Y'),
            'month_end'=>$date->addMonths(rand(1,3))->format('n'),
            'years_end'=>$date->addYears(rand(1,10))->format('Y'),
            'obligation'=>$this->faker->text(300),
            'resume_id'=>$resume
        ];
    }
}
