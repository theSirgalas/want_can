<?php

namespace Database\Factories\Entities\Resume;

use App\Entities\Category;
use App\Entities\Handbook\Handbook;
use App\Entities\Resume\Resume;
use App\Entities\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResumeFactory extends Factory
{

    protected $model = Resume::class;


    public function definition()
    {
        $category=Category::whereNull('parent_id')->inRandomOrder()->first();
        $work=Handbook::where('handbook_title_id',3)->inRandomOrder()->first();
        $education=Handbook::where('handbook_title_id',4)->inRandomOrder()->first();
        $user=User::inRandomOrder()->first();
        return [
            'post_name'=>$this->faker->sentence(1),
            'category_id'=>$category,
            'work_handbook_id'=>$work,
            'experience'=>$this->faker->sentence(1),
            'education_handbook_id'=>$education,
            'sex'=>rand(1,2),
            'age'=>$this->faker->sentence(1),
            'trips'=>rand(1,3),
            'moving'=>$this->faker->boolean,
            'citizenship'=>$this->faker->sentence(1),
            'about_us'=>$this->faker->sentence(2),
            'salary'=>(string)$this->faker->randomDigit,
            'author_user_id'=>$user
        ];
    }
}
