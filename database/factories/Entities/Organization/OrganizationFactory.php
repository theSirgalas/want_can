<?php

namespace Database\Factories\Entities\Organization;

use App\Entities\Organization\Organization;
use App\Entities\Region;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->company,
            'inn'=>$this->faker->randomNumber(),
            'email' => $this->faker->email,
            'region_id'=>rand(1, 4),
            'user_id'=>rand(1, 100),
            'legal_address'=>$this->faker->address,
            'fact_address'=>$this->faker->address,
            'phone' => $this->faker->unique()->phoneNumber,
            'about_us'=>$this->faker->text,
            'moderation_status'=>rand(0,2)
        ];
    }
}
