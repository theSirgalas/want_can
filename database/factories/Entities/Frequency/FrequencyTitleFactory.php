<?php

namespace Database\Factories\Entities\Frequency;

use App\Entities\Frequency\FrequencyTitle;
use Illuminate\Database\Eloquent\Factories\Factory;

class FrequencyTitleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FrequencyTitle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence(1),
            'description'=>$this->faker->sentence(5),
        ];
    }
}
