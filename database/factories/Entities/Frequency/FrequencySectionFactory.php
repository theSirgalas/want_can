<?php

namespace Database\Factories\Entities\Frequency;

use App\Entities\Frequency\FrequencySection;
use App\Entities\Frequency\FrequencyTitle;
use Illuminate\Database\Eloquent\Factories\Factory;

class FrequencySectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FrequencySection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title=FrequencyTitle::inRandomOrder()->first();
        return [
            'title'=>$this->faker->sentence(1),
            'short_description'=>$this->faker->sentence(5),
            'description'=>$this->faker->sentence(10),
            'frequency_titles_id'=>$title->id
        ];
    }
}
