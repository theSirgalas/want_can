<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->id();
            $table->string('name',256);
            $table->string('slug',256);
            $table->integer('parent_id')->nullable()->references('id')->on('regions')->onDelete('CASCADE');

            $table->timestamps();
        });

        Schema::table('users',function (Blueprint $table){
            $table->integer('region_id')->references('id')->on('regions')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('region_id');
        });
        Schema::dropIfExists('regions');
    }
}
