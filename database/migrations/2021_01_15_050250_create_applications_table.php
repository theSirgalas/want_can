<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{

    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->integer('price')->nullable();
            $table->text('comment')->nullable();
            $table->integer('task_id')->references('id')->on('task')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('user_id')->references('id')->on('user')->onDelete("CASCADE")->onUpdate('RESTRICT');
            $table->boolean('viewers')->default(false);
            $table->timestamps();
        });

        Schema::table('tasks',function (Blueprint $table){
            $table->integer('application_id')->nullable()->references('id')->on('application')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
