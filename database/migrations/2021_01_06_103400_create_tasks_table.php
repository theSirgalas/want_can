<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title',510);
            $table->string('slug',510);
            $table->text('description')->nullable();
            $table->string('from',1024)->nullable();
            $table->string('to',1024)->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('finish_date')->nullable();
            $table->boolean('now')->nullable()->default(false);
            $table->float('price');
            $table->boolean('is_need_documents')->nullable()->default(false);
            $table->boolean('is_safe_payment')->nullable()->default(false);
            $table->boolean('is_finished')->nullable()->default(false);
            $table->boolean('is_close')->nullable()->default(false);
            $table->smallInteger('place_service')->default(0);
            $table->integer('type_price')->nullable()->default(0);
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('executor_id')->nullable()->references('id')->on('users')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('payment_handbook_id')->references('id')->on('handbooks')->onDelete('SET NULL')->onUpdate('RESTRICT');
            $table->integer('category_id')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('RESTRICT');
            $table->integer('sub_category_id')->nullable()->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('RESTRICT');
            $table->integer('file_id')->nullable()->references('id')->on('file')->onDelete('SET NULL')->onUpdate('Restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
