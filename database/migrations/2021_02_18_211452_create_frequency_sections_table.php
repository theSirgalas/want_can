<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrequencySectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frequency_sections', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('short_description',612);
            $table->text('description');
            $table->integer('frequency_titles_id')->references('id')->on('frequency_titles')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frequency_sections');
    }
}
