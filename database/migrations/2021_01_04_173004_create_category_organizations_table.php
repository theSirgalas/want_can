<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_organizations', function (Blueprint $table) {
            $table->integer('organization_id')->references('id')->on('organisations')->onDelete('CASCADE');
            $table->integer('category_id')->references('id')->on('categories')->onDelete('CASCADE');
            $table->primary(['organization_id','category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_organizations');
    }
}
