<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationToFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_files', function (Blueprint $table) {
            $table->integer('organization_id')->references('id')->on('organizations')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('file_id')->references('id')->on('files')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->primary(['organization_id','file_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisation_to_files');

    }
}
