<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('inn',20);
            $table->integer('region_id')->references('id')->on('regions')->onDelete('SET NULL');
            $table->string('legal_address');
            $table->string('fact_address');
            $table->string('email');
            $table->string('phone',20);
            $table->text('about_us')->nullable();
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('moderation_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
