<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandbookTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handbook_titles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('key');
            $table->timestamps();
        });
        Schema::table('handbooks',function(Blueprint $table){
            $table->integer('handbook_title_id')->references('id')->on('handbook_title')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->index('handbook_title_id','idx-handbook-handbook_title_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('handbook',function(Blueprint $table){
            $table->dropIndex('idx-handbook-handbook_title_id');
            $table->dropForeign('handbook_title_id');
            $table->dropColumn('handbook_tile_id');
        });
        Schema::dropIfExists('handbook_titles');
    }
}
