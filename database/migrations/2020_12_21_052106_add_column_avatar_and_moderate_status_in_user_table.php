<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAvatarAndModerateStatusInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->integer('avatar_file_id')->nullable()->references('id')->on('file')->onDelete('CASCADE')->onUpdate('RESTRICT');
           $table->index('avatar_file_id','idx_users_avatar_file_id');
           $table->boolean('status_moderate')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign('avatar_file_id');
            $table->dropIndex('idx_users_avatar_file_id');
            $table->dropColumn('avatar_file_id');
            $table->dropColumn('status_moderate');
        });
    }
}
