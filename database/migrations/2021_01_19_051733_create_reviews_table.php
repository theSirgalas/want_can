<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->float('score',2,2);
            $table->text('comment');
            $table->integer('application_id')->references('id')->on('applications')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->timestamps();
        });
        Schema::table('users',function(Blueprint $table){
            $table->integer('reviews_count')->default(0);
            $table->float('reviews_score',6,4)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function(Blueprint $table){
            $table->dropColumn('reviews_count');
            $table->dropColumn('reviews_score');
        });
        Schema::dropIfExists('reviews');

    }
}
