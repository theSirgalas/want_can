<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_socials', function (Blueprint $table) {
            $table->integer('organization_id')->references('id')->on('organizations')->onDelete('CASCADE');
            $table->integer('social_id')->references('id')->on('socials')->onDelete('CASCADE');
            $table->primary(['organization_id','social_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_socials');
    }
}
