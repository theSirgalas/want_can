<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAvatarFileIdInOrganisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->integer('logo_file_id')->nullable()->references('id')->on('files')->onDelete('SET NULL')->onUpdate('RESTRICT');
            $table->index('logo_file_id','idx_organisation_logo_file_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('organization', function (Blueprint $table) {
            $table->dropIndex('idx_organisation_logo_file_id');
            $table->dropForeign('logo_file_id');
        });*/
    }
}
