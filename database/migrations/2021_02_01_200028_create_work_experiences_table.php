<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_experiences', function (Blueprint $table) {
            $table->id();
            $table->string('company_name',512);
            $table->string('post',512);
            $table->string('month_start',100);
            $table->string('years_start',24);
            $table->boolean('now')->default(false);
            $table->string('month_end',100)->nullable();
            $table->string('years_end',24)->nullable();
            $table->text('obligation');
            $table->integer('resume_id')->references('id')->on('resumes')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_experiences');
    }
}
