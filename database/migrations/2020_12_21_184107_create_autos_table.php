<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autos', function (Blueprint $table) {
                $table->id();
                $table->string('mark')->nullable();
                $table->string('color')->nullable();
                $table->string('number')->nullable();
                $table->integer('body_type_handbook_id')->nullable()->references('id')->on('handbooks')->onDelete('SET NULL')->onUpdate('RESTRICT');
                $table->index('body_type_handbook_id','idx-autos-body_type_handbook_id');
                $table->integer('user_id')->nullable()->references('id')->on('users')->onDelete('CASCADE')->onUpdate('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autos');
    }
}
