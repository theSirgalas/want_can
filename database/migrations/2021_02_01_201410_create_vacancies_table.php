<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->string('name',512);
            $table->integer('category_id')->references('id')->on('categories')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('work_handbook_id')->references('id')->on('handbooks')->onDelete("CASCADE")->onUpdate("RESTRICT");
            $table->string('salary',512);
            $table->integer('payments_handbook_id')->references('id')->on('handbooks')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->string('where_work',512);
            $table->string('address_work',512);
            $table->text('requirements');
            $table->text('obligation');
            $table->text('description');
            $table->integer('author_user_id')->references('id')->on('users')->onDelete("CASCADE")->onUpdate('RESTRICT');
            $table->boolean('is_closed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
