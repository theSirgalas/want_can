<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->id();
            $table->string('post_name',512);
            $table->integer('category_id')->references('id')->on('categories')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('work_handbook_id')->references('id')->on('handbooks')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->string('experience',512);
            $table->integer('education_handbook_id')->references('id')->on('handbooks')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->integer('sex');
            $table->string('age',100);
            $table->integer('trips');
            $table->boolean('moving');
            $table->text('citizenship');
            $table->text('about_us')->nullable();
            $table->string('salary',150);
            $table->integer('author_user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('RESTRICT');
            $table->boolean('is_closed')->default(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
