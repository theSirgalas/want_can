<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('notifications', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('author_user_id');
            $table->string('entity',512);
            $table->integer('primary');
            $table->string('notification',1000);
            $table->boolean('view')->default(false);
            $table->integer('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->dropIfExists('notifications');
    }
}
