<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('extension');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });
        Schema::table('organizations',function (Blueprint  $table){
            $table->integer('file_id')->nullable();
            $table->index('file_id','idx_organizations_file_id');
            $table->foreign('file_id','fk_organizations_to_file')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guests', function(Blueprint $table)
        {
            $table->dropUnique('idx_organizations_file_id');
            $table->dropForeign('fk_organizations_to_file');
            $table->dropColumn('file_id');

        });
        Schema::dropIfExists('files');
    }
}
