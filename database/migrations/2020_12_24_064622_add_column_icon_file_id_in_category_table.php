<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIconFileIdInCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
           $table->integer('file_id')->nullable()->references('id')->on('files')->onDelete('SET NULL')->onUpdate("RESTRICT");
           $table->index('file_id','category_icon_file_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropIndex('category_icon_file_id');
            $table->dropForeign("file_id");
            $table->dropColumn('file_id');
        });
    }
}
