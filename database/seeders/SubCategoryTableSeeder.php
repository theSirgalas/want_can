<?php

namespace Database\Seeders;

use App\Entities\Category;
use Illuminate\Database\Seeder;

class SubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()->times(100) ->create();
    }
}
