<?php

namespace Database\Seeders;

use App\Entities\Message;
use Illuminate\Database\Seeder;

class MessageTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::factory()->connection('mongodb')->times(150)->create();
    }
}
