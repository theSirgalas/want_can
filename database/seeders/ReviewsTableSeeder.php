<?php

namespace Database\Seeders;

use App\Entities\Task\Reviews;
use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reviews::factory()->times(150) ->create();
    }
}
