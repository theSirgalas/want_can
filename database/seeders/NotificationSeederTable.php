<?php

namespace Database\Seeders;

use App\Entities\Notification;
use Illuminate\Database\Seeder;

class NotificationSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notification::factory()->connection('mongodb')->times(500)->create();
    }
}
