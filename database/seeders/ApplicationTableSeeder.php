<?php

namespace Database\Seeders;

use App\Entities\Task\Application;
use Database\Factories\Entities\Entities\Task\ApplicationFactory;
use Illuminate\Database\Seeder;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Application::factory()->times(450) ->create();
    }
}
