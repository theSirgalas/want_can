<?php

namespace Database\Seeders;

use App\Entities\Vacancy\Vacancy;
use Illuminate\Database\Seeder;

class VacancySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vacancy::factory()->times(100)->create();
    }
}
