<?php

namespace Database\Seeders;

use App\Entities\Resume\WorkExperience;
use Illuminate\Database\Seeder;

class WorkExperienceSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkExperience::factory()->times(100) ->create();
    }
}
