<?php

namespace Database\Seeders;

use App\Entities\Frequency\FrequencyTitle;
use Illuminate\Database\Seeder;

class FrequencyTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FrequencyTitle::factory()->times(5)->create();
    }
}
