<?php

namespace Database\Seeders;

use App\Entities\Resume\Resume;
use Illuminate\Database\Seeder;

class ResumeSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Resume::factory()->times(100) ->create();
    }
}
