<?php

namespace Database\Seeders;

use App\Entities\Resume\ResumeEducation;
use Illuminate\Database\Seeder;

class ResumeEducationSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ResumeEducation::factory()->times(500) ->create();
    }
}
