<?php

namespace Database\Seeders;

use App\Entities\Chatroom;
use Illuminate\Database\Seeder;

class ChatroomTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Chatroom::factory()->times(10)->create();
    }
}
