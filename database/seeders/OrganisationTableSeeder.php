<?php

namespace Database\Seeders;

use App\Entities\Organization\Organization;
use App\Entities\User\User;
use Illuminate\Database\Seeder;

class OrganisationTableSeeder extends Seeder
{

    public function run()
    {
        Organization::factory()->times(10) ->create();
    }
}
