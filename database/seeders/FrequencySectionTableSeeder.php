<?php

namespace Database\Seeders;

use App\Entities\Frequency\FrequencySection;
use Illuminate\Database\Seeder;

class FrequencySectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FrequencySection::factory()->times(25)->create();
    }
}
