<?php

namespace Database\Seeders;

use App\Entities\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::create(['name'=>'Симферополь','slug'=>Str::slug('Симферополь')]);
        Region::create(['name'=>'Севастополь','slug'=>Str::slug('Симферополь')]);
        Region::create(['name'=>'Ялта','slug'=>Str::slug('Ялта')]);
        Region::create(['name'=>'Евпатория','slug'=>Str::slug('Евпатория')]);
    }
}
