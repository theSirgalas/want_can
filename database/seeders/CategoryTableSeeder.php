<?php

namespace Database\Seeders;

use App\Entities\Category;
use App\Entities\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name'=>'Курьерские услуги','slug'=>Str::slug('Курьерские услуги')]);
        Category::create(['name'=>'Попутчик','slug'=>Str::slug('Попутчик')]);
        Category::create(['name'=>'Грузоперевозки','slug'=>Str::slug('Грузоперевозки')]);
        Category::create(['name'=>'Уборка и помощь по хозяйству','slug'=>Str::slug('Уборка и помощь по хозяйству')]);
        Category::create(['name'=>'Виртиуальный помощник','slug'=>Str::slug('Виртиуальный помощник')]);
        Category::create(['name'=>'Компьютернная помощь','slug'=>Str::slug('Компьютернная помощь')]);
        Category::create(['name'=>'Мероприятия и промоакции','slug'=>Str::slug('Мероприятия и промоакции')]);
        Category::create(['name'=>'Дизайн','slug'=>Str::slug('Дизайн')]);
        Category::create(['name'=>'Ремонт и строительство','slug'=>Str::slug('Ремонт и строительство')]);
        Category::create(['name'=>'Фото- и видеоуслуги','slug'=>Str::slug('Фото- и видеоуслуги')]);
        Category::create(['name'=>'Установка и ремонт техники','slug'=>Str::slug('Установка и ремонт техники')]);
        Category::create(['name'=>'Ремонт цифровой техники','slug'=>Str::slug('Ремонт цифровой техники')]);
        Category::create(['name'=>'Юридическая помощь','slug'=>Str::slug('Юридическая помощь')]);
        Category::create(['name'=>'Репетиторы и обучение','slug'=>Str::slug('Репетиторы и обучение')]);
        Category::create(['name'=>'Каучсёрфинг','slug'=>Str::slug('Коучсёрфинг')]);
        Category::create(['name'=>'Ремонт транспорта','slug'=>Str::slug('Ремонт транспорта')]);
        Category::create(['name'=>'Туризм','slug'=>Str::slug('Туризм')]);
        Category::create(['name'=>'Красота и здоровье','slug'=>Str::slug('Красота и здоровье')]);
    }
}
