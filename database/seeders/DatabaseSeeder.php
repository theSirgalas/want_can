<?php

namespace Database\Seeders;

use App\Entities\Message;
use App\Entities\Notification;
use App\Entities\Task\Application;
use Illuminate\Database\Seeder;
use Illuminate\Notifications\Console\NotificationTableCommand;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionSeeder::class);
        $this->call(UserTableSeeder::class);//todo (отключить)
        $this->call(CategoryTableSeeder::class);
        $this->call(OrganisationTableSeeder::class);//todo (отключить)
        $this->call(HandbookTableSeeder::class);
        $this->call(CategoryUserTableSeeder::class);//todo (отключить)
        $this->call(SubCategoryTableSeeder::class);//todo (отключить)
        $this->call(TaskTableSeeder::class);//todo (отключить)
        $this->call(ApplicationTableSeeder::class);//todo (отключить)
        $this->call(ReviewsTableSeeder::class);//todo (отключить)
        $this->call(ChatroomTableSeeders::class);//todo (отключить)
        $this->call(VacancySeederTable::class);//todo (отключить)
        $this->call(ResumeSeederTable::class);//todo (отключить)
        $this->call(ResumeEducationSeederTable::class);//todo (отключить)
        $this->call(WorkExperienceSeederTable::class);//todo (отключить)
        $this->call(MessageTableSeeders::class);//todo (отключить)
        $this->call(FrequencyTitleTableSeeder::class);//todo (отключить)
        $this->call(FrequencySectionTableSeeder::class);//todo (отключить)
    }
}
