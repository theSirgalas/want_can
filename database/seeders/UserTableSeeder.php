<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Entities\User\User;
use Illuminate\Support\Str;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->times(100) ->create();
        User::create([
            'name' => env('ADMIN_NAME'),
            'email' => env('ADMIN_EMAIL'),
            'password' => env('ADMIN_PASSWORD'),
            'remember_token' => Str::random(10),
            'role'=>User::ADMIN,
            'phone' => env('ADMIN_PHONE'),
            'region_id'=>rand(1, 4)
        ]);
    }
}
