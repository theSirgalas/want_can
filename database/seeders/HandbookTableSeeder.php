<?php

namespace Database\Seeders;

use App\Entities\Handbook\Handbook;
use App\Entities\Handbook\HandbookTitle;
use Illuminate\Database\Seeder;

class HandbookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handbookTitleAuto=HandbookTitle::create(['title'=>'Авто','key'=>'auto']);
        Handbook::create(['title'=>'седан','handbook_title_id'=>$handbookTitleAuto->id]);
        Handbook::create(['title'=>'хетчбек','handbook_title_id'=>$handbookTitleAuto->id]);
        Handbook::create(['title'=>'микроавтобус','handbook_title_id'=>$handbookTitleAuto->id]);
        Handbook::create(['title'=>'грузовая','handbook_title_id'=>$handbookTitleAuto->id]);
        $handbookTitlePrice=HandbookTitle::create(['title'=>'Типы оплаты','key'=>'price']);
        Handbook::create(['title'=>'Наличными по окончинию','handbook_title_id'=>$handbookTitlePrice->id]);
        Handbook::create(['title'=>'По договоренности','handbook_title_id'=>$handbookTitlePrice->id]);
        Handbook::create(['title'=>'Перевод на карту','handbook_title_id'=>$handbookTitlePrice->id]);
        Handbook::create(['title'=>'50% вначале 50% в конце','handbook_title_id'=>$handbookTitlePrice->id]);
        $handbookTitleWork=HandbookTitle::create(['title'=>'График работы','key'=>'work']);
        Handbook::create(['title'=>'Вахтовый метод','handbook_title_id'=>$handbookTitleWork->id]);
        Handbook::create(['title'=>'Полный день','handbook_title_id'=>$handbookTitleWork->id]);
        Handbook::create(['title'=>'Свободный график','handbook_title_id'=>$handbookTitleWork->id]);
        Handbook::create(['title'=>'еще что то','handbook_title_id'=>$handbookTitleWork->id]);
        $handbookTitleEducation=HandbookTitle::create(['title'=>'Образование','key'=>'education']);
        Handbook::create(['title'=>'Неполное среднее','handbook_title_id'=>$handbookTitleEducation->id]);
        Handbook::create(['title'=>'Среднее','handbook_title_id'=>$handbookTitleEducation->id]);
        Handbook::create(['title'=>'Среднее специальное','handbook_title_id'=>$handbookTitleEducation->id]);
        Handbook::create(['title'=>'Высшее','handbook_title_id'=>$handbookTitleEducation->id]);
        $handbookTitlePayments=HandbookTitle::create(['title'=>'Оплата','key'=>'salary']);
        Handbook::create(['title'=>'Раз в месяц','handbook_title_id'=>$handbookTitlePayments->id]);
        Handbook::create(['title'=>'Два раза в месяц','handbook_title_id'=>$handbookTitlePayments->id]);
        Handbook::create(['title'=>'По окончанию работ','handbook_title_id'=>$handbookTitlePayments->id]);
        Handbook::create(['title'=>'По договоренности','handbook_title_id'=>$handbookTitlePayments->id]);
    }
}
